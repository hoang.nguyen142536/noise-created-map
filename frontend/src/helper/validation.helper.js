import { translate } from 'components/bases/Translate/Translate';

export class ValidationHelper
{
    static vietnamPhonePattern = /^(\+84|0)([345987])[0-9]{8}$/;
    static emailPattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    static indiaPhonePattern = /^\d{10}$/;
    static fullNamePattern = /^[a-zA-Z ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$/;

    static isEmail(email)
    {
        return ValidationHelper.emailPattern.test(String(email).toLowerCase());
    }

    static isPhoneNumber(phone)
    {
        return ValidationHelper.vietnamPhonePattern.test(String(phone).toLowerCase());
    }

    static validateField(validators, field, value)
    {
        const errors = [];
        const rules = validators[field];
        let isValid = true;
        for (const r of rules)
        {
            const checkValue = rules[r];

            switch (r)
            {
                case 'required':
                    value = typeof (value) === 'string' ? value?.trim() : value;
                    if (value === null || value === undefined || value === '' || value === {} || value === [])
                    {
                        errors.push(translate('Trường này là bắt buộc'));
                        isValid = false;
                    }
                    break;
                case 'minLength':
                    if (value && value.length < checkValue)
                    {
                        errors.push(translate('Trường này phải có độ dài tối thiểu %0% ký tự', [checkValue]));
                        isValid = false;
                    }
                    break;
                case 'maxLength':
                    if (value && value.length > checkValue)
                    {
                        errors.push(translate('Trường này phải có độ dài tối đa %0% ký tự', [checkValue]));
                        isValid = false;
                    }
                    break;
                case 'minValue':
                    if (value && value < checkValue)
                    {
                        errors.push(translate('Trường này phải lớn hơn %0%', [checkValue]));
                        isValid = false;
                    }
                    break;
                case 'maxValue':
                    if (value && value > checkValue)
                    {
                        errors.push(translate('Trường này phải nhỏ hơn %0%', [checkValue]));
                        isValid = false;
                    }
                    break;
                case 'pattern':
                    if (value && !value.match(checkValue))
                    {
                        errors.push(translate('Trường này là định dạng không hợp lệ'));
                        isValid = false;
                    }
                    break;
                default:
                    break;
            }
        }

        return { errors, isValid };
    }

    static validateForm(formData, validators)
    {
        let formValid = true;

        const formErrors = {};

        for (const field in validators)
        {
            const value = formData[field];
            const validation = ValidationHelper.validateField(validators, field, value);
    

            if (!validation.isValid)
            {
                formValid = false;
            }

            formErrors[field] = validation.errors.join('\r\n') || '';
        }

        return { formErrors, formValid };
    }
}
