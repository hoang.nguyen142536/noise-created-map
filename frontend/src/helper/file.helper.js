import FileSaver from 'file-saver';

export class FileHelper
{
    static saveExcelFileAs = (name, content) =>
    {
        const fileType = 'application/vnd.ms-excel';
        const fileExtension = '.xlsx';
        const fileName = `${name}-${new Date().getTime()}`;
        const data = new Blob([content], { type: fileType });
        FileSaver.saveAs(data, fileName + fileExtension);
    }
}
