module.exports = {
    LOCATION_TYPE: {
        home: 0,
        work: 1,
        incident: 2,
        organization: 3,
        company: 4,
        division: 5,
        shelter: 6,
        camp: 7,
        commandPost: 8,
        resource: 9
    },
    GENDER: {
        female: 0,
        male: 1,
        other: 2
    },
    APIStatus: {
        Success: 0,
        Error: -1,
        NoPermission: -2,
        SpecialCase: 1
    },
    SysIdTokenStatus: {
        inactive: 0,
        activated: 1,
        expired: 2
    }
};
