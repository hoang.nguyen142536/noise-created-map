import HttpClient from 'helper/http.helper';

export class AuthService
{
    http = new HttpClient();

    getProfile = () =>
    {
        return this.http.get('/api/user/profile');
    };
}
