const IMPACT_BARRIER_HIGH = 1.5;

export default class BusinessService
{
    calculateForDrawMap = (contents, x, y, volumn, soundDamping, barriers, isCalculatedeltaK = false) =>
    {
        if (!Array.isArray(contents) || contents.length === 0)
        {
            return 0;
        }

        const lttes = [];

        let maxdbA = 0;

        let deltaKTotal = 0;
        let deltaKCount = 0;

        const impactedBarriers = (Array.isArray(barriers) ? barriers : []).filter((b) => b.width > 0 && b.height > 0 && b.high > IMPACT_BARRIER_HIGH);

        for (let i = 0; i < contents.length; i++)
        {
            const c = contents[i];

            // const r = Math.sqrt( Math.pow(c.x - x, 2) / c.q + Math.pow(c.y - y, 2));
            // if (r <= 0.4)
            // {
            //     maxdbA = Math.max(c.ls, maxdbA);
            // }

            // const inLog = Math.pow((c.x + ((c.q - 1) / (10 * c.q)) * (this.getDataFromDo('x', c.omega) - x)) * this.getDataFromDo('x', c.omega), 2) / c.q +
            //               Math.pow((c.y + ((c.q - 1) / (10 * c.q)) * (this.getDataFromDo('y', c.omega) - y)) * this.getDataFromDo('y', c.omega), 2);
            
            const inLog = Math.pow(c.x - x, 2) + Math.pow(c.y - y, 2);
            const r = Math.sqrt(inLog);
            if (r <= 0.4)
            {
                maxdbA = Math.max(c.ls, maxdbA);
                lttes.push(c.ls);
                continue;
            }

            // tinh deltaK

            let currentdeltaK = 0;

            for(let j = 0; j < impactedBarriers.length; j++)
            {
                const barrier = impactedBarriers[j];
            
                let isBarrierDung = false;
            
                // Tính các điểm A, B, S, I
                const pointA = { x: barrier.x, y: barrier.y };
                const pointB = { x: 0, y: 0 };
                if (barrier.width > barrier.height)
                {
                    pointB = { x: barrier.x + barrier.width, y: barrier.y};
                }
                else
                {
                    isBarrierDung = true;
                    pointB = { x: barrier.x, y: barrier.y + barrier.height};
                }
            
                const pointI = { x, y };
                const pointS = { x: c.x, y: c.y }; 
            
                // Có 3 điều kiện check barrier có hợp lệ hay không.
                // Check điều kiện 1
                if ((pointS.y - pointI.y) / (pointI.x - pointS.x) === (pointA.y - pointB.y) / (pointB.x - pointA.x))
                {
                    continue; // không thỏa điều kiện 1
                }

            
                // Tính điểm giao nhau x0, y0
                let x0 = (pointB.x - pointA.x) * (pointI.x * pointS.y - pointS.x * pointI.y) - (pointI.x - pointS.x) * (pointB.x * pointA.y - pointA.x * pointB.y);
                x0 /= (pointS.y - pointI.y) * (pointB.x - pointA.x) - (pointA.y - pointB.y) * (pointI.x - pointS.x);
            
                let y0 = (pointS.y - pointI.y) * (pointB.x * pointA.y - pointA.x * pointB.y) - (pointA.y - pointB.y) * (pointI.x * pointS.y - pointS.x * pointI.y);
                y0 /= (pointS.y - pointI.y) * (pointB.x - pointA.x) - (pointA.y - pointB.y) * (pointI.x - pointS.x);
            
            
                const pointM = { x: x0, y: y0};
                // Check điều kiện 2
                const si = this.getDistance(pointS, pointI);
                const sm = this.getDistance(pointS, pointM);
                const im = this.getDistance(pointI, pointM);
                if (si <= sm || si <= im)
                {
                    continue;
                }

            
                // Check điều kiện 3
                const ab = this.getDistance(pointA, pointB);
                const am = this.getDistance(pointA, pointM);
                const mb = this.getDistance(pointM, pointB);
                if (ab <= am || ab <= mb)
                {
                    continue;
                }

                // Tính a,b
                const a = Math.sqrt(Math.pow(pointS.x - x0, 2) + Math.pow(pointS.y - y0, 2));
                const b = Math.sqrt(Math.pow(x0 - pointI.x, 2) + Math.pow(y0 - pointI.y, 2));
            
                // tính w
                const w = (barrier.high - IMPACT_BARRIER_HIGH) * Math.sqrt(2 / c.lamda) * Math.sqrt(1 / a + 1/ b);
            
                const xyByW = this.getXYByW(w);
                if (xyByW)
                {
                    const tempDeltaK = -3 + 10 * Math.log10(Math.pow(0.5 - xyByW.x, 2) + Math.pow(0.5 - xyByW.y, 2));
                
                    // Check điều kiện nếu deltaK < 0 đối với từng barrier. Nếu Barrier nào đủ điều kiện thì skip những cái còn lại
                    if (tempDeltaK < 0)
                    {
                        currentdeltaK = tempDeltaK;
                        break;
                    }
                }
            
            }

            deltaKCount++;
            deltaKTotal += currentdeltaK;
                          
            const ltt = c.ls - 10 * Math.log10(inLog) + 10 * Math.log10(c.q) - 11 + currentdeltaK;
            lttes.push(ltt);
        }

        const lwtotal = 10 * Math.log10(
            (contents.reduce((a, b) => {
                return a + Math.pow(10, 0.1 * b.ls);
            }, 0)) / lttes.length);

        // const totalLs = contents.reduce((a, b) => {
        //     return a + Math.pow(10, 0.1 * b.ls);
        // }, 0);

        // const lwtotal = 10 * Math.log10(totalLs);

        const lpx = lwtotal - 10 * Math.log10(volumn) - 10 * Math.log10(soundDamping) + 31.7;

        // Công thức tính ltd
        // Tổng cua 10 ^ (0.1 * ltt) 
        const totalltt = lttes.reduce((a, b) => {
            return a + Math.pow(10, 0.1 * b);
        }, 0);

        // Cộng thêm 10 ^ (0.1 * lpx) 
        const total = totalltt +  Math.pow(10, 0.1 * lpx);

        // Chia cho n + 1 => n là số nguôn
        // const totalA = total / (lttes.length + 1);

        // 10 * log (totalA)
        const ltd = 10 * Math.log10(total);

        if (isCalculatedeltaK)
        {
            return {
                ltd: Math.max(ltd, maxdbA),
                deltaK : deltaKTotal / deltaKCount
            }
        }
        return Math.max(ltd, maxdbA);
    };

    calculateFMapWithIDW = (contents, x, y) =>
    {
        if (!Array.isArray(contents) || contents.length === 0)
        {
            return 0;
        }

        const data = contents.map((c) => {
            const distance = this.getDistance({x, y}, { x: c.x, y: c.y });
            return {
                distance: distance,
                isSameC : distance < 0.1,
                ls: c.ls
            }
        });

        const sameC = data.find((d) => d.isSameC);
        if (sameC) return sameC.ls;

        const above = data.reduce((a, b) => {
            return a + b.ls / b.distance;
        }, 0);

        const below = data.reduce((a, b) => {
            return a + 1 / b.distance;
        }, 0);

        return above / below;
    };

    calculateForeCast = (foreCast, project) => {
        if (!foreCast || !project)
        {
            return foreCast;
        }

        if (!Array.isArray(foreCast.points) || foreCast.points.length === 0)
        {
            return foreCast;
        }


        foreCast.points.forEach((f) => {
            const result = this.calculateForDrawMap(foreCast.data, f.x, f.y, project.volumn, project.soundDamping, project.barriers, true);
            f.ltd = this.round(result.ltd, 2).toString();
            f.deltaK = this.round(result.deltaK, 2).toString();
        });

        return foreCast;
    }

    calculateNoiseDoes = (noiseDoesData, project) => {
        if (!project || !Array.isArray(noiseDoesData) || noiseDoesData.length === 0)
        {
            return {
                noiseDoseData: noiseDoesData || [],
                noiseSummaryData: []
            }
        }

        const employeeses = [];
        const resultNoiseDoseData = [];
        const resultNoiseSummaryData = [];

        noiseDoesData.forEach((d) => {
            if (d.name && !employeeses.find((e) => e === d.name))
            {
                employeeses.push(d.name);
            }

            if (d.x < 0 || d.y < 0 || !d.t || !d.twa)
            {
                d.leq = '';
                d.d1 = '';
            }
            else
            {
                // d.leq = this.round(this.calculateForDrawMap(project.contents, d.x, d.y, project.volumn, project.soundDamping, project.barriers), 2);
                d.leq = this.round(this.calculateFMapWithIDW(project.contents, d.x, d.y), 2);
                d.d1 = this.round(((100 * d.t) / 8) * Math.pow(10, (d.leq - d.twa) / 10), 2);
            }
            
            resultNoiseDoseData.push({
                ...d
            });
        });

        employeeses.forEach((e) => {
            const noiseDataForEs = noiseDoesData.filter((d) => d.name === e);

            const tca = noiseDataForEs.reduce((a, b) => {
                return a + b.t;
            }, 0);

            const totalLtd = noiseDataForEs.reduce((a, b) => {
                return a + b.t * Math.pow(10, 0.1 * b.leq);
            }, 0);

            const lex = 10 * Math.log10(totalLtd / 8);

            const d = noiseDataForEs.reduce((a, b) => {
                return a + b.d1;
            }, 0);

            resultNoiseSummaryData.push({
                name: e,
                tca,
                lex: this.round(lex, 2),
                d
            });
        });

        return {                 
            noiseDoseData: resultNoiseDoseData || [],
            noiseSummaryData: resultNoiseSummaryData || []
        };
    };

    calculateControl = (controlData) => {
        if (!controlData.d1 || !controlData.v1 || !controlData.s1)
            return controlData;

        const t1 = 60 / controlData.d1;
        const anphaA1 = (0.16 * controlData.v1) / (t1 * controlData.s1);

        if (!controlData.anphaA2 || !controlData.s2)
        {
            return {
                ...controlData,
                t1: this.round(t1, 2).toString(),
                anphaA1: this.round(anphaA1, 2).toString(),
                t2: '',
                d2: '',
                deltaL2: ''
            };
        }

        const t2 = (0.16 * controlData.v1) / (controlData.s1 * anphaA1 + controlData.s2 * controlData.anphaA2);
        const d2 = 60 / t2;
        const deltaL2 = 10 * Math.log10((controlData.s1 * anphaA1 + controlData.s2 * controlData.anphaA2) / (controlData.s1 * anphaA1));

        return {
            ...controlData,
            t1: this.round(t1, 2).toString(),
            anphaA1: this.round(anphaA1, 2).toString(),
            t2: this.round(t2, 2).toString(),
            d2: this.round(d2, 2).toString(),
            deltaL2: this.round(deltaL2, 2).toString()
        };
    };

    getDistance = (pointA, pointB) => {
        return Math.sqrt(Math.pow(pointA.x - pointB.x, 2) + Math.pow(pointA.y - pointB.y, 2));
    }

    getDataFromDo = (type, omega) =>
    {
        switch(type)
        {
            case 'x': 
                switch(omega)
                {
                    case 0:
                        return 1;
                    case 45:
                        return Math.sqrt(2) / 2;
                    case 90:
                        return 0;
                    case 135:
                        return -Math.sqrt(2) / 2;
                    case 180:
                        return -1;
                    case 225:
                        return -Math.sqrt(2) / 2;
                    case 270:
                        return 0;
                    case 315:
                        return Math.sqrt(2) / 2;
                    case 360:
                        return 1;
                    default: 
                        return 0;
                }
            case 'y':
                switch(omega)
                {
                    case 0:
                        return 0;
                    case 45:
                        return Math.sqrt(2) / 2;
                    case 90:
                        return 1;
                    case 135:
                        return Math.sqrt(2) / 2;
                    case 180:
                        return 0;
                    case 225:
                        return -Math.sqrt(2) / 2;
                    case 270:
                        return -1;
                    case 315:
                        return -Math.sqrt(2) / 2;
                    case 360:
                        return 0;
                    default: 
                        return 0;
                }
            default:
                return 0;
        }
    };

    getXYByW = (w) =>
    {
        if (w >= 0 && w < 0.05) return { x: 0, y: 0 };
        else if (w >= 0.05 && w < 0.15) return { x: 0.1000, y: 0.0005 };
        else if (w >= 0.15 && w < 0.25) return { x: 0.1999, y: 0.1999 };
        else if (w >= 0.25 && w < 0.35) return { x: 0.2994, y: 0.0141 };
        else if (w >= 0.35 && w < 0.45) return { x: 0.3975, y: 0.0334 };
        else if (w >= 0.45 && w < 0.55) return { x: 0.4923, y: 0.0647 };
        else if (w >= 0.55 && w < 0.65) return { x: 0.5811, y: 0.1105 };
        else if (w >= 0.65 && w < 0.75) return { x: 0.6597, y: 0.1721 };
        else if (w >= 0.75 && w < 0.85) return { x: 0.723, y: 0.2493 };
        else if (w >= 0.85 && w < 0.95) return { x: 0.7648, y: 0.3398 };
        else if (w >= 0.95 && w < 1.05) return { x: 0.7799, y: 0.4383 };
        else if (w >= 1.05 && w < 1.15) return { x: 0.7638, y: 0.5365 };
        else if (w >= 1.15 && w < 1.25) return { x: 0.7154, y: 0.6234 };
        else if (w >= 1.25 && w < 1.35) return { x: 0.6386, y: 0.6863 };
        else if (w >= 1.35 && w < 1.45) return { x: 0.5431, y: 0.7135 };
        else if (w >= 1.45 && w < 1.55) return { x: 0.4453, y: 0.6975 };
        else if (w >= 1.55 && w < 1.65) return { x: 0.3655, y: 0.6389 };
        else if (w >= 1.65 && w < 1.75) return { x: 0.3238, y: 0.5492 };
        else if (w >= 1.75 && w < 1.85) return { x: 0.3336, y: 0.4508 };
        else if (w >= 1.85 && w < 1.95) return { x: 0.3944, y: 0.3734 };
        else if (w >= 1.95 && w < 2.05) return { x: 0.4882, y: 0.3434 };
        else if (w >= 2.05 && w < 2.15) return { x: 0.5815, y: 0.3743 };
        else if (w >= 2.15 && w < 2.25) return { x: 0.6363, y: 0.4557 };
        else if (w >= 2.25 && w < 2.35) return { x: 0.6266, y: 0.5531 };
        else if (w >= 2.35 && w < 2.45) return { x: 0.555, y: 0.6197 };
        else if (w >= 2.45 && w < 2.55) return { x: 0.4574, y: 0.6192 };
        else if (w >= 2.55 && w < 2.65) return { x: 0.389, y: 0.55 };
        else if (w >= 2.65 && w < 2.75) return { x: 0.3925, y: 0.4529 };
        else if (w >= 2.75 && w < 2.85) return { x: 0.4675, y: 0.3915 };
        else if (w >= 2.85 && w < 2.95) return { x: 0.5626, y: 0.4101 };
        else if (w >= 2.95 && w < 3.05) return { x: 0.6058, y: 0.4963 };
        else if (w >= 3.05 && w < 3.15) return { x: 0.5616, y: 0.5818 };
        else if (w >= 3.15 && w < 3.25) return { x: 0.4664, y: 0.5933 };
        else if (w >= 3.25 && w < 3.35) return { x: 0.4058, y: 0.5192 };
        else if (w >= 3.35 && w < 3.45) return { x: 0.4385, y: 0.4296 };
        else if (w >= 3.45 && w < 3.55) return { x: 0.5326, y: 0.4152 };
        else if (w >= 3.55 && w < 3.65) return { x: 0.588, y: 0.4923 };
        else if (w >= 3.65 && w < 3.75) return { x: 0.542, y: 0.575 };
        else if (w >= 3.75 && w < 3.85) return { x: 0.4481, y: 0.5656 };
        else if (w >= 3.85 && w < 3.95) return { x: 0.4223, y: 0.4752 };
        else if (w >= 3.95 && w < 4.05) return { x: 0.4984, y: 0.4204 };
        else if (w >= 4.05 && w < 4.15) return { x: 0.5738, y: 0.4758 };
        else if (w >= 4.15 && w < 4.25) return { x: 0.5418, y: 0.5633 };
        else if (w >= 4.25 && w < 4.35) return { x: 0.4494, y: 0.554 };
        else if (w >= 4.35 && w < 4.45) return { x: 0.4383, y: 0.4622 };
        else if (w >= 4.45 && w < 4.55) return { x: 0.5261, y: 0.4342 };
        else if (w >= 4.55 && w < 4.65) return { x: 0.5673, y: 0.5162 };
        else if (w >= 4.65 && w < 4.75) return { x: 0.4914, y: 0.5672 };
        else if (w >= 4.75 && w < 4.85) return { x: 0.4338, y: 0.4968 };
        else if (w >= 4.85 && w < 4.95) return { x: 0.5002, y: 0.435 };
        else if (w >= 4.95 && w < 5.02) return { x: 0.5637, y: 0.4992 };
        else if (w >= 5.02 && w < 5.07) return { x: 0.545, y: 0.5442 };
        else if (w >= 5.07 && w < 5.12) return { x: 0.4998, y: 0.5624 };
        else if (w >= 5.12 && w < 5.17) return { x: 0.4553, y: 0.5427 };
        else if (w >= 5.17 && w < 5.22) return { x: 0.4389, y: 0.4969 };
        else if (w >= 5.22 && w < 5.27) return { x: 0.461, y: 0.4536 };
        else if (w >= 5.27 && w < 5.32) return { x: 0.5078, y: 0.4405 };
        else if (w >= 5.32 && w < 5.37) return { x: 0.549, y: 0.4662 };
        else if (w >= 5.37 && w < 5.42) return { x: 0.5573, y: 0.514 };
        else if (w >= 5.42 && w < 5.47) return { x: 0.5269, y: 0.5519 };
        else if (w >= 5.47 && w < 5.52) return { x: 0.4784, y: 0.5537 };
        else if (w >= 5.52 && w < 5.57) return { x: 0.4456, y: 0.5181 };
        else if (w >= 5.57 && w < 5.62) return { x: 0.4517, y: 0.47 };
        else if (w >= 5.62 && w < 5.67) return { x: 0.4926, y: 0.4441 };
        else if (w >= 5.67 && w < 5.72) return { x: 0.5385, y: 0.4595 };
        else if (w >= 5.72 && w < 6.77) return { x: 0.5551, y: 0.5049 };
        else if (w >= 5.77 && w < 5.82) return { x: 0.5298, y: 0.5461 };
        else if (w >= 5.82 && w < 5.87) return { x: 0.4819, y: 0.5513 };
        else if (w >= 5.87 && w < 5.92) return { x: 0.4486, y: 0.5163 };
        else if (w >= 5.92 && w < 5.97) return { x: 0.4566, y: 0.4688 };
        else if (w >= 5.97 && w < 6.02) return { x: 0.4995, y: 0.447 };
        else if (w >= 6.02 && w < 6.07) return { x: 0.5424, y: 0.4689 };
        else if (w >= 6.07 && w < 6.12) return { x: 0.5495, y: 0.5165 };
        else if (w >= 6.12 && w < 6.17) return { x: 0.5146, y: 0.5496 };
        else if (w >= 6.17 && w < 6.22) return { x: 0.4676, y: 0.5398 };
        else if (w >= 6.22 && w < 6.27) return { x: 0.4493, y: 0.4954 };
        else if (w >= 6.27 && w < 6.32) return { x: 0.476, y: 0.4555 };
        else if (w >= 6.32 && w < 6.37) return { x: 0.524, y: 0.456 };
        else if (w >= 6.37 && w < 6.42) return { x: 0.5496, y: 0.4965 };
        else if (w >= 6.42 && w < 6.47) return { x: 0.5292, y: 0.5398 };
        else if (w >= 6.47 && w < 6.52) return { x: 0.4816, y: 0.5454 };
        else if (w >= 6.52 && w < 6.57) return { x: 0.452, y: 0.5078 };
        else if (w >= 6.57 && w < 6.62) return { x: 0.469, y: 0.4631 };
        else if (w >= 6.62 && w < 6.67) return { x: 0.5161, y: 0.4549 };
        else if (w >= 6.67 && w < 6.72) return { x: 0.5467, y: 0.4915 };
        else if (w >= 6.72 && w < 6.77) return { x: 0.5302, y: 0.5362 };
        else if (w >= 6.77 && w < 6.82) return { x: 0.4831, y: 0.5436 };
        else if (w >= 6.82 && w < 6.87) return { x: 0.4539, y: 0.506 };
        else if (w >= 6.87 && w < 6.92) return { x: 0.4732, y: 0.4624 };
        else if (w >= 6.92 && w < 6.97) return { x: 0.5207, y: 0.4591 };
        else return;
    };

    round = (number, numberAfterDot) => {
        return Math.round(number * Math.pow(10, numberAfterDot)) / Math.pow(10, numberAfterDot);
    }
}
