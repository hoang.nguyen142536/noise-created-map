import "./Login.css";
import React, { Component } from "react";
import { Container } from "components/bases/Container/Container";
import {
    Button,
    Input
} from "components/bases/Form";
import { HD5 } from "components/bases/Text/Text";
import { toast } from "components/bases/Modal/Modal";

class Login extends Component {

    state = {
        username: '',
        password: ''
    };

    login() {
        const username = this.state.username;
        const password = this.state.password;

        if (username !== 'an' || password !== 'RvC5s3cE') {
            toast({ type: "error", message: `Tài khoản hoặc mật khẩu không đúng` });
            return;
        }

        const now = new Date();
        now.setHours(now.getHours() + 24);
        localStorage.setItem('userPassword', JSON.stringify({
            username,
            password,
            expireTime: now.getTime()
        }))

        window.location.reload();
    }

    render() {
        return (
            <Container className="container">
                <Container className="top"></Container>
                <Container className="bottom"></Container>
                <Container className="center">
                    <HD5>Vui lòng đăng nhập</HD5>
                    <Input
                        placeholder={"Tên đăng nhập"}
                        type={"text"}
                        value={this.state.username}
                        onChange={(event) => {
                            this.setState({
                                username: event
                            })
                        }}
                    />
                    <Input
                        placeholder={"Mật khẩu"}
                        type={"password"}
                        value={this.state.password}
                        onChange={(event) => {
                            this.setState({
                                password: event
                            })
                        }}
                    />
                    <Button text="Đăng nhập" type="info" onClick={() => this.login()} />
                </Container>
            </Container>
        )
    }
}

export default Login;
