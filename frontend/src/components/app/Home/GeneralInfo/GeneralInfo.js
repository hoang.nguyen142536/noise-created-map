import "./GeneralInfo.scss";
import React, { Component } from "react";
import { Container } from "components/bases/Container/Container";
import { Row } from "components/bases/Row/Row";
import { Spacer } from "components/bases/spacer/Spacer";
import {
    Button,
    FormControlLabel,
    FormGroup,
    Input,
    Section,
    InputAppend,
    InputGroup,
} from "components/bases/Form";
import { Table, TableRowCell, TableRow } from "components/bases/Table/Table";
import { inject, observer } from "mobx-react";
import New from "components/app/Home/Action/New";
import { TB1 } from "components/bases/Text/Text";
import { toast } from "components/bases/Modal/Modal";
import { ScrollView } from "components/bases/ScrollView/ScrollView";
class GernalInfo extends Component {
    constructor(props) {
        super(props);
        this.homeStore.getProjects();
        this.inputRef = React.createRef();
    }
    homeStore = this.props.appStore.homeStore;
    handleSelected(id) {
        this.homeStore.setSelectedProjectId(id);
    }
    handleOpenFile = () => {
        if (
            this.inputRef.current.ref.current.files &&
            this.inputRef.current.ref.current.files[0]
        ) {
            const reader = new FileReader();
            reader.onload = (e) => {
                try {
                    this.homeStore.openProject(JSON.parse(e.target.result));
                } catch (error) {
                    toast({ type: "error", message: "File không hợp lệ!" });
                }
            };
            reader.readAsText(this.inputRef.current.ref.current.files[0]);
        }
    };
    render() {
        const currentProject =
            this.homeStore.projects.find(
                (p) => p.id === this.homeStore.selectedProjectId
            ) || {};
        return (
            <Container className={"home-search-container"}>
                <Section header={"Danh sách đang mở"}>
                    <TB1 style={{fontSize: '14px', fontStyle: 'italic', marginLeft: '3px', marginTop: '10px'}}>Trang web được làm bởi: LÊ TRƯỜNG AN</TB1>
                    <Container className="list-project">
                        <Table
                            className={"result-table"}
                            isFixedHeader
                            headers={[
                                { label: "STT", width: 10 },
                                { label: "Tên", width: 40 },
                                { label: "Thông số", width: 30 },
                                { label: "Dài (m)", width: 30 },
                                { label: "Rộng (m)", width: 30 },
                                { label: "Đơn vị đo", width: 30 },
                            ]}
                        >
                            {this.homeStore.projects.map((row, i) => (
                                <TableRow
                                    key={row.id}
                                    onClick={() => this.handleSelected(row.id)}
                                    isSelected={
                                        row.id ===
                                        this.homeStore.selectedProjectId
                                            ? true
                                            : false
                                    }
                                >
                                    <TableRowCell>
                                        <TB1>{i + 1}</TB1>
                                    </TableRowCell>
                                    <TableRowCell>
                                        <TB1 className={row.name}>
                                            {row.name}
                                        </TB1>
                                    </TableRowCell>
                                    <TableRowCell>
                                        <TB1 className={row.hz}>{row.hz}</TB1>
                                    </TableRowCell>
                                    <TableRowCell>
                                        <TB1 className={row.width}>
                                            {row.width}
                                        </TB1>
                                    </TableRowCell>
                                    <TableRowCell>
                                        <TB1 className={row.height}>
                                            {row.height}
                                        </TB1>
                                    </TableRowCell>
                                    <TableRowCell>
                                        <TB1 className={row.unit}>
                                            {row.unit}
                                        </TB1>
                                    </TableRowCell>
                                </TableRow>
                            ))}
                        </Table>
                    </Container>
                    <Row mainAxisAlignment={"end"}>
                        <Button
                            text="Thêm"
                            onClick={() => {
                                this.homeStore.setPopupNewProject(true);
                            }}
                            icon={"plus"}
                        ></Button>
                        <Spacer />

                        <Container>
                            <InputGroup className={"btn-data"}>
                                <InputAppend>
                                    <Button
                                        icon={"folder"}
                                        text={"Mở"}
                                        onClick={() => {
                                            const input = this.inputRef;
                                            if (input && document.createEvent) {
                                                const evt = document.createEvent(
                                                    "MouseEvents"
                                                );
                                                evt.initEvent(
                                                    "click",
                                                    true,
                                                    false
                                                );
                                                input.current.ref.current.dispatchEvent(
                                                    evt
                                                );
                                            }
                                        }}
                                    />
                                </InputAppend>
                                <Input
                                    ref={this.inputRef}
                                    className={"hide"}
                                    type={"file"}
                                    accept={
                                        ".ncm"
                                    }
                                    onChange={() => {
                                        this.handleOpenFile();
                                    }}
                                />
                            </InputGroup>
                        </Container>
                        <Spacer />
                        <Button
                            text="Đóng"
                            icon={"trash"}
                            onClick={() => {
                                this.homeStore.closeProject(currentProject.id);
                            }}
                        ></Button>
                        <Spacer />
                    </Row>
                </Section>
                <ScrollView>
                    <Section header={"Thông tin"}>
                        <FormGroup>
                            <FormControlLabel
                                label={"Tên"}
                                control={
                                    <Input
                                        value={currentProject.name}
                                        onChange={(event) => {
                                            this.homeStore.changeGernalInfo(
                                                "name",
                                                event,
                                                currentProject.id
                                            );
                                        }}
                                    />
                                }
                            />
                        </FormGroup>
                        <FormGroup>
                            <FormControlLabel
                                label={"Chỉ số biểu diễn"}
                                control={
                                    <Input
                                        value={currentProject.hz}
                                        onChange={(event) =>
                                            this.homeStore.changeGernalInfo(
                                                "hz",
                                                event,
                                                currentProject.id
                                            )
                                        }
                                    />
                                }
                            />
                        </FormGroup>
                        <FormGroup>
                            <FormControlLabel
                                label={"Thể tích phòng"}
                                control={
                                    <Input
                                        type={"number"}
                                        step={1}
                                        min={1}
                                        value={currentProject.volumn}
                                        onChange={(event) =>
                                            this.homeStore.changeGernalInfo(
                                                "volumn",
                                                event,
                                                currentProject.id
                                            )
                                        }
                                    />
                                }
                            />
                        </FormGroup>
                        <FormGroup>
                            <FormControlLabel
                                label={"Độ giảm âm"}
                                control={
                                    <Input
                                        type={"number"}
                                        value={currentProject.soundDamping}
                                        onChange={(event) =>
                                            this.homeStore.changeGernalInfo(
                                                "soundDamping",
                                                event,
                                                currentProject.id
                                            )
                                        }
                                    />
                                }
                            />
                        </FormGroup>
                        <Row mainAxisAlignment={"end"}>
                            <Button
                                text="Lưu"
                                icon={"save"}
                                onClick={() =>
                                    this.homeStore.saveProject(
                                        currentProject.id
                                    )
                                }
                            ></Button>
                        </Row>
                    </Section>
                </ScrollView>

                <>{this.homeStore.isPopUpNewProject && <New />}</>
            </Container>
        );
    }
}
export default inject("appStore")(observer(GernalInfo));
