import "./Check.scss";
import React, { Component } from "react";
import { BorderPanel, FlexPanel } from "components/bases/Panel/Panel";
import { Container } from "components/bases/Container/Container";
import { Row } from "components/bases/Row/Row";
import { Spacer } from "components/bases/spacer/Spacer";
import { TB1 } from "components/bases/Text/Text";
import { Input, InputGroup, Section, Button } from "components/bases/Form";
import { Table, TableRowCell, TableRow } from "components/bases/Table/Table";
import { inject, observer } from "mobx-react";
import BusinessService from "services/business.service";

class Check extends Component {
    homeStore = this.props.appStore.homeStore;
    businessService = new BusinessService();

    handleAdd() {
        this.homeStore.addCheck(this.homeStore.selectedProjectId);
    }

    handleRemove() {
        this.homeStore.removeCheck(this.homeStore.selectedProjectId);
    }

    handleChangeValue(index, key, value) {
        this.homeStore.changeValueCheck(
            this.homeStore.selectedProjectId,
            index,
            key,
            value
        );

        const data = this.homeStore.getCheck(index);
        if (data) {
            const calculateData = this.businessService.calculateControl(data);
            ["t1", "anphaA1", "t2", "d2", "deltaL2"].forEach((t) => {
                this.homeStore.changeValueCheck(
                    this.homeStore.selectedProjectId,
                    index,
                    t,
                    calculateData[t]
                );
            });
        }
    }
    handleSelected(index) {
        this.homeStore.selectCheck(this.homeStore.selectedProjectId, index);
    }
    formatDataJson(filterVal, jsonData) {
        return jsonData.map((v) =>
            filterVal.map((j) => {
                return v[j];
            })
        );
    }
    exportData(currentProject) {
        const fields = {
            v1: "Thể tích 1 (V)",
            s1: "Diện tích 1 (S)",
            d1: "D1",
            t1: "T1",
            anphaA1: "Hệ số anpha 1",
            anphaA2: "Hệ số anpha 2",
            s2: "Diện tích 2 (S)",
            d2: "D2",
            t2: "T2",
            deltaL2: "Hệ số delta L 2",
        };
        if (
            Array.isArray(currentProject.checks) &&
            currentProject.checks.length > 0
        ) {
            import("components/libs/Export2Excel").then((excel) => {
                const data = this.formatDataJson(
                    Object.keys(fields),
                    currentProject.checks
                );
                excel.export_json_to_excel({
                    header: Object.values(fields),
                    data,
                    filename: "Checks",
                    autoWidth: true,
                    bookType: "csv",
                });
            });
        }
    }
    render() {
        const currentProject =
            this.homeStore.projects.find(
                (p) => p.id === this.homeStore.selectedProjectId
            ) || {};
        return (
            <Container className={"home-search-container"}>
                <Section>
                    <Row>
                        <Button
                            className={"btn-color"}
                            icon={"plus"}
                            text={"Thêm"}
                            onClick={() => {
                                this.handleAdd();
                            }}
                        ></Button>
                        <Spacer/>
                        <Button
                            className={"btn-color"}
                            icon={"trash"}
                            text={"Xóa"}
                            onClick={() => {
                                this.handleRemove();
                            }}
                        ></Button>
                        <Spacer/>
                        <Button
                            icon={"download"}
                            text={"Tải"}
                            className={"btn-color"}
                            onClick={() => {
                                this.exportData(currentProject);
                            }}
                        ></Button>
                    </Row>
                    <Container className="list-check">
                        <Row>
                            <Table
                                className={"result-table"}
                                isFixedHeader
                                headers={[
                                    { label: "STT", width: 25 },
                                    { label: "Hiện hữu", width: 100 },
                                    { label: "Cải thiện", width: 100 },
                                ]}
                            >
                                {(currentProject.checks || []).map((row, i) => (
                                    <TableRow
                                        key={i}
                                        onClick={() => this.handleSelected(i)}
                                        isSelected={
                                            currentProject.selectedCheckIndex ===
                                            i
                                                ? true
                                                : false
                                        }
                                    >
                                        <TableRowCell>
                                            <TB1>{i + 1}</TB1>
                                        </TableRowCell>
                                        <TableRowCell>
                                            <InputGroup>
                                                <Spacer />
                                                Thể tích (V):
                                                <Input
                                                    type={"number"}
                                                    step={0.01}
                                                    value={row.v1}
                                                    min={0}
                                                    color={"var(--text-color)"}
                                                    onChange={(event) => {
                                                        this.handleChangeValue(
                                                            i,
                                                            "v1",
                                                            parseFloat(event)
                                                        );
                                                    }}
                                                />
                                            </InputGroup>
                                            <InputGroup>
                                                <Spacer />
                                                Diện tích (S):
                                                <Input
                                                    type={"number"}
                                                    step={0.01}
                                                    value={row.s1}
                                                    min={0}
                                                    color={"var(--text-color)"}
                                                    onChange={(event) => {
                                                        this.handleChangeValue(
                                                            i,
                                                            "s1",
                                                            parseFloat(event)
                                                        );
                                                    }}
                                                />
                                            </InputGroup>
                                            <InputGroup>
                                                <Spacer />
                                                D:
                                                <Input
                                                    type={"number"}
                                                    step={0.01}
                                                    value={row.d1}
                                                    min={0}
                                                    color={"var(--text-color)"}
                                                    onChange={(event) => {
                                                        this.handleChangeValue(
                                                            i,
                                                            "d1",
                                                            parseFloat(event)
                                                        );
                                                    }}
                                                />
                                            </InputGroup>
                                            <InputGroup>
                                                <Spacer />
                                                T:
                                                <Spacer />
                                                <TB1>{row.t1}</TB1>
                                            </InputGroup>
                                            <InputGroup>
                                                <Spacer />
                                                ᾱ:
                                                <Spacer />
                                                <TB1>{row.anphaA1}</TB1>
                                            </InputGroup>
                                        </TableRowCell>
                                        <TableRowCell>
                                            <InputGroup>
                                                <Spacer />
                                                ᾱ:
                                                <Input
                                                    type={"number"}
                                                    step={0.01}
                                                    value={row.anphaA2}
                                                    min={0}
                                                    color={"var(--text-color)"}
                                                    onChange={(event) => {
                                                        this.handleChangeValue(
                                                            i,
                                                            "anphaA2",
                                                            parseFloat(event)
                                                        );
                                                    }}
                                                />
                                            </InputGroup>
                                            <InputGroup>
                                                <Spacer />
                                                Diện tích (S):
                                                <Input
                                                    type={"number"}
                                                    step={0.01}
                                                    value={row.s2}
                                                    min={0}
                                                    color={"var(--text-color)"}
                                                    onChange={(event) => {
                                                        this.handleChangeValue(
                                                            i,
                                                            "s2",
                                                            parseFloat(event)
                                                        );
                                                    }}
                                                />
                                            </InputGroup>

                                            <InputGroup>
                                                <Spacer />
                                                D:
                                                <Spacer />
                                                <TB1>{row.d2}</TB1>
                                            </InputGroup>

                                            <InputGroup>
                                                <Spacer />
                                                T:
                                                <Spacer />
                                                <TB1>{row.t2}</TB1>
                                            </InputGroup>

                                            <InputGroup>
                                                <Spacer />
                                                ΔL:
                                                <Spacer />
                                                <TB1>{row.deltaL2}</TB1>
                                            </InputGroup>
                                        </TableRowCell>
                                    </TableRow>
                                ))}
                            </Table>
                            <FlexPanel></FlexPanel>
                        </Row>
                    </Container>
                </Section>
            </Container>
        );
    }
}
Check = inject("appStore")(observer(Check));
export default Check;
