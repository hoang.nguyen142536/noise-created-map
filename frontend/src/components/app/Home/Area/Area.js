import "./Area.scss";
import React, { Component } from "react";
import { Container } from "components/bases/Container/Container";
import { Row } from "components/bases/Row/Row";
import { Spacer } from "components/bases/spacer/Spacer";
import { TB1 } from "components/bases/Text/Text";
import { inject, observer } from "mobx-react";
import { Button, EmptyButton } from "components/bases/Button/Button";
import { ColorPicker } from "components/bases/ColorPicker/ColorPicker";
import PopupAddNewArea from "components/app/Home/Action/PopupAddNewArea";
import PopupEditArea from "components/app/Home/Action/PopupEditArea";
import {
    Input,
    InputGroup,
    Section,
    InputAppend,
    AdvanceSelect,
} from "components/bases/Form";
import { FlexPanel, BorderPanel } from "components/bases/Panel/Panel";
import { Table, TableRowCell, TableRow } from "components/bases/Table/Table";
class Area extends Component {
    constructor(props) {
        super(props);

        this.inputRef = React.createRef();
    }
    homeStore = this.props.appStore.homeStore;

    handleAdd() {
        this.homeStore.setPopupNewArea(true);
    }

    handleRemove() {
        this.homeStore.removeArea(this.homeStore.selectedProjectId);
    }

    handleSelected(index) {
        this.homeStore.selectArea(this.homeStore.selectedProjectId, index);
    }

    handleChangeValue(index, key, value) {
        this.homeStore.changeValueArea(
            this.homeStore.selectedProjectId,
            index,
            key,
            value
        );
    }
    handleEdit() {
        this.homeStore.setPopupEditArea(true);
    }
    render() {
        const currentProject =
            this.homeStore.projects.find(
                (p) => p.id === this.homeStore.selectedProjectId
            ) || {};
        return (
            <Section header={"Quản lý khu vực"}>
                <Container className="list-area">
                    <Table
                        className={"result-table"}
                        isFixedHeader
                        headers={[
                            { label: "STT", width: 25 },
                            { label: "Loại", width: 30 },
                            { label: "Data", width: 160 },
                        ]}
                    >
                        {(currentProject.areas || []).map((row, i) => (
                            <TableRow
                                key={i}
                                onClick={() => this.handleSelected(i)}
                                isSelected={
                                    currentProject.selectedAreaIndex === i
                                        ? true
                                        : false
                                }
                            >
                                <TableRowCell>
                                    <TB1>{i + 1}</TB1>
                                </TableRowCell>
                                <TableRowCell>{row.type}</TableRowCell>
                                <TableRowCell>
                                    {row.type === "circle" && (
                                        <>
                                            <InputGroup>
                                                X (m):
                                                <Input
                                                    color={"var(--text-color)"}
                                                    type={"number"}
                                                    step={1}
                                                    value={row.x}
                                                    min={0}
                                                />
                                            </InputGroup>
                                            <InputGroup>
                                                Y (m):
                                                <Input
                                                    color={"var(--text-color)"}
                                                    type={"number"}
                                                    step={1}
                                                    value={row.y}
                                                    min={0}
                                                />
                                            </InputGroup>
                                            <InputGroup>
                                                Bán kính (m):
                                                <Input
                                                    color={"var(--text-color)"}
                                                    type={"number"}
                                                    step={1}
                                                    value={row.radius}
                                                    min={0}
                                                />
                                            </InputGroup>
                                            <InputGroup>
                                                Độ dày:
                                                <Input
                                                    color={"var(--text-color)"}
                                                    value={row.think}
                                                />
                                            </InputGroup>
                                            <InputGroup>
                                                Màu sắc:
                                                <Button
                                                    text={""}
                                                    backgroundColor={row.color}
                                                />
                                            </InputGroup>
                                        </>
                                    )}
                                    {row.type === "rectangle" && (
                                        <>
                                            <InputGroup>
                                                X (m):
                                                <Input
                                                    color={"var(--text-color)"}
                                                    type={"number"}
                                                    step={1}
                                                    value={row.x}
                                                    min={0}
                                                />
                                            </InputGroup>
                                            <InputGroup>
                                                Y (m):
                                                <Input
                                                    color={"var(--text-color)"}
                                                    type={"number"}
                                                    step={1}
                                                    value={row.y}
                                                    min={0}
                                                />
                                            </InputGroup>
                                            <InputGroup>
                                                Chiều dài:
                                                <Input
                                                    color={"var(--text-color)"}
                                                    type={"number"}
                                                    step={1}
                                                    value={row.width}
                                                    min={0}
                                                />
                                            </InputGroup>
                                            <InputGroup>
                                                Chiều rộng:
                                                <Input
                                                    color={"var(--text-color)"}
                                                    type={"number"}
                                                    step={1}
                                                    value={row.height}
                                                    min={0}
                                                />
                                            </InputGroup>
                                            <InputGroup>
                                                Độ dày:
                                                <Input
                                                    color={"var(--text-color)"}
                                                    value={row.think}
                                                />
                                            </InputGroup>
                                            <InputGroup>
                                                Màu sắc:
                                                <Button
                                                    text={""}
                                                    backgroundColor={row.color}
                                                />
                                            </InputGroup>
                                        </>
                                    )}
                                    {row.type === "polygon" && (
                                        <>
                                            <InputGroup>
                                                Độ dày:
                                                <Input
                                                    color={"var(--text-color)"}
                                                    value={row.think}
                                                />
                                            </InputGroup>
                                            <InputGroup>
                                                Màu sắc:
                                                <Button
                                                    text={""}
                                                    backgroundColor={row.color}
                                                />
                                            </InputGroup>
                                            {(row.data || []).map((r, i) => (
                                                <Row key={i}>
                                                    <InputGroup>
                                                        X:
                                                        <Input
                                                            color={
                                                                "var(--text-color)"
                                                            }
                                                            value={r.x}
                                                            min={0}
                                                        />
                                                    </InputGroup>
                                                    <Spacer />
                                                    <InputGroup>
                                                        Y:
                                                        <Input
                                                            color={
                                                                "var(--text-color)"
                                                            }
                                                            value={r.y}
                                                            min={0}
                                                        />
                                                    </InputGroup>
                                                </Row>
                                            ))}
                                        </>
                                    )}
                                </TableRowCell>
                            </TableRow>
                        ))}
                    </Table>
                </Container>
                <Row className={"group-btn-area"}>
                    <Button
                        icon={"plus"}
                        text={'Thêm'}
                        className={"btn-area"}
                        onClick={() => {
                            this.handleAdd();
                        }}
                    ></Button>
                    <Spacer />
                    <Button
                        icon={"trash"}
                        text={'Xóa'}
                        className={"btn-area"}
                        onClick={() => {
                            this.handleRemove();
                        }}
                    ></Button>
                    <Spacer />
                    <Button
                        icon={"edit"}
                        text={'Sửa'}
                        className={"btn-area"}
                        onClick={() => {
                            this.handleEdit();
                        }}
                    ></Button>
                    <Spacer />
                </Row>
                {this.homeStore.isPopUpNewArea && <PopupAddNewArea />}
                {this.homeStore.isPopUpEditArea && <PopupEditArea />}
            </Section>
        );
    }
}
Area = inject("appStore")(observer(Area));
export default Area;
