import "./Color.scss";
import React, { Component } from "react";
import { Container } from "components/bases/Container/Container";
import { Row } from "components/bases/Row/Row";
import { Section, Button, Input } from "components/bases/Form";
import { FlexPanel, BorderPanel } from "components/bases/Panel/Panel";
import { inject, observer } from "mobx-react";
import { TB1 } from "components/bases/Text/Text";
import { Table, TableRowCell, TableRow } from "components/bases/Table/Table";
import { ColorPicker } from "components/bases/ColorPicker/ColorPicker";
import { Spacer } from "components/bases/spacer/Spacer";

class Color extends Component {
    homeStore = this.props.appStore.homeStore;

    handleAdd() {
        this.homeStore.addColor(this.homeStore.selectedProjectId);
    }

    handleRemove() {
        this.homeStore.removeColor(this.homeStore.selectedProjectId);
    }

    handleSelected(index) {
        this.homeStore.selectColor(this.homeStore.selectedProjectId, index);
    }

    handleChangeValue(index, key, value) {
        this.homeStore.changeValueColor(
            this.homeStore.selectedProjectId,
            index,
            key,
            value
        );
    }

    render() {
        const currentProject =
            this.homeStore.projects.find(
                (p) => p.id === this.homeStore.selectedProjectId
            ) || {};

        return (
            <Container className={"home-search-container"}>
                <Section>
                    <Row>
                        <Button
                            className={"btn-color"}
                            icon={"plus"}
                            text={"Thêm"}
                            onClick={() => {
                                this.handleAdd();
                            }}
                        ></Button>
                        <Spacer />
                        <Button
                            className={"btn-color"}
                            icon={"trash"}
                            text={"Xóa"}
                            onClick={() => {
                                this.handleRemove();
                            }}
                        ></Button>
                    </Row>
                    <Container className="list-color">
                        <Table
                            className={"result-table"}
                            isFixedHeader
                            headers={[
                                { label: "STT", width: 25 },
                                { label: "Màu sắc", width: 50 },
                                { label: "Từ", width: 30 },
                                { label: "Đến", width: 30 },
                            ]}
                        >
                            {(currentProject.colorRanges || []).map(
                                (row, i) => (
                                    <TableRow
                                        key={i}
                                        onClick={() => this.handleSelected(i)}
                                        isSelected={
                                            currentProject.selectedColorRangeIndex ===
                                            i
                                                ? true
                                                : false
                                        }
                                    >
                                        <TableRowCell>
                                            <TB1>{i + 1}</TB1>
                                        </TableRowCell>
                                        <TableRowCell>
                                            <ColorPicker
                                                value={row.color}
                                                onChange={(color) =>
                                                    this.handleChangeValue(
                                                        i,
                                                        "color",
                                                        color
                                                    )
                                                }
                                            />
                                        </TableRowCell>
                                        <TableRowCell>
                                            <Input
                                                type={"number"}
                                                step={1}
                                                value={row.from}
                                                min={0}
                                                color={"var(--text-color)"}
                                                onChange={(event) => {
                                                    this.handleChangeValue(
                                                        i,
                                                        "from",
                                                        parseFloat(event)
                                                    );
                                                }}
                                            />
                                        </TableRowCell>
                                        <TableRowCell>
                                            <Input
                                                type={"number"}
                                                step={1}
                                                value={row.to}
                                                min={0}
                                                color={"var(--text-color)"}
                                                onChange={(event) => {
                                                    this.handleChangeValue(
                                                        i,
                                                        "to",
                                                        parseFloat(event)
                                                    );
                                                }}
                                            />
                                        </TableRowCell>
                                    </TableRow>
                                )
                            )}
                        </Table>
                    </Container>
                </Section>
            </Container>
        );
    }
}

Color = inject("appStore")(observer(Color));
export default Color;
