import "./Does.scss";
import React, { Component } from "react";
import { BorderPanel, FlexPanel } from "components/bases/Panel/Panel";
import { Container } from "components/bases/Container/Container";
import { Row } from "components/bases/Row/Row";
import { Spacer } from "components/bases/spacer/Spacer";
import { TB1 } from "components/bases/Text/Text";
import { Input, Section, Button, InputGroup } from "components/bases/Form";
import { Table, TableRowCell, TableRow } from "components/bases/Table/Table";
import { inject, observer } from "mobx-react";
import BusinessService from "services/business.service";
import { ScrollView } from "components/bases/ScrollView/ScrollView";

class Does extends Component {
    homeStore = this.props.appStore.homeStore;
    businessService = new BusinessService();

    handleAdd() {
        this.homeStore.addDoes(this.homeStore.selectedProjectId);
    }

    handleRemove() {
        this.homeStore.removeDoes(this.homeStore.selectedProjectId);
    }

    handleChangeValue(index, key, value) {
        this.homeStore.changeValueDoes(
            this.homeStore.selectedProjectId,
            index,
            key,
            value
        );
        const project = this.homeStore.getCurrentProject();
        const {
            noiseDoseData,
            noiseSummaryData,
        } = this.businessService.calculateNoiseDoes(
            project.noiseDoeses,
            project
        );
        this.homeStore.changeDoes(noiseDoseData, noiseSummaryData);
    }
    handleSelected(index) {
        this.homeStore.selectDoes(this.homeStore.selectedProjectId, index);
    }

    formatDataJson(filterVal, jsonData) {
        return jsonData.map((v) =>
            filterVal.map((j) => {
                return v[j];
            })
        );
    }

    exportData(currentProject, numberTable) {
        const fields1 = {
            name: "Họ tên",
            x: "Tọa độ X (m)",
            y: "Tọa độ Y (m)",
            t: "T (h)",
            leq: "Leq",
            twq: "TWA",
            d1: "D1 (%)",
        };
        const fields2 = {
            name: "Họ tên",
            tca: "Tca (h)",
            lex: "Lex",
            d: "D (%)",
        };
        let fields = {};
        let listExport = [];
        if (numberTable === 1) {
            fields = { ...fields1 };
            if (
                Array.isArray(currentProject.noiseDoeses) &&
                currentProject.noiseDoeses.length > 0
            ) {
                listExport = [...currentProject.noiseDoeses];
            }
        } else if (numberTable === 2) {
            fields = { ...fields2 };
            if (
                Array.isArray(currentProject.noiseDoesTotals) &&
                currentProject.noiseDoesTotals.length > 0
            ) {
                listExport = [...currentProject.noiseDoesTotals];
            }
        }
        if (Array.isArray(listExport) && listExport.length > 0 && fields) {
            import("components/libs/Export2Excel").then((excel) => {
                const data = this.formatDataJson(
                    Object.keys(fields),
                    listExport
                );
                excel.export_json_to_excel({
                    header: Object.values(fields),
                    data,
                    filename: "Noise_Does",
                    autoWidth: true,
                    bookType: "csv",
                });
            });
        }
    }
    render() {
        const currentProject =
            this.homeStore.projects.find(
                (p) => p.id === this.homeStore.selectedProjectId
            ) || {};
        const currentNoiseDoes =
            currentProject &&
            Array.isArray(currentProject.noiseDoeses) &&
            currentProject.noiseDoeses[currentProject.selectedDoesIndex];
        return (
            <Container className={"home-search-container"}>
                <Section>
                    <ScrollView>
                        <Container className="list-does">
                            <Row className={"group-btn-actions"}>
                                <Button
                                    className={"btn btn-default"}
                                    icon={"plus"}
                                    text={"Thêm"}
                                    onClick={() => {
                                        this.handleAdd();
                                    }}
                                ></Button>
                                <Spacer />
                                <Button
                                    className={"btn btn-default "}
                                    icon={"trash"}
                                    text={"Xóa"}
                                    onClick={() => {
                                        this.handleRemove();
                                    }}
                                ></Button>
                                <Spacer />
                                <Button
                                    icon={"download"}
                                    text={"Tải"}
                                    className={"btn btn-default"}
                                    onClick={() => {
                                        this.exportData(currentProject, 1);
                                    }}
                                ></Button>
                            </Row>
                            <Row className="list-does-content">
                                <Container>
                                    <Table
                                        className={"result-table"}
                                        isFixedHeader
                                        headers={[
                                            { label: "Họ Tên", width: 60 },
                                            { label: "Tọa độ (m)", width: 50 },
                                            { label: "T (h)", width: 30 },
                                            { label: "Leq", width: 40 },
                                            { label: "TWA", width: 40 },
                                            { label: "Di (%)", width: 30 },
                                        ]}
                                    >
                                        {(currentProject.noiseDoeses || []).map(
                                            (row, i) => (
                                                <TableRow
                                                    key={i}
                                                    onClick={() =>
                                                        this.handleSelected(i)
                                                    }
                                                    isSelected={
                                                        currentProject.selectedDoesIndex ===
                                                        i
                                                            ? true
                                                            : false
                                                    }
                                                >
                                                    <TableRowCell>
                                                        <Input
                                                            value={row.name}
                                                            color={
                                                                "var(--text-color)"
                                                            }
                                                            onChange={(
                                                                event
                                                            ) => {
                                                                this.handleChangeValue(
                                                                    i,
                                                                    "name",
                                                                    event
                                                                );
                                                            }}
                                                        />
                                                    </TableRowCell>
                                                    <TableRowCell>
                                                        <InputGroup>
                                                            X:
                                                            <Input
                                                                type={"number"}
                                                                step={1}
                                                                value={row.x}
                                                                min={0}
                                                                color={
                                                                    "var(--text-color)"
                                                                }
                                                                onChange={(
                                                                    event
                                                                ) => {
                                                                    this.handleChangeValue(
                                                                        i,
                                                                        "x",
                                                                        parseFloat(
                                                                            event
                                                                        )
                                                                    );
                                                                }}
                                                            />
                                                        </InputGroup>
                                                        <InputGroup>
                                                            Y:
                                                            <Input
                                                                type={"number"}
                                                                step={1}
                                                                value={row.y}
                                                                min={0}
                                                                color={
                                                                    "var(--text-color)"
                                                                }
                                                                onChange={(
                                                                    event
                                                                ) => {
                                                                    this.handleChangeValue(
                                                                        i,
                                                                        "y",
                                                                        parseFloat(
                                                                            event
                                                                        )
                                                                    );
                                                                }}
                                                            />
                                                        </InputGroup>
                                                    </TableRowCell>
                                                    <TableRowCell>
                                                        <Input
                                                            type={"number"}
                                                            step={1}
                                                            value={row.t}
                                                            min={0}
                                                            color={
                                                                "var(--text-color)"
                                                            }
                                                            onChange={(
                                                                event
                                                            ) => {
                                                                this.handleChangeValue(
                                                                    i,
                                                                    "t",
                                                                    parseFloat(
                                                                        event
                                                                    )
                                                                );
                                                            }}
                                                        />
                                                    </TableRowCell>
                                                    <TableRowCell>
                                                        <Input
                                                            type={"number"}
                                                            step={1}
                                                            value={row.leq}
                                                            min={0}
                                                            color={
                                                                "var(--text-color)"
                                                            }
                                                            onChange={(
                                                                event
                                                            ) => {
                                                                this.handleChangeValue(
                                                                    i,
                                                                    "leq",
                                                                    parseFloat(
                                                                        event
                                                                    )
                                                                );
                                                            }}
                                                        />
                                                    </TableRowCell>
                                                    <TableRowCell>
                                                        <Input
                                                            type={"number"}
                                                            step={1}
                                                            value={row.twa}
                                                            min={0}
                                                            color={
                                                                "var(--text-color)"
                                                            }
                                                            onChange={(
                                                                event
                                                            ) => {
                                                                this.handleChangeValue(
                                                                    i,
                                                                    "twa",
                                                                    parseFloat(
                                                                        event
                                                                    )
                                                                );
                                                            }}
                                                        />
                                                    </TableRowCell>
                                                    <TableRowCell>
                                                        <TB1>{row.d1}</TB1>
                                                    </TableRowCell>
                                                </TableRow>
                                            )
                                        )}
                                    </Table>
                                    <Spacer />
                                </Container>
                            </Row>
                        </Container>

                        <Container className="list-does-total list-does">
                            <Row>
                                <Container>
                                    <Table
                                        className={"result-table"}
                                        isFixedHeader
                                        headers={[
                                            { label: "STT", width: 25 },
                                            { label: "Họ Tên", width: 70 },
                                            { label: "Tca (h)", width: 30 },
                                            { label: "Lex", width: 30 },
                                            { label: "D (%)", width: 30 },
                                        ]}
                                    >
                                        {(
                                            currentProject.noiseDoesTotals || []
                                        ).map((row, i) => (
                                            <TableRow
                                                key={row.id}
                                                onClick={() =>
                                                    this.handleSelected(i)
                                                }
                                                isSelected={
                                                    currentProject.selectedDoesIndex ===
                                                    i
                                                        ? true
                                                        : false
                                                }
                                            >
                                                <TableRowCell>
                                                    <TB1>{i + 1}</TB1>
                                                </TableRowCell>
                                                <TableRowCell>
                                                    <TB1>{row.name}</TB1>
                                                </TableRowCell>
                                                <TableRowCell>
                                                    <TB1>{row.tca}</TB1>
                                                </TableRowCell>
                                                <TableRowCell>
                                                    <TB1>{row.lex}</TB1>
                                                </TableRowCell>
                                                <TableRowCell>
                                                    <TB1>{row.d}</TB1>
                                                </TableRowCell>
                                            </TableRow>
                                        ))}
                                    </Table>
                                    <Spacer />
                                </Container>
                            </Row>
                            <Button
                                icon={"download"}
                                text={"Tải"}
                                className={"btn btn-default"}
                                onClick={() => {
                                    this.exportData(currentProject, 2);
                                }}
                            ></Button>
                            <Row className={"group-btn-actions"}></Row>
                        </Container>
                    </ScrollView>
                </Section>
            </Container>
        );
    }
}

export default inject("appStore")(observer(Does));
