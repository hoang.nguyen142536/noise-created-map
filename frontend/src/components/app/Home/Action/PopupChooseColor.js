import React, { Component } from "react";
import { ColorPicker } from "components/bases/ColorPicker/ColorPicker";
import { Popup, PopupFooter } from "components/bases/Popup/Popup";
import PropTypes from "prop-types";
import { inject, observer } from "mobx-react";

class PopupChooseColor extends Component {
    homeStore = this.props.appStore.homeStore;
    handleChangeColor = (event) => {
        this.homeStore.changeText(
            this.homeStore.selectedProjectId,
            -1,
            "color",
            event
        );
        this.homeStore.setPopupColor(false);
    }
    render() {
        return (
            <Popup
                title={"Chọn màu"}
                width={"250px"}
                onClose={() => this.homeStore.setPopupColor(false)}
                height={"400px"}
            >
                <ColorPicker
                    value={this.props.value}
                    onChange={(event) => {
                        this.handleChangeColor(event)
                    }}
                />
            </Popup>
        );
    }
}
export default inject("appStore")(observer(PopupChooseColor));
