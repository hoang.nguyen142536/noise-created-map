import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import PropTypes from "prop-types";
import { Container } from "components/bases/Container/Container";
import { Popup, PopupFooter } from "components/bases/Popup/Popup";
import {
    Button,
    FormControlLabel,
    FormGroup,
    Input,
    InputGroup,
    AdvanceSelect,
    Radio,
} from "components/bases/Form";
import { TB2 } from "components/bases/Text/Text";
import { toast } from "components/bases/Modal/Modal";

import { CommonHelper } from "helper/common.helper";
class New extends Component {
    // state = {
    //     isWrite: false,
    // };
    homeStore = this.props.appStore.homeStore;
    handelAddProject(newProject) {
        if (!newProject.name) {
            toast({ type: "error", message: `Tên không được bỏ trống!` });
            return;
        }
        // if (!newProject.hz) {
        //     toast({
        //         type: "error",
        //         message: `Thông số không được bỏ trống!`,
        //     });
        //     return;
        // }
        if (!newProject.width) {
            toast({ type: "error", message: `Chiều dài không được bỏ trống!` });
            return;
        }
        if (!newProject.height) {
            toast({
                type: "error",
                message: `Chiều rộng không được bỏ trống!`,
            });
            return;
        }
        if (!newProject.volumn) {
            toast({
                type: "error",
                message: `Thể tích phòng không được bỏ trống!`,
            });
            return;
        }
        if (!newProject.soundDamping) {
            toast({
                type: "error",
                message: `Độ giảm âm không được bỏ trống!`,
            });
            return;
        }

        do {
            newProject.id = CommonHelper.uuid();
        } while (
            this.homeStore.projects.findIndex((p) => p.id === newProject.id) !==
            -1
        );
        this.homeStore.addNewProject(newProject);
        this.homeStore.setPopupNewProject(false)
    }
    render() {
        const newProject = this.homeStore.newProject || {};
        return (
            <Popup
                title={"Tạo Dự án mới"}
                width={"500px"}
                onClose={() => this.homeStore.setPopupNewProject(false)}
            >
                <Container className={"list-detail"}>
                    <FormGroup>
                        <FormControlLabel
                            label={"Tên"}
                            control={
                                <Input
                                    placeholder={"Nhập tên dự án"}
                                    value={newProject.name}
                                    onChange={(event) => {
                                        this.homeStore.setNewProject(
                                            "name",
                                            event
                                        );
                                    }}
                                />
                            }
                        />
                        <FormControlLabel
                            label="Thông số"
                            control={
                                <InputGroup>
                                    {/* <Radio
                                        label="Nhập"
                                        checked={this.state.isWrite}
                                        onChange={() => {
                                            this.setState({
                                                isWrite: !this.state.isWrite,
                                            });
                                        }}
                                    /> */}
                                    <Input
                                        placeholder={"Nhập chỉ số hiển thị"}
                                        value={newProject.hz}
                                        onChange={(event) => {
                                            this.homeStore.setNewProject(
                                                "hz",
                                                event
                                            );
                                        }}
                                        // visible={this.state.isWrite? true: false}
                                    />
                                    {/* <AdvanceSelect
                                        options={[
                                            { id: "L_SPL", label: "L_SPL" },
                                            { id: "L_Aeq", label: "L_Aeq" },
                                            { id: "L_max", label: "L_max" },
                                            { id: "L_min", label: "L_min" },
                                            { id: "L_peak", label: "L_peak" },
                                        ]}
                                        onChange={(event) => {
                                            this.homeStore.setNewProject(
                                                "hz",
                                                event
                                            );
                                        }}
                                        value={newProject.hz}
                                        // visible={
                                        //     !this.state.isWrite ? true : false
                                        // }
                                    /> */}
                                </InputGroup>
                            }
                        />
                        <FormControlLabel
                            label={"Chiều dài"}
                            control={
                                <InputGroup>
                                    <Input
                                        placeholder={"Nhập chiều dài"}
                                        type={"number"}
                                        step={1}
                                        value={newProject.width}
                                        min={0}
                                        onChange={(event) => {
                                            this.homeStore.setNewProject(
                                                "width",
                                                event
                                            );
                                        }}
                                    />
                                    <TB2>[M]</TB2>
                                </InputGroup>
                            }
                        />
                        <FormControlLabel
                            label={"Chiều rộng"}
                            control={
                                <InputGroup>
                                    <Input
                                        placeholder={"Nhập chiều rộng"}
                                        type={"number"}
                                        step={1}
                                        value={newProject.height}
                                        min={0}
                                        onChange={(event) => {
                                            this.homeStore.setNewProject(
                                                "height",
                                                event
                                            );
                                        }}
                                    />
                                    <TB2>[M]</TB2>
                                </InputGroup>
                            }
                        />
                        <FormControlLabel
                            label={"Thể tích phòng"}
                            control={
                                <InputGroup>
                                    <Input
                                        placeholder={"Nhập thể tích phòng"}
                                        type={"number"}
                                        step={1}
                                        value={newProject.volumn}
                                        min={0}
                                        onChange={(event) => {
                                            this.homeStore.setNewProject(
                                                "volumn",
                                                event
                                            );
                                        }}
                                    />
                                    <TB2>[M^3]</TB2>
                                </InputGroup>
                            }
                        />
                        <FormControlLabel
                            label={"Độ giảm âm"}
                            control={
                                <InputGroup>
                                    <Input
                                        placeholder={"Nhập độ giảm âm"}
                                        type={"number"}
                                        step={1}
                                        value={newProject.soundDamping}
                                        min={0}
                                        onChange={(event) => {
                                            this.homeStore.setNewProject(
                                                "soundDamping",
                                                event
                                            );
                                        }}
                                    />
                                    <TB2>[dBA/s]</TB2>
                                </InputGroup>
                            }
                        />
                        <FormControlLabel
                            label={"Đơn vị đo"}
                            control={
                                <InputGroup>
                                    <Input
                                        placeholder={"Đơn vị đo"}
                                        value={newProject.unit}
                                        min={0}
                                        onChange={(event) => {
                                            this.homeStore.setNewProject(
                                                "unit",
                                                event
                                            );
                                        }}
                                    />
                                </InputGroup>
                            }
                        />
                    </FormGroup>
                </Container>

                <PopupFooter>
                    <Button
                        type={false ? "warning" : "primary"}
                        text={false ? "Chỉnh sửa" : "Thêm mới"}
                        onClick={() => this.handelAddProject(newProject)}
                    />
                </PopupFooter>
            </Popup>
        );
    }
}
export default inject("appStore")(observer(New));
