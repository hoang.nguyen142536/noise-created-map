import "./PopupArea.scss"

import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import PropTypes from "prop-types";
import { Container } from "components/bases/Container/Container";
import { Popup, PopupFooter } from "components/bases/Popup/Popup";
import {
    Button,
    FormControlLabel,
    FormGroup,
    Input,
    InputGroup,
    AdvanceSelect,
    Radio,
} from "components/bases/Form";
import { TB2 } from "components/bases/Text/Text";
import { toast } from "components/bases/Modal/Modal";
import { ColorPicker } from "components/bases/ColorPicker/ColorPicker";
import { Row } from "components/bases/Row/Row";
import { Spacer } from "components/bases/spacer/Spacer";

import { CommonHelper } from "helper/common.helper";
class PopupAddNewArea extends Component {
    homeStore = this.props.appStore.homeStore;
    handelAddArea = (area) => {
        if (!area.type) {
            toast({ type: "error", message: `Loại không được bỏ trống!` });
            return;
        }
        this.homeStore.addNewArea(area);
        this.homeStore.setPopupNewArea(false);
    };
    render() {
        const newArea = this.homeStore.newArea || {};
        return (
            <Popup
                title={"Thêm Khu vực mới"}
                width={"500px"}
                onClose={() => this.homeStore.setPopupNewArea(false)}
            >
                <Container className={"list-detail"}>
                    <FormGroup>
                        <FormControlLabel
                            label={"Loại"}
                            control={
                                <AdvanceSelect
                                    options={[
                                        { id: "circle", label: "Hình tròn" },
                                        {
                                            id: "rectangle",
                                            label: "Hình chữ nhật",
                                        },
                                        {
                                            id: "polygon",
                                            label: "Hình đa giác",
                                        },
                                    ]}
                                    onChange={(event) => {
                                        this.homeStore.setNewArea(
                                            "type",
                                            event
                                        );
                                    }}
                                    value={newArea.type}
                                />
                            }
                        />
                        {(newArea.type === "circle" ||
                            newArea.type === "rectangle") && (
                            <FormControlLabel
                                label={"X (m)"}
                                control={
                                    <Input
                                        placeholder={"Nhập tọa độ X"}
                                        type={"number"}
                                        min={0}
                                        step={1}
                                        value={newArea.x}
                                        onChange={(event) => {
                                            this.homeStore.setNewArea(
                                                "x",
                                                event
                                            );
                                        }}
                                    />
                                }
                            />
                        )}
                        {(newArea.type === "circle" ||
                            newArea.type === "rectangle") && (
                            <FormControlLabel
                                label={"Y (m)"}
                                control={
                                    <Input
                                        placeholder={"Nhập tọa độ Y"}
                                        type={"number"}
                                        min={0}
                                        step={1}
                                        value={newArea.y}
                                        onChange={(event) => {
                                            this.homeStore.setNewArea(
                                                "y",
                                                event
                                            );
                                        }}
                                    />
                                }
                            />
                        )}
                        {newArea.type === "circle" && (
                            <FormControlLabel
                                label={"Bán kính (m)"}
                                control={
                                    <Input
                                        placeholder={"Nhập bán kính"}
                                        type={"number"}
                                        min={0}
                                        step={1}
                                        value={newArea.radius}
                                        onChange={(event) => {
                                            this.homeStore.setNewArea(
                                                "radius",
                                                event
                                            );
                                        }}
                                    />
                                }
                            />
                        )}
                        {newArea.type === "rectangle" && (
                            <FormControlLabel
                                label={"Chiều dài"}
                                control={
                                    <Input
                                        placeholder={"Nhập chiều dài"}
                                        type={"number"}
                                        min={0}
                                        step={1}
                                        value={newArea.width}
                                        onChange={(event) => {
                                            this.homeStore.setNewArea(
                                                "width",
                                                event
                                            );
                                        }}
                                    />
                                }
                            />
                        )}
                        {newArea.type === "rectangle" && (
                            <FormControlLabel
                                label={"Chiều rộng"}
                                control={
                                    <Input
                                        placeholder={"Nhập chiều rộng"}
                                        type={"number"}
                                        min={0}
                                        step={1}
                                        value={newArea.height}
                                        onChange={(event) => {
                                            this.homeStore.setNewArea(
                                                "height",
                                                event
                                            );
                                        }}
                                    />
                                }
                            />
                        )}
                        <FormControlLabel
                            label={"Chiều dày"}
                            control={
                                <Input
                                    placeholder={"Nhập chiều dày"}
                                    type={"number"}
                                    min={0}
                                    step={1}
                                    value={newArea.think}
                                    onChange={(event) => {
                                        this.homeStore.setNewArea(
                                            "think",
                                            event
                                        );
                                    }}
                                />
                            }
                        />
                        <FormControlLabel
                            label={"Màu sắc"}
                            control={
                                <ColorPicker
                                    value={newArea.color}
                                    onChange={(event) => {
                                        this.homeStore.setNewArea(
                                            "color",
                                            event
                                        );
                                    }}
                                />
                            }
                        />
                        {newArea.type === "polygon" &&
                            (newArea.data || []).map((data, i) => (
                                <Row>
                                    <FormControlLabel
                                        label={"X (m)"}
                                        control={
                                            <Input
                                                placeholder={"Nhập X"}
                                                type={"number"}
                                                step={1}
                                                min={0}
                                                value={data.x}
                                                onChange={(event) => {
                                                    this.homeStore.setDataNewArea(
                                                        "x",
                                                        event,
                                                        i
                                                    );
                                                }}
                                            />
                                        }
                                    />
                                    <Spacer />
                                    <Spacer />
                                    <Spacer />
                                    <Spacer />
                                    <Spacer />
                                    <Spacer />
                                    <FormControlLabel
                                        label={"Y (m)"}
                                        control={
                                            <Input
                                                placeholder={"Nhập Y"}
                                                type={"number"}
                                                step={1}
                                                min={0}
                                                value={data.y}
                                                onChange={(event) => {
                                                    this.homeStore.setDataNewArea(
                                                        "y",
                                                        event,
                                                        i
                                                    );
                                                }}
                                            />
                                        }
                                    />

                                    <Spacer />
                                    <Spacer />
                                    <Button
                                        className={'btn-clear'}
                                        icon={"trash"}
                                        onlyIcon
                                        onClick={() => {
                                            this.homeStore.deletedDataNewArea(
                                                i
                                            );
                                        }}
                                    ></Button>
                                </Row>
                            ))}
                        {newArea.type === "polygon" && (
                            <FormControlLabel
                                label={"Thêm Tọa độ"}
                                control={
                                    <Button
                                    icon={"plus"}
                                    onlyIcon
                                        onClick={() =>
                                            this.homeStore.addNewDataPolygon()
                                        }
                                    ></Button>
                                }
                            />
                        )}
                    </FormGroup>
                </Container>

                <PopupFooter>
                    <Button
                        type={false ? "warning" : "primary"}
                        text={false ? "Chỉnh sửa" : "Thêm mới"}
                        onClick={() => this.handelAddArea(newArea)}
                    />
                </PopupFooter>
            </Popup>
        );
    }
}
export default inject("appStore")(observer(PopupAddNewArea));
