import "./PopupArea.scss"

import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import PropTypes from "prop-types";
import { Container } from "components/bases/Container/Container";
import { Popup, PopupFooter } from "components/bases/Popup/Popup";
import {
    Button,
    FormControlLabel,
    FormGroup,
    Input,
    InputGroup,
    AdvanceSelect,
    Radio,
} from "components/bases/Form";
import { TB2 } from "components/bases/Text/Text";
import { toast } from "components/bases/Modal/Modal";
import { ColorPicker } from "components/bases/ColorPicker/ColorPicker";
import { Row } from "components/bases/Row/Row";
import { Spacer } from "components/bases/spacer/Spacer";

import { CommonHelper } from "helper/common.helper";
class PopupEditArea extends Component {
    homeStore = this.props.appStore.homeStore;
    handelAddArea = (area) => {
        if (!area.type) {
            toast({ type: "error", message: `Loại không được bỏ trống!` });
            return;
        }
        // this.homeStore.addNewArea(area);
        this.homeStore.setPopupEditArea(false);
    };
    render() {
        const currentProject =
            this.homeStore.projects.find(
                (p) => p.id === this.homeStore.selectedProjectId
            ) || {};
        const currentArea =
            (Array.isArray(currentProject.areas) &&
                currentProject.areas[currentProject.selectedAreaIndex]) ||
            [];
        return (
            <Popup
                title={"Thêm Khu vực mới"}
                width={"500px"}
                onClose={() => this.homeStore.setPopupEditArea(false)}
            >
                <Container className={"list-detail"}>
                    <FormGroup>
                        <FormControlLabel
                            label={"Loại"}
                            control={
                                <Input readonly value={currentArea.type} />
                            }
                        />
                        {(currentArea.type === "circle" ||
                            currentArea.type === "rectangle") && (
                            <FormControlLabel
                                label={"X (m)"}
                                control={
                                    <Input
                                        placeholder={"Nhập tọa độ X"}
                                        type={"number"}
                                        min={0}
                                        step={1}
                                        value={currentArea.x}
                                        onChange={(event) => {
                                            this.homeStore.changeValueArea(
                                                "x",
                                                event
                                            );
                                        }}
                                    />
                                }
                            />
                        )}
                        {(currentArea.type === "circle" ||
                            currentArea.type === "rectangle") && (
                            <FormControlLabel
                                label={"Y (m)"}
                                control={
                                    <Input
                                        placeholder={"Nhập tọa độ Y"}
                                        type={"number"}
                                        min={0}
                                        step={1}
                                        value={currentArea.y}
                                        onChange={(event) => {
                                            this.homeStore.changeValueArea(
                                                "y",
                                                event
                                            );
                                        }}
                                    />
                                }
                            />
                        )}
                        {currentArea.type === "circle" && (
                            <FormControlLabel
                                label={"Bán kính (m)"}
                                control={
                                    <Input
                                        placeholder={"Nhập bán kính"}
                                        type={"number"}
                                        min={0}
                                        step={1}
                                        value={currentArea.radius}
                                        onChange={(event) => {
                                            this.homeStore.changeValueArea(
                                                "radius",
                                                event
                                            );
                                        }}
                                    />
                                }
                            />
                        )}
                        {currentArea.type === "rectangle" && (
                            <FormControlLabel
                                label={"Chiều dài"}
                                control={
                                    <Input
                                        placeholder={"Nhập chiều dài"}
                                        type={"number"}
                                        min={0}
                                        step={1}
                                        value={currentArea.width}
                                        onChange={(event) => {
                                            this.homeStore.changeValueArea(
                                                "width",
                                                event
                                            );
                                        }}
                                    />
                                }
                            />
                        )}
                        {currentArea.type === "rectangle" && (
                            <FormControlLabel
                                label={"Chiều rộng"}
                                control={
                                    <Input
                                        placeholder={"Nhập chiều rộng"}
                                        type={"number"}
                                        min={0}
                                        step={1}
                                        value={currentArea.height}
                                        onChange={(event) => {
                                            this.homeStore.changeValueArea(
                                                "height",
                                                event
                                            );
                                        }}
                                    />
                                }
                            />
                        )}
                        <FormControlLabel
                            label={"Chiều dày"}
                            control={
                                <Input
                                    placeholder={"Nhập chiều dày"}
                                    type={"number"}
                                    min={0}
                                    step={1}
                                    value={currentArea.think}
                                    onChange={(event) => {
                                        this.homeStore.changeValueArea(
                                            "think",
                                            event
                                        );
                                    }}
                                />
                            }
                        />
                        <FormControlLabel
                            label={"Màu sắc"}
                            control={
                                <ColorPicker
                                    value={currentArea.color}
                                    onChange={(event) => {
                                        this.homeStore.changeValueArea(
                                            "color",
                                            event
                                        );
                                    }}
                                />
                            }
                        />
                        {currentArea.type === "polygon" &&
                            (currentArea.data || []).map((data, i) => (
                                <Row>
                                    <FormControlLabel
                                        label={"X (m)"}
                                        control={
                                            <Input
                                                placeholder={"Nhập X"}
                                                type={"number"}
                                                step={1}
                                                min={0}
                                                value={data.x}
                                                onChange={(event) => {
                                                    this.homeStore.setDataCurrentArea(
                                                        "x",
                                                        event,
                                                        i
                                                    );
                                                }}
                                            />
                                        }
                                    />
                                    <Spacer />
                                    <Spacer />
                                    <Spacer />
                                    <Spacer />
                                    <Spacer />
                                    <Spacer />
                                    <FormControlLabel
                                        label={"Y (m)"}
                                        control={
                                            <Input
                                                placeholder={"Nhập Y"}
                                                type={"number"}
                                                step={1}
                                                min={0}
                                                value={data.y}
                                                onChange={(event) => {
                                                    this.homeStore.setDataCurrentArea(
                                                        "y",
                                                        event,
                                                        i
                                                    );
                                                }}
                                            />
                                        }
                                    />

                                    <Spacer />
                                    <Spacer />
                                    <Button
                                        className={'btn-clear'}
                                        icon={"trash"}
                                        onlyIcon
                                        onClick={() => {
                                            this.homeStore.deletedDataCurrentArea(
                                                i
                                            );
                                        }}
                                    ></Button>
                                </Row>
                            ))}
                        {currentArea.type === "polygon" && (
                            <FormControlLabel
                                label={"Thêm Tọa độ"}
                                control={
                                    <Button
                                        icon={"plus"}
                                        onlyIcon
                                        onClick={() =>
                                            this.homeStore.addCurrentDataPolygon()
                                        }
                                    ></Button>
                                }
                            />
                        )}
                    </FormGroup>
                </Container>
            </Popup>
        );
    }
}
export default inject("appStore")(observer(PopupEditArea));
