import "./Home.scss";
import HomeContent from "components/app/Home/HomeContent";
import HomeSearch from "./HomeSearch";
import React, { Component } from "react";
import { Container } from "components/bases/Container/Container";
import { FlexPanel } from "components/bases/Panel/Panel";
import { inject, observer } from "mobx-react";
import Login from "../Login/Login";

class Home extends Component {

    homeStore = this.props.appStore.homeStore;
    render() {
        let userPassword = JSON.parse(localStorage.getItem('userPassword'));
        if (!userPassword) {
            // const username = prompt("Tên đăng nhập");
            // if (!username) {
            //     return (
            //         <Login/>
            //     );
            // }

            // const password = prompt("Mật khẩu");
            // if (!password) {
            //     return (
            //         <Container className={"home"}>
            //             403 - Không đủ quyền truy cập
            //         </Container>
            //     );
            // }

            // if (username !== 'an' && password !== 'RvC5s3cE') {
            //     return (
            //         <Container className={"home"}>
            //             403 - Không đủ quyền truy cập
            //         </Container>
            //     );
            // }
            // else {
            //     const hoang = new Date();
            //     hoang.setHours(hoang.getHours() + 24);
            //     localStorage.setItem('userPassword', JSON.stringify({
            //         username,
            //         password,
            //         expireTime: hoang.getTime()
            //     }))

            //     userPassword = {
            //         username,
            //         password,
            //         expireTime: hoang.getTime()
            //     };
            // }
            return <Login/>;
        }

        const now = new Date();
        if (userPassword.username !== 'an' || userPassword.password !== 'RvC5s3cE' || userPassword.expireTime < now.getTime()) {
            return <Login/>;
        }

        return (
            <Container className={"home"}>
                <FlexPanel className={"home-container"}>
                    <Container className={"content-config flex"}>
                        <HomeSearch />
                        <HomeContent />
                    </Container>
                </FlexPanel>
            </Container>
        );
    }
}

Home = inject("appStore")(observer(Home));
export default Home;
