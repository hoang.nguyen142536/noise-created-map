import "./ForeCast.scss";
import React, { Component } from "react";
import { Container } from "components/bases/Container/Container";
import { Row } from "components/bases/Row/Row";
import { Spacer } from "components/bases/spacer/Spacer";
import { TB1 } from "components/bases/Text/Text";
import { Input, Section, Button, AdvanceSelect, FormGroup, FormControlLabel } from "components/bases/Form";
import { Table, TableRowCell, TableRow } from "components/bases/Table/Table";
import { inject, observer } from "mobx-react";
import BusinessService from "services/business.service";
import { ScrollView } from "components/bases/ScrollView/ScrollView";
import * as FileSaver from 'file-saver';

class Does extends Component {
    homeStore = this.props.appStore.homeStore;
    businessService = new BusinessService();

    handleAddData() {
        this.homeStore.addForeCastData(this.homeStore.selectedProjectId);
    }

    handleRemoveData() {
        const project = this.homeStore.getCurrentProject();
        if (project && project.foreCast && Array.isArray(project.foreCast.data))
        {
            if (project.foreCast.data > project.selectedForeCastDataIndex) {
                project.foreCast.data.splice(project.selectedForeCastDataIndex, 1);
                project.selectedForeCastDataIndex = 0;
            }
            this.homeStore.changeForeCast(this.businessService.calculateForeCast(project.foreCast, project));
        }
    }

    handleChangeDataValue(index, key, value) {
        const project = this.homeStore.getCurrentProject();
        if (project && project.foreCast && Array.isArray(project.foreCast.data) && project.foreCast.data[index])
        {
            project.foreCast.data[index][key] = value;
            this.homeStore.changeForeCast(this.businessService.calculateForeCast(project.foreCast, project));
        }
    }

    handleDataSelected(index) {
        this.homeStore.selectForeCastData(index, this.homeStore.selectedProjectId);
    }

    handleAddPoint() {
        this.homeStore.addForeCastPoint(this.homeStore.selectedProjectId);
    }

    handleRemovePoint() {
        const project = this.homeStore.getCurrentProject();
        if (project && project.foreCast && Array.isArray(project.foreCast.points))
        {
            if (project.foreCast.points > project.selectedForeCastPointIndex) {
                project.foreCast.points.splice(project.selectedForeCastPointIndex, 1);
                project.selectedForeCastPointIndex = 0;
            }
            this.homeStore.changeForeCast(this.businessService.calculateForeCast(project.foreCast, project));
        }
    }

    handleChangePointValue(index, key, value) {
        debugger;
        const project = this.homeStore.getCurrentProject();
        if (project && project.foreCast && Array.isArray(project.foreCast.points) && project.foreCast.points[index])
        {
            project.foreCast.points[index][key] = value;
            this.homeStore.changeForeCast(this.businessService.calculateForeCast(project.foreCast, project));
        }
    }

    handleChangeSounđ

    handlePointSelected(index) {
        this.homeStore.selectForeCastPoint(index, this.homeStore.selectedProjectId);
    }



    formatDataJson(filterVal, jsonData) {
        return jsonData.map((v) =>
            filterVal.map((j) => {
                return v[j];
            })
        );
    }

    exportData(project) {
        /*
            { label: `Ls(${currentProject.unit})`, width: 40 },
            { label: "Q", width: 40 },
            { label: "ω", width: 30 },
            { label: "λ", width: 30 },
            { label: "x (m)", width: 30 },
            { label: "y (m)", width: 30 },
        */

        /*
            { label: "STT", width: 25 },
            { label: "x (m)", width: 50 },
            { label: "y (m)", width: 50 },
            { label: "Δ(k)", width: 30 },
            { label: "Ltd", width: 70 }
        */

        const input = {
            ls: `Ls(${project.unit})`,
            x: "Q",
            y: "ω",
            lamda: "λ",
            x: "x (m)",
            y: "y (m)"
        };
        const output = {
            stt: "STT",
            x: "x (m)",
            y: "y (m)",
            deltaK: "Δ(k)",
            ltd: "Ltd"
        };

        let content = '';
        content += `Tên dự án,${project.name}\r\n`; // infomation
        content += `Thông số,${project.hz}\r\n`;
        content += `Chiều dài,${project.width}\r\n`;
        content += `Chiều rộng,${project.height}\r\n`;
        content += `Thể tích phòng,${project.volumn}\r\n`;
        content += `Đơn vị đo,${project.unit}\r\n`;

        content += `-------------------------------\r\n`;

        const inputKeys = Object.keys(input);
        content += `${inputKeys.map((k) => input[k]).join(',')}\r\n`;
        if (project.foreCast && Array.isArray(project.foreCast.data))
        {
            project.foreCast.data.forEach((d) => {
                content += `${inputKeys.map((k) => d[k]).join(',')}\r\n`;
            });
        }

        content += `-------------------------------\r\n`;

        const outputKeys = Object.keys(output);
        debugger;
        content += `${outputKeys.map((k) => output[k]).join(',')}\r\n`;
        if (project.foreCast && Array.isArray(project.foreCast.points))
        {
            project.foreCast.points.forEach((d, index) => {
                const data = {...d, stt: index + 1};
                content += `${outputKeys.map((k) => data[k]).join(',')}\r\n`;
            });
        }

        this.saveAsCvs(content, project.name);
    }

    exportDataForeCast(project) {
        const output = {
            stt: "STT",
            x: "x (m)",
            y: "y (m)",
            deltaK: "Δ(k)",
            ltd: "Ltd"
        };

        let content = '';

        const outputKeys = Object.keys(output);
        content += `${outputKeys.map((k) => output[k]).join(',')}\r\n`;
        if (project.foreCast && Array.isArray(project.foreCast.points))
        {
            project.foreCast.points.forEach((d, index) => {
                const data = {...d, stt: index + 1};
                content += `${outputKeys.map((k) => data[k]).join(',')}\r\n`;
            });
        }

        this.saveAsCvs(content, `${project.name}-forecast`);
    }

    saveAsCvs(text, fileName) {
        const data = new Blob([text], { type: 'csv' });
        FileSaver.saveAs(data, `${fileName}.csv`);
    }

    render() {
        const currentProject =
            this.homeStore.projects.find(
                (p) => p.id === this.homeStore.selectedProjectId
            ) || {};
        const currentData = currentProject && currentProject.foreCast && Array.isArray(currentProject.foreCast.data) ? currentProject.foreCast.data : [];
        const currentPoint = currentProject && currentProject.foreCast && Array.isArray(currentProject.foreCast.points) ? currentProject.foreCast.points : [];
        const soundDamping =  currentProject && currentProject.foreCast ? currentProject.foreCast.soundDamping : 1;
        return (
            <Container className={"home-search-container"}>
                <Section>
                    <ScrollView>
                        <Container className="list-does">
                            <FormControlLabel
                                label={"Độ giảm âm"}
                                control={
                                    <Input
                                        type={"number"}
                                        value={soundDamping}
                                        onChange={(event) =>
                                            this.homeStore.changeGernalInfo(
                                                "soundDamping",
                                                event,
                                                currentProject.id
                                            )
                                        }
                                    />
                                }
                            />
                            <Row className={"group-btn-actions"}>
                                <Button
                                    className={"btn btn-default"}
                                    icon={"plus"}
                                    text={"Thêm"}
                                    onClick={() => {
                                        this.handleAddData();
                                    }}
                                ></Button>
                                <Spacer />
                                <Button
                                    className={"btn btn-default "}
                                    icon={"trash"}
                                    text={"Xóa"}
                                    onClick={() => {
                                        this.handleRemoveData();
                                    }}
                                ></Button>
                            </Row>
                            <Row className="list-does-content">
                                <Container>
                                    <Table
                                        className={"result-table"}
                                        isFixedHeader
                                        headers={[
                                            { label: `Ls(${currentProject.unit})`, width: 40 },
                                            { label: "Q", width: 40 },
                                            { label: "ω", width: 30 },
                                            { label: "λ", width: 30 },
                                            { label: "x (m)", width: 30 },
                                            { label: "y (m)", width: 30 },
                                        ]}
                                    >
                                        {currentData.map(
                                            (row, i) => (
                                                <TableRow
                                                    key={i}
                                                    onClick={() =>
                                                        this.handleDataSelected(i)
                                                    }
                                                    isSelected={
                                                        currentProject.selectedForeCastDataIndex ===
                                                        i
                                                            ? true
                                                            : false
                                                    }
                                                >
                                                    <TableRowCell>
                                                        <Input
                                                            type={"number"}
                                                            step={1}
                                                            value={row.ls}
                                                            min={0}
                                                            color={"var(--text-color)"}
                                                            onChange={(event) => {
                                                                this.handleChangeDataValue(
                                                                    i,
                                                                    "ls",
                                                                    parseFloat(event)
                                                                );
                                                            }}
                                                        />
                                                    </TableRowCell>
                                                    <TableRowCell>
                                                        <AdvanceSelect
                                                            options={[
                                                                {
                                                                    id: 1,
                                                                    label: "1",
                                                                },
                                                                {
                                                                    id: 2,
                                                                    label: "2",
                                                                },
                                                                {
                                                                    id: 4,
                                                                    label: "4",
                                                                },
                                                                {
                                                                    id: 8,
                                                                    label: "8",
                                                                }
                                                            ]}
                                                            onChange={(event) => {
                                                                this.handleChangeDataValue(
                                                                    i,
                                                                    "q",
                                                                    event
                                                                );
                                                            }}
                                                            value={row.q}
                                                        />
                                                    </TableRowCell>
                                                    <TableRowCell>
                                                        <Input
                                                            type={"number"}
                                                            step={45}
                                                            value={row.omega}
                                                            min={0}
                                                            max={360}
                                                            color={"var(--text-color)"}
                                                            onChange={(event) => {
                                                                this.handleChangeDataValue(
                                                                    i,
                                                                    "omega",
                                                                    parseFloat(event)
                                                                );
                                                            }}
                                                        />
                                                    </TableRowCell>
                                                    <TableRowCell>
                                                        <Input
                                                            type={"number"}
                                                            step={0.01}
                                                            value={row.lamda}
                                                            min={0.01}
                                                            color={"var(--text-color)"}
                                                            onChange={(event) => {
                                                                this.handleChangeDataValue(
                                                                    i,
                                                                    "lamda",
                                                                    parseFloat(event)
                                                                );
                                                            }}
                                                        />
                                                    </TableRowCell>
                                                    <TableRowCell>
                                                        <Input
                                                            type={"number"}
                                                            step={0.01}
                                                            value={row.x}
                                                            min={0}
                                                            color={"var(--text-color)"}
                                                            onChange={(event) => {
                                                                this.handleChangeDataValue(
                                                                    i,
                                                                    "x",
                                                                    parseFloat(event)
                                                                );
                                                            }}
                                                        />
                                                    </TableRowCell>
                                                    <TableRowCell>
                                                        <Input
                                                            type={"number"}
                                                            step={0.01}
                                                            value={row.y}
                                                            min={0}
                                                            color={"var(--text-color)"}
                                                            onChange={(event) => {
                                                                this.handleChangeDataValue(
                                                                    i,
                                                                    "y",
                                                                    parseFloat(event)
                                                                );
                                                            }}
                                                        />
                                                    </TableRowCell>
                                                </TableRow>
                                            )
                                        )}
                                    </Table>
                                    <Spacer />
                                </Container>
                            </Row>
                        </Container>

                        <Container className="list-does-total list-does">
                            <Row>
                                <Container>
                                    <Table
                                        className={"result-table"}
                                        isFixedHeader
                                        headers={[
                                            { label: "STT", width: 25 },
                                            { label: "x (m)", width: 50 },
                                            { label: "y (m)", width: 50 },
                                            { label: "Δ(k)", width: 30 },
                                            { label: "Ltd", width: 70 }
                                        ]}
                                    >
                                        {currentPoint.map((row, i) => (
                                            <TableRow
                                                key={row.id}
                                                onClick={() =>
                                                    this.handlePointSelected(i)
                                                }
                                                isSelected={
                                                    currentProject.selectedForeCastPointIndex ===
                                                    i
                                                        ? true
                                                        : false
                                                }
                                            >
                                                <TableRowCell>
                                                    <TB1>{i + 1}</TB1>
                                                </TableRowCell>
                                                <TableRowCell>
                                                    <Input
                                                        type={"number"}
                                                        step={0.01}
                                                        value={row.x}
                                                        min={0}
                                                        color={"var(--text-color)"}
                                                        onChange={(event) => {
                                                            this.handleChangePointValue(
                                                                i,
                                                                "x",
                                                                parseFloat(event)
                                                            );
                                                        }}
                                                    />
                                                </TableRowCell>
                                                <TableRowCell>
                                                    <Input
                                                        type={"number"}
                                                        step={1}
                                                        value={row.y}
                                                        min={0}
                                                        color={"var(--text-color)"}
                                                        onChange={(event) => {
                                                            this.handleChangePointValue(
                                                                i,
                                                                "y",
                                                                parseFloat(event)
                                                            );
                                                        }}
                                                    />
                                                </TableRowCell>
                                                <TableRowCell>
                                                    <TB1>{row.deltaK}</TB1>
                                                </TableRowCell>
                                                <TableRowCell>
                                                    <TB1>{row.ltd}</TB1>
                                                </TableRowCell>
                                            </TableRow>
                                        ))}
                                    </Table>
                                    <Spacer />
                                </Container>
                            </Row>
                            <Row className={"group-btn-actions"}>
                                <Button
                                    className={"btn btn-default"}
                                    icon={"plus"}
                                    text={"Thêm"}
                                    onClick={() => {
                                        this.handleAddPoint();
                                    }}
                                ></Button>
                                <Spacer />
                                <Button
                                    className={"btn btn-default "}
                                    icon={"trash"}
                                    text={"Xóa"}
                                    onClick={() => {
                                        this.handleRemovePoint();
                                    }}
                                ></Button>
                                <Spacer />
                                <Button
                                    icon={"download"}
                                    text={"Tải"}
                                    className={"btn btn-default"}
                                    onClick={() => {
                                        this.exportData(currentProject);
                                    }}
                                ></Button>
                                <Spacer />
                                <Button
                                    icon={"download"}
                                    text={"Tải dự đoán"}
                                    className={"btn btn-default"}
                                    onClick={() => {
                                        this.exportDataForeCast(currentProject);
                                    }}
                                ></Button>
                            </Row>
                        </Container>
                    </ScrollView>
                </Section>
            </Container>
        );
    }
}

export default inject("appStore")(observer(Does));
