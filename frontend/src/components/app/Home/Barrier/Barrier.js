import "./Barrier.scss";
import React, { Component } from "react";
import { Container } from "components/bases/Container/Container";
import { Row } from "components/bases/Row/Row";
import { Spacer } from "components/bases/spacer/Spacer";
import { TB1 } from "components/bases/Text/Text";
import { inject, observer } from "mobx-react";
import { Button, EmptyButton } from "components/bases/Button/Button";

import { Input, InputGroup, Section, InputAppend } from "components/bases/Form";
import { FlexPanel, BorderPanel } from "components/bases/Panel/Panel";
import { Table, TableRowCell, TableRow } from "components/bases/Table/Table";
import { Image } from "components/bases/Image/Image";

class Barrier extends Component {
    constructor(props) {
        super(props);

        this.inputRef = React.createRef();
    }
    homeStore = this.props.appStore.homeStore;

    handleAdd() {
        this.homeStore.addBarrier(this.homeStore.selectedProjectId);
    }

    handleRemove() {
        this.homeStore.removeBarrier(this.homeStore.selectedProjectId);
    }

    handleSelected(index) {
        this.homeStore.selectBarrier(this.homeStore.selectedProjectId, index);
    }

    handleChangeValue(index, key, value) {
        this.homeStore.changeValueBarrier(
            this.homeStore.selectedProjectId,
            index,
            key,
            value
        );
    }
    handleChangeImage(index, key) {
        if (
            this.inputRef.current.ref.current.files &&
            this.inputRef.current.ref.current.files[0]
        ) {
            const reader = new FileReader();
            reader.onload = (e) => {
                this.homeStore.changeValueBarrier(
                    this.homeStore.selectedProjectId,
                    -1,
                    key,
                    e.target.result
                );
            };
            reader.readAsDataURL(this.inputRef.current.ref.current.files[0]);
        }
    }
    render() {
        const currentProject =
            this.homeStore.projects.find(
                (p) => p.id === this.homeStore.selectedProjectId
            ) || {};
        return (
            <Section header={"Quản lý vật cản"}>
                <FlexPanel className="list-barrier-panel">
                    <Container className="list-barrier">
                        <Table
                            className={"result-table"}
                            isFixedHeader
                            headers={[
                                { label: "STT", width: 15 },
                                { label: "Data", width: 30 },
                            ]}
                        >
                            {(currentProject.barriers || []).map((row, i) => (
                                <TableRow
                                    key={i}
                                    onClick={() => this.handleSelected(i)}
                                    isSelected={
                                        currentProject.selectedBarriersIndex ===
                                        i
                                            ? true
                                            : false
                                    }
                                >
                                    <TableRowCell>
                                        <TB1>{i + 1}</TB1>
                                    </TableRowCell>
                                    <TableRowCell>
                                        <InputGroup>
                                            X:
                                            <Input
                                                type={"number"}
                                                step={1}
                                                value={row.x}
                                                min={0}
                                                color={"var(--text-color)"}
                                                onChange={(event) => {
                                                    this.handleChangeValue(
                                                        i,
                                                        "x",
                                                        parseFloat(event)
                                                    );
                                                }}
                                            />
                                        </InputGroup>
                                        <InputGroup>
                                            Y:
                                            <Input
                                                type={"number"}
                                                step={1}
                                                value={row.y}
                                                min={0}
                                                color={"var(--text-color)"}
                                                onChange={(event) => {
                                                    this.handleChangeValue(
                                                        i,
                                                        "y",
                                                        parseFloat(event)
                                                    );
                                                }}
                                            />
                                        </InputGroup>
                                        <InputGroup>
                                            Dài:
                                            <Input
                                                type={"number"}
                                                step={1}
                                                value={row.width}
                                                min={0}
                                                color={"var(--text-color)"}
                                                onChange={(event) => {
                                                    this.handleChangeValue(
                                                        i,
                                                        "width",
                                                        parseFloat(event)
                                                    );
                                                }}
                                            />
                                        </InputGroup>
                                        <InputGroup>
                                            Rộng:
                                            <Input
                                                type={"number"}
                                                step={1}
                                                value={row.height}
                                                min={0}
                                                color={"var(--text-color)"}
                                                onChange={(event) => {
                                                    this.handleChangeValue(
                                                        i,
                                                        "height",
                                                        parseFloat(event)
                                                    );
                                                }}
                                            />
                                        </InputGroup>
                                        <InputGroup>
                                            Cao:
                                            <Input
                                                type={"number"}
                                                step={1}
                                                value={row.high}
                                                min={0}
                                                color={"var(--text-color)"}
                                                onChange={(event) => {
                                                    this.handleChangeValue(
                                                        i,
                                                        "high",
                                                        parseFloat(event)
                                                    );
                                                }}
                                            />
                                        </InputGroup>
                                        <InputGroup>
                                            Ảnh:
                                            <InputAppend>
                                                <EmptyButton
                                                    icon={"upload"}
                                                    onlyIcon
                                                    onClick={() => {
                                                        const input = this
                                                            .inputRef;
                                                        if (
                                                            input &&
                                                            document.createEvent
                                                        ) {
                                                            const evt = document.createEvent(
                                                                "MouseEvents"
                                                            );
                                                            evt.initEvent(
                                                                "click",
                                                                true,
                                                                false
                                                            );
                                                            input.current.ref.current.dispatchEvent(
                                                                evt
                                                            );
                                                        }
                                                    }}
                                                />
                                            </InputAppend>
                                            <Input
                                                ref={this.inputRef}
                                                className={"hide"}
                                                type={"file"}
                                                accept={"image/png, image/jpeg"}
                                                onChange={() => {
                                                    this.handleChangeImage(
                                                        i,
                                                        "image"
                                                    );
                                                }}
                                            />
                                            <Image
                                                alt={"image"}
                                                src={row.image}
                                                width={"60px"}
                                                height={"20px"}
                                            />
                                        </InputGroup>
                                    </TableRowCell>
                                </TableRow>
                            ))}
                        </Table>
                    </Container>
                    <Row className={"group-btn-barrier"}>
                        <Button
                            icon={"plus"}
                            text={'Thêm'}
                            className={"btn-barrier"}
                            onClick={() => {
                                this.handleAdd();
                            }}
                        ></Button>
                        <Button
                            icon={"trash"}
                            text={'Xóa'}
                            className={"btn-barrier"}
                            onClick={() => {
                                this.handleRemove();
                            }}
                        ></Button>
                    </Row>
                </FlexPanel>
            </Section>
        );
    }
}
Barrier = inject("appStore")(observer(Barrier));
export default Barrier;
