import "./BackgroundManagement.scss";
import { inject, observer } from "mobx-react";
import React, { Component } from "react";
import { Container } from "components/bases/Container/Container";
import { Row } from "components/bases/Row/Row";
import { Spacer } from "components/bases/spacer/Spacer";
import { TB1 } from "components/bases/Text/Text";
import {
    Radio,
    Input,
    Section,
    Button,
    InputGroup,
    InputAppend,
} from "components/bases/Form";
import { FlexPanel } from "components/bases/Panel/Panel";
// import ProfileImage from "images/profile.png";
import { Table, TableRowCell, TableRow } from "components/bases/Table/Table";
import Barrier from "components/app/Home/Barrier/Barrier";
import Area from "../Area/Area";
import PopupChooseColor from "../Action/PopupChooseColor";
import { Image } from "components/bases/Image/Image";
import { ScrollView } from "components/bases/ScrollView/ScrollView";
import Employee from "../Employee/Employee";

import imageData from 'data/images.json';

class BackgroundManagement extends Component {
    constructor(props) {
        super(props);

        this.inputRef = React.createRef();
    }
    handleOpenImage = () => {
        if (
            this.inputRef.current.ref.current.files &&
            this.inputRef.current.ref.current.files[0]
        ) {
            const reader = new FileReader();
            reader.onload = (e) => {
                this.homeStore.uploadBackground(e.target.result);
            };
            reader.readAsDataURL(this.inputRef.current.ref.current.files[0]);
        }
    };
    homeStore = this.props.appStore.homeStore;
    render() {
        const currentProject =
            this.homeStore.projects.find(
                (p) => p.id === this.homeStore.selectedProjectId
            ) || {};
        return (
            <Container className={"home-search-container"}>
                <Section header={"Quản lý nền"}>
                    <Row className="group-background">
                        <InputGroup className={"btn-data"}>
                            <InputAppend>
                                <Button
                                    icon={"upload"}
                                    onlyIcon
                                    onClick={() => {
                                        const input = this.inputRef;
                                        if (input && document.createEvent) {
                                            const evt = document.createEvent(
                                                "MouseEvents"
                                            );
                                            evt.initEvent("click", true, false);
                                            input.current.ref.current.dispatchEvent(
                                                evt
                                            );
                                        }
                                    }}
                                />
                            </InputAppend>
                            <Input
                                ref={this.inputRef}
                                className={"hide"}
                                type={"file"}
                                accept={"image/png, image/jpeg"}
                                onChange={() => {
                                    this.handleOpenImage();
                                }}
                            />
                            <TB1>Tải ảnh</TB1>
                        </InputGroup>
                        <Spacer />
                        <Radio
                            label="Hiển thị nền"
                            checked={
                                currentProject.background &&
                                currentProject.background.enable
                            }
                            className={"radio-color"}
                            onChange={(event) => {
                                this.homeStore.enableBackground(
                                    event,
                                    currentProject.id
                                );
                            }}
                        />
                    </Row>
                    <Image
                        alt={"bg-tmp"}
                        src={
                            (currentProject.background &&
                                currentProject.background.image) ||
                                imageData.profile
                        }
                        height={"30vh"}
                        className={"bg-temp"}
                    />
                </Section>
                <Row className={"row-bg"}>
                    <Section header={"Quản lý text"}>
                        <FlexPanel className="list-text-panel">
                            <Container className="list-text-background">
                                <Table
                                    className={"table-text"}
                                    isFixedHeader
                                    headers={[
                                        { label: "STT", width: 5 },
                                        { label: "data", width: 15 },
                                    ]}
                                >
                                    {Array.isArray(currentProject.texts) &&
                                        currentProject.texts.map((row, i) => (
                                            <TableRow
                                                key={i}
                                                className={"table-text"}
                                                isSelected={
                                                    currentProject.selectedTextIndex ===
                                                    i
                                                        ? true
                                                        : false
                                                }
                                                onClick={() => {
                                                    this.homeStore.selectText(
                                                        this.homeStore
                                                            .selectedProjectId,
                                                        i
                                                    );
                                                }}
                                            >
                                                <TableRowCell>
                                                    <TB1>{i + 1}</TB1>
                                                </TableRowCell>
                                                <TableRowCell>
                                                    <InputGroup>
                                                        X:
                                                        <Input
                                                            value={row.x}
                                                            color={
                                                                "var(--text-color)"
                                                            }
                                                            type={"number"}
                                                            step={1}
                                                            min={0}
                                                            onChange={(
                                                                event
                                                            ) => {
                                                                this.homeStore.changeText(
                                                                    this
                                                                        .homeStore
                                                                        .selectedProjectId,
                                                                    i,
                                                                    "x",
                                                                    parseFloat(
                                                                        event
                                                                    )
                                                                );
                                                            }}
                                                        />
                                                    </InputGroup>
                                                    <InputGroup>
                                                        Y:
                                                        <Input
                                                            value={row.y}
                                                            color={
                                                                "var(--text-color)"
                                                            }
                                                            type={"number"}
                                                            step={1}
                                                            min={0}
                                                            onChange={(
                                                                event
                                                            ) => {
                                                                this.homeStore.changeText(
                                                                    this
                                                                        .homeStore
                                                                        .selectedProjectId,
                                                                    i,
                                                                    "y",
                                                                    parseFloat(
                                                                        event
                                                                    )
                                                                );
                                                            }}
                                                        />
                                                    </InputGroup>
                                                    <InputGroup>
                                                        Màu sắc:
                                                        <Button
                                                            value={row.color}
                                                            text={""}
                                                            backgroundColor={
                                                                row.color
                                                            }
                                                            onClick={() => {
                                                                this.homeStore.setPopupColor(
                                                                    true,
                                                                    i
                                                                );
                                                            }}
                                                        />
                                                    </InputGroup>
                                                    <InputGroup>
                                                        Văn bản:
                                                        <Input
                                                            value={row.text}
                                                            color={
                                                                "var(--text-color)"
                                                            }
                                                            onChange={(
                                                                event
                                                            ) => {
                                                                this.homeStore.changeText(
                                                                    this
                                                                        .homeStore
                                                                        .selectedProjectId,
                                                                    i,
                                                                    "text",
                                                                    event
                                                                );
                                                            }}
                                                        />
                                                    </InputGroup>
                                                    <InputGroup>
                                                        Kích thước chữ:
                                                        <Input
                                                            value={row.fontSize}
                                                            color={
                                                                "var(--text-color)"
                                                            }
                                                            onChange={(
                                                                event
                                                            ) => {
                                                                this.homeStore.changeText(
                                                                    this
                                                                        .homeStore
                                                                        .selectedProjectId,
                                                                    i,
                                                                    "fontSize",
                                                                    event
                                                                );
                                                            }}
                                                        />
                                                    </InputGroup>
                                                </TableRowCell>
                                            </TableRow>
                                        ))}
                                </Table>
                            </Container>
                            <Row className="group-btn-background">
                                <Button
                                    icon={"plus"}
                                    text={"Thêm"}
                                    className={"btn-background"}
                                    onClick={() => {
                                        this.homeStore.addText(
                                            currentProject.id
                                        );
                                    }}
                                ></Button>
                                <Button
                                    icon={"trash"}
                                    text={"Xóa"}
                                    className={"btn-background"}
                                    onClick={() => {
                                        this.homeStore.removeText(
                                            currentProject.id
                                        );
                                    }}
                                ></Button>
                            </Row>
                        </FlexPanel>
                    </Section>
                    <Barrier />
                </Row>
                <Container>
                    <Area />
                </Container>
                <ScrollView>
                    <Employee />
                </ScrollView>
                {this.homeStore.isPopUpColor && <PopupChooseColor />}
            </Container>
        );
    }
}
export default inject("appStore")(observer(BackgroundManagement));
