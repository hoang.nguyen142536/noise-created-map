import React, { Component } from "react";
import { Tab, Tabs } from "components/bases/Tabs/Tabs";
import { BorderPanel, PanelBody } from "components/bases/Panel/Panel";
import { Container } from "components/bases/Container/Container";
import GeneralInfo from "components/app/Home/GeneralInfo/GeneralInfo";
import Color from "components/app/Home/Color/Color";
import Check from "components/app/Home/Check/Check";
import Data from "components/app/Home/Data/Data";
import Does from "components/app/Home/Does/Does";
import BackgroundManagement from "components/app/Home/BackgroundManagement/BackgroundManagement";
import { inject, observer } from "mobx-react";
import { ScrollView } from "components/bases/ScrollView/ScrollView";
import ForeCast from "./ForeCast/ForeCast";

class HomeSearch extends Component {
    homeStore = this.props.appStore.homeStore;
    render() {
        return (
            <BorderPanel width={"30%"}>
                <Container className={"content-homSearch"}>
                    <PanelBody style={{ overflow: "initial" }}>
                        <Tabs
                            selected={this.homeStore.tabSelected}
                            onSelect={(tabSelected) =>
                                this.homeStore.setTab(tabSelected)
                            }
                        >
                            <Tab id="general-info" title="Thông tin chung">
                                <ScrollView>
                                    <GeneralInfo />
                                </ScrollView>
                            </Tab>
                            <Tab id="background-management" title="Quản lý nền">
                                <ScrollView>
                                    <BackgroundManagement />
                                </ScrollView>
                            </Tab>
                            <Tab id="data" title="Dữ liệu">
                                <ScrollView>
                                    <Data />
                                </ScrollView>
                            </Tab>
                            <Tab id="color" title="Màu sắc">
                                <ScrollView>
                                    <Color />
                                </ScrollView>
                            </Tab>
                            <Tab id="check" title="Kiểm soát">
                                <ScrollView>
                                    <Check />
                                </ScrollView>
                            </Tab>
                            <Tab id="forecast" title="Dự đoán">
                                <ScrollView>
                                    <ForeCast />
                                </ScrollView>
                            </Tab>
                            <Tab id="does" title="Noise dose">
                                <ScrollView>
                                    <Does />
                                </ScrollView>
                            </Tab>
                        </Tabs>
                    </PanelBody>
                </Container>
            </BorderPanel>
        );
    }
}
HomeSearch = inject("appStore")(observer(HomeSearch));
export default HomeSearch;
