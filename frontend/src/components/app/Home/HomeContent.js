import React, { Component } from "react";
import {
    BorderPanel
} from "components/bases/Panel/Panel";
import {
    Button
} from "components/bases/Form";
import { inject, observer } from "mobx-react";
import { Container} from "components/bases/Container/Container";
import BusinessService from "services/business.service";
import { toast } from "components/bases/Modal/Modal";
import { Row } from "components/bases/Row/Row";
import { Spacer } from "components/bases/spacer/Spacer";
import { ProgressBar } from "components/bases/ProgressBar/ProgressBar";
import { TB1 } from "components/bases/Text/Text";
import { CommonHelper } from "helper/common.helper";

import imageData from 'data/images.json';

const CALCULATE_AREA_WIDTH = 1;
const CALCULATE_AREA_OFFSET = 0;

const employeeImages = {
    // '1': require('../../../assets/images/nguoi-tren.png'),
    // '2': require('../../../assets/images/nguoi-phai.png'),
    // '3': require('../../../assets/images/nguoi-duoi.png'),
    // '4': require('../../../assets/images/nguoi-trai.png')
    '1': imageData.top,
    '2': imageData.right,
    '3': imageData.bottom,
    '4': imageData.left
};
const EMPLOYEE_WIDTH = 0.4;
const EMPLOYEE_HEIGHT = 0.4;

class HomeContent extends Component {

    homeStore = this.props.appStore.homeStore;
    businessService = new BusinessService();

    constructor(props) {
        super(props);

        this.contentRef = React.createRef();

        this.ratio = (function () {
            const ctx = document.createElement("canvas").getContext("2d"),
                dpr = window.devicePixelRatio || 1,
                bsr = ctx.webkitBackingStorePixelRatio ||
                      ctx.mozBackingStorePixelRatio ||
                      ctx.msBackingStorePixelRatio ||
                      ctx.oBackingStorePixelRatio ||
                      ctx.backingStorePixelRatio || 1;
        
            return dpr / bsr;
        })();
    }

    paint(drawContent)
    {
        setTimeout(async () => {
            const canvas = this.contentRef.current;
            const context = canvas.getContext('2d');

            const ratioData = this.calculateMapRatio(canvas, drawContent.width, drawContent.height);
            this.createHiDPICanvas(canvas, ratioData.width, ratioData.height);

            this.clearCanvas(context, ratioData);
            await this.drawBackground(context, drawContent, ratioData);
            await this.drawData(context, drawContent);
            await this.drawBarrier(context, drawContent, ratioData);
            await this.drawEmployee(context, drawContent, ratioData);
            this.drawDataPoint(context, drawContent.contents, ratioData);
            this.drawAreas(context, drawContent, ratioData);
            this.drawText(context, drawContent, ratioData);
            this.homeStore.setDrawMapProgessInfo({
                text: 'Hoàn tất',
                value: this.homeStore.drawMapProgessInfo.total
            });
        }, 1);
    }

    createHiDPICanvas = (canvas, x, y) => {
        const width = x || canvas.width;
        const height = y || canvas.height;
        canvas.width = width * this.ratio;
        canvas.height = height * this.ratio;
        canvas.style.width = width + "px";
        canvas.style.height = height + "px";
        canvas.getContext("2d").setTransform(this.ratio, 0, 0, this.ratio, 0, 0);
    }

    clearCanvas(context, ratioData)
    {
        context.clearRect(0, 0, ratioData.width, ratioData.height);
        context.fillStyle = "#FFFFFF";
        context.fillRect(0, 0, ratioData.width, ratioData.height);
    }

    calculateMapRatio(canvas, width, height)
    {
        const { clientWidth } = canvas;
        return {
            ratio: width / clientWidth,
            width: clientWidth,
            height: height / (width / clientWidth)
        }
    }

    async drawBackground(context, project, ratioData)
    {
        return new Promise((resolve) => {
            if (!project.background || !project.background.image || !project.background.enable)
            {
                resolve();
                return;
            }

            const image = new Image();
            image.crossOrigin = 'anonymous';
            image.onload = () => {
                // context.globalAlpha = project.background.opacity || 0.2;
                context.globalAlpha = 1;
                context.drawImage(image, 0, 0, ratioData.width, ratioData.height);
                context.globalAlpha = 1;
                resolve();
            };
            image.onerror = (err) => {
                resolve();
            };
            image.src = project.background.image;
        })
    }

    drawAreas(context, project, ratioData)
    {
        if (Array.isArray(project.areas) && project.areas.length > 0)
        {
            project.areas.forEach((d) => {
                context.strokeStyle = d.color;
                context.lineWidth = d.think || 1;

                if (d.type === 'circle')
                {
                    context.beginPath();
                    context.arc(d.x / ratioData.ratio, d.y / ratioData.ratio, d.radius / ratioData.ratio, 0, 2 * Math.PI);
                    context.closePath();
                    context.stroke();
                }
                else if (d.type === 'rectangle')
                {
                    context.beginPath();
                    context.rect(d.x / ratioData.ratio, d.y / ratioData.ratio, d.width / ratioData.ratio, d.height / ratioData.ratio);
                    context.closePath();
                    context.stroke();
                }
                else if (d.type === 'polygon' && Array.isArray(d.data) && d.data.length > 1)
                {
                    context.beginPath();
                    for(let i = 0; i < d.data.length; i++)
                    {
                        const point = d.data[i];
                        if (i === 0)
                        {
                            context.moveTo(point.x / ratioData.ratio, point.y / ratioData.ratio);
                        }
                        else
                        {
                            context.lineTo(point.x / ratioData.ratio, point.y / ratioData.ratio);
                        }
                    }
                    context.closePath();
                    context.stroke();
                }
            });
        }
    }

    async drawBarrier(context, project, ratioData)
    {
        if (Array.isArray(project.barriers))
        {
            for(let i = 0; i < project.barriers.length; i++)
            {
                const b = project.barriers[i];
                await new Promise((resolve) => {
                    if (!b.image)
                    {
                        resolve();
                        return;
                    }
                
                    const image = new Image();
                    image.crossOrigin = 'anonymous';
                    image.onload = () => {
                        // context.globalAlpha = 0.8;
                        context.drawImage(image, b.x / ratioData.ratio, b.y / ratioData.ratio, b.width / ratioData.ratio, b.height / ratioData.ratio);
                        // context.globalAlpha = 1;
                        resolve();
                    };
                    image.onerror = (err) => {

                        resolve();
                    };
                    image.src = b.image;
                });
            }
        }
    }

    async drawEmployee(context, project, ratioData)
    {
        if (project.isShowEmployee && Array.isArray(project.employeeses))
        {
            for(let i = 0; i < project.employeeses.length; i++)
            {
                const b = project.employeeses[i];
                await new Promise((resolve) => {
                    if (!b.image)
                    {
                        resolve();
                        return;
                    }
                
                    const image = new Image();
                    image.crossOrigin = 'anonymous';
                    image.onload = () => {
                        // context.globalAlpha = 0.8;
                        context.drawImage(image, b.x / ratioData.ratio - ((EMPLOYEE_WIDTH / 2) / ratioData.ratio), b.y / ratioData.ratio - ((EMPLOYEE_HEIGHT / 2) / ratioData.ratio), EMPLOYEE_WIDTH / ratioData.ratio, EMPLOYEE_HEIGHT / ratioData.ratio);
                        // context.globalAlpha = 1;
                        resolve();
                    };
                    image.onerror = (err) => {

                        resolve();
                    };
                    image.src = employeeImages[b.image];
                });
            }
        }
    }

    drawText(context, project, ratioData)
    {
        if (Array.isArray(project.texts))
        {
            project.texts.forEach((t) => {
                context.font = `${t.fontSize} Arial`;
                context.fillStyle = t.color;
                context.fillText(t.text, t.x / ratioData.ratio, t.y / ratioData.ratio);
            });
        }
    }

    async drawData(context, drawContent)
    {
        const total = parseInt(drawContent.ratioData.width) * parseInt(drawContent.ratioData.height);
        const setProgessBar = async (current) =>
        {
            this.homeStore.setDrawMapProgessInfo({
                total,
                value: current,
                text: 'Đang vẽ bản đồ...'
            })
            await CommonHelper.sleep(0.01);
        }
        const offset = CALCULATE_AREA_WIDTH > 1 ? 1 : 0;
        let current = 0;

        const expectAlpha = CALCULATE_AREA_WIDTH > 1 ? 0.7 : 0.25;
        context.globalAlpha = drawContent.background && drawContent.background.enable ? expectAlpha : 1;
        const xKeys = Object.keys(drawContent.findData).filter((k) => drawContent.findData.hasOwnProperty(k));
        if (xKeys.length > 0)
        {
            for(let i = 0; i < xKeys.length; i++)
            {
                await setProgessBar(current);

                const row = drawContent.findData[xKeys[i]];
                const yKeys = Object.keys(row).filter((k) => row.hasOwnProperty(k));
                current += parseInt(drawContent.ratioData.height);
                if (yKeys.length === 0) continue;
                if (CALCULATE_AREA_WIDTH > 1)
                {
                    for(let j = 0; j < yKeys.length; j++)
                    {
                        const data = row[yKeys[j]];
    
                        context.fillStyle = data.color;
                        context.strokeStyle = data.color;
                        context.lineWidth = 2;
                        context.fillRect(xKeys[i] - CALCULATE_AREA_OFFSET, yKeys[j] - CALCULATE_AREA_OFFSET, CALCULATE_AREA_WIDTH - offset, CALCULATE_AREA_WIDTH - offset);
                        context.strokeRect(xKeys[i] - CALCULATE_AREA_OFFSET, yKeys[j] - CALCULATE_AREA_OFFSET, CALCULATE_AREA_WIDTH - offset, CALCULATE_AREA_WIDTH - offset);
                    }
                }
                else
                {
                    for(let j = 0; j < yKeys.length; j++)
                    {
                        const data = row[yKeys[j]];

                        const fromTo = yKeys[j].split('-').map((l) => parseInt(l));

                        for(let f = fromTo[0]; f <= fromTo[1]; f++)
                        {
                            context.fillStyle = data.color;
                            context.strokeStyle = data.color;
                            context.lineWidth = 2;
                            context.fillRect(xKeys[i] - CALCULATE_AREA_OFFSET, f - CALCULATE_AREA_OFFSET, CALCULATE_AREA_WIDTH - offset, CALCULATE_AREA_WIDTH - offset);
                            context.strokeRect(xKeys[i] - CALCULATE_AREA_OFFSET, f - CALCULATE_AREA_OFFSET, CALCULATE_AREA_WIDTH - offset, CALCULATE_AREA_WIDTH - offset);
                        }
                    }
                }
            }
        }
        context.globalAlpha = 1;
    }

    drawDataPoint(context, contents, ratioData)
    {
        if (Array.isArray(contents))
        {
            contents.filter((d) => d.ls).forEach((d) => {

                const point = {x : d.x / ratioData.ratio, y: d.y / ratioData.ratio};

                // context.fillStyle = '#336633';
                context.fillStyle = 'black';
                context.beginPath();
                context.arc(point.x, point.y, 1, 0, 2 * Math.PI);
                context.fill();
    
                // context.fillStyle = 'black';
                // context.beginPath();
                // context.arc(point.x, point.y, 3, 0, 2 * Math.PI);
                // context.stroke();
    
                context.fillStyle = 'black';
                context.font = `10px Arial`;
                context.fillText((d.ls || '').toString(), point.x - 7, point.y - 7);
            });
            // context.globalAlpha = 1;
        }
    }

    calculateItensity(x , y)
    {
        if (this.homeStore.drawContent && this.homeStore.drawContent.findData)
        {
            const currentX = parseInt(x / CALCULATE_AREA_WIDTH) * CALCULATE_AREA_WIDTH + 1;
            const currentY = parseInt(y / CALCULATE_AREA_WIDTH) * CALCULATE_AREA_WIDTH + 1;
            const xData = this.homeStore.drawContent.findData[currentX];
            if (!xData)
            {
                return '';
            }

            const xKeys = Object.keys(xData).filter((k) => xData.hasOwnProperty(k));
            for(let i = 0; i < xKeys.length; i++)
            {
                const fromTo = xKeys[i].split('-').map((l) => parseInt(l));
                if (fromTo[0] <= y && y <= fromTo[1])
                {
                    return xData[xKeys[i]].data.toString();
                }
            }
        }
        return '';
    }

    getMockData(witdth, height, maxItensity, minItensity)
    {
        function getRandomInt(max, min = 0) {
            return Math.floor(Math.random() * (max - min)) + min;
        }
        const data = [];
        const count = getRandomInt(200, 100);
        for(let i = 0; i < count; i++)
        {
            data.push({
                x: getRandomInt(witdth),
                y: getRandomInt(height),
                itensity: getRandomInt(maxItensity, minItensity),
                radius: getRandomInt(200)
            });
        }
        return data;
    }

    handleMouseMove = (event) =>
    {
        if (this.homeStore.drawContent)
        {
            const pos = this.getMousePos(this.contentRef.current, event);
            this.homeStore.setMapToolTip({ isShow: true, ratio: this.homeStore.drawContent.ratioData.ratio, x: pos.x, y: pos.y, text: this.calculateItensity(parseInt(pos.x), parseInt(pos.y)) });
        }
    }

    handleMouseOut = () =>
    {
        this.homeStore.setMapToolTip({ isShow: false });
    }

    getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
          x: evt.clientX - rect.left,
          y: evt.clientY - rect.top
        };
    }

    handleDraw = async () => {
        await new Promise(async (resolve) => {

            const drawData = [];
            const findData = {};
            const project = this.homeStore.projects.find((p) => p.id === this.homeStore.selectedProjectId);
            if (project)
            {
                const ratioData = this.calculateMapRatio(this.contentRef.current, project.width, project.height);
                const width = parseInt(ratioData.width);
                const height = parseInt(ratioData.height) + 1;
                const totalProgess = parseInt(ratioData.width) * parseInt(ratioData.height);
                for(let x = 0; x < width; x += CALCULATE_AREA_WIDTH)
                {
                    const currentX = x + CALCULATE_AREA_OFFSET;
                    const curentProgess = x * parseInt(ratioData.height);
                    this.homeStore.setDrawMapProgessInfo({
                        total: totalProgess,
                        value: curentProgess,
                        text: 'Đang tính toán...'
                    });

                    await CommonHelper.sleep(0.01);

                    if (!findData[currentX])
                    {
                        findData[currentX] = {};
                    }

                    if (CALCULATE_AREA_WIDTH > 1)
                    {
                        for(let y = 0; y < height; y += CALCULATE_AREA_WIDTH)
                        {
                            const currentY = y + CALCULATE_AREA_OFFSET;
    
                            let tempData = this.calculateData(currentX, currentY, project.expectedExpression, project, ratioData);
                            let data = tempData;
                            if (data) {
                                data = Math.round(data * 10) / 10;
                            }
    
                            if (tempData)
                            {
                                tempData = Math.round(data * 100) / 100;
                            }
                            const color = (project.colorRanges || []).find((c) => c.from <= data && c.to >= data) || { color: 'black' };

                            findData[currentX][currentY] = {
                                data,
                                color: color.color
                            };
                        }
                    }
                    else
                    {
                        let currentData = -1;
                        let currentColor = {};
                        let fromYData = 0;
                        let toYData = 0;
                        
                        for(let y = 0; y < height; y += CALCULATE_AREA_WIDTH)
                        {
                            const currentY = y + CALCULATE_AREA_OFFSET;
    
                            let tempData = this.calculateData(currentX, currentY, project.expectedExpression, project, ratioData);
                            let data = tempData;
                            if (data) {
                                data = Math.round(data * 10) / 10;
                            }
    
                            if (tempData)
                            {
                                tempData = Math.round(data * 100) / 100;
                            }
                            const color = (project.colorRanges || []).find((c) => c.from <= data && c.to >= data) || { color: 'black' };
    
                            if (currentY === 0)
                            {
                                currentData = data;
                                fromYData = currentY;
                                toYData = currentY;
                                currentColor = color.color;
                            }
                            else
                            {
                                if (data === currentData)
                                {
                                    if (currentY < height -1)
                                    {
                                        toYData = currentY;
                                    }
                                    else
                                    {
                                        findData[currentX][`${fromYData}-${currentY}`] = {
                                            data,
                                            color: color.color
                                        };
                                    }
                                }
                                else
                                {
                                    findData[currentX][`${fromYData}-${toYData}`] = {
                                        data: currentData,
                                        color: currentColor
                                    };

                                    if (currentY < height - 1)
                                    {
                                        currentData = data;
                                        fromYData = currentY;
                                        toYData = currentY;
                                        currentColor = color.color;
                                    }
                                    else
                                    {
                                        findData[currentX][`${currentY}-${currentY}`] = {
                                            data,
                                            color: color.color
                                        };
                                    }
                                }
                            }
                        }
                    }
                }
    
                this.homeStore.setDrawContent({
                    ratioData,
                    barriers: project.barriers,
                    isShowEmployee: project.isShowEmployee,
                    employeeses: project.employeeses,
                    contentData: drawData,
                    findData: findData,
                    width: project.width,
                    height: project.height,
                    contents: project.contents,
                    background: project.background,
                    areas: project.areas,
                    texts: project.texts,
                    colorRanges: project.colorRanges,
                    rulerData: [
                        { data: parseInt(project.width / 2), width: parseInt(project.width / 2) / ratioData.ratio },
                        { data: parseInt(project.width / 4), width: parseInt(project.width / 4) / ratioData.ratio },
                        { data: parseInt(project.width / 8), width: parseInt(project.width / 8) / ratioData.ratio },
                        { data: parseInt(project.width / 16), width: parseInt(project.width / 16) / ratioData.ratio }
                    ]
                });

                resolve();
            }
        })
    }

    handleExport = async () => {
        if (!this.homeStore.drawContent)
        {
            toast({ type: "error", message: `Bấm vẽ bản đồ trước khi xuất!` });
            return;
        }

        try
        {
            const { PADDING_TOP, PADDING_LEFT, COLOR_AREA_WIDTH, RULER_AREA_HEIGHT, 
                    COLOR_MAP_MARGIN, COLOR_ITEM_MARGIN, COLOR_RECT_WIDTH,
                    COLOR_RECT_HEIGHT, COLOR_TOP_MARGIN, COLOR_RECT_WIDTH_MARGIN,
                    COLOR_TEXT_PADDING, RULER_BORDER_THINK, RULER_MAP_MARGIN,
                    RULER_HEIGHT, RULER_TEXTUNIT_MARGIN_LEFT, RULER_UNIT_DATA_MARGIN_TOP, 
                    RULER_UNIT_DATA_MARGIN_LEFT, RULER_TEXTUNIT_MARGIN_TOP, RULER_ODD_COLOR, RULER_EVEN_COLOR,
                    TEXT_FONT_SIZE, TEXT_FONT, DEFAULT_COLOR } = {
                PADDING_TOP: 10,
                PADDING_LEFT: 10,
                COLOR_AREA_WIDTH: 190,
                COLOR_MAP_MARGIN: 10,
                COLOR_ITEM_MARGIN: 40,
                COLOR_RECT_WIDTH: 40,
                COLOR_RECT_HEIGHT: 20,
                COLOR_TOP_MARGIN: 10,
                COLOR_RECT_WIDTH_MARGIN: 10,
                COLOR_TEXT_PADDING: 15,
                RULER_AREA_HEIGHT: 50,
                RULER_BORDER_THINK: 1,
                RULER_MAP_MARGIN: 40,
                RULER_HEIGHT: 10,
                RULER_TEXTUNIT_MARGIN_LEFT: 20,
                RULER_TEXTUNIT_MARGIN_TOP: 7,
                RULER_UNIT_DATA_MARGIN_TOP: -8,
                RULER_UNIT_DATA_MARGIN_LEFT: -5,
                RULER_ODD_COLOR: 'white',
                RULER_EVEN_COLOR: 'black',
                TEXT_FONT_SIZE: '12px',
                TEXT_FONT: 'Arial',
                DEFAULT_COLOR: 'black'
            };

            const drawContent = this.homeStore.drawContent;

            const mapCanvas = this.contentRef.current;
            const mapCanvasSize = { width: mapCanvas.width, height: mapCanvas.height };

            const exportCanvas = document.createElement('canvas');
            exportCanvas.setAttribute('crossOrigin', 'anonymous');

            let realWidth = mapCanvasSize.width;
            let realHeight = mapCanvasSize.height;
            let realExportMapRatio = 1;

            // calculate range color height
            const colorRanges = drawContent.colorRanges;
            let colorHeight = 0;
            if (Array.isArray(colorRanges) && colorRanges.length > 0)
            {
                colorHeight = PADDING_TOP + COLOR_TOP_MARGIN + colorRanges.length * COLOR_ITEM_MARGIN;
            }


            if (colorHeight > 0)
            {
                const exportMapRatio = realWidth / realHeight;
                const zoomWidth = Math.round(exportMapRatio * colorHeight) + 1;
                realExportMapRatio = zoomWidth / realWidth;
                realHeight = colorHeight;
                realWidth = zoomWidth;
            }

            exportCanvas.width = realWidth + PADDING_LEFT + COLOR_AREA_WIDTH;
            exportCanvas.height = realHeight + PADDING_TOP + RULER_AREA_HEIGHT;

            const exportContext = exportCanvas.getContext('2d');
            exportContext.drawImage(mapCanvas, PADDING_LEFT, PADDING_TOP, realWidth, realHeight);

            // add ruler
            let rulerData = drawContent.rulerData;
            if (Array.isArray(rulerData) && rulerData.length > 0)
            {
                rulerData = rulerData.sort((a, b) => {
                    return a.data >= b.data ? -1 : 1;
                })

                rulerData.forEach((r, index) => {
                    if (index === 0)
                    {
                        exportContext.fillStyle = DEFAULT_COLOR;
                        exportContext.strokeRect(PADDING_LEFT - RULER_BORDER_THINK, realHeight + RULER_MAP_MARGIN - RULER_BORDER_THINK, r.width * this.ratio * realExportMapRatio + RULER_BORDER_THINK, RULER_HEIGHT + RULER_BORDER_THINK);
                        exportContext.font = `${TEXT_FONT_SIZE} ${TEXT_FONT}`;
                        exportContext.fillText('(Meter)', r.width * this.ratio * realExportMapRatio + RULER_TEXTUNIT_MARGIN_LEFT, realHeight + RULER_MAP_MARGIN + RULER_TEXTUNIT_MARGIN_TOP);
                    }
                    exportContext.fillStyle = index % 2 === 0 ? RULER_ODD_COLOR : RULER_EVEN_COLOR;
                    exportContext.fillRect(PADDING_LEFT, realHeight + RULER_MAP_MARGIN, r.width * this.ratio * realExportMapRatio, RULER_HEIGHT);

                    exportContext.fillStyle = DEFAULT_COLOR;
                    exportContext.font = `${TEXT_FONT_SIZE} ${TEXT_FONT}`;
                    exportContext.fillText(r.data, r.width * this.ratio * realExportMapRatio - RULER_UNIT_DATA_MARGIN_LEFT, realHeight + RULER_MAP_MARGIN + RULER_UNIT_DATA_MARGIN_TOP);
                });
            }

            // add color ranges
            if (Array.isArray(colorRanges) && colorRanges.length > 0)
            {
                exportContext.font = `${TEXT_FONT_SIZE} ${TEXT_FONT}`;
                colorRanges.forEach((c, index) => {
                    const { x, y, width, height, color } = {
                        x: realWidth + PADDING_LEFT + COLOR_MAP_MARGIN,
                        y: PADDING_TOP + COLOR_TOP_MARGIN + index * COLOR_ITEM_MARGIN,
                        width: COLOR_RECT_WIDTH,
                        height: COLOR_RECT_HEIGHT,
                        color: c.color
                    };
                    exportContext.fillStyle = color;
                    exportContext.fillRect(x, y, width, height);
                    exportContext.fillStyle = DEFAULT_COLOR;
                    exportContext.strokeRect(x, y, width, height);
                    exportContext.fillText(`${c.from} - ${c.to}`, x + width + COLOR_RECT_WIDTH_MARGIN, y + COLOR_TEXT_PADDING);
                });
            }

            const linkDownload = document.createElement('a');
            linkDownload.download = 'map.png';
            linkDownload.href = exportCanvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
            linkDownload.click();
        }
        catch (err)
        {
            toast({ type: "error", message: err });
        }
    };

    calculateData = (x, y, number, project, ratioData) => 
    {
        x = x * ratioData.ratio;
        y = y * ratioData.ratio;
        if (Array.isArray(project.contents) && project.contents.length > 0)
        {
            return this.businessService.calculateFMapWithIDW(project.contents, x, y, project.volumn, project.soundDamping, project.barriers, project.deltaK);
        }
        else
        {
            return 0;
        }
    }

    render() {
        const drawContent = this.homeStore.drawContent;
        if (drawContent)
        {
            this.paint(drawContent);
        }

        return (
            <BorderPanel
            width={"70%"}
            className={`home-content ${this.props.className}`}
            flex={1}
            >
                    <Row className={"group-btn-actions"}>
                        <Button text="Vẽ bản đồ" onClick={() => {this.handleDraw()}}/>
                        <Spacer />
                        <Button text="Xuất bản đổ" onClick={() => {this.handleExport()}}/>
                    </Row>
                    <Container className={`home-container flex-panel`}>
                        <HomeContentDrawMapProgressBar />
                        <Container className={'draw-container'}>
                            <canvas ref={this.contentRef} onMouseMove={this.handleMouseMove} onMouseOut={this.handleMouseOut}>
                            </canvas>
                            <HomeContentMapToolTip />
                        </Container>
                        <HomeContentMapRuler drawContent={drawContent}/>
                    </Container>
            </BorderPanel>
        );
    }
}

class HomeContentMapToolTip extends Component {

    homeStore = this.props.appStore.homeStore;

    constructor(props) {
        super(props);
    }

    render() {
        const { isShow, x, y, text, ratio } = this.homeStore.mapToolTip;
        const xM = Math.round(x * ratio * 100) / 100;
        const yM = Math.round(y * ratio * 100) / 100;

        let locationX = -50;
        let locationY = -53;

        if (x < 50)
        {
            locationX = 5;
        }
        
        if (y < 53)
        {
            locationY = 5;
        }

        return (
            <>
                {
                    isShow && text !== 0 && text && <Container className={'map-tooltip'} style={{ left: `${x + locationX}px`, top: `${y + locationY}px` }}>
                        <TB1>x: {xM}</TB1>
                        <TB1>y: {yM}</TB1>
                        <TB1>dBA: {text}</TB1>
                    </Container>
                }
            </>
        )
    }
}

class HomeContentDrawMapProgressBar extends Component {

    homeStore = this.props.appStore.homeStore;

    render() {
        const drawMapProgessInfo = this.homeStore.drawMapProgessInfo;
        return (
            <>
                {
                    drawMapProgessInfo && drawMapProgessInfo.total > 0 && <Container>
                        <TB1>{drawMapProgessInfo.text}</TB1>
                        <ProgressBar total={drawMapProgessInfo.total} value={drawMapProgessInfo.value}/>
                    </Container>
                }
            </>
        )
    }
}

class HomeContentMapRuler extends Component {
    render() {
        let rulerData = this.props.drawContent && Array.isArray(this.props.drawContent.rulerData) ? this.props.drawContent.rulerData : [];
        
        rulerData = rulerData.sort((a, b) => {
            return a.data >= b.data ? -1 : 1;
        })

        return (
            <>
                {
                    rulerData.length > 0 && <Container className={'map-ruler'}>
                        {
                            rulerData.map((d, index) => {
                                return <Container key={index} className={'ruler-conent'} style={{width: `${d.width}px`, backgroundColor: (index % 2 === 0 ? 'white': 'black'), zIndex: index + 1}}>
                                    <Container className={'ruler-data'}>
                                        <Container className={'ruler-data-content'}>{ d.data } (m)</Container>
                                    </Container>
                                </Container>
                            })
                        }
                    </Container>
                }
            </>
        )
    }
}

HomeContent = inject("appStore")(observer(HomeContent));
HomeContentMapToolTip = inject("appStore")(observer(HomeContentMapToolTip));
HomeContentDrawMapProgressBar = inject("appStore")(observer(HomeContentDrawMapProgressBar));

export default HomeContent;

