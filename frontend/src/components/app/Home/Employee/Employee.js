import "./Employee.scss";
import React, { Component } from "react";
import { Container } from "components/bases/Container/Container";
import { Row } from "components/bases/Row/Row";
import { Spacer } from "components/bases/spacer/Spacer";
import { TB1 } from "components/bases/Text/Text";
import { inject, observer } from "mobx-react";
import { Button } from "components/bases/Button/Button";
import {
    Input,
    Section,
    AdvanceSelect,
    Radio
} from "components/bases/Form";
import { Table, TableRowCell, TableRow } from "components/bases/Table/Table";

// const employeeImages = {
//     '1': require('../../../../assets/images/nguoi-tren.png'),
//     '2': require('../../../../assets/images/nguoi-phai.png'),
//     '3': require('../../../../assets/images/nguoi-duoi.png'),
//     '4': require('../../../../assets/images/nguoi-trai.png')
// };

class Employee extends Component {
    constructor(props) {
        super(props);

        this.inputRef = React.createRef();
    }
    homeStore = this.props.appStore.homeStore;

    handleAdd() {
        this.homeStore.addEmployee(this.homeStore.selectedProjectId);
    }

    handleRemove() {
        this.homeStore.removeEmployee(this.homeStore.selectedProjectId);
    }

    handleSelected(index) {
        this.homeStore.selectEmployee(index, this.homeStore.selectedProjectId);
    }

    handleChangeValue(index, key, value) {
        this.homeStore.changeValueEmployee(
            index,
            key,
            value,
            this.homeStore.selectedProjectId
        );
    }

    enableEmployee(data) {
        this.homeStore.enableEmployee(
            data,
            this.homeStore.selectedProjectId
        );
    }

    render() {
        const currentProject =
            this.homeStore.projects.find(
                (p) => p.id === this.homeStore.selectedProjectId
            ) || {};
        const employeeses = Array.isArray(currentProject.employeeses) ? currentProject.employeeses : [];
        return (
            <Section header={"Quản lý nhân viên"}>
                <Container>
                    <Radio
                        label="Hiển thị ảnh nhân viên"
                        checked={
                            currentProject.isShowEmployee
                        }
                        className={"radio-color"}
                        onChange={(event) => {
                            this.homeStore.enableEmployee(
                                event
                            );
                        }}
                    />
                </Container>
                <Container className="list-area">
                    <Table
                        className={"result-table"}
                        isFixedHeader
                        headers={[
                            { label: "STT", width: 25 },
                            { label: "x(m)", width: 50 },
                            { label: "y(m)", width: 50 },
                            { label: "Hình ảnh", width: 160 }
                        ]}
                    >
                        {employeeses.map((row, i) => (
                            <TableRow
                                key={i}
                                onClick={() => this.handleSelected(i)}
                                isSelected={
                                    currentProject.selectedEmployeeIndex === i
                                        ? true
                                        : false
                                }
                            >
                                <TableRowCell>
                                    <TB1>{i + 1}</TB1>
                                </TableRowCell>
                                <TableRowCell>
                                    <Input
                                        type={"number"}
                                        step={1}
                                        value={row.x}
                                        min={0}
                                        color={"var(--text-color)"}
                                        onChange={(event) => {
                                            this.handleChangeValue(
                                                i,
                                                "x",
                                                parseFloat(event)
                                            );
                                        }}
                                    />
                                </TableRowCell>
                                <TableRowCell>
                                    <Input
                                        type={"number"}
                                        step={1}
                                        value={row.y}
                                        min={0}
                                        color={"var(--text-color)"}
                                        onChange={(event) => {
                                            this.handleChangeValue(
                                                i,
                                                "y",
                                                parseFloat(event)
                                            );
                                        }}
                                    />
                                </TableRowCell>
                                <TableRowCell>
                                    <AdvanceSelect
                                        options={[
                                            {
                                                id: '1',
                                                label: 'Người hướng 0 độ',
                                                // image: employeeImages['1']
                                            },
                                            {
                                                id: '2',
                                                label: 'Người hướng 90 độ',
                                                // image: employeeImages['2']
                                            },
                                            {
                                                id: '3',
                                                label: 'Người hướng 180 độ',
                                                // image: employeeImages['3']
                                            },
                                            {
                                                id: '4',
                                                label: 'Người hướng 270 độ',
                                                // image: employeeImages['4']
                                            }
                                        ]}
                                        onChange={(event) => {
                                            this.handleChangeValue(
                                                i,
                                                "image",
                                                event
                                            );
                                        }}
                                        value={row.image}
                                    />
                                </TableRowCell>
                            </TableRow>
                        ))}
                    </Table>
                </Container>
                <Row className={"group-btn-area"}>
                    <Button
                        icon={"plus"}
                        text={'Thêm'}
                        className={"btn-area"}
                        onClick={() => {
                            this.handleAdd();
                        }}
                    ></Button>
                    <Spacer />
                    <Button
                        icon={"trash"}
                        text={'Xóa'}
                        className={"btn-area"}
                        onClick={() => {
                            this.handleRemove();
                        }}
                    ></Button>
                    <Spacer />
                </Row>
            </Section>
        );
    }
}
Employee = inject("appStore")(observer(Employee));
export default Employee;
