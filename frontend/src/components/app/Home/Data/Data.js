import "./Data.scss";
import React, { Component } from "react";
import { Container } from "components/bases/Container/Container";
import { Row } from "components/bases/Row/Row";
import { Spacer } from "components/bases/spacer/Spacer";
import { TB1 } from "components/bases/Text/Text";
import {
    Input,
    InputGroup,
    Section,
    InputAppend,
    Button,
} from "components/bases/Form";
import XLSX from "xlsx";

import { Table, TableRowCell, TableRow } from "components/bases/Table/Table";
import { inject, observer } from "mobx-react";
import { ScrollView } from "components/bases/ScrollView/ScrollView";
import * as FileSaver from 'file-saver';
import { toast } from "components/bases/Modal/Modal";

class Data extends Component {
    constructor(props) {
        super(props);

        this.inputRef = React.createRef();
    }
    homeStore = this.props.appStore.homeStore;

    handleAdd() {
        this.homeStore.addData(this.homeStore.selectedProjectId);
    }

    handleRemove() {
        this.homeStore.removeData(this.homeStore.selectedProjectId);
    }
    
    handleChangeValue(index, key, value) {
        this.homeStore.changeValueData(
            this.homeStore.selectedProjectId,
            index,
            key,
            value
        );
    }
    handleSelected(index) {
        this.homeStore.selectData(this.homeStore.selectedProjectId, index);
    }

    handleImport = (currentProject) => {
        debugger;
        if (!currentProject || !currentProject.id)
        {
            toast({
                type: "error",
                message: `Vui lòng chọn dự án trước...`,
            });
            return;
        }

        if (
            this.inputRef.current.ref.current.files &&
            this.inputRef.current.ref.current.files[0]
        ) {
            const reader = new FileReader();
            reader.onload = () => {
                const fileData = reader.result;
                if (fileData)
                {
                    const data = fileData.replace('\r', '').split('\n').filter((d) => d).map((d, index) => {
                        if (index === 0)
                        {
                            return undefined;
                        }

                        const row = d.split(',');
                        if (row.length === 5)
                        {
                            try
                            {
                                return {
                                    ls: parseFloat(row[4]),
                                    x: parseFloat(row[1]),
                                    y: parseFloat(row[2])
                                }
                            }
                            catch {}
                        }
                        else
                        {
                            return undefined;
                        }
                    }).filter((d) => d);

                    if (Array.isArray(data) && data.length > 0)
                    {
                        this.homeStore.setData(data);
                    }            
                }
            };
            reader.readAsText(
                this.inputRef.current.ref.current.files[0]
            );
        }
    };

    handleChangeExpression = (number) => {
        this.homeStore.setExpressionNumber(number, this.homeStore.selectedProjectId);
    }

    exportTemplate() {
        const content = `STT,x (m),y (m),Δ(k),Ltd\r\n1,1,1,0,81.69\r\n2,2,2,0,83.9`;
        this.saveAsCvs(content, 'data-template');
    }

    saveAsCvs(text, fileName) {
        const data = new Blob([text], { type: 'csv' });
        FileSaver.saveAs(data, `${fileName}.csv`);
    }

    render() {
        const currentProject =
            this.homeStore.projects.find(
                (p) => p.id === this.homeStore.selectedProjectId
            ) || {};
        return (
            <Container className={"home-search-container data-container"}>
                <ScrollView><Section header={"Quản lý Dữ liệu"}>
                    <Row className={"group-btn-actions"}>
                        <InputGroup className={"btn-data"}>
                            <InputAppend>
                                <Button
                                    icon={"upload"}
                                    onlyIcon
                                    onClick={() => {
                                        const input = this.inputRef;
                                        if (
                                            input &&
                                            document.createEvent
                                        ) {
                                            const evt = document.createEvent(
                                                "MouseEvents"
                                            );
                                            evt.initEvent(
                                                "click",
                                                true,
                                                false
                                            );
                                            input.current.ref.current.dispatchEvent(
                                                evt
                                            );
                                        }
                                    }}
                                />
                            </InputAppend>
                            <Input
                                ref={this.inputRef}
                                className={"hide"}
                                type={"file"}
                                accept={
                                    ".csv"
                                }
                                onChange={() => {
                                    this.handleImport(
                                        currentProject
                                    );
                                }}
                            />
                            <TB1>Tải lên</TB1>
                        </InputGroup>
                        <Spacer />
                        <Button
                            className={"btn-data"}
                            icon={"plus"}
                            text="Thêm"
                            onClick={() => {
                                this.handleAdd();
                            }}
                        ></Button>
                        <Spacer />
                        <Button
                            className={"btn-data"}
                            text="Xóa"
                            icon={"trash"}
                            onClick={() => {
                                this.handleRemove();
                            }}
                        ></Button>
                        <Spacer />
                        <Button
                            className={"btn-data"}
                            text="Tải mẫu"
                            icon={"download"}
                            onClick={() => {
                                this.exportTemplate();
                            }}
                        ></Button>
                    </Row>
                    <Row>
                        <Container className="list-data">
                            <Table
                                className={"result-table"}
                                isFixedHeader
                                headers={[
                                    { label: "STT", width: 30 },
                                    // { label: `Ls(${currentProject.unit})`, width: 40 },
                                    // { label: "Q", width: 40 },
                                    // { label: "ω", width: 30 },
                                    // { label: "Δ(k)", width: 30 },
                                    { label: `Đơn vị đo (${currentProject.unit})`, width: 100 },
                                    { label: "x (m)", width: 30 },
                                    { label: "y (m)", width: 30 },
                                ]}
                            >
                                {(currentProject.contents || []).map(
                                    (row, i) => (
                                        <TableRow
                                            key={i}
                                            onClick={() =>
                                                this.handleSelected(i)
                                            }
                                            isSelected={currentProject.selectedContentDataIndex === i?true:false}
                                        >
                                            <TableRowCell>
                                                { i + 1 }
                                            </TableRowCell>
                                            {/* <TableRowCell>
                                                <Input
                                                    type={"number"}
                                                    step={1}
                                                    value={row.ls}
                                                    min={0}
                                                    color={"var(--text-color)"}
                                                    onChange={(event) => {
                                                        this.handleChangeValue(
                                                            i,
                                                            "ls",
                                                            parseFloat(event)
                                                        );
                                                    }}
                                                />
                                            </TableRowCell> */}
                                            {/* <TableRowCell>
                                                <AdvanceSelect
                                                    options={[
                                                        {
                                                            id: 1,
                                                            label: "1",
                                                        },
                                                        {
                                                            id: 2,
                                                            label: "2",
                                                        },
                                                        {
                                                            id: 4,
                                                            label: "4",
                                                        },
                                                        {
                                                            id: 8,
                                                            label: "8",
                                                        }
                                                    ]}
                                                    onChange={(event) => {
                                                        this.handleChangeValue(
                                                            i,
                                                            "q",
                                                            event
                                                        );
                                                    }}
                                                    value={row.q}
                                                />
                                            </TableRowCell>
                                            <TableRowCell>
                                                <Input
                                                    type={"number"}
                                                    step={45}
                                                    value={row.omega}
                                                    min={0}
                                                    max={360}
                                                    color={"var(--text-color)"}
                                                    onChange={(event) => {
                                                        this.handleChangeValue(
                                                            i,
                                                            "omega",
                                                            parseFloat(event)
                                                        );
                                                    }}
                                                />
                                            </TableRowCell>
                                            <TableRowCell>
                                                <Input
                                                    type={"number"}
                                                    step={0.01}
                                                    value={row.deltaK}
                                                    min={0}
                                                    color={"var(--text-color)"}
                                                    onChange={(event) => {
                                                        this.handleChangeValue(
                                                            i,
                                                            "deltaK",
                                                            parseFloat(event)
                                                        );
                                                    }}
                                                />
                                            </TableRowCell> */}
                                            <TableRowCell>
                                                <Input
                                                    type={"number"}
                                                    step={0.1}
                                                    value={row.ls}
                                                    min={0}
                                                    color={"var(--text-color)"}
                                                    onChange={(event) => {
                                                        this.handleChangeValue(
                                                            i,
                                                            "ls",
                                                            parseFloat(event)
                                                        );
                                                    }}
                                                />
                                            </TableRowCell>
                                            <TableRowCell>
                                                <Input
                                                    type={"number"}
                                                    step={1}
                                                    value={row.x}
                                                    min={0}
                                                    color={"var(--text-color)"}
                                                    onChange={(event) => {
                                                        this.handleChangeValue(
                                                            i,
                                                            "x",
                                                            parseFloat(event)
                                                        );
                                                    }}
                                                />
                                            </TableRowCell>
                                            <TableRowCell>
                                                <Input
                                                    type={"number"}
                                                    step={1}
                                                    value={row.y}
                                                    min={0}
                                                    color={"var(--text-color)"}
                                                    onChange={(event) => {
                                                        this.handleChangeValue(
                                                            i,
                                                            "y",
                                                            parseFloat(event)
                                                        );
                                                    }}
                                                />
                                            </TableRowCell>
                                        </TableRow>
                                    )
                                )}
                            </Table>
                        </Container>
                    </Row>
                </Section></ScrollView>
                
            </Container>
        );
    }
}
Data = inject("appStore")(observer(Data));
export default Data;
