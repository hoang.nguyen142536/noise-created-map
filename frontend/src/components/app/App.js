import React, { Component } from "react";
import { isMobile } from "react-device-detect";
import "mobx-react-lite/batchingForReactDom";
import { setLocale } from "components/bases/Translate/Translate";
import Home from "./Home/Home";
import appStore from "components/app/stores/AppStore";
import { Provider } from "mobx-react";
import { ThemeProvider } from "components/bases/Theme/ThemeContext";
class App extends Component {
    constructor(props) {
        super(props);

        this.appRef = React.createRef();

        // setup moment
        setLocale();
    }

    componentDidMount() {
        this.screenClickEvent = new CustomEvent("onScreenClick");
        this.screenClickEvent.initEvent("onScreenClick", true, true);

        if (isMobile) {
            this.appRef.current.addEventListener(
                "touchstart",
                this.onScreenClick
            );
        } else {
            this.appRef.current.addEventListener(
                "mousedown",
                this.onScreenClick
            );
        }
    }

    onScreenClick = () => {
        document.dispatchEvent(this.screenClickEvent);
    };

    render() {
        const theme =  { name: 'Đỏ tối', base: 'dark', className: 'theme-red' };
        return (
            <div ref={this.appRef}>
                <Provider appStore={appStore}>
                    <ThemeProvider theme={theme}>
                        <Home />
                    </ThemeProvider>
                </Provider>
            </div>
        );
    }
}

export default App;
