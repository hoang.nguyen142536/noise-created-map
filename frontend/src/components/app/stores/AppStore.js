
import {HomeStore} from './HomeStore';
class AppStore
{
    constructor()
    {
        this.homeStore = new HomeStore(this);

    }
}

export default new AppStore();
