import { decorate, observable } from "mobx";
import { action } from "mobx";
import { CommonHelper } from "helper/common.helper";
const contentDataSample = require("../../../data/contentDataSample.json");
const colorRangeSample = require("../../../data/colorRangeSample.json");
const areasSample = require("../../../data/areasSample.json");

export class HomeStore {
    appStore = null;
    isCollapseSearch = false;
    tabSelected = "general-info";
    isPopUpNewProject = false;
    isPopUpColor = false;
    selectedProjectId = "uuid"; // lấy id của project. Mặc định lúc new project sẽ tự đổi luôn
    newProject = {};
    newArea = {};
    projects = [];

    drawContent = undefined;
    drawMapProgessInfo = {
        total: 0,
        value: 0,
        text: ''
    }

    mapToolTip = {
        isShow: false,
        x: 0,
        y: 0,
    };

    constructor(appStore) {
        this.appStore = appStore;
    }

    setTab(tab) {
        this.tabSelected = tab;
    }

    setPopupNewProject(popup) {
        this.isPopUpNewProject = popup;
    }
    setPopupNewArea(popup) {
        this.isPopUpNewArea = popup;
    }

    setPopupEditArea(popup) {
        this.isPopUpEditArea = popup;
    }

    setSearchCollapse(isCollapseSearch) {
        this.isCollapseSearch = isCollapseSearch;
    }

    setSelectedProjectId(id) {
        this.selectedProjectId = id;
    }

    setMapToolTip(data) {
        this.mapToolTip = {
            ...this.mapToolTip,
            ...data,
        };
    }
    addNewProject(project) {
        this.projects = [...this.projects, {...project, isShowEmployee: true, colorRanges: [
            {
              "from": 0,
              "to": 0,
              "color": "#ff0606"
            },
            {
              "from": 0,
              "to": 0,
              "color": "#f84a4a"
            },
            {
              "from": 0,
              "to": 0,
              "color": "#f77a7a"
            },
            {
              "from": 0,
              "to": 0,
              "color": "#eba8a8"
            },
            {
              "from": 0,
              "to": 0,
              "color": "#f7cfcf"
            },
            {
              "from": 0,
              "to": 0,
              "color": "#84f706"
            },
            {
              "from": 0,
              "to": 0,
              "color": "#94e041"
            },
            {
              "from": 0,
              "to": 0,
              "color": "#b0e07c"
            },
            {
              "from": 0,
              "to": 0,
              "color": "#c7d7b6"
            },
            {
              "from": 0,
              "to": 0,
              "color": "#06eab7"
            },
            {
              "from": 0,
              "to": 0,
              "color": "#63e2c6"
            },
            {
              "from": 0,
              "to": 0,
              "color": "#a7e1d4"
            },
            {
              "from": 0,
              "to": 0,
              "color": "#0673f3"
            },
            {
              "from": 0,
              "to": 0,
              "color": "#6096d7"
            },
            {
              "from": 0,
              "to": 0,
              "color": "#abbcd0"
            }
          ]}];
        this.selectedProjectId = project.id;
        this.newProject = {};
        // localStorage.setItem("Projects", JSON.stringify(this.projects));
    }
    setNewProject = (key, value) => {
        this.newProject = {
            ...this.newProject,
            [key]: value,
        };
    };
    changeGernalInfo = (key, value, id) => {
        const index = this.projects.findIndex((p) => p.id === id);
        if (index === -1) return;
        this.projects[index] = {
            ...this.projects[index],
            [key]: value,
        };
        this.projects = [...this.projects];
    };

    enableBackground = (value, id) => {
        const index = this.projects.findIndex((p) => p.id === id);
        if (index === -1) return;
        this.projects[index].background = this.projects[index].background && {
            ...this.projects[index].background,
            enable: value,
        };
        this.projects = [...this.projects];
    };
    changeText = (projectId = this.selectedProjectId, index, key, value) => {
        const project = this.projects.find((p) => p.id === projectId);
        index = index < 0 ? project.selectedTextIndex : index;
        if (project) {
            if (project.texts.length > index) {
                if (!Array.isArray(project.texts)) {
                    project.texts = [];
                }
                project.texts[index][key] = value;
                this.projects = [...this.projects];
            }
        }
    };
    addText = (projectId = this.selectedProjectId) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            if (!Array.isArray(project.texts)) {
                project.texts = [];
            }
            project.texts.push({
                x: 0,
                y: 0,
                text: "",
                color: "#000",
                fontSize: "0px",
            });
            this.selectText(projectId, project.texts.length - 1);
            this.projects = [...this.projects];
        }
    };
    removeText = (projectId = this.selectedProjectId) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            if (!Array.isArray(project.texts)) {
                project.texts = [];
            }
        }
        if (project.texts.length > project.selectedTextIndex) {
            project.texts.splice(project.selectedTextIndex, 1);
            project.selectedTextIndex = 0;
            this.projects = [...this.projects];
        }
    };
    selectText = (projectId, index) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            project.selectedTextIndex = index;
            this.projects = [...this.projects];
        }
    };
    saveProject = (id) => {
        const index = this.projects.findIndex((p) => p.id === id);
        if (index === -1) return;
        var a = document.createElement("a");
        var file = new Blob([JSON.stringify(this.projects[index])], {
            type: "text/plain",
        });
        a.href = URL.createObjectURL(file);
        a.download = `${this.projects[index].name}.ncm`;
        a.click();
        // localStorage.setItem("Projects", JSON.stringify(this.projects));
    };
    closeProject = (id) => {
        const index = this.projects.findIndex((p) => p.id === id);
        if (index === -1) return;
        this.projects.splice(index, 1);
        if (this.projects.length > 0) {
            this.selectedProjectId = this.projects[0].id;
        } else {
            this.selectedProjectId = "";
        }
        // localStorage.setItem("Projects", JSON.stringify(this.projects));
    };
    openProject = (project) => {
        if (this.projects.find((p) => project.id === p.id)) {
            project.id = CommonHelper.uuid();
        }
        this.selectedProjectId = project.id;
        this.projects = [...this.projects, project];
        // console.log(JSON.stringify(project.colorRanges));
        // localStorage.setItem("Projects", JSON.stringify(this.projects));
    };

    getCurrentProject = () => {
        return this.projects.find((project) => project.id === this.selectedProjectId);
    };

    addColor = (projectId) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            if (!Array.isArray(project.colorRanges)) {
                project.colorRanges = [];
            }

            project.colorRanges.push({
                from: 0,
                to: 0,
                color: "#000",
            });
            this.selectColor(
                this.selectedProjectId,
                project.colorRanges.length - 1
            );
            this.projects = [...this.projects];
        }
    };

    removeColor = (projectId) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            if (!Array.isArray(project.colorRanges)) {
                project.colorRanges = [];
            }

            if (project.colorRanges.length > project.selectedColorRangeIndex) {
                project.colorRanges.splice(project.selectedColorRangeIndex, 1);
                project.selectedColorRangeIndex = 0;
                this.projects = [...this.projects];
            }
        }
    };

    selectColor = (projectId, index) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            project.selectedColorRangeIndex = index;
            this.projects = [...this.projects];
        }
    };

    changeValueColor = (projectId, index, key, value) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            if (!Array.isArray(project.colorRanges)) {
                project.colorRanges = [];
            }

            if (project.colorRanges.length > index) {
                project.colorRanges[index][key] = value;
                this.projects = [...this.projects];
            }
        }
    };
    uploadBackground = (image) => {
        const project = this.projects.find(
            (p) => p.id === this.selectedProjectId
        );
        if (project) {
            if (!project.background) {
                project.background = {};
            }
            project.background.image = image;
            this.projects = [...this.projects];
        }
    };

    addData = (projectId) => {
        const project = this.projects.find(
            (p) => p.id === (projectId || this.selectedProjectId)
        );
        if (project) {
            if (!Array.isArray(project.contents)) {
                project.contents = [];
            }

            project.contents.push({
                x: 0,
                y: 0,
                q: 1,
                ls: 0,
                omega: 0,
                deltaK: 0
            });
            this.selectData(
                this.selectedProjectId,
                project.contents.length - 1
            );
            this.projects = [...this.projects];
        }
    };

    setData = (data, projectId) => {
        if (!Array.isArray(data))
        {
            return;
        }

        const project = this.projects.find(
            (p) => p.id === (projectId || this.selectedProjectId)
        );

        if (project) {
            project.contents = data;
            this.projects = [...this.projects];
        }
    };

    removeData = (projectId) => {
        const project = this.projects.find(
            (p) => p.id === (projectId || this.selectedProjectId)
        );
        if (project) {
            if (!Array.isArray(project.contents)) {
                project.contents = [];
            }

            if (project.contents.length > project.selectedContentDataIndex) {
                project.contents.splice(project.selectedContentDataIndex, 1);
                project.selectedContentDataIndex = 0;
                this.projects = [...this.projects];
            }
        }
    };

    selectData = (projectId, index) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            project.selectedContentDataIndex = index;
            this.projects = [...this.projects];
        }
    };

    changeValueData = (projectId, index, key, value) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            if (!Array.isArray(project.contents)) {
                project.contents = [];
            }

            if (project.contents.length > index) {
                project.contents[index][key] = value;
                this.projects = [...this.projects];
            }
        }
    };

    addBarrier = (projectId) => {
        const project = this.projects.find(
            (p) => p.id === (projectId || this.selectedProjectId)
        );
        if (project) {
            if (!Array.isArray(project.barriers)) {
                project.barriers = [];
            }

            project.barriers.push({
                x: 0,
                y: 0,
                width: 0,
                height: 0,
                image: "",
            });
            this.selectBarrier(
                this.selectedProjectId,
                project.barriers.length - 1
            );
            this.projects = [...this.projects];
        }
    };

    removeBarrier = (projectId) => {
        const project = this.projects.find(
            (p) => p.id === (projectId || this.selectedProjectId)
        );
        if (project) {
            if (!Array.isArray(project.barriers)) {
                project.barriers = [];
            }

            if (project.barriers.length > project.selectedBarriersIndex) {
                project.barriers.splice(project.selectedBarriersIndex, 1);
                project.selectedBarriersIndex = 0;
                this.projects = [...this.projects];
            }
        }
    };

    selectBarrier = (projectId, index) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            project.selectedBarriersIndex = index;
            this.projects = [...this.projects];
        }
    };

    changeValueBarrier = (projectId, index, key, value) => {
        const project = this.projects.find((p) => p.id === projectId);
        index = index < 0 ? project.selectedBarriersIndex : index;
        if (project) {
            if (!Array.isArray(project.barriers)) {
                project.barriers = [];
            }

            if (project.barriers.length > index) {
                project.barriers[index][key] = value;
                this.projects = [...this.projects];
            }
        }
    };
    getProjects = () => {
        if (!localStorage.getItem("Projects")) return;
        // this.projects = JSON.parse(localStorage.getItem("Projects"));
    };

    setExpressionNumber = (number, projectId) => {
        projectId = projectId || this.selectedProjectId;
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            process.expectedExpression = number;
            this.projects = [...this.projects];
        }
    };
    setPopupColor(popup, index) {
        this.isPopUpColor = popup;
        this.selectedTextIndex = index;
    }

    removeArea = (projectId) => {
        const project = this.projects.find(
            (p) => p.id === (projectId || this.selectedProjectId)
        );
        if (project) {
            if (!Array.isArray(project.areas)) {
                project.areas = [];
            }

            if (project.areas.length > project.selectedAreaIndex) {
                project.areas.splice(project.selectedAreaIndex, 1);
                project.selectedAreaIndex = 0;
                this.projects = [...this.projects];
            }
        }
    };

    selectArea = (projectId, index) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            project.selectedAreaIndex = index;
            this.projects = [...this.projects];
        }
    };

    changeValueArea = (key, value) => {
        const project = this.projects.find(
            (p) => p.id === this.selectedProjectId
        );
        if (project) {
            if (project.areas.length > project.selectedAreaIndex) {
                project.areas[project.selectedAreaIndex][key] = value;
                this.projects = [...this.projects];
            }
        }
    };

    addNewArea(area) {
        const project = this.projects.find(
            (p) => p.id === this.selectedProjectId
        );
        if (project) {
            if (!Array.isArray(project.areas)) {
                project.areas = [];
            }
            project.areas.push(area);
            project.selectedAreaIndex = project.areas.length - 1;
            this.newArea = {};
            this.projects = [...this.projects];
        }
       
    }
    setNewArea = (key, value) => {
        this.newArea = {
            ...this.newArea,
            [key]: value,
        };
    };

    addNewDataPolygon = () => {
        if (!this.newArea) return;
        if (!Array.isArray(this.newArea.data)) {
            this.newArea = { ...this.newArea, data: [{ x: 0, y: 0 }] };
        } else {
            this.newArea.data.push({ x: 0, y: 0 });
        }
    };

    setDataNewArea = (key, value, index) => {
        this.newArea.data[index] = {
            ...this.newArea.data[index],
            [key]: value,
        };
    };
    deletedDataNewArea = (index) => {
        if (this.newArea.data.length > index) {
            this.newArea.data.splice(index, 1);
            this.newArea = { ...this.newArea };
        }
    };
    setDataCurrentArea = (key, value, index) => {
        const project = this.projects.find(
            (p) => p.id === this.selectedProjectId
        );
        if (project) {
            if (project.areas.length > project.selectedAreaIndex) {
                if (project.areas[project.selectedAreaIndex].data) {
                    project.areas[project.selectedAreaIndex].data[index][
                        key
                    ] = value;
                    this.projects = [...this.projects];
                }
            }
        }
    };

    addCurrentDataPolygon = () => {
        const project = this.projects.find(
            (p) => p.id === this.selectedProjectId
        );
        if (project) {
            if (project.areas.length > project.selectedAreaIndex) {
                if (
                    !Array.isArray(
                        project.areas[project.selectedAreaIndex].data
                    )
                ) {
                    project.areas[project.selectedAreaIndex].data = {
                        ...project.areas[project.selectedAreaIndex],
                        data: [{ x: 0, y: 0 }],
                    };
                } else {
                    project.areas[project.selectedAreaIndex].data.push({
                        x: 0,
                        y: 0,
                    });
                }
            }
        }
    };
    deletedDataCurrentArea = (index) => {
        const project = this.projects.find(
            (p) => p.id === this.selectedProjectId
        );
        if (project) {
            if (Array.isArray(project.areas[project.selectedAreaIndex].data)) {
                if (project.areas[project.selectedAreaIndex].data.length > index) 
                {
                    project.areas[project.selectedAreaIndex].data.splice(index, 1);
                    this.projects = [...this.projects];
                }
            }
        }
    };

    getCheck = (index, projectId) => {
        const project = this.projects.find((p) => p.id === (projectId || this.selectedProjectId));
        if (project) {
            if (!Array.isArray(project.checks) || project.checks.length < index) {
                return {};
            }

            return project.checks[index];
        }
        else
        {
            return {};
        }
    }

    addCheck = (projectId) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            if (!Array.isArray(project.checks)) {
                project.checks = [];
            }

            project.checks.push({
                v: 0,
                s: 0,
                anpha: 0.2,
                anphaA: 0.2,
                d: 0
            });
            this.selectColor(this.selectedProjectId, project.checks.length - 1);
            this.projects = [...this.projects];
        }
    };

    removeCheck = (projectId) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            if (!Array.isArray(project.checks)) {
                project.checks = [];
            }

            if (project.checks.length > project.selectedCheckIndex) {
                project.checks.splice(project.selectedCheckIndex, 1);
                project.selectedCheckIndex = 0;
                this.projects = [...this.projects];
            }
        }
    };

    selectCheck = (projectId, index) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            project.selectedCheckIndex = index;
            this.projects = [...this.projects];
        }
    };

    changeValueCheck = (projectId, index, key, value) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            if (!Array.isArray(project.checks)) {
                project.checks = [];
            }

            if (project.checks.length > index) {
                project.checks[index][key] = value;
                this.projects = [...this.projects];
            }
        }
    };


    addDoes = (projectId) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            if (!Array.isArray(project.noiseDoeses)) {
                project.noiseDoeses = [];
            }

            project.noiseDoeses.push({
                x: 0,
                y: 0,
                t: 0,
                leq: 0,
                twa: 0,
                d1: 0,
            });

            this.selectDoes(this.selectedProjectId, project.noiseDoeses.length - 1);
        }
    };

    removeDoes = (projectId) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            if (!Array.isArray(project.noiseDoeses)) {
                project.noiseDoeses = [];
            }

            if (project.noiseDoeses.length > project.selectedDoesIndex) {
                project.noiseDoeses.splice(project.selectedDoesIndex, 1);
                project.selectedDoesIndex = 0;
            }
        }
    };

    selectDoes = (projectId, index) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            project.selectedDoesIndex = index;
            this.projects = [...this.projects];
        }
    };

    changeValueDoes = (projectId, index, key, value) => {
        const project = this.projects.find((p) => p.id === projectId);
        if (project) {
            if (!Array.isArray(project.noiseDoeses)) {
                project.noiseDoeses = [];
            }

            if (project.noiseDoeses.length > index) {
                project.noiseDoeses[index][key] = value;
            }
        }
    };

    changeDoes = (data, summaryData, projectId) => {
        const project = this.projects.find((p) => p.id === (projectId || this.selectedProjectId));
        if (project) {
            project.noiseDoeses = data || [];
            project.noiseDoesTotals = summaryData || [];
            this.projects = [...this.projects];
        }
    };

    setDrawContent = (data) => {
        this.drawContent = data;
    };

    setDrawMapProgessInfo = (data) => {
        this.drawMapProgessInfo = Object.assign(this.drawMapProgessInfo, data);
    }
    
    addForeCastData = (projectId) => {
        const project = this.projects.find((p) => p.id === (projectId || this.selectedProjectId));
        if (project) {
            if (!project.foreCast)
            {
                project.foreCast = {};
            }
            
            if (!Array.isArray(project.foreCast.data))
            {
                project.foreCast.data = [];
            }

            project.foreCast.data.push({
                x: 0,
                y: 0,
                q: 1,
                ls: 0,
                omega: 0,
                lamda: 0.01
            });

            this.projects = [...this.projects];
        }
    }

    addForeCastPoint = (projectId) => {
        const project = this.projects.find((p) => p.id === (projectId || this.selectedProjectId));
        if (project) {
            if (!project.foreCast)
            {
                project.foreCast = {};
            }
            
            if (!Array.isArray(project.foreCast.points))
            {
                project.foreCast.points = [];
            }

            project.foreCast.points.push({
                x: 0,
                y: 0,
                ltd: ''
            });

            this.projects = [...this.projects];
        }
    }

    changeForeCast = (data, projectId) => {
        const project = this.projects.find((p) => p.id === (projectId || this.selectedProjectId));
        if (project) {

            project.foreCast = data;

            this.projects = [...this.projects];
        }
    }

    getForeCast = (projectId) => {
        const project = this.projects.find((p) => p.id === (projectId || this.selectedProjectId));
        if (project) {

            return project.foreCast;
        }

        return undefined;
    }

    selectForeCastData = (index, projectId) => {
        const project = this.projects.find((p) => p.id === (projectId || this.selectedProjectId));
        if (project) {

            project.selectedForeCastDataIndex = index;
            this.projects = [...this.projects];
        }
    }

    selectForeCastPoint = (index, projectId) => {
        const project = this.projects.find((p) => p.id === (projectId || this.selectedProjectId));
        if (project) {

            project.selectedForeCastPointIndex = index;
            this.projects = [...this.projects];
        }
    }

    addEmployee = (projectId) => {
        const project = this.projects.find((p) => p.id === (projectId || this.selectedProjectId));
        if (project) {
            if (!project.employeeses)
            {
                project.employeeses = [];
            }
        

            project.employeeses.push({
                x: 0,
                y: 0,
                image: "1"
            });

            this.projects = [...this.projects];
        }
    }

    removeEmployee = (projectId) => {
        const project = this.projects.find((p) => p.id === (projectId || this.selectedProjectId));
        if (project) {
            if (!Array.isArray(project.employeeses)) {
                project.employeeses = [];
            }

            if (project.employeeses.length > project.selectedEmployeeIndex) {
                project.employeeses.splice(project.selectedEmployeeIndex, 1);
                project.selectedEmployeeIndex = 0;

                this.projects = [...this.projects];
            }
        }
    };

    changeValueEmployee = (index, key, value, projectId) => {
        const project = this.projects.find((p) => p.id === (projectId || this.selectedProjectId));
        if (project) {
            if (!Array.isArray(project.employeeses)) {
                project.employeeses = [];
            }

            if (project.employeeses.length > index) {
                project.employeeses[index][key] = value;

                this.projects = [...this.projects];
            }
        }
    };

    selectEmployee = (index, projectId) => {
        const project = this.projects.find((p) => p.id === (projectId || this.selectedProjectId));
        if (project) {
            project.selectedEmployeeIndex = index;

            this.projects = [...this.projects];
        }
    };

    enableEmployee = (data, projectId) => {
        const project = this.projects.find((p) => p.id === (projectId || this.selectedProjectId));
        if (project) {
            project.isShowEmployee = data;

            this.projects = [...this.projects];
        }
    }
}

decorate(HomeStore, {
    appStore: observable,
    isCollapseSearch: observable,
    tabSelected: observable,
    projects: observable,
    selectedProjectId: observable,
    mapToolTip: observable,
    setSearchCollapse: action,
    setTab: action,
    setSelectedProjectId: action,
    setMapToolTip: action,
    isPopUpNewProject: observable,
    setPopupNewProject: action,
    addNewProject: action,
    setNewProject: action,
    newProject: observable,
    changeText: action,
    changeGernalInfo: action,
    enableBackground: action,
    addText: action,
    removeText: action,
    selectText: action,
    saveProject: action,
    closeProject: action,
    addColor: action,
    removeColor: action,
    selectColor: action,
    changeValueColor: action,
    openProject: action,
    uploadBackground: action,
    addData: action,
    setData: action,
    selectData: action,
    changeValueData: action,
    removeData: action,
    addBarrier: action,
    selectBarrier: action,
    changeValueBarrier: action,
    removeBarrier: action,
    getProjects: action,
    setExpressionNumber: action,
    isPopUpColor: observable,
    setPopupColor: action,
    drawContent: observable,
    setDrawContent: action,
    setPopupNewProject: action,
    isPopUpNewArea: observable,
    selectArea: action,
    changeValueArea: action,
    removeArea: action,
    addNewArea: action,
    setNewArea: action,
    newArea: observable,
    addNewDataPolygon: action,
    setDataNewArea: action,
    addCheck: action,
    selectCheck: action,
    changeValueCheck: action,
    removeCheck: action,
    setPopupNewArea: action,
    setPopupEditArea: action,
    isPopUpEditArea: observable,
    setDataCurrentArea: action,
    addCurrentDataPolygon: action,
    deletedDataNewArea: action,
    deletedDataCurrentArea: action,
    addDoes: action,
    selectDoes: action,
    removeDoes: action,
    drawMapProgessInfo: observable,
    setDrawMapProgessInfo: action,
    changeDoes: action,
    addForeCastData: action,
    addForeCastPoint: action,
    changeForeCast: action,
    selectForeCastData: action,
    selectForeCastPoint: action,
    addEmployee: action,
    removeEmployee: action,
    changeValueEmployee: action,
    selectEmployee: action,
    enableEmployee: action
});
