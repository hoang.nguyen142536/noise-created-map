import React from 'react';
import ReactDOM from 'react-dom';
import { action } from '@storybook/addon-actions';

import { ContextMenu } from 'components/bases/ContextMenu/ContextMenu';
import { Button } from 'components/bases/Button/Button';


export default {
    title: 'Bases/ContextMenu',
    component: ContextMenu,
};

const Template = (args) =>
{

    const handleClose = () =>
    {
        (action('onClose'))();
    };

    args.onClose = handleClose;

    const handleOpen = (event, args) =>
    {
        args.id = 'storybook-context-menu';
        args.position = {
            x: event.clientX,
            y: event.clientY,
        };

        const containerId = args.id + '-container';
        let element = document.getElementById(containerId);
        if (!element)
        {
            element = document.createElement('div');
            element.setAttribute('id', containerId);

            const root = document.getElementById('root');
            root.appendChild(element);
        }

        // render empty to div, but still keep the div
        const originOnClose = args.onClose;
        args.onClose = () =>
        {
            ReactDOM.render(null, element);
            if (originOnClose)
            {
                originOnClose();
            }
        };

        ReactDOM.render(
            <ContextMenu {...args} />,
            element
        );
    };

    return (
        <>
            <Button
                text='Open context menu'
                onClick={(event) => handleOpen(event, args)}
            />
        </>
    );
};

export const Default = Template.bind({});
Default.args = {
    width: 200,
    maxHeight: 500,
    header: 'Go to:',
    isCloseOnBlur: true,
    actions: [
        {
            label: 'Home',
            icon: 'home',
            onClick: () => action('onClickContextMenuItem')
        },
        {
            label: '-'
        },
        {
            label: 'Sign out',
            icon: 'sign-out',
            onClick: () => action('onClickContextMenuItem')
        }
    ],
};


