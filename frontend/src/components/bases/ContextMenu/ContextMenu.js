import './ContextMenu.scss';

import React, { Component } from 'react';
import classNames from 'classnames';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { T } from 'components/bases/Translate/Translate';
import { FAIcon } from 'components/bases/Icon/FAIcon';

export class ContextMenu extends Component
{
    constructor(props)
    {
        super(props);

        document.addEventListener('onScreenClick', this.onScreenClick);

        this.divRef = React.createRef();
    }

    onScreenClick = () =>
    {
        this.handleOnClose();
    };

    menu = {
        id: null,
        header: '',
        position: {
            x: 0,
            y: 0
        },
        isTopLeft: true, // have two options, top-left and top-right
        isCloseOnAction: true,
        disabled: false,
        actions: []
    };

    setProps = (props) =>
    {
        const menu = this.menu;
        Object.keys(props).forEach(function (key)
        {
            if (key in menu)
            {
                if (props[key] !== undefined)
                {
                    menu[key] = props[key];
                }
            }
        });
    };

    handleOnClose = () =>
    {
        if (this.props.onClose)
        {
            this.props.onClose();
        }

        document.removeEventListener('onScreenClick', this.onScreenClick);
    };

    handleOnClickAction = (i) =>
    {
        if (this.props.actions[i].onClick && !this.props.actions[i].disabled)
        {
            this.props.actions[i].onClick();
        }

        if (this.menu.isCloseOnAction && !this.props.actions[i].disabled)
        {
            this.handleOnClose();
        }
    };

    handleContainerClick = () =>
    {
        // to prevent it auto close on iOS, we just close when it's in fullscreen mode
        if (this.props.isResponsive)
        {
            this.handleOnClose();
        }
    };

    componentDidMount()
    {
        this.divRef.current.focus();
    }

    handleOnBlur = () =>
    {
        if (this.props.isCloseOnBlur)
        {
            this.handleOnClose();
        }
    };

    render()
    {
        this.setProps(this.props);

        const menuStyle = classNames({
            'fullscreen': this.props.isResponsive
        });

        const menuSize = {};
        const menuPos = {};
        const popupTransformStyle = { x: 0, y: 0 };

        if (!this.props.isResponsive)
        {
            menuSize.minWidth = menuPos.minWidth = 150;
            if (this.props.width)
            {
                menuSize.width = menuPos.minWidth = this.props.width;
            }

            if (this.props.maxHeight)
            {
                menuSize.maxHeight = this.props.maxHeight;
            }

            if (this.menu.isTopLeft)
            {
                menuPos.left = this.menu.position.x;
            }
            else
            {
                menuPos.right = this.menu.position.x;
            }
            menuPos.top = this.menu.position.y;

            popupTransformStyle.x = (window.innerWidth - this.menu.position.x) >= 250 ? 0 : '-100%';
            popupTransformStyle.y = (window.innerHeight - this.menu.position.y) >= 250 ? 0 : '-100%';
        }
        else
        {
            menuSize.minWidth = menuSize.maxWidth = window.innerWidth - 100;
            menuSize.maxHeight = window.innerHeight;
            if (this.props.header)
            {
                menuSize.maxHeight -= 25;
            }
            menuSize.maxHeight -= 150;
        }

        return (
            <div
                tabIndex="0"
                ref={this.divRef}
                onBlur={this.handleOnBlur}
                id={this.menu.id}
                className={'action-menu ' + menuStyle}
                onContextMenu={(e) =>
                {
                    e.preventDefault();
                }}
            >

                <div className="modal-screen" />

                <div
                    className="action-menu-container"
                    style={menuPos}
                    onClick={this.handleContainerClick}
                >
                    <div
                        className="action-menu-widget"
                        style={{ transform: `translate(${popupTransformStyle.x}, ${popupTransformStyle.y})` }}
                    >
                        <div>
                            {
                                this.menu.header &&
                                <div className="action-menu-header">
                                    <div className="action-menu-entry-text">
                                        <T>{this.menu.header}</T>
                                    </div>
                                    {
                                        // todo: add close button for context menu which css not belong to other
                                        // this.props.isResponsive &&
                                        // <div className="modal-close-row">
                                        //     <button className="close-button close-button-white-circle"></button>
                                        // </div>
                                    }
                                </div>
                            }

                            {
                                this.menu.header &&
                                <div className="action-menu-horizontal-line" />
                            }

                            <PerfectScrollbar
                                className="action-menu-content"
                                style={menuSize}
                            >
                                {
                                    this.menu.actions.map((action, i) =>
                                    {
                                        if (!action.label)
                                        {
                                            return null;
                                        }

                                        if (action.label === '-')
                                        {
                                            return (
                                                <div
                                                    key={i}
                                                    className="action-menu-horizontal-line"
                                                />
                                            );
                                        }
                                        else
                                        {
                                            return (
                                                <div
                                                    key={i}
                                                    className={`action-menu-entry ${action.className || ''} ${action.disabled && 'disabled'}`}
                                                    style={action.style}
                                                    onClick={() =>
                                                    {
                                                        this.handleOnClickAction(i);
                                                    }}
                                                >
                                                    {
                                                        <div
                                                            className={'action-menu-entry-icon ' + (action.iconClassName || '')}
                                                            style={action.iconStyle}
                                                        >
                                                            {
                                                                action.icon && typeof action.icon === 'string' ? (
                                                                    <FAIcon
                                                                        type={'regular'}
                                                                        icon={action.icon}
                                                                        size={'1rem'}
                                                                    />
                                                                ) : action.icon
                                                            }
                                                        </div>
                                                    }
                                                    <div className={'action-menu-entry-text'}>
                                                        <div className="ml-ellipsis">
                                                            {action.to ? <Link to={action.to}><T>{action.label}</T></Link> : <T>{action.label}</T>}
                                                        </div>
                                                        {
                                                            action.sub &&
                                                            <div className="action-menu-sub ml-ellipsis">
                                                                <T>{action.sub}</T>
                                                            </div>
                                                        }
                                                    </div>
                                                </div>
                                            );
                                        }
                                    })
                                }
                            </PerfectScrollbar>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ContextMenu.propTypes = {
    onClose: PropTypes.func,
    isCloseOnBlur: PropTypes.bool,
    actions: PropTypes.array,
    isResponsive: PropTypes.bool,
    maxHeight: PropTypes.number,
    width: PropTypes.number,
    header: PropTypes.string
};
