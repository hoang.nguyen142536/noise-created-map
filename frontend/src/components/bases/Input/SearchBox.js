import './SearchBox.scss';

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { FAIcon } from 'components/bases/Icon/FAIcon';
import { EmptyButton } from 'components/bases/Button/Button';
import { FormControl } from 'components/bases/Form/FormControl';

import { Input } from './Input';
import { Container } from 'components/bases/Container/Container';

export const SearchBox = ({ value, onChange, ...rest }) =>
{
    const handleClearText = () =>
    {
        onChange('');
    };

    const handleChangeValue = (value) =>
    {
        onChange(value);
    };

    return (
        <Container {...rest}>
            <FormControl className={'search-box-container'}>
                <Input
                    className={'search-box-control'}
                    type={'text'}
                    autoComplete="off"
                    spellCheck="false"
                    value={value}
                    onChange={handleChangeValue}
                    {...rest}
                />

                <div className="search-box-icon-cover">
                    {
                        value ?
                            <EmptyButton
                                onlyIcon
                                className="search-box-close-icon"
                                icon={'times'}
                                onClick={handleClearText}
                            /> :
                            <FAIcon
                                className="search-box-icon"
                                icon={'search'}
                            />
                    }
                </div>
            </FormControl>
        </Container>
    );
};

SearchBox.propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    autoFocus: PropTypes.bool,
    flex: PropTypes.number
};

SearchBox.defaultProps = {
    flex: 1
};

export default SearchBox;
