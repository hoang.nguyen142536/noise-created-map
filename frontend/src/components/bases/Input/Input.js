import './Input.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Sub1 } from 'components/bases/Text/Text';
import { translate } from 'components/bases/Translate/Translate';

export class Input extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            hidden: true
        };

        this.ref = React.createRef();
    }

    handleChange = (event) =>
    {
        if (typeof this.props.onChange === 'function')
        {
            this.props.onChange(event.target.value);
        }
    };

    render()
    {
        return (
            <>
                <input
                    ref={this.ref}
                    className={`${this.props.className} ${this.props.type === 'range' ? ' slider' : ''}`}
                    type={this.props.type === 'password' ? (this.state.hidden ? 'password' : 'text') : this.props.type}
                    max={this.props.max}
                    min={this.props.min}
                    placeholder={translate(this.props.placeholder)}
                    value={this.props.value}
                    onFocus={this.props.onFocus}
                    autoFocus={this.props.autoFocus}
                    disabled={this.props.disabled}
                    step={this.props.type === 'date' ? undefined : this.props.step}
                    style={this.props.visible?{ width: this.props.width, border: this.props.border, height: this.props.height, color: this.props.color }: { width: this.props.width, border: this.props.border, height: this.props.height, color: this.props.color, display:'none' }}
                    accept={this.props.accept}
                    defaultValue={this.props.defaultValue}
                    readOnly={this.props.readOnly}
                    onChange={this.handleChange}
                    onBlur={this.props.onBlur}
                    onKeyDown={this.props.onKeyDown}
                    onMouseOut={this.props.onMouseOut}
                    onMouseOver={this.props.onMouseOver}
                    onMouseDown={this.props.onMouseDown}
                    onMouseUp={this.props.onMouseUp}
                />
                {
                    this.props.errorText &&
                    <Sub1 color={'danger'}>{this.props.errorText}</Sub1>
                }
            </>
        );
    }
}

Input.propTypes = {
    disabled: PropTypes.bool,
    autoFocus: PropTypes.bool,
    readOnly: PropTypes.bool,
    placeholder: PropTypes.string,
    value: PropTypes.any,
    defaultValue: PropTypes.any,
    min: PropTypes.any,
    max: PropTypes.any,
    step: PropTypes.any,
    width: PropTypes.string,
    height: PropTypes.string,
    className: PropTypes.string,
    border: PropTypes.string,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    type: PropTypes.string,
    accept: PropTypes.string,
    errorText: PropTypes.string,
    onBlur: PropTypes.func,
    onFocusOut: PropTypes.func,
    onKeyDown: PropTypes.func,
    onMouseOut: PropTypes.func,
    onMouseOver: PropTypes.func,
    onMouseDown: PropTypes.func,
    onMouseUp: PropTypes.func,
    visible: PropTypes.bool,
};

Input.defaultProps = {
    disabled: false,
    autoFocus: false,
    readOnly: false,
    placeholder: '',
    step: 1,
    border: 'block',
    width: '100%',
    className: '',
    type: 'text',
    value: '',
    visible: true
};
