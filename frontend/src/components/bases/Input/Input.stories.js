import React, { useState, useEffect } from 'react';
import { action } from '@storybook/addon-actions';

import { Input } from 'components/bases/Input/Input';

export default {
    title: 'Bases/Inputs/Input',
    component: Input,
};

const Template = (args) =>
{
    const [value, setValue] = useState(args.value ? args.value : '');

    function handleChange(value)
    {
        setValue(value);
        (action('onChange'))(value);
    }

    return (
        <Input
            {...args}
            value={value}
            onChange={handleChange}
        />
    );
};

export const Default = Template.bind({});


