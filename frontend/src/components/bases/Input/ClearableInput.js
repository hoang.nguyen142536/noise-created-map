import './ClearableInput.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { T, translate } from 'components/bases/Translate/Translate';
import { FAIcon } from 'components/bases/Icon/FAIcon';
import { Input } from 'components/bases/Form';

export class ClearableInput extends Component
{
    state = {
        hasValue: false
    };
    constructor(props)
    {
        super(props);

        this.ref = React.createRef();
    }

    componentDidMount = () =>
    {
        this.setState({
            hasValue: !!this.props.value
        });
    };

    handleChange = (value) =>
    {
        this.setState({ hasValue: !!value });

        if (typeof this.props.onChange === 'function')
        {
            this.props.onChange(value);
        }
    };
    
    handleClearValueClick = () =>
    {
        this.setState({ hasValue: false });
        this.props.onChange && this.props.onChange();
    };

    render()
    {
        return (
            <div
                style={{ width: '100%' }}
                className="ci-container"
                tabIndex={0}
            >
                <div
                    className="ci-control-container"
                >
                    <Input
                        ref={this.inputRef}
                        className={`input-text  ${this.props.className + (this.props.type === 'range' ? ' slider' : '')}`}
                        type={this.props.type}
                        max={this.props.max}
                        min={this.props.min}
                        placeholder={translate(this.props.placeholder)}
                        onChange={this.handleChange}
                        value={this.props.value || ''}
                        onFocus={this.props.onFocus}
                        autoFocus={this.props.autoFocus}
                        onBlur={this.props.onBlur}
                        disabled={this.props.disabled}
                        step={this.props.type === 'date' ? undefined : this.props.step}
                        style={{ width: this.props.width, border: this.props.border, height: this.props.height }}
                        accept={this.props.accept}
                        onKeyDown={this.props.onKeyDown}
                    />
                    {
                        this.props.clearable && !this.props.disabled && this.state.hasValue &&
                        <div className="clear">
                            <FAIcon
                                icon={'times'}
                                size={'1rem'}
                                onClick={this.handleClearValueClick}
                            />
                        </div>
                    }
                </div>
            </div>
        );
    }
}

ClearableInput.propTypes = {
    disabled: PropTypes.bool,
    autoFocus: PropTypes.bool,
    placeholder: PropTypes.string,
    value: PropTypes.any,
    min: PropTypes.number,
    max: PropTypes.number,
    step: PropTypes.number,
    width: PropTypes.string,
    height: PropTypes.string,
    className: PropTypes.string,
    border: PropTypes.string,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    type: PropTypes.string,
    accept: PropTypes.string,
    onBlur: PropTypes.func,
    onFocusOut: PropTypes.func,
    onKeyDown: PropTypes.func,
    errorText: PropTypes.string,
    clearable: PropTypes.bool
};

ClearableInput.defaultProps = {
    disabled: false,
    autoFocus: false,
    placeholder: '',
    value: '',
    step: 1,
    border: 'block',
    width: '100%',
    className: '',
    type: 'text',
    clearable: false,
    onChange: () =>
    {
    },
    onFocus: () =>
    {
    },
    onBlur: () =>
    {
    },
    onFocusOut: () =>
    {

    },
    onKeyDown: () =>
    {

    }
};
