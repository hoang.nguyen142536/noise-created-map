// import 'index.scss';

// import { storiesOf } from '@storybook/react';
// import React from 'react';

// import { CircleMarker } from 'components/bases/Marker/CircleMarker';
// import { TrackingMarker } from 'components/bases/Marker/TrackingMarker';
// import { WindowScreen } from 'components/bases/WindowScreen/WindowScreen';
// import { WindowPopup } from 'components/bases/WindowScreen/WindowPopup';

// storiesOf('Window', module)
//     .add('Window', () =>
//     {
//         return (
//             <div>
//                 <WindowScreen
//                     width="100%"
//                     height="600px"
//                 />
//             </div>
//         );
//     })
//     .add('CircleMarker', () =>
//     {
//         return (
//             <div style={{ margin: 100 }}>
//                 <CircleMarker
//                     icon="camera"
//                     size={48}
//                     backgroundColor={'rgb(48, 48, 48)'}
//                 />
//                 <CircleMarker
//                     icon="video"
//                     size={48}
//                     backgroundColor={'rgb(48, 48, 48)'}
//                 />
//                 <CircleMarker
//                     icon="car"
//                     size={48}
//                     backgroundColor={'rgb(48, 48, 48)'}
//                 />
//                 <CircleMarker
//                     icon="car-garage"
//                     size={48}
//                     backgroundColor={'rgb(48, 48, 48)'}
//                 />
//                 <CircleMarker
//                     icon="mobile"
//                     size={48}
//                     backgroundColor={'rgb(48, 48, 48)'}
//                 />
//                 <CircleMarker
//                     icon="motorcycle"
//                     size={48}
//                     backgroundColor={'rgb(48, 48, 48)'}
//                 />
//                 <CircleMarker
//                     icon="camera-retro"
//                     size={48}
//                     backgroundColor={'rgb(48, 48, 48)'}
//                 />
//             </div>
//         );
//     })
//     .add('CircleMarkerWithNoti', () =>
//     {
//         return (
//             <div>
//                 <CircleMarker
//                     icon="trash"
//                     isNotify
//                     size={32}
//                     backgroundColor={'blue'}
//                 />
//                 <CircleMarker
//                     icon="trash"
//                     isNotify
//                     size={48}
//                     backgroundColor={'blue'}
//                 />
//                 <CircleMarker
//                     icon="trash"
//                     isNotify
//                     size={96}
//                     backgroundColor={'blue'}
//                 />
//                 <CircleMarker
//                     icon="trash"
//                     isNotify
//                     size={182}
//                     backgroundColor={'blue'}
//                 />
//             </div>
//         );
//     })
//     .add('TrackingMarker', () =>
//     {
//         return (
//             <div>
//                 <TrackingMarker
//                     icon="car"
//                     size={32}
//                     color={'green'}
//                     backgroundColor={'black'}
//                     heading={90}
//                 />
//                 <TrackingMarker
//                     icon="car"
//                     size={48}
//                     color={'green'}
//                     backgroundColor={'black'}
//                     heading={80}
//                 />
//                 <TrackingMarker
//                     icon="car"
//                     size={96}
//                     color={'green'}
//                     backgroundColor={'black'}
//                     heading={90}
//                 />
//                 <TrackingMarker
//                     icon="car"
//                     size={182}
//                     color={'green'}
//                     backgroundColor={'black'}
//                     heading={100}
//                 />
//             </div>
//         );
//     })
//     .add('WindowPopup', () =>
//     {
//         return (
//             <div style={{ position: 'relative', width: '100%', height: '600px', backgroundColor: 'black' }}>
//                 <WindowPopup title="Test title 1">
//                 Content of window popup 1
//                 </WindowPopup>
//                 <WindowPopup title="Test title 2">
//                 Content of window popup 2
//                 </WindowPopup>
//             </div>
//         );
//     });
