import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class Expanded extends Component
{
    render()
    {
        return (
            <div
                className={`${this.props.className}`}
                style={{
                    flex: 1,
                    width: '100%',
                    height: '100%',
                    padding: this.props.padding,
                    margin: this.props.margin
                }}
                {...this.props}
            >
                {this.props.children}
            </div>
        );
    }
}

Expanded.propTypes = {
    className: PropTypes.string,
    padding: PropTypes.string,
    margin: PropTypes.string
};

Expanded.defaultProps = {
    className: '',
    padding: '0 0 0 0',
    margin: '0 0 0 0'
};
