import React from 'react';

import { Expanded } from 'components/bases/Expanded/Expanded';

export default {
    title: 'Bases/Layout/Expanded',
    component: Expanded,
}

const Template = (args) =>
{
    return (
        <Expanded {...args}>
            Content
        </Expanded>
    );
}

export const Default = Template.bind({});



