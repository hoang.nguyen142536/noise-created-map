import React from 'react';

import { Row } from 'components/bases/Row/Row';

export default {
    title: 'Bases/Layout/Row - Column/Row',
    component: Row,
    args: {
        style: { backgroundColor: 'aqua' },
    },
    decorators: [(Story) => <div style={{ width: '50vw', height: '50vh' }}><Story /></div>]
};

const Template = (args) =>
{
    return (
        <Row {...args} >
            <div style={{ backgroundColor: 'aquamarine' }}>
                Pikachu
            </div>
            <div style={{ backgroundColor: 'lime' }}>
                Songoku
            </div>
        </Row>
    );
};

export const Default = Template.bind({});

export const Center = Template.bind({});
Center.args = {
    mainAxisAlignment: 'center',
    crossAxisAlignment: 'center',
};

export const WithMargin = Template.bind({});
WithMargin.args = {
    itemMargin: 'lg',
};
