import './Row.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class Row extends Component
{
    render()
    {
        const { className, mainAxisAlignment, crossAxisAlignment, mainAxisSize, crossAxisSize, itemMargin, ...rest } = this.props;

        return (
            <div
                className={`${className} layout-row main-axis-alignment-${mainAxisAlignment} cross-axis-alignment-${crossAxisAlignment} main-axis-size-${mainAxisSize} cross-axis-size-${crossAxisSize} ${itemMargin !== 'none' ? `item-margin-${itemMargin}` : ''}`}
                {...rest}
            >
                {this.props.children}
            </div>
        );
    }
}

Row.propTypes = {
    className: PropTypes.string,
    style: PropTypes.object,
    mainAxisAlignment: PropTypes.oneOf([
        'start',
        'center',
        'end',
        'space-around',
        'space-between',
        'space-evenly'
    ]),
    crossAxisAlignment: PropTypes.oneOf([
        'start',
        'center',
        'end',
        'baseline',
        'stretch'
    ]),
    mainAxisSize: PropTypes.oneOf(['min', 'max']),
    crossAxisSize: PropTypes.oneOf(['min', 'max']),
    itemMargin: PropTypes.oneOf(['none', 'sm', 'md', 'lg'])
};

Row.defaultProps = {
    className: '',
    mainAxisAlignment: 'start',
    crossAxisAlignment: 'stretch',
    mainAxisSize: 'max',
    crossAxisSize: 'max',
    itemMargin: 'none'
};
