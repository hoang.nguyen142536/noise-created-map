import React, { useState } from 'react';

import { Container } from 'components/bases/Container/Container';
import { PageTitle } from 'components/bases/Page/PageTitle';
import { Row } from 'components/bases/Row/Row';
import { Column } from 'components/bases/Column/Column';


export default {
    title: 'Bases/Layout/Row - Column',
};

export const Default = (args) =>
{
    return (
        <Container className={'admin'}>
            <PageTitle>Page title</PageTitle>
            <Row>
                <Column>
                    Column 1 - Row 1
                </Column>
                <Column>
                    Column 2 - Row 1
                </Column>
            </Row>
            <Row>
                <Column>
                    Column 1 - Row 2
                </Column>
                <Column>
                    Column 2 - Row 2
                </Column>
            </Row>
        </Container>
    );
};

// eslint-disable-next-line react/no-multi-comp
export const Alignment = (args) =>
{
    return (
        <>
            <Container
                style={{ height: '50vh' }}
            >
                <Row
                    mainAxisSize={'max'}
                >
                    <Column
                        mainAxisSize={'max'}
                        mainAxisAlignment={'center'}
                        style={{ backgroundColor: 'grey' }}
                    >
                        Column mainAxisAlignment = center
                    </Column>
                    <Column
                        mainAxisSize={'max'}
                        mainAxisAlignment={'end'}
                        style={{ backgroundColor: 'darkgrey' }}
                    >
                        Column mainAxisAlignment = end
                    </Column>
                </Row>
            </Container>
            <Container style={{ height: '50vh' }}>
                <Row
                    mainAxisSize={'max'}
                    mainAxisAlignment={'space-around'}
                >
                    <Column
                        crossAxisSize={'min'}
                        style={{ backgroundColor: 'darkgrey' }}
                    >
                        Row mainAxisAlignment = space-around
                    </Column>
                    <Column
                        crossAxisSize={'min'}
                        style={{ backgroundColor: 'grey' }}
                    >
                        Row mainAxisAlignment = space-around
                    </Column>
                </Row>
            </Container>
        </>
    );
};
