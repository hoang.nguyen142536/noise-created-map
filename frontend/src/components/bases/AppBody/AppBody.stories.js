import React from 'react';

import { AppBody } from 'components/bases/AppBody/AppBody';

export default {
    title: 'Bases/Layout/AppBody',
    component: AppBody,
}

const Template = (args) =>
{
    return (
        <AppBody>
            AppBody inner elements
        </AppBody>
    )
}

export const Default = Template.bind({});



