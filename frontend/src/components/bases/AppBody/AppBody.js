import './AppBody.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { BorderPanel } from '../Panel/Panel';

export class AppBody extends Component
{
    render()
    {
        return (
            <BorderPanel className={'app-body'} flex={1}>
                {this.props.children}
            </BorderPanel>
        );
    }
}

AppBody.propTypes = {
    className: PropTypes.string
};

AppBody.defaultProps = {
    className: ''
};
