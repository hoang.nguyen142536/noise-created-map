import './InputMoment.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import { T } from 'components/bases/Translate/Translate';

import RangeDateTime from './RangeDateTime';
import { Time } from './Time';
import { Calendar } from './Calendar';

export default class InputMoment extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            tab: 0,
            rangeTime: props.rangeTime || [0, 24],
        };
    }

    handleClickTab = (e, tab) =>
    {
        e.preventDefault();
        this.setState({ tab: tab });
    };

    handleCalendarChange = (value) =>
    {
        const { onChange, showTimeRange } = this.props;
        const { rangeTime } = this.state;

        if (showTimeRange)
        {
            onChange && onChange(value, rangeTime);
        }
        else
        {
            onChange && onChange(value);
        }
    };

    handleRangeTimeChange = (rangeTime) =>
    {
        if (Array.isArray(rangeTime))
        {
            this.props.onChange && this.props.onChange(this.props.moment, rangeTime);
            this.setState({ rangeTime });
        }
    };

    render()
    {
        const { tab } = this.state;

        const {
            className,
            moment: m,
            minStep,
            hourStep,

            showTimeSelect,
            showTimeSelectOnly,
            showTimeRange,

            disabled,
            minDate,
            maxDate,
            minTime,
            maxTime,
        } = this.props;

        const cls = cx('m-input-moment', className);

        return (
            <div className={cls}>
                {
                    showTimeSelect && !showTimeSelectOnly &&
                    <div className="options">
                        <button
                            type="button"
                            className={cx('ion-calendar im-btn', { 'active': tab === 0 || tab === 'month' || tab === 'year' })}
                            disabled={disabled}
                            onClick={(e) => this.handleClickTab(e, 0)}
                        >
                            <T>Ngày</T>
                        </button>
                        <button
                            type="button"
                            className={cx('ion-clock im-btn', { 'active': tab === 1 })}
                            disabled={disabled}
                            onClick={(e) => this.handleClickTab(e, 1)}
                        >
                            <T>Giờ</T>
                        </button>
                    </div>
                }
                <div className="tabs">
                    {
                        (!showTimeSelectOnly && tab === 0) &&
                        <Calendar
                            className={'tab'}
                            value={m}
                            onChange={this.handleCalendarChange}
                            disabled={disabled}
                            minDate={minDate}
                            maxDate={maxDate}
                        />
                    }
                    {
                        (showTimeSelectOnly || (showTimeSelect && tab === 1)) &&
                        <Time
                            className={'tab'}
                            moment={m}
                            minStep={minStep}
                            hourStep={hourStep}
                            onChange={this.props.onChange}
                            minTime={minTime}
                            maxTime={maxTime}
                        />
                    }
                    {
                        showTimeRange &&
                        <RangeDateTime
                            rangeTime={this.state.rangeTime}
                            onAfterChange={this.handleRangeTimeChange}
                        />
                    }
                </div>
            </div>
        );
    }
}

InputMoment.propTypes = {
    className: PropTypes.string,

    moment: PropTypes.object,
    rangeTime: PropTypes.arrayOf(PropTypes.number),

    minStep: PropTypes.number,
    hourStep: PropTypes.number,

    showTimeSelect: PropTypes.bool,
    showTimeSelectOnly: PropTypes.bool,
    showTimeRange: PropTypes.bool,

    minDate: PropTypes.any,
    maxDate: PropTypes.any,
    minTime: PropTypes.any,
    maxTime: PropTypes.any,
    onChange: PropTypes.func,
};

InputMoment.defaultProps = {
    minStep: 1,
    hourStep: 1,
    showTimeSelect: false,
    showTimeSelectOnly: false
};

