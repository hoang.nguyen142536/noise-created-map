import './Calendar.scss';

import React from 'react';
import PropTypes from 'prop-types';

export const Month = (props) =>
{
    const classArrays = ['month-item'];
    if (props.selected)
    {
        classArrays.push('current-value');
    }
    if (!props.valid)
    {
        classArrays.push('invalid');
    }

    const handleClick = () =>
    {
        if (!props.valid)
        {
            return;
        }
        props.onClick();
    };

    return (
        <td className={classArrays.join(' ')} onClick={handleClick}>
            <div className={'month-content'}>
                {props.label}
            </div>
        </td>
    );
};

Month.propTypes = {
    selected: PropTypes.bool,
    valid: PropTypes.bool,
    label: PropTypes.node
};

export default Month;
