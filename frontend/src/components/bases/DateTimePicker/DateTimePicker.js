import './DateTimePicker.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import InputMoment from './InputMoment';
import { translate } from 'components/bases/Translate/Translate';
import { FAIcon } from 'components/bases/Icon/FAIcon';
import { PopOver } from 'components/bases/Modal/PopOver';

export class DateTimePicker extends Component
{
    state = {
        isVisible: false,
        value: null,
        rangeTime: [0, 24],
    };

    constructor(props)
    {
        super(props);
        this.inputRef = React.createRef();
        if (props.rangeTime && props.rangeTime.length === 2)
        {
            this.rangeTime = [props.rangeTime[0], props.rangeTime[1]];
        }
    }

    componentDidMount()
    {
        if (this.props.value)
        {
            this.setState({ value: moment(this.props.value) });
        }
        else
        {
            // this.setState({ value: moment() });
        }
    }

    // DateTimePicker received a new date whenever Edit Form was called, but have no method to set the new date to the input box, so here it is
    static getDerivedStateFromProps = (nextProps, prevState) =>
    {
        if (!nextProps.value)
        {
            return { value: null };
        }

        if (moment(nextProps.value).diff(prevState.value, 'minutes') !== 0 && nextProps.value)
        {
            return { value: moment(nextProps.value) }; // return new state
        }

        return null; // don't change state
    };

    closeControl = () => this.setState({ isVisible: false });

    handleControlClick = () =>
    {
        this.setState({ isVisible: !this.state.isVisible, keyPressValue: -1 });
    };

    onBlur = () =>
    {
        const { isVisible } = this.state;

        if (isVisible)
        {
            this.closeControl();
        }
    };

    handleChange = (val, rangeTime = null) =>
    {
        if (!this.props.showTimeSelect && !this.props.showTimeSelectOnly)
        {
            val = val.startOf('date');
        }

        this.setState({ value: val, rangeTime });
        this.props.onChange && this.props.onChange(val, rangeTime);
    };

    formatValueControl = () =>
    {
        if (!this.state.value)
        {
            return '';
        }

        let valueFormat = '';

        if (this.props.showTimeSelectOnly)
        {
            valueFormat = this.state.value.format('LT');
        }
        else
        {
            if (this.props.showTimeRange)
            {
                const values = [];
                this.state.rangeTime.forEach((time) =>
                {
                    const dateAdjust = (time > 24) ? this.state.value.get('date') + 1 : (time < 0 ? this.state.value.get('date') - 1 : this.state.value.get('date'));
                    const timeAdjust = (time > 24) ? time - 24 : (time < 0 ? time + 24 : time);

                    values.push(new moment().set('date', dateAdjust).hour(timeAdjust).minute(0).format('L hA'));
                });

                valueFormat = values.join(' - ');

                console.log(valueFormat);
            }
            else if (this.props.showTimeSelect)
            {
                valueFormat = this.state.value.format('L LT');
            }
            else
            {
                valueFormat = this.state.value.format('L');
            }
        }

        return valueFormat;
    };

    handleClearValueClick = (e) =>
    {
        e.stopPropagation();
        this.setState({ value: null });
        this.props.onChange && this.props.onChange();
    };

    render()
    {
        const valueFormat = this.formatValueControl();

        return (
            <div
                style={{ width: '100%' }}
                className="dtp-container"
                tabIndex={0}
            >
                <div
                    className="dtp-control-container"
                    onClick={this.handleControlClick}
                >
                    <input
                        className={`input-text ${this.props.className}`}
                        placeholder={translate(this.props.placeholder)}
                        ref={this.inputRef}
                        type="text"
                        value={valueFormat}
                        disabled={this.props.disabled}
                        readOnly
                    />
                    {
                        this.props.clearable && !this.props.disabled && valueFormat &&
                        <div className="clear">
                            <FAIcon
                                icon={'times'}
                                size={'1rem'}
                                onClick={this.handleClearValueClick}
                            />
                        </div>
                    }
                    {
                        !this.props.showTimeSelectOnly &&
                        <div className="calendar">
                            <FAIcon
                                icon={'calendar-alt'}
                                size={'1rem'}
                                onClick={this.handleControlClick}
                            />
                        </div>
                    }
                </div>
                {
                    this.state.isVisible && !this.props.disabled &&
                    <PopOver
                        anchorEl={this.inputRef}
                        onBackgroundClick={this.onBlur}
                        width={'20rem'}
                    >
                        <InputMoment
                            moment={this.state.value}
                            onChange={this.handleChange}
                            rangeTime={this.state.rangeTime}
                            minStep={1} // default
                            hourStep={1} // default
                            showTimeSelect={this.props.showTimeSelect && !this.props.showTimeRange}
                            showTimeSelectOnly={this.props.showTimeSelectOnly && !this.props.showTimeRange}
                            showTimeRange={this.props.showTimeRange}
                            minDate={this.props.minDate && moment(this.props.minDate)}
                            maxDate={this.props.maxDate && moment(this.props.maxDate)}
                            minTime={this.props.minTime && moment(this.props.minTime)}
                            maxTime={this.props.maxTime && moment(this.props.maxTime)}
                        />
                    </PopOver>
                }
            </div>
        );
    }
}

DateTimePicker.propTypes = {
    className: PropTypes.string,
    onChange: PropTypes.func,
    value: PropTypes.any,
    rangeTime: PropTypes.arrayOf(PropTypes.number),
    disabled: PropTypes.bool,
    showTimeSelect: PropTypes.bool,
    showTimeSelectOnly: PropTypes.bool,
    showTimeRange: PropTypes.bool,
    clearable: PropTypes.bool,
    placeholder: PropTypes.string,
    minDate: PropTypes.any,
    maxDate: PropTypes.any,
    minTime: PropTypes.any,
    maxTime: PropTypes.any
};

DateTimePicker.defaultProps = {
    className: '',
    onChange: () =>
    {
    },
    disabled: false,
    clearable: false,
    showTimeRange: false
};
