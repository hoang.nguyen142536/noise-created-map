import './Calendar.scss';

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import range from 'lodash/range';
import chunk from 'lodash/chunk';

import { FAIcon } from 'components/bases/Icon/FAIcon';

const Year = (props) =>
{
    const classArrays = ['year-item'];
    props.selected && classArrays.push('current-value');
    !props.valid && classArrays.push('invalid');

    const handleClick = () => props.valid && props.onClick();

    return (
        <td className={classArrays.join(' ')} onClick={handleClick}>
            <div className={'year-content'}>
                {props.label}
            </div>
        </td>
    );
};

export function YearPicker(props)
{
    const getYears = (currentYear) =>
    {
        return [].concat(
            range(currentYear - 7, currentYear),
            [currentYear],
            range(currentYear + 1, currentYear + 8)
        );
    };

    const currentValue = props.value?.clone() || moment();

    const [center, setCenter] = useState(currentValue.get('year'));
    const [years, setYears] = useState(getYears(center) || []);

    const handleClick = (year) =>
    {
        const newMoment = currentValue.set('year', year);
        props.onChange && props.onChange(newMoment);
    };

    const prevBatch = () =>
    {
        setCenter(center - 15);
        setYears(getYears(center - 15));
    };

    const nextBatch = () =>
    {
        setCenter(center + 15);
        setYears(getYears(center + 15));
    };

    const isMinValid = (y) =>
    {
        return !props.minDate || props.minDate.get('year') <= y;
    };

    const isMaxValid = (y) =>
    {
        return !props.maxDate || props.maxDate.get('year') >= y;
    };

    return (
        <div className={`m-calendar ${props.className}`}>
            <div className="toolbar">
                <button
                    type="button"
                    className="prev-month"
                    onClick={prevBatch}
                >
                    <FAIcon type='solid' icon='angle-left' />
                </button>
                <button
                    type="button"
                    className="next-month"
                    onClick={nextBatch}
                >
                    <FAIcon type='solid' icon='angle-right' />
                </button>
            </div>

            <table>
                <tbody>
                    {
                        chunk(years, 3).map((row, row_index) =>
                            <tr key={row_index}>
                                {
                                    row.map((i) =>
                                        <Year
                                            key={i}
                                            label={i}
                                            valid={isMinValid(i) && isMaxValid(i)}
                                            selected={i === currentValue.get('year')}
                                            onClick={() => handleClick(i)}
                                        />
                                    )}
                            </tr>
                        )}
                </tbody>
            </table>
        </div>
    );
}

YearPicker.propTypes = {
    className: PropTypes.string,
    value: PropTypes.any,
    onChange: PropTypes.func,
    minDate: PropTypes.any,
    maxDate: PropTypes.any
};

export default YearPicker;
