import React from 'react';
import PropTypes from 'prop-types';
import chunk from 'lodash/chunk';

import moment from 'moment';

import { Month } from './Month';

export function MonthPicker(props)
{
    const currentValue = props.value?.clone() || moment();

    const handleClick = (month_index) =>
    {
        const newMoment = currentValue.clone().set('month', month_index);
        props.onChange && props.onChange(newMoment);
    };

    const isMinValid = (m) =>
    {
        return !props.minDate || (m && m.isSameOrAfter(props.minDate, 'month'));
    };

    const isMaxValid = (m) =>
    {
        return !props.maxDate || (m && m.isSameOrBefore(props.maxDate, 'month'));
    };

    const isCurrent = (m) =>
    {
        return m && m.isSame(currentValue, 'month');
    };

    return (
        <div className={`m-calendar ${props.className}`}>
            <table>
                <tbody>
                    {/* Month list */}
                    {
                        chunk(Array(12), 3).map((row, row_index) =>
                            <tr key={row_index}>
                                {
                                    row.map((i, col_index) =>
                                    {
                                        const monthIndex = row_index * 3 + col_index;
                                        const current = currentValue.clone().set('month', monthIndex);

                                        return (
                                            <Month
                                                key={col_index}
                                                label={current.format('MMMM')}
                                                valid={isMinValid(current) && isMaxValid(current)}
                                                selected={isCurrent(current)}
                                                onClick={() => handleClick(monthIndex)}
                                            />
                                        );
                                    })}
                            </tr>
                        )}
                </tbody>
            </table>
        </div>
    );
}

MonthPicker.propTypes = {
    className: PropTypes.string,
    value: PropTypes.any,
    onChange: PropTypes.func,
    minDate: PropTypes.any,
    maxDate: PropTypes.any
};

export default MonthPicker;
