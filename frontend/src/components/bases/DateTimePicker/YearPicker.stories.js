import React, { useState } from 'react';
import { action } from '@storybook/addon-actions';

import { YearPicker } from 'components/bases/DateTimePicker/YearPicker';

export default {
    title: 'Bases/Inputs/Year Picker',
    component: YearPicker,
};

const Template = (args) =>
{
    const [value, setValue] = useState();

    const handleChange = (value) =>
    {
        setValue(value);
        (action('onChange'))(value);
    };

    return (
        <YearPicker {...args} value={value} onChange={handleChange} />
    );
};

export const Default = Template.bind({});
