import React, { useState } from 'react';
import { action } from '@storybook/addon-actions';

import { Calendar } from 'components/bases/DateTimePicker/Calendar';

export default {
    title: 'Bases/Inputs/Calendar',
    component: Calendar
};

const Template = (args) =>
{
    const [value, setValue] = useState();

    const handleChange = (value) =>
    {
        setValue(value);
        (action('onChange'))(value);
    };

    return (
        <Calendar {...args} value={value} onChange={handleChange} />
    );
};

export const Default = Template.bind({});

export const HighLight = Template.bind({});
HighLight.args = {
    highlightDates: ['2021-01-11', '2021-01-12']
};
