import './RangeDateTime.scss';

import React from 'react';
import PropTypes from 'prop-types';

import { Slider } from 'components/bases/Slider/Slider';

const RangeDateTime = (props) =>
{
    const { rangeTime } = props;

    const startTime = (props.rangeTime && props.rangeTime.length === 2) ? props.rangeTime[0] : 0;
    const endTime = (props.rangeTime && props.rangeTime.length === 2) ? props.rangeTime[1] : 24;

    const dateDisplay = (time) => (time >= 24) ? 'Tomorrow' : (time < 0 ? 'Yesterday' : 'Today');
    const timeDisplay = (time) =>
    {
        const adjustedTime = (time > 24) ? time - 24 : (time < 0 ? time + 24 : time);
        if (adjustedTime === 24 || adjustedTime === 0)
        {
            return '12 am';
        }
        if (adjustedTime === 12)
        {
            return '12 pm';
        }
        return (adjustedTime < 12) ? adjustedTime + ' am' : (adjustedTime - 12) + ' pm';
    };

    const displayMap = {};
    for (let i = 0; i <= 40; i++)
    {
        displayMap[i * 2.5] = `${timeDisplay(i - 8)}`;
    }

    return (
        <div className={'range-date-time-container'}>
            <Slider
                range
                max={32}
                min={-8}
                value={rangeTime}
                thumbSize={'12px'}
                rangeSize={'3px'}
                step={1}
                typeMark={'line'}
                onAfterChange={props.onAfterChange}
                showIndicator
                displayMap={displayMap}
            />
            <div className={'range-date-time-display'}>
                <div>
                    {dateDisplay(startTime)} {timeDisplay(startTime)}
                </div>
                <div>-</div>
                <div>
                    {dateDisplay(endTime)} {timeDisplay(endTime)}
                </div>
            </div>
        </div>
    );
};


RangeDateTime.propTypes = {
    rangeTime: PropTypes.array,
    onAfterChange: PropTypes.func,
};

RangeDateTime.defaultProps = {
    rangeTime: [0, 24],
    onAfterChange: () =>
    { }
};

export default RangeDateTime;
