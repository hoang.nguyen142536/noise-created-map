import './Time.scss';

import React, { Component } from 'react';
import cx from 'classnames';
import InputSlider from 'react-input-slider';

import { T } from 'components/bases/Translate/Translate';


export class Time extends Component
{

    constructor(props)
    {
        super(props);
        this.state = {
            displayMoment: props.moment.clone(),
        };
    }

    changeHours = (hour) =>
    {
        const m = this.state.displayMoment.clone();
        m.hours(hour);
        this.setState({ displayMoment: m });
    };

    changeMinutes = (minute) =>
    {
        const m = this.state.displayMoment.clone();
        m.minutes(minute);
        this.setState({ displayMoment: m });
    };

    onMouseUp = () =>
    {
        this.props.onChange(this.state.displayMoment);
    };

    componentDidMount()
    {
        document.addEventListener('mouseup', this.onMouseUp);
    }

    componentWillUnmount()
    {
        document.removeEventListener('mouseup', this.onMouseUp);
    }

    render()
    {
        const m = this.state.displayMoment;

        const minHour = this.props.minTime?.hour() || 0;
        let minMin = 0;
        if (this.props.minTime && m.hour() === minHour)
        {
            minMin = this.props.minTime.minute();
            if (minMin > m.get('minute'))
            {
                this.changeMinutes(minMin);
            }
        }

        const maxHour = this.props.maxTime?.hour() || 23;
        let maxMin = 59;
        if (this.props.maxTime && m.hour() === maxHour)
        {
            maxMin = this.props.maxTime.minute();
            if (maxMin < m.get('minute'))
            {
                this.changeMinutes(maxMin);
            }
        }

        return (
            <div className={cx('m-time', this.props.className)}>
                <div className="showtime">
                    <span className="time">{m.format('HH')}</span>
                    <span className="separater">:</span>
                    <span className="time">{m.format('mm')}</span>
                </div>

                <div className="sliders">
                    <div className="time-text"><T>Giờ</T>:</div>
                    <InputSlider
                        className="u-slider-time"
                        xmin={minHour}
                        xmax={maxHour}
                        xstep={this.props.hourStep}
                        x={m.hour()}
                        onChange={(pos) => this.changeHours(pos.x)}
                    />
                    <div className="time-text"><T>Phút</T>:</div>
                    <InputSlider
                        className="u-slider-time"
                        xmin={minMin}
                        xmax={maxMin}
                        xstep={this.props.minStep}
                        x={m.minute()}
                        onChange={(pos) => this.changeMinutes(pos.x)}
                    />
                </div>
            </div>
        );
    }
}

