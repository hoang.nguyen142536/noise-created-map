import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

export const Day = (props) =>
{
    const { day, week, currentValue, className, valid, onClick } = props;

    const prevMonth = week === 0 && day > 7;
    const nextMonth = week >= 4 && day <= 14;

    const cls = cx({
        'prev-month': prevMonth,
        'next-month': nextMonth,
        'current-value': currentValue,
        'invalid': !valid
    });

    const handleClick = () =>
    {
        valid && onClick && onClick();
    };

    return (
        <td
            className={cls}
            onClick={handleClick}
        >
            <div className={'day-content ' + className}>
                {day}
            </div>
        </td>
    );
};

Day.propTypes = {
    day: PropTypes.number,
    week: PropTypes.number,
    currentValue: PropTypes.bool,
    valid: PropTypes.bool,
    onClick: PropTypes.func,
};

export default Day;
