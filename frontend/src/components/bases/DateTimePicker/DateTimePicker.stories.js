import React, { useState } from 'react';
import { action } from '@storybook/addon-actions';

import { DateTimePicker } from 'components/bases/DateTimePicker/DateTimePicker';

export default {
    title: 'Bases/Inputs/Datetime Picker',
    component: DateTimePicker,
};

const Template = (args) =>
{
    const [value, setValue] = useState();
    const [rangeTime, setRangeTime] = useState([]);

    const handleChange = (value, rangeTime) =>
    {
        setValue(value);
        if (rangeTime)
        {
            setRangeTime(rangeTime);
        }
        
        (action('onChange'))(value);
    };

    return (
        <DateTimePicker
            {...args}
            value={value}
            onChange={handleChange}
            rangeTime={rangeTime}
        />
    );
};

export const Default = Template.bind({});

export const RangeDateTime = Template.bind({});
RangeDateTime.args = {
    showTimeRange: true
};
