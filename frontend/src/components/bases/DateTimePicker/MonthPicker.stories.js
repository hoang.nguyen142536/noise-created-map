import React, { useState } from 'react';
import { action } from '@storybook/addon-actions';

import { MonthPicker } from 'components/bases/DateTimePicker/MonthPicker';

export default {
    title: 'Bases/Inputs/Month Picker',
    component: MonthPicker,
};

const Template = (args) =>
{
    const [value, setValue] = useState();

    const handleChange = (value) =>
    {
        setValue(value);
        (action('onChange'))(value);
    };

    return (
        <MonthPicker {...args} value={value} onChange={handleChange} />
    );
};

export const Default = Template.bind({});
