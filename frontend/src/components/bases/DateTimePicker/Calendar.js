import './Calendar.scss';

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import cx from 'classnames';
import chunk from 'lodash/chunk';
import range from 'lodash/range';

import { FAIcon } from 'components/bases/Icon/FAIcon';
import { T } from 'components/bases/Translate/Translate';

import { Day } from './Day';
import MonthPicker from 'components/bases/DateTimePicker/MonthPicker';
import YearPicker from 'components/bases/DateTimePicker/YearPicker';

export function Calendar(props)
{
    const currentValue = props.value?.clone() || moment();
    const [display, setDisplay] = useState(currentValue);
    const [picker, setPicker] = useState('date');

    useEffect(() =>
    {
        setDisplay(currentValue);
    }, [props.value]);

    const getDate = (i, w) =>
    {
        const prevMonth = w === 0 && i > 7;
        const nextMonth = w >= 4 && i <= 14;

        const m = display.clone();

        if (prevMonth)
        {
            m.subtract(1, 'month');
        }
        if (nextMonth)
        {
            m.add(1, 'month');
        }

        m.date(i);

        return m;
    };

    const selectDate = (i, w) =>
    {
        const m = getDate(i, w);
        props.onChange && props.onChange(m);
    };

    const prevMonth = (e) =>
    {
        e.preventDefault();

        const value = display.clone().subtract(1, 'month');

        setDisplay(value);
        props.onMonthYearChange && props.onMonthYearChange(value);
    };

    const nextMonth = (e) =>
    {
        e.preventDefault();

        const value = display.clone().add(1, 'month');

        setDisplay(value);
        props.onMonthYearChange && props.onMonthYearChange(value);
    };

    const nextYear = () =>
    {
        const value = display.clone().add(1, 'year');

        setDisplay(value);
        props.onMonthYearChange && props.onMonthYearChange(value);
    };

    const prevYear = () =>
    {
        const value = display.clone().subtract(1, 'year');

        setDisplay(value);
        props.onMonthYearChange && props.onMonthYearChange(value);
    };

    const handleChangeDisplay = (value) =>
    {
        setPicker('date');

        setDisplay(value);
        props.onMonthYearChange && props.onMonthYearChange(value);
    };

    const isMinValid = (m, minMoment) =>
    {
        return !minMoment || (m && m.isSameOrAfter(minMoment, 'day'));
    };

    const isMaxValid = (m, maxMoment) =>
    {
        return !maxMoment || (m && m.isSameOrBefore(maxMoment, 'day'));
    };

    const d1 = display.clone().subtract(1, 'month').endOf('month').date();
    const d2 = display.clone().date(1).day();
    const d3 = display.clone().endOf('month').date();

    const days = [].concat(
        range(d1 - d2 + 1, d1 + 1),
        range(1, d3 + 1),
        range(1, 42 - d3 - d2 + 1)
    );

    const weeks = ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'];

    const disabled = props.disabled;

    return (
        <>
            {
                (picker === 'date') &&
                <div className={cx('m-calendar', props.className)}>
                    <div className="toolbar">
                        <div className="button-group">
                            <button
                                type="button"
                                className="prev-month"
                                disabled={disabled}
                                onClick={prevYear}
                            >
                                <FAIcon icon='angle-double-left' />
                            </button>
                            <button
                                type="button"
                                className="prev-month"
                                disabled={disabled}
                                onClick={prevMonth}
                            >
                                <FAIcon icon='angle-left' />
                            </button>
                        </div>
                        <span className="current-date">
                            <button onClick={() => setPicker('month')}>{display.format('MMMM')}</button>
                            <button onClick={() => setPicker('year')}>{display.format('YYYY')}</button>
                        </span>
                        <div className="button-group">
                            <button
                                type="button"
                                className="next-month"
                                disabled={disabled}
                                onClick={nextMonth}
                            >
                                <FAIcon icon='angle-right' />
                            </button>
                            <button
                                type="button"
                                className="prev-month"
                                disabled={disabled}
                                onClick={nextYear}
                            >
                                <FAIcon icon='angle-double-right' />
                            </button>
                        </div>
                    </div>

                    <table>
                        <thead>
                            <tr>
                                {weeks.map((w, i) => <td key={i}><T>{w}</T></td>)}
                            </tr>
                        </thead>

                        <tbody>
                            {
                                chunk(days, 7).map((row, week) =>
                                    <tr key={week}>
                                        {
                                            row.map((day) =>
                                            {
                                                let isHighlighted = false;
                                                const current = getDate(day, week);

                                                if (Array.isArray(props.highlightDates) && props.highlightDates.find((x) => moment(x).format('DD/MM/YYYY') === current.format('DD/MM/YYYY')))
                                                {
                                                    isHighlighted = true;
                                                }

                                                return (
                                                    <Day
                                                        className={isHighlighted ? 'highlight' : ''}
                                                        key={current.format('DD-MM-YYYY')}
                                                        day={day}
                                                        week={week}
                                                        valid={isMaxValid(current, props.maxDate) && isMinValid(current, props.minDate)}
                                                        currentValue={current.isSame(currentValue, 'day')}
                                                        onClick={() => selectDate(day, week)}
                                                    />
                                                );
                                            })}
                                    </tr>
                                )}
                        </tbody>
                    </table>
                </div>
            }
            {
                (picker === 'month') &&
                <MonthPicker
                    // className={'tab'}
                    value={display}
                    onChange={handleChangeDisplay}
                    minDate={props.minDate}
                    maxDate={props.maxDate}
                />
            }
            {
                (picker === 'year') &&
                <YearPicker
                    // className={'tab'}
                    value={display}
                    onChange={handleChangeDisplay}
                    minDate={props.minDate}
                    maxDate={props.maxDate}
                />
            }
        </>
    );
}

Calendar.propTypes = {
    className: PropTypes.string,
    value: PropTypes.any,
    onChange: PropTypes.func,
    onMonthYearChange: PropTypes.func,
    highlightDates: PropTypes.array,
    disabled: PropTypes.bool,
    minDate: PropTypes.any,
    maxDate: PropTypes.any
};

