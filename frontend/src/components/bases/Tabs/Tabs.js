import './Tabs.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { T } from 'components/bases/Translate/Translate';

export class Tabs extends Component
{
    render()
    {
        const tabs = [];
        React.Children.forEach(this.props.children, (child) =>
        {
            if (child.type === Tab)
            {
                tabs.push(child);
            }
        });

        return (
            <div className={'tab-control'}>
                <div className={'tab-header-wrap'}>
                    <div className={'tab-header'}>
                        {tabs.map((child) =>
                            <Tab
                                key={child.props.id}
                                {...child.props}
                                flex={this.props.flexHeader}
                                active={child.props.id === this.props.selected}
                                onClick={(id) => this.props.onSelect(id)}
                            />
                        )}
                    </div>
                </div>

                <div className={'tab-content'}>
                    {
                        tabs.map((child) =>
                        {
                            const active = child.props.id === this.props.selected;

                            return (
                                <TabContent
                                    key={child.props.id}
                                    {...child.props}
                                    active={active}
                                >
                                    {child.props.children}
                                </TabContent>
                            );
                        })
                    }
                </div>
            </div>
        );
    }
}

Tabs.propTypes = {
    className: PropTypes.string,
    flexHeader: PropTypes.bool,
    onSelect: PropTypes.func,
    selected: PropTypes.string,
};

Tabs.defaultProps = {
    className: '',
    flexHeader: true,
    onSelect: () =>
    {
    }
};

export class Tab extends Component
{
    render()
    {
        const style = this.props.flex ? {} : { flex: 'none' };

        return (
            <div
                className={`tab-item-header ${this.props.active ? 'active' : ''}`}
                style={style}
                onClick={(e) => this.props.onClick(this.props.id)}
            >
                <span>
                    <T>{this.props.title}</T>
                </span>
            </div>
        );
    }
}

Tab.propTypes = {
    className: PropTypes.string,
    flex: PropTypes.bool,
    onClick: PropTypes.func,
    active: PropTypes.bool,
    title: PropTypes.string,
};

Tab.defaultProps = {
    className: '',
    flex: true,
    onClick: () =>
    {
    }
};

class TabContent extends Component
{
    render()
    {
        return <div className={`tab-item-content ${this.props.active ? 'active' : ''}`}>{this.props.children}</div>;
    }
}

TabContent.propTypes = {
    active: PropTypes.bool,
}
