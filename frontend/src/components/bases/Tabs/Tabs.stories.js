import React, { useState } from 'react';
import { action } from '@storybook/addon-actions';

import { Tabs, Tab } from 'components/bases/Tabs/Tabs';

export default {
    title: 'Bases/Navigation/Tabs',
    component: Tabs,
    subcomponents: { Tab },
    args: {}
};

const Template = (args) =>
{
    const [selected, setSelected] = useState('tab_1');

    const onSelectEventHandler = (tab_id) =>
    {
        setSelected(tab_id);
        (action('onSelect'))(tab_id);
    };

    return (
        <Tabs
            {...args}
            onSelect={onSelectEventHandler}
            selected={selected}
        >
            <Tab
                id='tab_1'
                title='Pikachu'
            >
                Thunderbolt
            </Tab>
            <Tab
                id='tab_2'
                title='Songoku'
            >
                Yellow hair
            </Tab>
        </Tabs>
    );
};

export const Default = Template.bind({});
