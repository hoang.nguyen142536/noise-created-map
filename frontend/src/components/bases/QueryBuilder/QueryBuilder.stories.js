import React, { useState, useEffect } from 'react';
import { action } from '@storybook/addon-actions';

import { QueryBuilderGroup } from 'components/bases/QueryBuilder/QueryBuilderGroup';
import { Container } from 'components/bases/Container/Container';
import { ThemeProvider } from '../Theme/ThemeContext';

export default {
    title: 'Bases/QueryBuilder',
    component: QueryBuilderGroup,
    args: {}
};

const queryData = {
    condition: '',
    layer: '',
    no: '',
};

const props = [
    {
        ColumnName: 'column_1',
        DisplayName: 'Column 1',
        DataType: 1,
    },
    {
        ColumnName: 'column_2',
        DisplayName: 'Column 2',
        DataType: 3,
    },
    {
        ColumnName: 'column_3',
        DisplayName: 'Column 3',
        DataType: 3,
    },
];

const Template = (args) =>
{
    return (
        <Container>
            <QueryBuilderGroup
                {...args}
                deleteGroup={action('onDeleteGroup')}
                deleteRule={action('onDeleteRule')}
            />
        </Container>
    );
};

export const Default = Template.bind({});
Default.args = {
    queryData,
    props,
};
