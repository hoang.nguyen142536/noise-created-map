import React, { useState } from 'react';
import { action } from '@storybook/addon-actions';

import { CheckBox } from 'components/bases/CheckBox/CheckBox';

export default {
    title: 'Bases/Inputs/Checkbox',
    component: CheckBox,
    subcomponents: { CheckBox },
};

const Template = (args) =>
{
    const [checked, setChecked] = useState(false);
    const onChangeEventHandler = (value) =>
    {
        setChecked(value);
        (action('onChangeEventHandler'))();
    };

    return (
        <CheckBox
            {...args}
            checked={checked}
            onChange={onChangeEventHandler}
        />
    );
};

export const Default = Template.bind({});
