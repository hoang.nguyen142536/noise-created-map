import './CheckBox.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { FAIcon } from 'components/bases/Icon/FAIcon';
import { T } from 'components/bases/Translate/Translate';

export class CheckBox extends Component
{
    handleChange = () =>
    {

        if (!this.props.disabled && this.props.onChange)
        {
            this.props.onChange(!this.props.checked);
        }
    };

    render()
    {
        const { className, displayAs, checked, label, disabled, indeterminate } = this.props;

        return (
            <div className={`${className || ''} checkbox-form ${disabled ? 'disabled' : ''}`}>
                <button
                    className={`checkbox-btn ${displayAs === 'checkbox' ? '' : 'radio-btn'} ${checked ? 'checked' : ''} ${disabled ? 'disabled' : ''}`}
                    onClick={this.handleChange}
                >
                    {
                        checked ?
                            <FAIcon
                                icon={displayAs === 'checkbox' ? (indeterminate ? 'square' : 'check') : 'dot-circle'}
                                type={'solid'}
                            /> :
                            <FAIcon
                                color={'var(--primary)'}
                            />
                    }
                </button>

                <div
                    className='checkbox-label'
                    onClick={this.handleChange}
                >
                    <T>{label}</T>
                </div>
            </div>
        );
    }
}

CheckBox.propTypes = {
    className: PropTypes.string,
    label: PropTypes.string,
    checked: PropTypes.bool,
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    displayAs: PropTypes.oneOf(['checkbox', 'radio']),
    indeterminate: PropTypes.bool
};

CheckBox.defaultProps = {
    displayAs: 'checkbox',
    className: '',
    label: '',
    checked: false,
    disabled: false
};

