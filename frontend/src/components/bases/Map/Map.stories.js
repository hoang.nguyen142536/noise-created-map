import React from 'react';
import { action } from '@storybook/addon-actions';

import Map from 'components/bases/Map/Map';

export default {
    title: 'Bases/Map',
    component: Map,
};

const Template = (args) =>
{
    return (
        <div style={{ width: '100%', height: '95vh' }}><Map {...args} onViewportChange={action('onViewportChange')} /></div>
    );
}

export const Default = Template.bind({});
Default.args = {
    center: { lng: 106.6029738547868, lat: 10.754634350198572 },
    zoomLevel: [12.5],
    isMainMap: true,
};
