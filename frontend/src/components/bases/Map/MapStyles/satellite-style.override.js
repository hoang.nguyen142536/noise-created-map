const override = {
    'layers': [
        {
            'id': 'label-vnpoi-police-index',
            'paint': {
                'text-color': '#FFB200',
                'text-halo-color': '#000',
                'text-halo-width': 1.4
            }
        }
    ]
};

export default override;
