const override = {
    'layers': [
        {
            'id': 'label-vnpoi-police-index',
            'paint': {
                'text-halo-width': 0.3
            }
        }
    ]
};

export default override;
