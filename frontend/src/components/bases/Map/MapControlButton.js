import './MapControlButton.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'components/bases/Button/Button';

export class MapControlButton extends Component
{
    render()
    {
        return (
            <Button
                className={`map-control-button ${this.props.active ? 'active' : ''}`}
                onClick={this.props.onClick}
                icon={this.props.icon}
                onlyIcon
            />
        );
    }
}

MapControlButton.propTypes = {
    className: PropTypes.string,
    active: PropTypes.bool,
    onClick: PropTypes.func,
    icon: PropTypes.string
};

MapControlButton.defaultProps = {
    className: ''
};
