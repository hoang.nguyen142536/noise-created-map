import './Map.scss';

import lightStyle from 'components/bases/Map/MapStyles/light-style.json';
import darkStyle from 'components/bases/Map/MapStyles/dark-style.json';
import satelliteStyle from 'components/bases/Map/MapStyles/satellite-style.json';
import terrainStyle from 'components/bases/Map/MapStyles/terrain-style.json';
import boundaryStyle from 'components/bases/Map/MapStyles/boundary-style.json';

import lightStyleOverride from 'components/bases/Map/MapStyles/light-style.override';
import darkStyleOverride from 'components/bases/Map/MapStyles/dark-style.override.js';
import satelliteStyleOverride from 'components/bases/Map/MapStyles/satellite-style.override.js';
import terrainStyleOverride from 'components/bases/Map/MapStyles/terrain-style.override.js';
import boundaryStyleOverride from 'components/bases/Map/MapStyles/boundary-style.override.js';

import globalOverride from 'components/bases/Map/MapStyles/global.override.json';

import mapDefaultImage from 'images/map-style-default.png';
import mapDarkImage from 'images/map-style-dark.png';
import mapTerrainImage from 'images/map-style-terrain.png';
import mapSatelliteImage from 'images/map-style-satellite.png';
import mapBoundaryImage from 'images/map-style-boundary.png';
import mapBoxBuildingImage from 'images/map-style-box-building.png';
import map3dBuildingImage from 'images/map-style-3d-building.png';
import compassImage from 'images/compass.svg';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactMapboxGl from 'react-mapbox-gl';
import { ZoomControl, ScaleControl, RotationControl } from 'react-mapbox-gl';
import { isMobile } from 'react-device-detect';

import { StyleSwitcher } from 'components/bases/Map/StyleSwitcher/StyleSwitcher';
import { BuildingLayer } from 'components/bases/Map/VBD3DBuildings';
import { ThemeContext } from 'components/bases/Theme/ThemeContext';

import { B3DMData, GLTFData } from './model3d-data';
import { AppConstant } from 'constant/app-constant';
import { tenantConfig } from 'helper/tenant.helper';

export const mapStyleList = tenantConfig['mapStyleList'] || [
    { id: 'light', label: 'Tiêu chuẩn' },
    { id: 'dark', label: 'Màu Tối' },
    { id: 'satellite', label: 'Vệ Tinh' },
    { id: 'terrain', label: 'Địa hình' },
    { id: 'boundary', label: 'Ranh giới' }
];

class Map extends Component
{
    pressTimeout = undefined;
    rotating = false;

    // Prevent Map from re-rendering
    shouldComponentUpdate(nextProps, nextState)
    {
        const { lng: nextLng, lat: nextLat } = nextProps.center;
        const { zoomLevel: nextZoomLevel } = nextProps;
        const { lng, lat } = this.props.center;
        const { zoomLevel } = this.props;

        if (lat !== nextLat || lng !== nextLng || zoomLevel?.length !== nextZoomLevel?.length)
        {
            return true;
        }

        for (let i = 0; i < zoomLevel?.length; i++)
        {
            if (zoomLevel[i] !== nextZoomLevel[i])
            {
                return true;
            }
        }

        return false;
    }

    constructor(props)
    {
        super(props);

        this.map = React.createRef();
        // global override
        this.overrideStyle(lightStyle, globalOverride);
        this.overrideStyle(darkStyle, globalOverride);
        this.overrideStyle(satelliteStyle, globalOverride);
        this.overrideStyle(terrainStyle, globalOverride);
        this.overrideStyle(boundaryStyle, globalOverride);

        // local override
        this.overrideStyle(lightStyle, lightStyleOverride);
        this.overrideStyle(darkStyle, darkStyleOverride);
        this.overrideStyle(satelliteStyle, satelliteStyleOverride);
        this.overrideStyle(terrainStyle, terrainStyleOverride);
        this.overrideStyle(boundaryStyle, boundaryStyleOverride);

        // component override
        this.overrideStyle(lightStyle, this.props.lightStyleOverride);
        this.overrideStyle(darkStyle, this.props.darkStyleOverride);
        this.overrideStyle(satelliteStyle, this.props.satelliteStyleOverride);
        this.overrideStyle(terrainStyle, this.props.terrainStyleOverride);
        this.overrideStyle(boundaryStyle, this.props.boundaryStyleOverride);

        const mapImageURL = window.location.origin + AppConstant.map.url;
        if (mapImageURL)
        {
            const changeSource = (style) =>
            {
                for (const key of Object.keys(style.sources))
                {
                    if (style.sources[key].url)
                    {
                        style.sources[key].url = style.sources[key].url.replace('https://images.vietbando.com', mapImageURL);
                    }

                    // $RELATIVE_URL
                    if (Array.isArray(style.sources[key].tiles) && style.sources[key].tiles.length)
                    {
                        style.sources[key].tiles[0] = style.sources[key].tiles[0].replace('$RELATIVE_URL', window.location.origin);
                    }
                }

                style.sprite = style.sprite.replace('https://images.vietbando.com', mapImageURL);
                style.glyphs = style.glyphs.replace('https://images.vietbando.com', mapImageURL);
            };

            changeSource(lightStyle);
            changeSource(darkStyle);
            changeSource(satelliteStyle);
            changeSource(terrainStyle);
            changeSource(boundaryStyle);
        }

        if (props.droneURL != null)
        {
            // proxy drone orthomosaic tile
            satelliteStyle.sources.area.tiles[0] = `${props.droneURL}`;
        }

        if (props.fontURL != null)
        {
            lightStyle.glyphs = darkStyle.glyphs = satelliteStyle.glyphs = terrainStyle.glyphs = boundaryStyle.glyphs = `${props.fontURL}/fonts/{fontstack}/{range}.pbf`;
        }
        this.mapStyles = mapStyleList.map((style) =>
        {
            switch (style.id)
            {
                case 'light':
                    style['style'] = lightStyle;
                    style['image'] = mapDefaultImage;
                    break;
                case 'dark':
                    style['style'] = darkStyle;
                    style['image'] = mapDarkImage;
                    break;
                case 'satellite':
                    style['style'] = satelliteStyle;
                    style['image'] = mapSatelliteImage;
                    break;
                case 'terrain':
                    style['style'] = terrainStyle;
                    style['image'] = mapTerrainImage;
                    break;
                case 'boundary':
                    style['style'] = boundaryStyle;
                    style['image'] = mapBoundaryImage;
                    break;
            }

            return style;
        });

        this.mapOverlays = [
            {
                id: '3d-building',
                label: 'Tòa nhà 3D',
                image: map3dBuildingImage
            },
            {
                id: 'box-building',
                label: 'Tòa vuông',
                image: mapBoxBuildingImage
            }
        ];

        this.defaultOverlays = [this.mapOverlays[1]];
    }

    overrideStyle = (origin, override) =>
    {
        if (!override)
        {
            return;
        }

        for (const key in override.sources)
        {
            const source = override.sources[key];
            const originSource = origin.sources[key];
            if (originSource)
            {
                origin.sources[key] = JSON.parse(JSON.stringify(source));
            }
            else
            {
                origin.sources.push(JSON.parse(JSON.stringify(source)));
            }
        }

        for (const layer of override.layers)
        {
            if (layer.add)
            {
                const originLayer = origin.layers.find((l) => l.id === layer.id);
                const afterLayerIndex = origin.layers.findIndex((l) => l.id === layer.after);

                if (!originLayer && afterLayerIndex >= 0)
                {
                    origin.layers.splice(afterLayerIndex, 0, JSON.parse(JSON.stringify(layer)));
                }
            }
            else
            {
                const originLayer = origin.layers.find((l) => l.id === layer.id);

                if (originLayer)
                {
                    this.overrideObject(originLayer, layer);
                }
            }
        }
    };

    overrideObject = (origin, override) =>
    {
        for (const key of Object.keys(override))
        {
            const prop = override[key];

            if (Array.isArray(prop))
            {
                origin[key] = Object.assign([], prop);
            }
            else if (typeof prop !== 'object')
            {
                origin[key] = prop;
            }
            else
            {
                if (typeof origin[key] === 'object')
                {
                    this.overrideObject(origin[key], prop);
                }
                else
                {
                    origin[key] = prop;
                }
            }
        }
    };

    getViewport = () =>
    {
        const bounds = this.map.getBounds();
        const level = this.map.getZoom();

        return {
            left: bounds.getWest(),
            top: bounds.getNorth(),
            right: bounds.getEast(),
            bottom: bounds.getSouth(),
            level: Math.round(level)
        };
    };

    onRender = (map) =>
    {
        this.map = map;
        if (this.props.onViewportChange)
        {
            this.props.onViewportChange(this.getViewport());
        }

        if (this.props.isMainMap && !window.map)
        {
            window.map = map;
            window.map.boxZoom.disable();
        }

        if (this.props.onRender)
        {
            this.props.onRender(map);
        }
    };

    findFirstLayerType = (type) =>
    {
        const layers = this.map.getStyle().layers;

        // Find the index of the first symbol layer in the map style
        const layer = layers.find((layer) => layer.type === type);

        return layer ? layer.id : null;
    };

    onStyleLoad = (map) =>
    {
        if (this.props.onStyleLoad)
        {
            this.props.onStyleLoad(map);
        }

        // temporary leave it here until find another good place
        const element = this.map._container.querySelector('.map-rotation-control > button > span');
        if (element)
        {
            element.innerHTML = `<img src='${compassImage}' alt={'compass'}>`;
        }
    };

    handleChangeMapStyle = (mapStyle) =>
    {
        this.map.setStyle(mapStyle.style);
    };

    handleToggleMapOverlay = (overlay, checked) =>
    {
        if (overlay.id === '3d-building')
        {
            if (checked)
            {
                // turn it on
                const firstSymbolId = this.findFirstLayerType('symbol');

                this.map.addLayer(new BuildingLayer('b3dm', B3DMData), firstSymbolId);
                this.map.addLayer(new BuildingLayer('gltf', GLTFData));
            }
            else
            {
                // turn it off
                if (this.map.getLayer('b3dm'))
                {
                    this.map.removeLayer('b3dm');
                }

                if (this.map.getLayer('gltf'))
                {
                    this.map.removeLayer('gltf');
                }
            }
        }
        else if (overlay.id === 'box-building')
        {
            if (this.map.getLayer('region_building3d_index'))
            {
                if (checked)
                {
                    // turn it on
                    this.map.setLayoutProperty('region_building3d_index', 'visibility', 'visible');
                }
                else
                {
                    // turn it off
                    this.map.setLayoutProperty('region_building3d_index', 'visibility', 'none');
                }
            }
        }
    };

    onMove = (map, event) =>
    {
        if (this.props.onMove)
        {
            this.props.onMove(map, event);
        }
    };

    saveMapState = () =>
    {
        if (this.props.saveViewport && this.map)
        {
            localStorage.setItem('center', JSON.stringify(this.map.getCenter()));
            localStorage.setItem('zoom', this.map.getZoom());
        }
    };

    onMoveEnd = (map, event) =>
    {
        this.saveMapState();

        if (this.props.onViewportChange)
        {
            this.props.onViewportChange(this.getViewport());
        }

        this.cancelContextMenuMobile();

        if (this.props.onMoveEnd)
        {
            this.props.onMoveEnd(map, event);
        }
    };

    onZoomEnd = () =>
    {
        this.saveMapState();

        if (this.props.onViewportChange)
        {
            this.props.onViewportChange(this.getViewport());
        }
    };

    onContextMenu = (map, event) =>
    {
        if (!isMobile && !this.rotating && this.props.onContextMenu)
        {
            this.props.onContextMenu(map, event);
        }
    };

    onRotateStart = () =>
    {
        this.rotating = true;
    };

    onRotateEnd = () =>
    {
        setTimeout(() =>
        {
            this.rotating = false;
        }, 100);
    };

    onTouchStart = (map, event) =>
    {
        if (isMobile && this.pressTimeout === undefined && this.props.onContextMenu)
        {
            this.pressTimeout = setTimeout(() =>
            {
                this.props.onContextMenu(map, event);
            }, 500);
        }
    };

    onTouchEnd = () =>
    {
        this.cancelContextMenuMobile();
    };

    onTouchCancel = () =>
    {
        this.cancelContextMenuMobile();
    };

    onTouchMove = () =>
    {
        this.cancelContextMenuMobile();
    };

    cancelContextMenuMobile = () =>
    {
        if (isMobile && this.pressTimeout)
        {
            clearTimeout(this.pressTimeout);
        }
    };

    render()
    {
        const MapBox = new ReactMapboxGl({
            ref: this.map,
            accessToken: '',
            scrollZoom: this.props.scrollZoom,
            boxZoom: this.props.boxZoom,
            dragRotate: this.props.dragRotate,
            dragPan: this.props.dragPan,
            interactive: this.props.interactive
        });

        let { center, zoomLevel } = this.props;

        if (this.props.saveViewport)
        {
            const saveCenter = JSON.parse(localStorage.getItem('center'));
            if (saveCenter && saveCenter.lat && saveCenter.lng)
            {
                center = saveCenter;
            }
            else
            {
                localStorage.setItem('zoom', JSON.stringify(center));
            }

            const saveZoom = +localStorage.getItem('zoom');
            if (saveZoom >= 0 && saveZoom <= 20)
            {
                zoomLevel = [saveZoom];
            }
            else
            {
                localStorage.setItem('zoom', zoomLevel);
            }
        }

        const mapStyle = this.props.mapStyle || this.context?.theme?.base;
        const defaultStyle = mapStyle ? this.mapStyles.find((ms) => ms.id === mapStyle) : this.mapStyles[1];

        return (
            <MapBox
                style={defaultStyle.style}
                onStyleLoad={this.onStyleLoad}
                containerStyle={{
                    height: this.props.height,
                    width: this.props.width
                }}
                center={center}
                zoom={zoomLevel}
                trackResize
                onRender={this.onRender}
                onZoomEnd={this.onZoomEnd}
                onMoveEnd={this.onMoveEnd}
                onContextMenu={this.onContextMenu}
                onTouchStart={this.onTouchStart}
                onTouchEnd={this.onTouchEnd}
                onTouchCancel={this.onTouchCancel}
                onTouchMove={this.onTouchMove}
                onMove={this.onMove}
                onClick={this.props.onClick}
                onRotateStart={this.onRotateStart}
                onRotateEnd={this.onRotateEnd}
            >
                {
                    !this.props.isNotControl &&
                    <>
                        <ZoomControl className={'map-zoom-control'} />
                        <ScaleControl
                            className={'map-scale-control'}
                            position={'bottom-left'}
                        />
                        <RotationControl className={'map-rotation-control'} />
                        <StyleSwitcher
                            mapStyles={this.mapStyles}
                            style={defaultStyle}
                            mapOverlays={this.mapOverlays}
                            overlays={this.defaultOverlays}
                            showOverlays={this.props.showOverlays}
                            onChangeMapStyle={this.handleChangeMapStyle}
                            onToggleMapOverlay={this.handleToggleMapOverlay}
                        />
                    </>
                }
                {this.props.children}
            </MapBox>
        );
    }
}

Map.propTypes = {
    className: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
    center: PropTypes.object,
    zoomLevel: PropTypes.array,
    style: PropTypes.object,
    onViewportChange: PropTypes.func,
    saveViewport: PropTypes.bool,
    droneURL: PropTypes.string,
    fontURL: PropTypes.string,
    mapStyle: PropTypes.string,

    lightStyleOverride: PropTypes.object,
    darkStyleOverride: PropTypes.object,
    satelliteStyleOverride: PropTypes.object,
    terrainStyleOverride: PropTypes.object,
    boundaryStyleOverride: PropTypes.object,

    showOverlays: PropTypes.bool
};

Map.defaultProps = {
    className: '',
    width: '100%',
    height: '100%',
    saveViewport: false,
    showOverlays: true
};

Map.contextType = ThemeContext;
export default Map;
