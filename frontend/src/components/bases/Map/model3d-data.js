const B3DMData = [
    { name: '/api/model3d/b3dm/quan_1-ab_tower/root.json' },
    { name: '/api/model3d/b3dm/quan_1-bcm_building/root.json' },
    { name: '/api/model3d/b3dm/quan_1-bitexco/root.json' },
    { name: '/api/model3d/b3dm/quan_1-bo_cong_thuong/root.json' },
    { name: '/api/model3d/b3dm/quan_1-citilight_tower/root.json' },
    { name: '/api/model3d/b3dm/quan_1-exchange_tower/root.json' },
    { name: '/api/model3d/b3dm/quan_1-hanwa_life/root.json' },
    { name: '/api/model3d/b3dm/quan_1-hmc_tower/root.json' },
    { name: '/api/model3d/b3dm/quan_1-indochina_park_tower/root.json' },
    { name: '/api/model3d/b3dm/quan_1-intercontinential_hotel_mplaza/root.json' },
    { name: '/api/model3d/b3dm/quan_1-petrosetco/root.json' },
    { name: '/api/model3d/b3dm/quan_1-prudential_building/root.json' },
    { name: '/api/model3d/b3dm/quan_1-russian_tower/root.json' },
    { name: '/api/model3d/b3dm/quan_1-saigon_sky_garden_the_landcaster_building/root.json' },
    { name: '/api/model3d/b3dm/quan_1-sheraton_hotel/root.json' },
    { name: '/api/model3d/b3dm/quan_1-somerset_hotel/root.json' },
    { name: '/api/model3d/b3dm/quan_1-takashimaya/root.json' },
    { name: '/api/model3d/b3dm/quan_1-techcombank_tower/root.json' },
    { name: '/api/model3d/b3dm/quan_1-the_golden_tower/root.json' },
    { name: '/api/model3d/b3dm/quan_1-times_square/root.json' },
    { name: '/api/model3d/b3dm/quan_1-vietcombank_tower/root.json' },
    { name: '/api/model3d/b3dm/quan_1-vincom_center/root.json' },
    { name: '/api/model3d/b3dm/quan_1-vpbank_tower/root.json' },
    { name: '/api/model3d/b3dm/quan_2-binh_khanh_apartment/root.json' },
    { name: '/api/model3d/b3dm/quan_2-can_ho_hoang_anh_gia_lai/root.json' },
    { name: '/api/model3d/b3dm/quan_2-cao_oc_an_phu/root.json' },
    { name: '/api/model3d/b3dm/quan_2-centana_thu_thiem/root.json' },
    { name: '/api/model3d/b3dm/quan_2-chung_cu_an_cu_chung_cu_an_thinh/root.json' },
    { name: '/api/model3d/b3dm/quan_2-chung_cu_an_hoa/root.json' },
    { name: '/api/model3d/b3dm/quan_2-chung_cu_bo_cong_an/root.json' },
    { name: '/api/model3d/b3dm/quan_2-cuc_thue_tp_hcm/root.json' },
    { name: '/api/model3d/b3dm/quan_2-dao_kim_cuong/root.json' },
    { name: '/api/model3d/b3dm/quan_2-estella/root.json' },
    { name: '/api/model3d/b3dm/quan_2-gateway_thao_dien/root.json' },
    { name: '/api/model3d/b3dm/quan_2-homyland_riverside/root.json' },
    { name: '/api/model3d/b3dm/quan_2-imperia_an_phu/root.json' },
    { name: '/api/model3d/b3dm/quan_2-intresco_an_khang/root.json' },
    { name: '/api/model3d/b3dm/quan_2-masteri_thao_dien/root.json' },
    { name: '/api/model3d/b3dm/quan_2-new_city_thu_thiem/root.json' },
    { name: '/api/model3d/b3dm/quan_2-palm_city/root.json' },
    { name: '/api/model3d/b3dm/quan_2-petroland/root.json' },
    { name: '/api/model3d/b3dm/quan_2-river_garden/root.json' },
    { name: '/api/model3d/b3dm/quan_2-sadora/root.json' },
    { name: '/api/model3d/b3dm/quan_2-thao_dien_pearl/root.json' },
    { name: '/api/model3d/b3dm/quan_2-the_ascent/root.json' },
    { name: '/api/model3d/b3dm/quan_2-the_krista/root.json' },
    { name: '/api/model3d/b3dm/quan_2-the_nassim/root.json' },
    { name: '/api/model3d/b3dm/quan_2-the_sun_avenue/root.json' },
    { name: '/api/model3d/b3dm/quan_2-the_vista/root.json' },
    { name: '/api/model3d/b3dm/quan_2-the_vista_verde_benh_vien_da_khoa_quoc_te/root.json' },
    { name: '/api/model3d/b3dm/quan_2-thuan_viet_apartment/root.json' },
    { name: '/api/model3d/b3dm/quan_2-tropic_garden/root.json' },
    { name: '/api/model3d/b3dm/quan_2-xi_riverview_palace/root.json' },
    { name: '/api/model3d/b3dm/quan_3-ccb/root.json' },
    { name: '/api/model3d/b3dm/quan_3-chung_cu_nguyen_phuc_nguyen/root.json' },
    { name: '/api/model3d/b3dm/quan_3-sacombank_building/root.json' },
    { name: '/api/model3d/b3dm/quan_3-sherwood_residence/root.json' },
    { name: '/api/model3d/b3dm/quan_3-shinhan_bank_building/root.json' },
    { name: '/api/model3d/b3dm/quan_3-t78_hotel/root.json' },
    { name: '/api/model3d/b3dm/quan_3-the_vista_an_phu/root.json' },
    { name: '/api/model3d/b3dm/quan_3-toa_nha_mb_bank/root.json' },
    { name: '/api/model3d/b3dm/quan_3-van_phong_quan_uy_quan_3/root.json' },
    { name: '/api/model3d/b3dm/quan_3-vietbank_building/root.json' },
    { name: '/api/model3d/b3dm/quan_4-chung_cu_h3/root.json' },
    { name: '/api/model3d/b3dm/quan_4-copac_square/root.json' },
    { name: '/api/model3d/b3dm/quan_4-gold_view/root.json' },
    { name: '/api/model3d/b3dm/quan_4-rivergate_residence_milenium_masteri/root.json' },
    { name: '/api/model3d/b3dm/quan_4-the_tresor_ree_tower/root.json' },
    { name: '/api/model3d/b3dm/quan_5-chung_cu_phuc_thinh/root.json' },
    { name: '/api/model3d/b3dm/quan_5-hung_vuong_plaza/root.json' },
    { name: '/api/model3d/b3dm/quan_5-ngan_hang_tmcp_sai_gon_quan_5/root.json' },
    { name: '/api/model3d/b3dm/quan_5-the_garden_mall/root.json' },
    { name: '/api/model3d/b3dm/quan_6-lucky_palace/root.json' },
    { name: '/api/model3d/b3dm/quan_6-remax_plaza/root.json' },
    { name: '/api/model3d/b3dm/quan_6-viva_riverside/root.json' },
    { name: '/api/model3d/b3dm/quan_7-chung_cu_luxgarden/root.json' },
    { name: '/api/model3d/b3dm/quan_7-chung_cu_my_phu/root.json' },
    { name: '/api/model3d/b3dm/quan_7-chung_cu_quoc_cuong_gia_lai/root.json' },
    { name: '/api/model3d/b3dm/quan_7-dockland_saigon_cosmo_city_the_golden_star/root.json' },
    { name: '/api/model3d/b3dm/quan_7-golden_king/root.json' },
    { name: '/api/model3d/b3dm/quan_7-grandview_the_panorama/root.json' },
    { name: '/api/model3d/b3dm/quan_7-him_lam_riverside/root.json' },
    { name: '/api/model3d/b3dm/quan_7-ipc/root.json' },
    { name: '/api/model3d/b3dm/quan_7-jamona_city/root.json' },
    { name: '/api/model3d/b3dm/quan_7-maple_tree/root.json' },
    { name: '/api/model3d/b3dm/quan_7-m_one_nam_sai_gon/root.json' },
    { name: '/api/model3d/b3dm/quan_7-petroland_tower/root.json' },
    { name: '/api/model3d/b3dm/quan_7-phu_my_hung_tower_crescent_mall/root.json' },
    { name: '/api/model3d/b3dm/quan_7-riviera_point/root.json' },
    { name: '/api/model3d/b3dm/quan_7-sunrise_city_sunrise_city_view_hoang_anh_thanh_binh_apartment/root.json' },
    { name: '/api/model3d/b3dm/quan_8-chung_cu_resco_481/root.json' },
    { name: '/api/model3d/b3dm/quan_8-diamond_lotus_riverside/root.json' },
    { name: '/api/model3d/b3dm/quan_10-ha_do_centrosa/root.json' },
    { name: '/api/model3d/b3dm/quan_10-viettel_tower/root.json' },
    { name: '/api/model3d/b3dm/quan_binh_thanh-can_ho_soho_premier_binh_thanh/root.json' },
    { name: '/api/model3d/b3dm/quan_binh_thanh-chung_cu_d5/root.json' },
    { name: '/api/model3d/b3dm/quan_binh_thanh-chung_cu_phu_dat/root.json' },
    { name: '/api/model3d/b3dm/quan_binh_thanh-chung_cu_sgc_nguyen_cuu_van/root.json' },
    { name: '/api/model3d/b3dm/quan_binh_thanh-city_garden/root.json' },
    { name: '/api/model3d/b3dm/quan_binh_thanh-coteccons_office_tower/root.json' },
    { name: '/api/model3d/b3dm/quan_binh_thanh-dai_hoc_hong_bang/root.json' },
    { name: '/api/model3d/b3dm/quan_binh_thanh-pearl_plaza/root.json' },
    { name: '/api/model3d/b3dm/quan_binh_thanh-saigonres_plaza/root.json' },
    { name: '/api/model3d/b3dm/quan_binh_thanh-the_manor/root.json' },
    { name: '/api/model3d/b3dm/quan_binh_thanh-the_morning_star_building/root.json' },
    { name: '/api/model3d/b3dm/quan_nha_be-can_ho_silver_star/root.json' },
    { name: '/api/model3d/b3dm/quan_nha_be-cao_oc_hung_phat/root.json' },
    { name: '/api/model3d/b3dm/quan_nha_be-dragon_hill_the_park_residence/root.json' },
    { name: '/api/model3d/b3dm/quan_nha_be-hoang_anh_gold_house/root.json' },
    { name: '/api/model3d/b3dm/quan_nha_be-kenton_node/root.json' },
    { name: '/api/model3d/b3dm/quan_nha_be-new_saigon/root.json' },
    { name: '/api/model3d/b3dm/quan_nha_be-saigon_south_residence/root.json' },
    { name: '/api/model3d/b3dm/quan_nha_be-sunrise_riverside_1/root.json' },
    { name: '/api/model3d/b3dm/quan_nha_be-sunrise_riverside_2/root.json' },

    // update 2021-01-08
    {
        label: 'Quận 10 - Chung cư Nguyễn Kim',
        name: '/api/model3d/3dbuidings_08012021/quan-10-chung_cu_nguyen_kim-1/root.json'
    },
    {
        label: 'Quận 10 - Chung cư Xi Grand Court',
        name: '/api/model3d/3dbuidings_08012021/quan-10-chung_cu_xi_grand_court/root.json'
    },
    {
        label: 'Quận 11 - Chung cư Tân Phước 1',
        name: '/api/model3d/3dbuidings_08012021/quan-11-chung_cu_tan_phuoc-1/root.json'
    },
    {
        label: 'Quận 11 - Chung cư Tân Phước 2',
        name: '/api/model3d/3dbuidings_08012021/quan-11-chung_cu_tan_phuoc-2/root.json'
    },
    {
        label: 'Quận 11 - Flemmington Tower 1',
        name: '/api/model3d/3dbuidings_08012021/quan-11-flemmington_tower-1/root.json'
    },
    {
        label: 'Quận 11 - Flemming Tower 2',
        name: '/api/model3d/3dbuidings_08012021/quan-11-flemmington_tower-2/root.json'
    },
    {
        label: 'Quận 12 - Chung cư The Parkland',
        name: '/api/model3d/3dbuidings_08012021/quan-12-chung_cu_the_parkland/root.json'
    },
    {
        label: 'Quận 12 - Hiệp Thành Building 1',
        name: '/api/model3d/3dbuidings_08012021/quan-12-hiep_thanh_building-1/root.json'
    },
    {
        label: 'Quận 12 - Hiệp Thành Building 2',
        name: '/api/model3d/3dbuidings_08012021/quan-12-hiep_thanh_building-2/root.json'
    },
    {
        label: 'Quận 2 - Căn hộ Bella vida',
        name: '/api/model3d/3dbuidings_08012021/quan-2-can_ho_bella_vida/root.json'
    },
    {
        label: 'Quận 2 - Cao ốc Thịnh Vượng',
        name: '/api/model3d/3dbuidings_08012021/quan-2-cao_oc_thinh_vuong/root.json'
    },
    {
        label: 'Quận 2 - Chung cư Thủ Thiêm Xanh',
        name: '/api/model3d/3dbuidings_08012021/quan-2-chung_cu_thu_thiem_xanh/root.json'
    },
    {
        label: 'Quận 2 - Citihome 1',
        name: '/api/model3d/3dbuidings_08012021/quan-2-citihome-1/root.json'
    },
    {
        label: 'Quận 2 - Citihome 2',
        name: '/api/model3d/3dbuidings_08012021/quan-2-citihome-2/root.json'
    },
    {
        label: 'Quận 2 - Citihoso',
        name: '/api/model3d/3dbuidings_08012021/quan-2-citihoso/root.json'
    },
    {
        label: 'Quận 2 - Homyland 2',
        name: '/api/model3d/3dbuidings_08012021/quan-2-homyland_2/root.json'
    },
    {
        label: 'Quận 2 - La Astoria',
        name: '/api/model3d/3dbuidings_08012021/quan-2-la_astoria/root.json'
    },
    {
        label: 'Quận 6 - Chung cư Him Lam Chợ Lớn 1',
        name: '/api/model3d/3dbuidings_08012021/quan-6-chung_cu_him_lam_cho_lon-1/root.json'
    },
    {
        label: 'Quận 6 - Chung cư Him Lam Chợ Lớn 2',
        name: '/api/model3d/3dbuidings_08012021/quan-6-chung_cu_him_lam_cho_lon-2/root.json'
    },
    {
        label: 'Quận 6 - Chung cư Him Lam Chợ Lớn 3',
        name: '/api/model3d/3dbuidings_08012021/quan-6-chung_cu_him_lam_cho_lon-3/root.json'
    },
    {
        label: 'Quận 6 - Chung cư Him Lam Chợ Lớn 4',
        name: '/api/model3d/3dbuidings_08012021/quan-6-chung_cu_him_lam_cho_lon-4/root.json'
    },
    {
        label: 'Quận 6 - Chung cư Him Lam Chợ Lớn 5',
        name: '/api/model3d/3dbuidings_08012021/quan-6-chung_cu_him_lam_cho_lon-5/root.json'
    },
    {
        label: 'Quận 6 - Chung cư Him Lam Chợ Lớn 6',
        name: '/api/model3d/3dbuidings_08012021/quan-6-chung_cu_him_lam_cho_lon-6/root.json'
    },
    {
        label: 'Quận 6 - Chung cư Him Lam Chợ Lớn 7',
        name: '/api/model3d/3dbuidings_08012021/quan-6-chung_cu_him_lam_cho_lon-7/root.json'
    },
    {
        label: 'Quận 6 - Khu Căn hộ An Phú',
        name: '/api/model3d/3dbuidings_08012021/quan-6-khu_can_ho_an_phu/root.json'
    },
    {
        label: 'Quận 8 - Căn hộ Tara Residences 1',
        name: '/api/model3d/3dbuidings_08012021/quan-8-can_ho_tara_residences-1/root.json'
    },
    {
        label: 'Quận 8 - Căn hộ Tara Residences 2',
        name: '/api/model3d/3dbuidings_08012021/quan-8-can_ho_tara_residences-2/root.json'
    },
    {
        label: 'Quận 8 - Căn hộ The Pegasuite One',
        name: '/api/model3d/3dbuidings_08012021/quan-8-can_ho_the_pegasuite_one/root.json'
    },
    {
        label: 'Quận 8 - Chung cư Carina Plaza',
        name: '/api/model3d/3dbuidings_08012021/quan-8-chung_cu_carina_plaza/root.json'
    },
    {
        label: 'Quận 8 - Chung cư City Gate Towers',
        name: '/api/model3d/3dbuidings_08012021/quan-8-chung_cu_city_gate_towers/root.json'
    },
    {
        label: 'Quận 8 - Chung cư Diamond Riverside',
        name: '/api/model3d/3dbuidings_08012021/quan-8-chung_cu_diamond_riverside/root.json'
    },
    {
        label: 'Quận 8 - Chung cư Ngọc Phương Nam',
        name: '/api/model3d/3dbuidings_08012021/quan-8-chung_cu_ngoc_phuong_nam/root.json'
    },
    {
        label: 'Quận 8 - Chung cư The Avila',
        name: '/api/model3d/3dbuidings_08012021/quan-8-chung_cu_the_avila/root.json'
    },
    {
        label: 'Quận 8 - Chung cư Topaz City 1',
        name: '/api/model3d/3dbuidings_08012021/quan-8-chung_cu_topaz_city-1/root.json'
    },
    {
        label: 'Quận 8 - Chung cư Topaz City 2',
        name: '/api/model3d/3dbuidings_08012021/quan-8-chung_cu_topaz_city-2/root.json'
    },
    {
        label: 'Quận 8 - Chung cư Topaz City 3',
        name: '/api/model3d/3dbuidings_08012021/quan-8-chung_cu_topaz_city-3/root.json'
    },
    {
        label: 'Quận 9 - Căn hộ Jamila Khang Điền',
        name: '/api/model3d/3dbuidings_08012021/quan-9-can_ho_jamila_khang_dien/root.json'
    },
    {
        label: 'Quận 9 - Chung cư 9 View',
        name: '/api/model3d/3dbuidings_08012021/quan-9-chung_cu_9_view/root.json'
    },
    {
        label: 'Quận 9 - Chung cư Him Lam Phú An',
        name: '/api/model3d/3dbuidings_08012021/quan-9-chung_cu_him_lam_phu_an-1/root.json'
    },
    {
        label: 'Quận 9 - Chung cư Him Lam Phú An',
        name: '/api/model3d/3dbuidings_08012021/quan-9-chung_cu_him_lam_phu_an-2/root.json'
    },
    {
        label: 'Quận 9 - Chung cư Sky 9 - 1',
        name: '/api/model3d/3dbuidings_08012021/quan-9-chung_cu_sky_9-1/root.json'
    },
    {
        label: 'Quận 9 - Chung cư Sky 9 - 2',
        name: '/api/model3d/3dbuidings_08012021/quan-9-chung_cu_sky_9-2/root.json'
    },
    {
        label: 'Quận 9 - Chung cư The Eastern',
        name: '/api/model3d/3dbuidings_08012021/quan-9-chung_cu_the_eastern/root.json'
    },
    {
        label: 'Quận 9 - Chung cư Thủ Thiêm Garden',
        name: '/api/model3d/3dbuidings_08012021/quan-9-chung_cu_thu_thiem_garden/root.json'
    },
    {
        label: 'Quận 9 - Citrine Apartment',
        name: '/api/model3d/3dbuidings_08012021/quan-9-citrine_apartment/root.json'
    },
    {
        label: 'Quận 9 - Flora Anh Đào Apartment',
        name: '/api/model3d/3dbuidings_08012021/quan-9-flora_anh_dao_apartment/root.json'
    },
    {
        label: 'Quận 9 - Flora Fuji Residence',
        name: '/api/model3d/3dbuidings_08012021/quan-9-flora_fuji_residence/root.json'
    },
    {
        label: 'Quận 9 - Flora Kikyo Apartment',
        name: '/api/model3d/3dbuidings_08012021/quan-9-flora_kikyo_apartment/root.json'
    },
    {
        label: 'Quận 9 - Khu Dân Cư eHome S 1',
        name: '/api/model3d/3dbuidings_08012021/quan-9-khu_dan_cu_ehome_s-1/root.json'
    },
    {
        label: 'Quận 9 - Khu Dân Cư eHome S 2',
        name: '/api/model3d/3dbuidings_08012021/quan-9-khu_dan_cu_ehome_s-2/root.json'
    },
    {
        label: 'Quận 9 - Khu Dân Cư eHome S 3',
        name: '/api/model3d/3dbuidings_08012021/quan-9-khu_dan_cu_ehome_s-3/root.json'
    },
    {
        label: 'Quận 9 - Mega Sapphire Khang Điền 1',
        name: '/api/model3d/3dbuidings_08012021/quan-9-mega_sapphire_khang_dien-1/root.json'
    },
    {
        label: 'Quận 9 - Mega Sapphire Khang Điền 2',
        name: '/api/model3d/3dbuidings_08012021/quan-9-mega_sapphire_khang_dien-2/root.json'
    },
    {
        label: 'Quận 9 - The Art Apartment 1',
        name: '/api/model3d/3dbuidings_08012021/quan-9-the_art_apartment-1/root.json'
    },
    {
        label: 'Quận 9 - The Art Apartment 2',
        name: '/api/model3d/3dbuidings_08012021/quan-9-the_art_apartment-2/root.json'
    },
    {
        label: 'Quận 9 - The Art Apartment 3',
        name: '/api/model3d/3dbuidings_08012021/quan-9-the_art_apartment-3/root.json'
    },
    {
        label: 'Quận 9 - The Art Apartment 4',
        name: '/api/model3d/3dbuidings_08012021/quan-9-the_art_apartment-4/root.json'
    },
    {
        label: 'Quận Bình Chánh - Chung cư Conic Đông Nam Á 1',
        name: '/api/model3d/3dbuidings_08012021/quan-binh_chanh-chung_cu_conic_dong_nam_a-1/root.json'
    },
    {
        label: 'Quận Bình Chánh - Chung cư Conic Đông Nam Á 2',
        name: '/api/model3d/3dbuidings_08012021/quan-binh_chanh-chung_cu_conic_dong_nam_a-2/root.json'
    },
    {
        label: 'Quận Bình Chánh - Chung cư Happy City',
        name: '/api/model3d/3dbuidings_08012021/quan-binh_chanh-chung_cu_happy_city/root.json'
    },
    {
        label: 'Quận Bình Chánh - Chung cư The Mansion',
        name: '/api/model3d/3dbuidings_08012021/quan-binh_chanh-chung_cu_the_mansion/root.json'
    },
    {
        label: 'Quận Bình Chánh - Khu Dân cư Greenlife 13c',
        name: '/api/model3d/3dbuidings_08012021/quan-binh_chanh-khu_dan_cu_greenlife_13c/root.json'
    },
    {
        label: 'Quận Bình Chánh - Khu Dân cư Terra Rosa',
        name: '/api/model3d/3dbuidings_08012021/quan-binh_chanh-khu_dan_cu_terra_rosa/root.json'
    },
    {
        label: 'Quận Bình Tân - Căn hộ Monnlight Boulevard',
        name: '/api/model3d/3dbuidings_08012021/quan-binh_tan-can_ho_moonlight_boulevard/root.json'
    },
    {
        label: 'Quận Bình Tân - Chung cư An Lạc',
        name: '/api/model3d/3dbuidings_08012021/quan-binh_tan-chung_cu_an_lac/root.json'
    },
    {
        label: 'Quận Bình Tân - Chung cư Imperial Place 1',
        name: '/api/model3d/3dbuidings_08012021/quan-binh_tan-chung_cu_imperial_place-1/root.json'
    },
    {
        label: 'Quận Bình Tân - Chung cư Imperial Place 2',
        name: '/api/model3d/3dbuidings_08012021/quan-binh_tan-chung_cu_imperial_place-2/root.json'
    },
    {
        label: 'Quận Bình Tân - Chung cxư Lê Thành Block A',
        name: '/api/model3d/3dbuidings_08012021/quan-binh_tan-chung_cu_le_thanh_block_a/root.json'
    },
    {
        label: 'Quận Bình Tân - Chung cxư Lê Thành Block B',
        name: '/api/model3d/3dbuidings_08012021/quan-binh_tan-chung_cu_le_thanh_block_b/root.json'
    },
    {
        label: 'Quận Bình Tân - Chung cư Long Phụng',
        name: '/api/model3d/3dbuidings_08012021/quan-binh_tan-chung_cu_long_phung/root.json'
    },
    {
        label: 'Quận Bình Tân - Monnlight parkview Apartment 1',
        name: '/api/model3d/3dbuidings_08012021/quan-binh_tan-moonlight_parkview_apartment-1/root.json'
    },
    {
        label: 'Quận Bình Tân - Monnlight parkview Apartment 2',
        name: '/api/model3d/3dbuidings_08012021/quan-binh_tan-moonlight_parkview_apartment-2/root.json'
    }

];

const GLTFData = [
    // { name: '/api//api/model3d/gtlf/Saigon_One_Tower.gltf', ll: [106.705624, 10.770242, 0] },
    // { name: '/api//api/model3d/gtlf/Bitexco+Financial+Tower.gltf', ll: [106.704363, 10.771636, 0] },
    // { name: '/api//api/model3d/gtlf/SG+TS.gltf', ll: [106.704745, 10.773838, 0] },
    // { name: '/api//api/model3d/gtlf/Melinh+Point+V7+Revised.gltf', ll: [106.705791415212, 10.774256063837, 0] },
    // { name: '/api//api/model3d/gtlf/LegendHotelRevised.gltf', ll: [106.706827557404, 10.778704774638, 0] },
    // { name: '/api//api/model3d/gtlf/RenaissanceRevised.gltf', ll: [106.706159160481, 10.774380225278, 0] },
    // { name: '/api//api/model3d/gtlf/VietcombankSG4.gltf', ll: [106.705503, 10.775646, 0] },
    // { name: '/api//api/model3d/gtlf/The_ONE_HCMC3.gltf', ll: [106.698446, 10.770184, 0] },
    // { name: '/api//api/model3d/gtlf/SaiGon+Tower.gltf', ll: [106.701263, 10.782688, 0] },
    // { name: '/api//api/model3d/gtlf/Sailing+Tower.gltf', ll: [106.695965, 10.779944, 0] },
    // { name: '/api//api/model3d/gtlf/CentecTower.gltf', ll: [106.697415, 10.782745, 0] },
    // { name: '/api//api/model3d/gtlf/AGRICULTURAL.gltf', ll: [106.699659, 10.791817, 0] },
    // { name: '/api//api/model3d/gtlf/HTV+01.gltf', ll: [106.702431, 10.786448, 0] },
    // { name: '/api//api/model3d/gtlf/Petro+Vietnam+Tower+01.gltf', ll: [106.704956, 10.786459, 0] },
    // { name: '/api//api/model3d/gtlf/Diamond+Plaza+New.gltf', ll: [106.698627256259, 10.780932431596, 0] },
    // { name: '/api//api/model3d/gtlf/VincomCenter.gltf', ll: [106.70205, 10.77831, 0] },
    // { name: '/api//api/model3d/gtlf/Tan+Son+Nhat.gltf', ll: [106.664114, 10.816108, 0] },
    // { name: '/api//api/model3d/gtlf/Cau+Phu+My.gltf', ll: [106.744671, 10.745288, 0] },
    // { name: '/api//api/model3d/gtlf/EximbankTower3.gltf', ll: [106.698672, 10.769952, 0] },
    // { name: '/api//api/model3d/gtlf/Havana.gltf', ll: [106.70045, 10.771379, 0] }
];

export { B3DMData, GLTFData };
