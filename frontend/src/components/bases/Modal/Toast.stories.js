import React, { useState, useEffect } from 'react';
import { action } from '@storybook/addon-actions';

import { Toast } from 'components/bases/Modal/Toast';

export default {
    title: 'Bases/Modal/Toast',
    component: Toast,
};

const Template = (args) =>
{
    return (
        <Toast {...args} />
    );
};

export const Default = Template.bind({});
