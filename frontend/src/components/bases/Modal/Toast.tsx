import './Toast.scss';

import React from 'react';
import { Animated } from 'react-animated-css';
import PropTypes from 'prop-types';

import { Container } from 'components/bases/Container/Container';
import { FAIcon } from 'components/bases/Icon/FAIcon';

export interface ToastStackProps
{
    id?: string,
    location?: string,
    items?: ToastProps[]
}

export function ToastStack (props: ToastStackProps)
{
    return (
        <Container
            id={props.id}
            className={`toast-items toast-location-${props.location}`}
        >
            {
                Array.isArray(props.items) && props.items.map((toast, index) =>
                    <Toast
                        key={toast.id || index}
                        message={toast.message}
                        type={toast.type}
                        icon={toast.icon}
                        onClick={toast.onClick}
                    />
                )
            }
        </Container>
    );

}

ToastStack.propTypes = {
    location: PropTypes.string
};

ToastStack.defaultProps = {
    location: 'top-right'
};

export interface ToastProps
{
    id?: string,
    message?: string,
    icon?: string,
    type: 'error' | 'info' | 'success' | 'warning' | 'default',
    child?: any,
    onClick?: () => void
}

export function Toast (props: ToastProps)
{
    const { message, type } = props;

    return (
        <Animated
            animationIn="slideInRight"
            animationOut="slideOutLeft"
            animationInDuration={250}
            isVisible
        >
            <Container
                className={`toast toast-${type}`}
                style={{ cursor: props.onClick ? 'pointer' : '' }}
                onClick={props.onClick}
            >
                {
                    props.child ||
                    <Container
                        className={'toast-container '}
                    >
                        {
                            props.icon &&
                            <FAIcon
                                className={'toast-icon'}
                                icon={props.icon}
                            />
                        }
                        <Container className={'toast-message'}>{message}</Container>
                    </Container>
                }
            </Container>
        </Animated>
    );
}
