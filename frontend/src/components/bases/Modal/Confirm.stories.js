import React, { useState, useEffect } from 'react';
import { action } from '@storybook/addon-actions';

import { Confirm } from 'components/bases/Modal/Confirm';

export default {
    title: 'Bases/Modal/Confirm',
    component: Confirm,
};

const Template = (args) =>
{
    return (
        <Confirm
            {...args}
            onOk={action('onOk')}
            onCancel={action('onCancel')}
        />
    );
};

export const Default = Template.bind({});
Default.args = {
    title: 'Test title',
    message: 'Test message',
};
