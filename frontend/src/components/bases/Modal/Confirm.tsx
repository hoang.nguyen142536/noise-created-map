import './Dialog.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Popup, PopupFooter } from 'components/bases/Popup/Popup';
import { Button, EmptyButton } from 'components/bases/Button/Button';
import { T } from 'components/bases/Translate/Translate';

export interface ConfirmProps
{
    title?: string,
    message?: string,
    okText?: string,
    onOk?: () => void,

    cancelText?: string,
    onCancel?: () => void
}

export function Confirm(props: ConfirmProps)
{
    const { title = 'Xác nhận', message } = props;
    const { okText = 'Xác nhận', onOk } = props;
    const { cancelText = 'Hủy', onCancel } = props;

    return (
        <Popup
            className={'dialog-popup'}
            title={title}
            padding={'2rem'}
            onClose={onCancel}
        >
            <T>{message}</T>

            <PopupFooter>
                <EmptyButton
                    text={cancelText}
                    onClick={onCancel}
                />

                <Button
                    type={'primary'}
                    text={okText}
                    onClick={onOk}
                />
            </PopupFooter>
        </Popup>
    );
}
