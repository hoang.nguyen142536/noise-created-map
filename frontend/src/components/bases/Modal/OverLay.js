import './OverLay.scss';

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

const modalRoot = document.getElementById('modal-root');

export class OverLay extends Component
{
    state = {
        mounted: false
    };

    constructor(props)
    {
        super(props);
        this.el = document.createElement('div');
        this.popupRef = React.createRef();
    }

    componentDidMount()
    {
        // The portal element is inserted in the DOM tree after its children are mounted
        // that's mean its children will be mounted on a detached DOM node.
        modalRoot.appendChild(this.el);

        // If we need the children to be attached to the DOM tree immediately when mounted
        // for example to measure a DOM node (width, height, scroll, autoFocus...)
        // don't render children until Overlay is mounted in the DOM tree.
        this.setState({ mounted: true }, () =>
        {
            if (this.popupRef && this.popupRef.current && this.props.anchorEl && this.props.anchorEl.current)
            {
                const rect = this.props.anchorEl.current.getBoundingClientRect();
                const popupWidth = this.popupRef.current.clientWidth;
                if (rect.left + popupWidth > window.innerWidth)
                {
                    this.popupRef.current.style.left = null;
                    this.popupRef.current.style.right = (window.innerWidth - rect.right) + 'px';
                }
                else
                {
                    this.popupRef.current.style.right = null;
                    this.popupRef.current.style.left = rect.left + 'px';
                }
            }
        });
    }

    componentWillUnmount()
    {
        modalRoot.removeChild(this.el);
    }

    render()
    {
        let rect;
        const style = {
            width: this.props.width,
            maxWidth: '90vw',
            height: this.props.height,
            maxHeight: '90vh'
        };

        if (this.props.anchorEl && this.props.anchorEl.current)
        {
            rect = this.props.anchorEl.current.getBoundingClientRect();

            // Vertical position
            if (rect.top > window.innerHeight * 0.65 || rect.top + 300 > window.innerHeight)
            {
                style.bottom = window.innerHeight - rect.top;
            }
            else
            {
                style.top = rect.bottom;
            }

            style.left = rect.left;
            style.minWidth = this.props.width || rect.width;
        }

        const content = (
            <div className={`${this.props.className || ''} app-overlay ${rect ? 'overlay-popover' : ''}`}>
                {
                    this.props.backdrop &&
                    <div
                        className={'overlay-background'}
                        onClick={this.props.onBackgroundClick}
                        onMouseMove={this.props.onBackgroundMouseMove}
                    />
                }
                <div
                    ref={this.popupRef}
                    className={'overlay-main'}
                    style={style}
                >
                    {this.state.mounted && this.props.children}
                </div>
            </div>
        );

        // return content;
        return ReactDOM.createPortal(content, this.el);
    }
}

OverLay.propTypes = {
    className: PropTypes.string,
    width: PropTypes.any,
    height: PropTypes.any,
    anchorEl: PropTypes.object,
    backdrop: PropTypes.bool,
    onBackgroundClick: PropTypes.func,
    onBackgroundMouseMove: PropTypes.func
};

OverLay.defaultProps = {
    className: '',
    height: 'auto',
    backdrop: true,
    onBackgroundClick: (event) =>
    {

    }
};
