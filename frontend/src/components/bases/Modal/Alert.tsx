import './Dialog.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Popup, PopupFooter } from 'components/bases/Popup/Popup';
import { Button } from 'components/bases/Button/Button';
import { T } from 'components/bases/Translate/Translate';

export interface AlertProps
{
    title: string,
    message: string,
    onOk: () => void
}
export function Alert(props: AlertProps)
{

    const { title = 'Thông báo', message, onOk } = props;

    return (
        <Popup
            title={title}
            className={'dialog-popup'}
            onClose={onOk}
        >
            <T>{message}</T>

            <PopupFooter>
                <Button
                    type={'primary'}
                    text={'Đồng ý'}
                    onClick={onOk}
                />
            </PopupFooter>
        </Popup>
    );

}
