import React from 'react';
import { action } from '@storybook/addon-actions';

import { OverLay } from 'components/bases/Modal/OverLay';

export default {
    title: 'Bases/Modal/OverLay',
    component: OverLay,
};

const Template = (args) =>
{
    const handleBackgroundClick = () =>
    {
        (action('onBackgroundClick'))();
    };

    return (
        <>
            <OverLay
                {...args}
                onBackgroundClick={handleBackgroundClick}
            />
        </>
    );
};

export const Default = Template.bind({});
