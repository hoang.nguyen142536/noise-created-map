import React, { useState, useEffect } from 'react';
import { action } from '@storybook/addon-actions';

import { PopOver } from 'components/bases/Modal/PopOver';

export default {
    title: 'Bases/Modal/PopOver',
    component: PopOver,
};

const Template = (args) =>
{
    return (
        <PopOver {...args}>
            Child DOM elements
        </PopOver>
    );
};

export const Default = Template.bind({});
