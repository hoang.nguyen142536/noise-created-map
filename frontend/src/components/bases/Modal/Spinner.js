import './Spinner.scss';
import spinnerImage from 'images/loading.gif';

import React, { Component } from 'react';

import { OverLay } from 'components/bases/Modal/OverLay';
import { Image } from 'components/bases/Image/Image';

export class Spinner extends Component
{
    render()
    {
        return (
            <OverLay
                className={'spinner'}
                width={'100px'}
                height={'100px'}
            >
                <Image
                    src={spinnerImage}
                    height={'100px'}
                    width={'100px'}
                />
            </OverLay>
        );
    }
}
