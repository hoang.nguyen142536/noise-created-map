import './PopOver.scss';

import React, { Component } from 'react';
import { OverLay } from 'components/bases/Modal/OverLay';

export class PopOver extends Component
{
    render()
    {
        return (
            <OverLay {...this.props}>
                <div className={'popover-container'}>{this.props.children}</div>
            </OverLay>
        );
    }
}
