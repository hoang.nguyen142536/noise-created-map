import React from 'react';

import { confirm, toast, alert, spin } from 'components/bases/Modal/Modal';
import { Button } from 'components/bases/Form';

export default {
    title: 'Bases/Modal'
};

export function Confirm(props)
{
    function handleClick()
    {
        confirm({
            title: 'Title',
            message: 'Message'
        });
    }

    return <Button onClick={handleClick} text='Confirm' />;
}

export function Toast(props)
{
    return (
        <>
            <Button onClick={() => toast({ location: 'top-right', message: 'Toast 1', type: 'info' })} text='Short' />
            <Button onClick={() => toast({ location: 'top-right', message: 'This is a very long toast message. This is a very long toast message. ', type: 'info' })} text='Long' />
            <Button onClick={() => toast({ location: 'top-right', message: 'This is info toast', type: 'info' })} text='Info' />
            <Button onClick={() => toast({ location: 'top-right', message: 'This is success toast', type: 'success' })} text='Success' />
            <Button onClick={() => toast({ location: 'top-right', message: 'This is warning toast', type: 'warning' })} text='Warning' />
            <Button onClick={() => toast({ location: 'top-right', message: 'This is error toast', type: 'error' })} text='Error' />
            <Button onClick={() => toast({ location: 'top-right', message: 'This is default toast', type: 'default' })} text='Default' />
            <Button onClick={() => toast({ location: 'top-right', child: <Button text={'Hello, Ima button'} />, type: 'default' })} text='Custom' />
        </>
    );
}

export function Alert(props)
{
    function handleClick()
    {
        alert({ title: 'Alert Title', message: 'Alert Message' });
    }

    return <Button onClick={handleClick} text='Alert' />;
}

export function Spinner(props)
{
    function handleClick()
    {
        spin({ timeout: 2000 });
    }

    return <Button onClick={handleClick} text='Spin' />;
}

export function SpinnerPromise(props)
{
    function handleClick()
    {
        const promise = new Promise((resolve, reject) => setTimeout(() =>
        {
            resolve();
        }, 2000));

        spin({ timeout: promise });
    }

    return <Button onClick={handleClick} text='Spin' />;
}
