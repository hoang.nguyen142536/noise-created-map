import React, { useState, useEffect } from 'react';
import { action } from '@storybook/addon-actions';

import { Alert } from 'components/bases/Modal/Alert';

export default {
    title: 'Bases/Modal/Alert',
    component: Alert,
};

const Template = (args) =>
{
    return (
        <Alert
            {...args}
            onOk={action('onOk')}
        />
    );
};

export const Default = Template.bind({});
Default.args = {
    title: 'Test title',
    message: 'Test message',
};
