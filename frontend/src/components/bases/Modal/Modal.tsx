import React, { ReactElement } from 'react';
import ReactDOM from 'react-dom';

import { Toast, ToastStack, ToastProps, ToastStackProps } from 'components/bases/Modal/Toast';
import { Alert, AlertProps } from 'components/bases/Modal/Alert';
import { Spinner } from 'components/bases/Modal/Spinner';
import { Confirm, ConfirmProps } from 'components/bases/Modal/Confirm';

type Timeout = number | Promise<any>;

const renderModal = (element: ReactElement, options: {timeout?: Timeout} = {}) =>
{
    const { timeout } = options;

    function close ()
    {
        destroy(div);
    }

    const modalRoot = document.getElementById('modal-root');
    const div = document.createElement('div');
    modalRoot?.appendChild(div);

    ReactDOM.render(element, div);

    if (timeout)
    {
        if (typeof timeout === 'number')
        {
            setTimeout(() => close(), timeout);
        }
        else
        {
            Promise.resolve(timeout).finally(() => close());
        }
    }

    return {
        close
    };
};

function destroy (div: Element)
{
    const unmountResult = ReactDOM.unmountComponentAtNode(div);

    if (unmountResult && div.parentNode)
    {
        div.parentNode.removeChild(div);
    }

    return;
}

export function confirm (props: ConfirmProps)
{
    const { onCancel, onOk } = props;

    function handleClickCancel ()
    {
        onCancel && onCancel();
        render.close();
    }

    function handleClickOk ()
    {
        onOk && onOk();
        render.close();
    }

    const render = renderModal(
        <Confirm
            {...props}
            onOk={handleClickOk}
            onCancel={handleClickCancel}
        />
    );
}

export function alert (props: AlertProps)
{
    const { onOk } = props;

    function handleClickOk ()
    {
        onOk && onOk();
        render.close();
    }

    const render = renderModal(
        <Alert
            {...props}
            onOk={handleClickOk}
        />
    );
}


const toastIcons = {
    'info': 'info-circle',
    'success': 'check-circle',
    'warning': 'exclamation-triangle',
    'error': 'exclamation-circle',
    'default': '',
};

export function toast (props: ToastProps & ToastStackProps & {timeout?: number})
{
    const { location, onClick, timeout = 3000 } = props;

    const icon = props.icon || toastIcons[props.type] || toastIcons['default'];

    function handleClick (toastDiv: HTMLDivElement)
    {
        onClick && onClick();
        destroy(toastDiv);
    }

    function addToast ()
    {
        const toastDiv = document.createElement('div');
        const toastStack = document.getElementById('toast-stack');

        if (toastStack)
        {
            toastStack.appendChild(toastDiv);

            ReactDOM.render(
                <Toast
                    {...props}
                    icon={icon}
                    onClick={handleClick.bind(this, toastDiv)}
                />,
                toastDiv
            );

            timeout !== 0 && setTimeout(() => destroy(toastDiv), timeout);
        }
    }

    const div = document.getElementById('toast-stack');

    if (!div)
    {
        renderModal(
            <ToastStack
                location={location}
                id={'toast-stack'}
            />
        );

        // Wait until React render ToastStack to render Toast
        setTimeout(() => addToast(), 100);
    }
    else
    {
        addToast();
    }
}

export function spin (props: {timeout?: Timeout} = {})
{
    const { timeout } = props;

    const render = renderModal(<Spinner />, { timeout });

    return render;
}
