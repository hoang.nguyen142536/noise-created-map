import React from 'react';

import { Spinner } from 'components/bases/Modal/Spinner';

export default {
    title: 'Bases/Modal/Spinner',
    component: Spinner
};

const Template = (args) =>
{
    return (
        <Spinner {...args} />
    );
};

export const Default = Template.bind({});
