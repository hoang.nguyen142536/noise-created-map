import React, { useState, useEffect } from 'react';
import { action } from '@storybook/addon-actions';

import { AdvanceSelect } from 'components/bases/AdvanceSelect/AdvanceSelect';

export default {
    title: 'Bases/AdvanceSelect',
    component: AdvanceSelect,
    argTypes: {
        onTextChange: {
            action: 'onTextChange'
        }
    }
};

const options = [
    { id: 'option_1', label: 'Option 1' },
    { id: 'option_2', dropdownDisplay: 'Option 2', inputDisplay: 'option input display 2' },
    { id: 'option_3', dropdownDisplay: 'Option 3', inputDisplay: 'option input display 3' }
];

const Template = (args) =>
{
    const [value, setValue] = useState([]);

    function onChangeEventHandler(newValue)
    {
        args.multi ? setValue([...value, ...newValue]) : setValue(newValue);
        (action('onChange'))(newValue);
    }

    return (
        <AdvanceSelect
            {...args}
            onChange={onChangeEventHandler}
            value={value}
        />);
};

export const Default = Template.bind({});
Default.args = {
    options: options
};

export const WithSearchBox = Template.bind({});
WithSearchBox.args = {
    hasSearch: true,
    options: options
};

export const WithDividers = Template.bind({});
WithDividers.args = {
    hasDividers: true,
    options: options
};
