import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { FAIcon } from '../Icon/FAIcon';
import { Tag } from 'components/bases/Tag/Tag';
import { Input } from 'components/bases/Input/Input';
import { Constants } from 'constant/Constants';
import { T } from 'components/bases/Translate/Translate';
import { Container } from 'components/bases/Container/Container';

export default class AdvanceSelectControl extends Component
{
    state = {
        text: '',
        value: '',
        options: [],
        isTextChange: false
    };

    static getDerivedStateFromProps(nextProps, prevState)
    {
        if (!prevState.isTextChange && nextProps.options)
        {
            let text = '';

            if (nextProps.value !== prevState.value) // on click option
            {
                const option = nextProps.options?.find((o) => o.id === nextProps.value);

                if (option)
                {
                    text = option?.inputDisplay;
                }

                return { text, value: nextProps.value, options: nextProps.options }; // return new state
            }
            else if (nextProps.value && prevState.value) // just onblur
            {
                const option = prevState.options?.find((o) => o.id === nextProps.value);

                if (option)
                {
                    text = option?.inputDisplay;
                }

                return { text };
            }
        }

        return null; // don't change state
    }

    handleTextChange = (value) =>
    {
        const { options, onTextChange } = this.props;

        if (options?.length > 0 && onTextChange)
        {
            if (this.props.disabled)
            {
                return;
            }

            this.setState({ text: value, isTextChange: true });
            onTextChange(value);
        }
    };

    handleTextInputBlur = () =>
    {
        this.setState({ isTextChange: false });
    };

    handleInputKeyDown = (e) =>
    {
        if (e.which === Constants.KEYS.ENTER)
        {
            this.setState({ isTextChange: false });
        }
    };

    handleClearValueClick = (e) =>
    {
        e.stopPropagation();
        this.setState({ text: '', isTextChange: true });

        this.props.onTextChange && this.props.onTextChange('');
        this.props.onClear && this.props.onClear();
    };

    render()
    {
        const { value, options, multi, onTagRemoved, onControlClick, placeholder, isVisible, defaultValue, isNoneSelect, editable, disabled, selectedOptions } = this.props;
        let content;
        const placeholderTag = <span className={'placeholder'}><T>{placeholder}</T></span>;

        if (multi && Array.isArray(value) && value.length)
        {
            content = value.map((val) =>
                <Tag
                    key={val}
                    text={options?.find((o) => o.id === val)?.label ?? selectedOptions?.find((o) => o.id === val)?.label ?? placeholder}
                    onCloseClick={(event) =>
                    {
                        event.stopPropagation();
                        onTagRemoved && onTagRemoved(event, val);
                    }}
                />
            );
        }
        else
        {
            if ((defaultValue === undefined && (value === undefined || (Array.isArray(value) && value.length === 0))) || isNoneSelect)
            {
                content = placeholderTag;
            }
            else if (value === undefined)
            {
                content = options?.find((o) => o.id === defaultValue);
                content = content ? content?.inputDisplay : this.state.text || placeholderTag || '';
            }
            else
            {
                content = options?.find((o) => o.id === value);
                content = content ? content?.inputDisplay : this.state.text || placeholderTag || '';
            }

            content = <div className={'dropdown-btn-text'}><T>{content}</T></div>;
        }

        const hasValue = this.props.value !== null && this.props.value !== undefined && this.props.value !== '' && (!Array.isArray(this.props.value) || this.props.value.length > 0);
        
        return (
            <div
                className={`as-container ${disabled ? 'as-container-disabled' : ''}`}
                tabIndex={0}
                onBlur={this.props.onBlur}
                style={{ width: this.props.width || '' }}
                onClick={onControlClick}
            >
                <div
                    className="as-control-container"
                    ref={this.props.innerRef}
                >
                    {
                        editable ?
                            <Input
                                className="dropdown-input"
                                placeholder={placeholder}
                                onChange={this.handleTextChange}
                                value={this.state.text}
                                onBlur={this.handleTextInputBlur}
                                onKeyDown={this.handleInputKeyDown}
                            /> :
                            <Container
                                className="dropdown-btn"
                            >
                                <T>{content}</T>
                            </Container>
                    }
                    {
                        this.props.clearable && !this.props.disabled && hasValue &&
                        <div className="clear">
                            <FAIcon
                                icon={'times'}
                                size={'1rem'}
                                onClick={this.handleClearValueClick}
                            />
                        </div>
                    }
                    <div className="arrow">
                        <FAIcon
                            icon={isVisible ? 'chevron-up' : 'chevron-down'}
                            size={'1rem'}
                            type={'regular'}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

AdvanceSelectControl.propTypes = {
    onControlClick: PropTypes.func,
    onChange: PropTypes.func,
    onTextChange: PropTypes.func,
    onBlur: PropTypes.func,
    onTagRemoved: PropTypes.func,

    options: PropTypes.array,
    multi: PropTypes.bool,
    value: PropTypes.any,
    placeholder: PropTypes.string,
    isVisible: PropTypes.bool,
    isNoneSelect: PropTypes.bool,
    defaultValue: PropTypes.any,
    innerRef: PropTypes.any,
    disabled: PropTypes.bool,
    editable: PropTypes.bool,

    width: PropTypes.string,
    clearable: PropTypes.bool
};

AdvanceSelectControl.defaultProps = {
    placeholder: '...',
    isNoneSelect: false,
    clearable: false,
    onChange: () =>
    {
    }
};
