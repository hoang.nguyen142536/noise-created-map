import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';

import SearchBox from 'components/bases/Input/SearchBox';
import { T } from 'components/bases/Translate/Translate';
import { Constants } from 'constant/Constants';

export default class AdvanceSelectPopup extends Component
{
    state = { searchKey: '', options: [], isSearching: false, keyPressValue: null };

    componentDidMount()
    {
        const { options, noneSelectValue, searchKey } = this.props;
        this.setState({ searchKey });

        const isEmpty = (value) =>
        {
            return !value || Object.keys(value).length === 0;
        };

        if (!isEmpty(noneSelectValue))
        {
            options.unshift({ id: 'none-select', dropdownDisplay: noneSelectValue });
        }

        document.addEventListener('keydown', this.handleKeyPress);

        const element = document.querySelector('.as-dropdown-item.active');
        element && element.scrollIntoView({ block: 'center' });
    }

    componentWillUnmount()
    {
        document.removeEventListener('keydown', this.handleKeyPress);
        this.props.onSearchKeyChange(this.state.searchKey);
    }

    static getDerivedStateFromProps(nextProps, prevState)
    {
        if (nextProps.options && nextProps.options !== prevState.options)
        {
            if (nextProps.isVisible)
            {
                // when searching
                if (prevState.isSearching)
                {
                    return { options: prevState.options };
                }
                else // load state when option change
                {
                    return { options: nextProps.options, keyPressValue: nextProps.selectedValue };
                }
            }
            else
            {
                // when turn off popup
                return { options: nextProps.options, isSearching: false, searchKey: '' };
            }
        }

        return null;
    }

    handleKeyPress = (e) =>
    {
        const { keyPressValue, options } = this.state;

        if (this.props.isVisible)
        {
            let index = options?.findIndex((op) => op.id === keyPressValue) || 0;

            switch (e.which)
            {
                case Constants.KEYS.ENTER:
                    this.props.onSelectChange && this.props.onSelectChange(e, keyPressValue);
                    return;
                case Constants.KEYS.UP:
                    index--;
                    index = index < 0 ? options.length - 1 : index;
                    break;
                case Constants.KEYS.DOWN:
                    index++;
                    index = index >= options.length ? 0 : index;
                    break;
                default:
                    return;
            }

            if (options[index])
            {
                this.setState({ keyPressValue: options[index].id }, () =>
                {
                    const element = document.querySelector('.as-dropdown-item.keypress-active');
                    element && element.scrollIntoView({ block: 'center' });
                });
            }
        }
    };

    onSelectItem = (e, option, active) =>
    {
        const { multi, selectedValue, onSelectChange } = this.props;

        if (multi && Array.isArray(selectedValue))
        {
            let values;

            if (active)
            {
                values = [...selectedValue, option.id];
            }
            else
            {
                values = selectedValue.filter((val) => val !== option.id);
            }

            onSelectChange(e, values, option.id);
        }
        else
        {
            onSelectChange(e, option.id);
        }
    };

    renderDropdownContent = (option, hasDividers) =>
    {
        const { selectedValue, multi } = this.props;
        const keyPressActive = this.state.keyPressValue === option.id;

        let isActive = false;

        if (multi && Array.isArray(selectedValue))
        {
            isActive = selectedValue.includes(option.id);
        }
        else
        {
            isActive = selectedValue === option.id;
        }

        return (
            <li
                className={
                    `as-dropdown-item ${isActive ? 'active' : ''} \
                     ${keyPressActive ? 'keypress-active' : ''} \
                     ${hasDividers ? 'dividers' : ''}`
                }
                key={option.id}
                onClick={(e) => this.onSelectItem(e, option, !isActive)}
            >
                <div className="as-dropdown-item-button">
                    <T>{option.dropdownDisplay}</T>
                </div>
            </li>
        );
    };

    handleChangeKey = async (searchKey) =>
    {
        let options = this.props.options;

        this.setState({ searchKey });

        if (this.props.searchMode === 'local')
        {
            if (searchKey)
            {
                options = this.props.options.filter((o) => o.dropdownDisplay.toLowerCase().includes(searchKey.toLowerCase()));
            }
        }
        else
        {
            await this.props.onRemoteFetch(searchKey);

            options = this.props.options;
        }

        this.setState({ options });
    };


    handleTextBoxFocus = () =>
    {
        this.setState({ isSearching: true });
    };

    render()
    {
        const { searchKey, options } = this.state;
        const { isVisible, titleText, width, hasSearch, hasDividers } = this.props;

        return (
            <div
                className={`as-dropdown-container ${isVisible ? 'visible' : ''}`}
                style={{ width }}
            >
                {titleText && (
                    <div className="as-dropdown-title">
                        <span><T>{titleText}</T></span>
                    </div>
                )}

                {
                    hasSearch &&
                    <div className={'as-dropdown-filter-cover'}>
                        <SearchBox
                            value={searchKey}
                            onChange={this.handleChangeKey}
                            placeholder={'Nhập giá trị cần tìm'}
                            icon='search'
                            onFocus={this.handleTextBoxFocus}
                            size={'small'}
                            autoFocus
                        />
                    </div>
                }

                <PerfectScrollbar>
                    <ul className="as-dropdown-list">
                        {
                            options && options.map((option) =>
                            {
                                return this.renderDropdownContent(option, hasDividers);
                            })
                        }
                    </ul>
                </PerfectScrollbar>
            </div>
        );
    }
}

AdvanceSelectPopup.propTypes = {
    onSelectChange: PropTypes.func,
    hasSearch: PropTypes.bool,
    multi: PropTypes.bool,
    isVisible: PropTypes.bool,
    options: PropTypes.array.isRequired,
    selectedValue: PropTypes.any,
    width: PropTypes.string,
    noneSelectValue: PropTypes.string,
    titleText: PropTypes.string,
    searchMode: PropTypes.oneOf(['local', 'remote']),
    onRemoteFetch: PropTypes.func
};

AdvanceSelectPopup.defaultProps = {
    hasSearch: false,
    selectedValue: '',
    titleText: '',
    noneSelectValue: '',
    isVisible: false,
    searchMode: 'local',
    onSelectChange: () =>
    {
    },
    onRemoteFetch: () =>
    {}
};

