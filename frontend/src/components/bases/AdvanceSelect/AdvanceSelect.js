import './AdvanceSelect.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { OverLay } from 'components/bases/Modal/OverLay';
import { Container } from 'components/bases/Container/Container';

import AdvanceSelectControl from './AdvanceSelectControl';
import AdvanceSelectPopup from './AdvanceSelectPopup';

class AdvanceSelect extends Component
{
    state = {
        isVisible: false,
        value: this.props.value,
        isNoneSelect: false,
        searchKey: '',
        controlWidth: this.props.width || '100px'
    };

    constructor(props)
    {
        super(props);

        this.inputRef = React.createRef();
    }

    componentDidMount()
    {
        document.addEventListener('keydown', this.eventListener, false);
    }

    componentWillUnmount()
    {
        document.removeEventListener('keydown', this.eventListener, false);
    }

    eventListener = (event) =>
    {
        if (event.code === 'Escape')
        {
            this.closeAdvanceSelect();
        }
    };

    static getDerivedStateFromProps(nextProps, prevState)
    {
        // this is here because QueryBuilderRule use value as a state and it set state whenever it's parent call fillRuleStatus
        if (nextProps.value !== prevState.value)
        {
            return { value: nextProps.value }; // return new state
        }

        return null; // don't change state
    }

    closeAdvanceSelect = () => this.setState({ isVisible: false });

    handleControlClick = () =>
    {
        if (this.props.disabled)
        {
            return;
        }

        this.setState({
            isVisible: !this.state.isVisible,
            controlWidth: this.inputRef.current.offsetWidth + 'px'
        });
    };

    onBlur = () =>
    {
        const { isVisible } = this.state;

        if (isVisible)
        {
            this.closeAdvanceSelect();
        }
    };

    handleTextChange = (value) =>
    {
        this.setState({ isVisible: !!value });

        if (!value)
        {
            this.handleSelectChange({}, 'none-select', '');
        }

        this.props.onTextChange && this.props.onTextChange(value);
    };

    handleSelectChange = (e, value, currentSelect) =>
    {
        const { onChange, multi } = this.props;

        let isNoneSelect = false;

        if (value === 'none-select')
        {
            value = null;
            isNoneSelect = true;
        }

        if (onChange)
        {
            onChange(value, currentSelect);
        }

        this.setState({ value, isNoneSelect });

        if (!multi || !(e.ctrlKey || e.metaKey))
        {
            this.onBlur();
        }
    };

    handleTagRemoved = (e, val) =>
    {
        e.stopPropagation();

        let value = this.state.value;
        value = value.filter((v) => v !== val);

        this.handleSelectChange({}, value, val);
    };

    render()
    {
        const { isVisible, isNoneSelect, value } = this.state;

        const {
            hasSearch,
            multi,
            placeholder,
            noneSelectValue,
            defaultValue,
            editable,
            disabled,
            flex,
            width,
            searchMode,
            onRemoteFetch,
            clearable,
            onClear,
            hasDividers
        } = this.props;

        const options = this.props.options?.map(option => (
            {
                ...option,
                dropdownDisplay: option.dropdownDisplay || option.label || option.id || '',
                inputDisplay: option.inputDisplay || option.dropdownDisplay || option.label || option.id || ''
            }
        ));

        return (
            <Container
                flex={flex}
                width={width}
            >
                <AdvanceSelectControl
                    innerRef={this.inputRef}
                    defaultValue={defaultValue}
                    value={value}
                    selectedOptions={this.props.selectedOptions || []}
                    options={options}
                    multi={multi}
                    placeholder={placeholder}
                    isVisible={isVisible}
                    isNoneSelect={isNoneSelect}
                    editable={editable}
                    onTextChange={this.handleTextChange}
                    onTagRemoved={this.handleTagRemoved}
                    onControlClick={this.handleControlClick}
                    // onBlur={this.onBlur}
                    width={width}
                    disabled={disabled}
                    hasValue={!!value}
                    clearable={clearable}
                    onClear={onClear}
                />
                {
                    isVisible &&
                    <OverLay
                        anchorEl={this.inputRef}
                        onBackgroundClick={this.onBlur}
                    >
                        <AdvanceSelectPopup
                            width={this.state.controlWidth}
                            multi={multi}
                            noneSelectValue={noneSelectValue}
                            selectedValue={value}
                            isVisible={isVisible}
                            options={options}
                            hasSearch={hasSearch}
                            searchMode={searchMode}
                            onRemoteFetch={onRemoteFetch}
                            hasDividers={hasDividers}
                            onSelectChange={this.handleSelectChange}
                            onSearchKeyChange={(searchKey) => this.setState({ searchKey })}
                            searchKey={this.state.searchKey}
                        />
                    </OverLay>
                }
            </Container>
        );
    }
}

AdvanceSelect.propTypes = {
    options: PropTypes.array.isRequired, // [{id, label, inputDisplay, dropdownDisplay}]

    flex: PropTypes.number,
    width: PropTypes.string,
    hasSearch: PropTypes.bool,
    hasDividers: PropTypes.bool,
    disabled: PropTypes.bool,
    editable: PropTypes.bool,
    multi: PropTypes.bool,

    value: PropTypes.any,
    defaultValue: PropTypes.any,

    noneSelectValue: PropTypes.string,
    placeholder: PropTypes.string,

    onClear: PropTypes.func,
    onChange: PropTypes.func,
    onTextChange: PropTypes.func,

    searchMode: PropTypes.oneOf(['local', 'remote']),
    onRemoteFetch: PropTypes.func,

    clearable: PropTypes.bool
};

AdvanceSelect.defaultProps = {
    disabled: false,
    placeholder: '...',
    multi: false,
    editable: false,
    searchMode: 'local',
    clearable: false,
    onRemoteFetch: () =>
    {},
    onClear: () =>
    {},
    hasDividers: false
};

export { AdvanceSelect };
