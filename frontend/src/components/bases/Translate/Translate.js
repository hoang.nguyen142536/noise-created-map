import PropTypes from 'prop-types';
import moment from 'moment';

import { tenantConfig } from 'helper/tenant.helper';
import Moment from 'react-moment';

export function getLocale()
{
    return localStorage.getItem('locale') || tenantConfig['locale'] || 'vi';
}

export function setLocale(locale)
{
    if (!locale)
    {
        locale = getLocale();
    }

    if (locale !== 'en')
    {
        require('moment/locale/' + locale);
    }

    Moment.globalLocal = locale;
    moment.updateLocale(locale);
}

export const language = localStorage.getItem('language') || tenantConfig['language'] || 'vi';

let lang = language;
if (language.startsWith('en'))
{
    lang = 'en';
}
const translates = require('data/translates/' + lang) || {};

export function setLanguage(newLanguage)
{
    localStorage.setItem('language', newLanguage);

    // also set locale
    localStorage.setItem('locale', newLanguage);

    window.location.reload();
}

export function addTranslate(key, value)
{
    translates[key] = value;
}

export function addTranslates(trans)
{
    if (Array.isArray(trans))
    {
        trans.forEach((d) =>
        {
            translates[d.key] = d.value;
        });
    }
}

export function T(props)
{
    const { children, params } = props;

    if (!children)
    {
        return null;
    }

    return translate(children, params);
}

export function translate(text, params)
{
    if (typeof text !== 'string')
    {
        return text;
    }

    let output = text;
    if (translates && translates[output])
    {
        output = translates[output];
    }

    if (Array.isArray(params))
    {
        params.forEach((p, index) =>
        {
            output = output.replace(`%${index}%`, p);
        });
    }

    return output;
}

T.propTypes = {
    params: PropTypes.array
};

T.defaultProps = {
    params: []
};
