import React, { useState, useEffect } from 'react';

import { Line } from 'components/bases/Line/Line';

export default {
    title: 'Bases/Layout/Line',
    component: Line,
};

const Template = (args) =>
{
    return (
        <Line {...args} />
    );
};

export const Default = Template.bind({});
Default.args = {
    width: '50%',
    color: 'cyan',
    height: 3,
};
