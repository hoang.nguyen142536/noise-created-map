import './Iconstories.scss';
import data1 from 'components/bases/IconSvg/icondata/userinterface.json';
import data2 from 'components/bases/IconSvg/icondata/locationandmap.json';
import data3 from 'components/bases/IconSvg/icondata/direction.json';
import data4 from 'components/bases/IconSvg/icondata/system.json';
import data5 from 'components/bases/IconSvg/icondata/category.json';
import data6 from 'components/bases/IconSvg/icondata/logo.json';

import React, { useRef, useState } from 'react';
import { SVG } from 'components/bases/IconSvg/SVGIcon';

// const SVGIcons = (props) => {

//     const { icons } = props;
//     const [activeIdx, setActiveIdx] = useState();
//     const textareaRef = useRef([]);

//     const copyCodeToClipboard = (idx) => {
//         setActiveIdx(idx)
//         textareaRef.current[idx].select();
//         document.execCommand('copy');
//     }

//     const renderIcons = () => {
//         return icons.map((ic, idx) => {
//             return (
//                 <div
//                     key={idx}
//                     className={activeIdx === idx ? 'item active' : 'item'}
//                     onClick={() => copyCodeToClipboard(idx)}
//                 >
//                     <SVG fill={props.fill} name={ic.name} width={"32px"} height={"32px"} />
//                     <textarea
//                         ref={ref => textareaRef.current[idx] = ref}
//                         value={ic.name}
//                         readOnly
//                         rows='2'
//                     />
//                 </div>
//             )
//         })
//     }

//     return (
//         <div className="items-category">
//             {renderIcons()}
//         </div>
//     )
// }
const SVGIcons = (props) =>
{

    const { icons } = props;
    const [activeIdx, setActiveIdx] = useState();
    const textareaRef = useRef([]);

    const copyCodeToClipboard = (idx) =>
    {
        setActiveIdx(idx);
        textareaRef.current[idx].select();
        document.execCommand('copy');
    };

    const renderIcons = () =>
    {
        return icons.map((ic, idx) =>
        {
            return (
                <div
                    key={idx}
                    className={activeIdx === idx ? 'item active' : 'item'}
                    onClick={() => copyCodeToClipboard(idx)}
                >
                    <div className="item_body">
                        <span className="item_checked">
                            <svg className="item_checked-svg" viewBox="0 0 12 10">
                                <polyline points="1.5 6 4.5 9 10.5 1" />
                            </svg>
                        </span>
                        <SVG fill={props.fill} name={ic.name} />
                        <textarea
                            ref={ref => textareaRef.current[idx] = ref}
                            value={ic.name}
                            readOnly
                            rows='2'
                        />

                    </div>
                </div>
            );
        });
    };

    return (
        <div className="items-category">
            {renderIcons()}
        </div>
    );
};

export default {
    title: 'Bases/Icon Library',
    component: SVGIcons,
    argTypes: {
        fill: {
            control: 'color'
        },
        icons: {
            table: {
                disable: true
            }
        }
    }
};

const Template = (args) => <SVGIcons {...args} />;

export const UserInterface = Template.bind({});
UserInterface.args = {
    icons: data1,
    fill: 'var(--primary)'
};

export const LocationAndMap = Template.bind({});
LocationAndMap.args = {
    icons: data2,
    fill: 'var(--primary)'
};

export const Direction = Template.bind({});
Direction.args = {
    icons: data3,
    fill: 'var(--primary)'
};

export const System = Template.bind({});
System.args = {
    icons: data4,
    fill: 'var(--primary)'
};

export const Category = Template.bind({});
Category.args = {
    icons: data5,
    fill: 'var(--primary)'
};

export const Logo = Template.bind({});
Logo.args = {
    icons: data6,
    fill: 'var(--primary)'
};

export const AllIcon = Template.bind({});
AllIcon.args = {
    icons: data1.concat(data2, data3, data4, data5, data6),
    fill: 'var(--primary)'
};
