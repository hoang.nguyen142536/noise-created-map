import './SVGIcon.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { translate } from 'components/bases/Translate/Translate';

class SVGIcon extends Component
{
    render()
    {
        const { width, height, viewBox, className, d, fillRule, title } = this.props;

        return (
            <svg
                className={`svg-icon-container ${className}`}
                viewBox={viewBox}
                width={width}
                height={height}
            >
                <path
                    fillRule={fillRule}
                    d={d}
                />
                {
                    title && <rect>
                        <title>{ translate(title) }</title>
                    </rect>
                }
            </svg>
        );
    }
}

SVGIcon.propTypes = {
    className: PropTypes.string,
    viewBox: PropTypes.string,
    width: PropTypes.number,
    height: PropTypes.number,
    d: PropTypes.string,
    fillRule: PropTypes.string,
    title: PropTypes.string
};

SVGIcon.defaultProps = {
    className: '',
    viewBox: '0 0 24 24',
    width: 24,
    height: 24,
    d: '',
    fillRule: ''
};

export { SVGIcon };
