import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { translate } from 'components/bases/Translate/Translate';

class FAIcon extends Component
{
    render()
    {
        let prefix = '';

        switch (this.props.type)
        {
            case 'solid':
                prefix = 'fas';
                break;
            case 'regular':
                prefix = 'far';
                break;
            case 'duotone':
                prefix = 'fad';
                break;
            default:
            case 'light':
                prefix = 'fal';
                break;
        }

        return (
            <i
                className={`${prefix} fa-${this.props.icon} ${this.props.spin ? 'fa-spin' : ''} ${this.props.className}`}
                style={{
                    fontSize: this.props.size,
                    color: this.props.color,
                    backgroundColor: this.props.backgroundColor,
                    cursor: this.props.disabled ? 'not-allowed' : (this.props.onClick ? 'pointer' : '')
                }}
                title={translate(this.props.title)}
                onClick={this.props.disabled ? null : this.props.onClick}
            />
        );
    }
}

FAIcon.propTypes = {
    className: PropTypes.string,
    icon: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['solid', 'regular', 'light', 'duotone']),
    size: PropTypes.string,
    color: PropTypes.string,
    backgroundColor: PropTypes.string,
    onClick: PropTypes.func,
    title: PropTypes.string,
    spin: PropTypes.bool,
    disabled: PropTypes.bool
};

FAIcon.defaultProps = {
    className: '',
    icon: '',
    type: 'light',
    size: '24px',
    color: 'inherit',
    backgroundColor: 'transparent',
    onClick: null
};

export { FAIcon };
