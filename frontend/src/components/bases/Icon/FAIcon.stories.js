import React from 'react';
import { FAIcon } from 'components/bases/Icon/FAIcon';
import { Panel } from 'components/bases/Panel/Panel';

export default {
    title: 'Bases/Font Awesome Icon',
    component: FAIcon,
    parameters: {
        controls: { expanded: true }
    },
    argTypes: {
        color: {
            control: 'color'
        },
        backgroundColor: {
            control: 'color'
        }
    }
};

const Template = (args) => <Panel><FAIcon {...args} /></Panel>;

export const Default = Template.bind({});
Default.args = {
    icon: 'user',
    color: 'red',
    size: '48px'
};

export const Spin = Template.bind({});
Spin.args = {
    icon: 'spinner-third',
    color: 'red',
    type: 'duotone',
    spin: true,
    size: '48px'
};

