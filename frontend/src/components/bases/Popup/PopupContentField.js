import './PopupContentField.scss';

import PropTypes from 'prop-types';
import React from 'react';

import { T } from 'components/bases/Translate/Translate';

export const ContainField = (props) =>
{
    return (
        <div className={'popup-contain'}>
            {props.children}
        </div>
    );
};

export const Field = (props) =>
{
    return (
        <div className={`popup-field ${props.layout === 'horizontal' ? 'pf-horizontal' : ''} ${props.className}`}>
            {props.children}
        </div>
    );
};

Field.propTypes = {
    className: PropTypes.string,
    layout: PropTypes.string
};

export const Label = (props) =>
{
    return (
        <div
            className={'popup-label'}
            title={props.children}
            style={{ ...props.width && { flex: `0 0 ${props.width}` } }}
        >
            <T>{props.children}</T>
        </div>
    );
};

Label.propTypes = {
    width: PropTypes.string
};

Label.defaultProps = {
    width: '120px'
};

export const Info = (props) =>
{
    return (
        <div
            style={{ color: props.color }}
            className={`popup-info ${props.className}`}
        >
            {props.children}
        </div>
    );
};

Info.propTypes = {
    className: PropTypes.string,
    color: PropTypes.string
};

Info.defaultProps = {
    color: ''
};
