import './Popup.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Animated } from 'react-animated-css';

import { FAIcon } from 'components/bases/Icon/FAIcon';
import { OverLay } from 'components/bases/Modal/OverLay';
import { T } from 'components/bases/Translate/Translate';
import { Button, EmptyButton } from 'components/bases/Button/Button';

class PopupManager
{
    popups = [];
    static instance;

    static init()
    {
        if (!this.instance)
        {
            this.instance = new PopupManager();
            document.addEventListener('keydown', this.instance.handleKeydown.bind(this.instance), false);

            return this.instance;
        }
    }

    handleKeydown(event)
    {
        if (event.code === 'Escape')
        {
            const popup = this.popups[this.popups.length - 1];
            if (popup)
            {
                popup.handleClose();
            }
        }
    }

    add(popup)
    {
        this.popups.push(popup);
    }

    remove(popup)
    {
        this.popups = this.popups.filter((p) => p !== popup);
    }
}

const popupManager = PopupManager.init();

export class Popup extends Component
{
    state = {
        isVisible: true
    };

    handleClose = async () =>
    {
        let beforeClose = !this.props.onBeforeClose || this.props.onBeforeClose();
        const isClose = beforeClose instanceof Promise ? await beforeClose : beforeClose;

        if (isClose)
        {
            this.setState({ isVisible: false });
            const { onClose, animationDurationOut } = this.props;
            setTimeout(() =>
            {
                this.props.escape && popupManager.remove(this);
                onClose && onClose();
            }, animationDurationOut);
        }
    };

    componentDidMount()
    {
        this.props.escape && popupManager.add(this);
    }

    render()
    {
        const { isShowContentOnly, scroll } = this.props;
        const { animationIn, animationOut, animationDurationIn, animationDurationOut } = this.props;
        const { cancelText, onCancel } = this.props;
        const { okText, okType, onOk } = this.props;

        let children = Array.isArray(this.props.children) ? this.props.children : [this.props.children];

        const footer = children.find((child) => child?.type === PopupFooter);
        children = children.filter((child) => child?.type !== PopupFooter);

        const content =
            <Animated
                className={'popup-container'}
                animationIn={animationIn}
                animationOut={animationOut}
                animationInDuration={animationDurationIn}
                animationOutDuration={animationDurationOut}
                isVisible={this.state.isVisible}
            >
                {
                    !isShowContentOnly &&
                    <div className="popup-header">
                        <h3><T>{this.props.title}</T></h3>
                    </div>
                }
                <div className="popup-header-actions">
                    {
                        this.props.headerActions && this.props.headerActions.map((action, index) =>
                            <button
                                key={index}
                                onClick={action.onClick}
                            >
                                <FAIcon
                                    icon={action.icon}
                                    size={'1.25rem'}
                                />
                            </button>
                        )
                    }
                    <button
                        onClick={this.handleClose}
                    >
                        <FAIcon
                            icon="times"
                            size={'1.25rem'}
                        />
                    </button>
                </div>
                {
                    scroll ?
                        <PerfectScrollbar
                            className="popup-body"
                            style={{ padding: this.props.padding }}
                        >
                            {children}
                        </PerfectScrollbar> :
                        <div
                            className="popup-body"
                            style={{ padding: this.props.padding }}
                        >
                            {children}
                        </div>
                }
                {
                    footer || onCancel || onOk ?
                        <div className="popup-footer">
                            {
                                footer ? footer :
                                    <PopupFooter>
                                        {
                                            onCancel &&
                                            <EmptyButton
                                                text={cancelText}
                                                onClick={() =>
                                                {
                                                    this.handleClose();
                                                    onCancel();
                                                }}
                                            />
                                        }
                                        {
                                            onOk &&
                                            <Button
                                                type={okType}
                                                text={okText}
                                                onClick={() =>
                                                {
                                                    this.handleClose();
                                                    onOk();
                                                }}
                                            />
                                        }
                                    </PopupFooter>
                            }
                        </div> :
                        scroll && <div className="empty-popup-footer" />
                }
            </Animated>;

        return (
            <OverLay
                className={`${this.props.className || ''}`}
                width={this.props.width}
                height={this.props.height}
                onBackgroundClick={this.handleClose}
            >
                {content}
            </OverLay>);
    }
}

Popup.propTypes = {
    className: PropTypes.string,
    title: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
    padding: PropTypes.string,
    isShowContentOnly: PropTypes.bool,
    onBeforeClose: PropTypes.func,
    onClose: PropTypes.func,
    onCancel: PropTypes.func,
    onOk: PropTypes.func,
    okType: PropTypes.string,
    okText: PropTypes.string,
    cancelText: PropTypes.string,
    animationIn: PropTypes.string,
    animationOut: PropTypes.string,
    animationDurationIn: PropTypes.number,
    animationDurationOut: PropTypes.number,
    scroll: PropTypes.bool,
    headerActions: PropTypes.array,
    escape: PropTypes.bool
};

Popup.defaultProps = {
    className: '',
    title: '',
    isShowContentOnly: false,
    width: 'fit-content',
    height: 'auto',
    padding: '1rem',
    okType: 'primary',
    okText: 'Xác nhận',
    cancelText: 'Hủy',
    animationIn: 'fadeIn',
    animationOut: 'zoomOut',
    animationDurationIn: 250,
    animationDurationOut: 250,
    scroll: true,
    escape: true,
    onBeforeClose: () =>
    {
        return true;
    }
};

const PopupFooter = (props) =>
{
    return (
        <div className={`popup-footer-content ${props.className}`}>
            {props.children}
        </div>
    );
};

export { PopupFooter };
