import React, { useState, useEffect } from 'react';

import { Popup, PopupFooter } from './Popup';
import CloseBlueIcon from 'images/icon/close_blue.png';
import CloseWhiteIcon from 'images/icon/close_white.png';
import { LazyLoadList } from '../LazyLoadList/LazyLoadList';

export default {
    title: 'Bases/Popup',
    component: Popup,
};

const items = [
    {
        'label': 'Item 1',
        'description': 'This is item 1',
        'icon': '',
    },
    {
        'label': 'Item 2',
        'description': 'This is item 2',
        'icon': CloseBlueIcon,
    },
    {
        'label': 'Item 3',
        'description': 'This is item 3',
        'icon': CloseWhiteIcon,
    },
];

const Template = (args) =>
{
    return (
        <Popup {...args}>
            <LazyLoadList
                items={items}
                titleField='label'
                subTitleField='description'
                iconUrlField='icon'
            />
            <PopupFooter>
                Footer
            </PopupFooter>
        </Popup>
    );
};

export const Default = Template.bind({});
Default.args = {
    title: 'Test',
};
