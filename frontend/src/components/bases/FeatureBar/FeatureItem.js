import './FeatureItem.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FAIcon } from '../Icon/FAIcon';

export class FeatureItem extends Component
{
    handleClick = (e) =>
    {
        this.props.onClick(this.props, e);
    };

    render()
    {
        const badge = this.props.badgeCount === 0 ? null : this.props.badgeCount;

        return (
            <button
                className={`feature-item ${this.props.active ? 'active' : ''} ${this.props.className}`}
                onClick={this.handleClick}
                badge-count={badge}
                title={this.props.title}
            >
                {
                    this.props.content ||
                    <FAIcon
                        icon={this.props.icon}
                        size={'1.25rem'}
                    />
                }
            </button>
        );
    }
}

FeatureItem.propTypes = {
    id: PropTypes.string,
    className: PropTypes.string,
    icon: PropTypes.string.isRequired,
    content: PropTypes.node,
    active: PropTypes.bool,
    badgeCount: PropTypes.number,
    title: PropTypes.string,
    onClick: PropTypes.func
};

FeatureItem.defaultProps = {
    id: '',
    className: '',
    badgeCount: null,
    active: false,
    icon: '',
    onClick: () =>
    {
    }
};
