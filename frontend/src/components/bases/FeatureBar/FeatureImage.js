import './FeatureImage.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Image } from '../Image/Image';

export class FeatureImage extends Component
{
    handleClick = (event) =>
    {
        this.props.onClick(this.props, event);
    };

    render()
    {
        return (
            <button
                className={`feature-item feature-image ${this.props.active ? 'active' : ''} ${this.props.className}`}
                onClick={this.handleClick}
                title={this.props.title}
            >
                <Image
                    src={this.props.src}
                    width={'1.5rem'}
                    height={'1.5rem'}
                />
            </button>
        );
    }
}

FeatureImage.propTypes = {
    id: PropTypes.string,
    className: PropTypes.string,
    src: PropTypes.string.isRequired,
    active: PropTypes.bool,
    badgeCount: PropTypes.number,
    title: PropTypes.string,
    onClick: PropTypes.func
};

FeatureImage.defaultProps = {
    id: '',
    className: '',
    badgeCount: null,
    active: false,
    icon: '',
    onClick: () =>
    {
    }
};
