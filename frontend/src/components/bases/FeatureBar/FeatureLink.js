import './FeatureItem.scss';

import React from 'react';
import PropTypes from 'prop-types';
import { FAIcon } from '../Icon/FAIcon';
import { NavLink } from 'react-router-dom';

export const FeatureLink = (props) =>
{
    const { badgeCount, className, icon, to, title } = props;
    const badge = badgeCount === 0 ? null : badgeCount;

    return (
        <NavLink
            className={`feature-item ${className}`}
            badge-count={badge}
            to={to}
        >
            <FAIcon
                icon={icon}
                type={'light'}
                size={'1.25rem'}
                title={title}
            />
        </NavLink >
    );
};

FeatureLink.propTypes = {
    id: PropTypes.string,
    className: PropTypes.string,
    icon: PropTypes.string.isRequired,
    active: PropTypes.bool,
    badgeCount: PropTypes.number,
    title: PropTypes.string
};

FeatureLink.defaultProps = {
    className: '',
    badgeCount: null,
    active: false,
    icon: ''
};
