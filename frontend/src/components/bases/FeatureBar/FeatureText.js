import './FeatureText.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { translate } from 'components/bases/Translate/Translate';

export class FeatureText extends Component
{
    handleClick = (event) =>
    {
        this.props.onClick(this.props, event);
    };

    render()
    {
        const badge = this.props.badgeCount === 0 ? null : this.props.badgeCount;
        let content = this.props.content;

        if (content && content.length > 2)
        {
            content = content.substr(0, 2);
        }

        return (
            <button
                className={`feature-item feature-text ${this.props.active ? 'active' : ''} ${this.props.className}`}
                onClick={this.handleClick}
                badge-count={badge}
                title={translate(this.props.title)}
            >
                <div className="text-container">{content}</div>
            </button>
        );
    }
}

FeatureText.propTypes = {
    id: PropTypes.string,
    className: PropTypes.string,
    content: PropTypes.string.isRequired,
    active: PropTypes.bool,
    badgeCount: PropTypes.number,
    title: PropTypes.string,
    onClick: PropTypes.func
};

FeatureText.defaultProps = {
    id: '',
    className: '',
    badgeCount: null,
    active: false,
    icon: '',
    onClick: () =>
    {
    }
};
