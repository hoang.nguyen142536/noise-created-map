import 'components/bases/Tree/TagSelected.scss';
import React, { useEffect, useState } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import PropTypes from 'prop-types';

import { Tag } from 'components/bases/Tag/Tag';

const TagSelected = ({ data, onRemoveTag }) =>
{
    const [tagData, setTagData] = useState([]);

    useEffect(() =>
    {
        setTagData(data);
    }, [data]);

    const handleRemoveTag = async (tag) =>
    {
        const newTagData = tagData.filter((tagItem) => tagItem.id !== tag.id);

        setTagData(newTagData);
        onRemoveTag(tag);
    };

    return (
        tagData && tagData.length > 0 &&
        <PerfectScrollbar className={'tsp-tag-data-container'}>
            {
                tagData.map((tag) =>
                    <Tag
                        key={tag.id}
                        text={tag.label}
                        onCloseClick={() => handleRemoveTag(tag)}
                    />
                )
            }
        </PerfectScrollbar>

    );
};

export default TagSelected;

TagSelected.propTypes = {
    data: PropTypes.array,
    onRemoveTag: PropTypes.func
};

TagSelected.defaultProps = {
    data: [],
    onRemoveTag: () =>
    {
    }
};
