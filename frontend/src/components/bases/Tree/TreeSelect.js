import './TreeSelect.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';

import { TreeItem } from './TreeItem';
import { Input } from 'components/bases/Input/Input';
import TagSelected from 'components/bases/Tree/TagSelected';
import { FormControlLabel } from 'components/bases/Form';

export class TreeSelect extends Component
{
    state = {
        data: [],
        keySearch: '',
        nodeSelected: []
    };

    componentDidMount()
    {
        const data = JSON.parse(JSON.stringify(this.props.data)); // deep clone
        this.expandAll(data, this.props.expandAll);

        // assign parent for easy go up
        this.treeIterate(data, (node, parent) =>
        {
            node.parent = parent;
        });

        this.setState({ data }, () =>
        {
            // wait for state already set

            if (this.props.nodeSelected)
            {
                this.loadNodeSelected(data, this.props.nodeSelected);
            }
        });
    }

    loadNodeSelected = (data, nodeSelected) =>
    {
        for (const nodeSelect of nodeSelected)
        {
            this.treeIterate(data, (node) =>
            {
                if (node.id === nodeSelect.id)
                {
                    this.handleChecked(node, 1);
                }
            });
        }
    };

    // function to traverse tree and callback on every node, to simplify tree manipulation -> use it like forEach on array
    treeIterate = (trees, cb, done) =>
    {
        const iterate = (parent, cb) =>
        {
            if (parent.child)
            {
                for (const node of parent.child)
                {
                    cb(node, parent);
                    iterate(node, cb);
                }
            }
        };

        for (const node of trees)
        {
            cb(node, null);
            iterate(node, cb);
        }

        done && done();
    };

    treeUp = (node, cb) =>
    {
        if (node.parent)
        {
            cb(node.parent);

            this.treeUp(node.parent, cb);
        }
    };

    recursiveChecked = (node, checkingType) =>
    {
        this.treeIterate([node], (child) =>
        {
            child.checkingType = checkingType;
        });
    };

    recursiveParentChecked = (node) =>
    {
        this.treeUp(node, (parent) =>
        {
            const childPartialChecked = parent.child.filter((child) => child.checkingType === 2).length;
            if (childPartialChecked === 0)
            {
                const childChecked = parent.child.filter((child) => child.checkingType === 1).length;
                parent.checkingType = childChecked === 0 ? 0 : parent.child.length === childChecked ? 1 : 2;
            }
            else
            {
                parent.checkingType = 2;
            }
        });
    };

    expandAll = (nodes, expand, saveOrigin = false) =>
    {
        this.treeIterate(nodes, (node) =>
        {
            if (saveOrigin && node.originExpand == null)
            {
                node.originExpand = node.expand;
            }

            node.expand = expand;
        });
    };

    restoreExpand = (nodes) =>
    {
        this.treeIterate(nodes, (node) =>
        {
            if (node.originExpand != null)
            {
                node.expand = node.originExpand;
                delete node.originExpand;
            }
        });
    };

    handleChecked = (node, checkingType) =>
    {
        node.checkingType = checkingType;

        // child
        this.recursiveChecked(node, checkingType);

        // parent
        this.recursiveParentChecked(node);

        // event
        const nodeSelected = [];
        this.treeIterate(this.state.data, (node) =>
        {
            if (node.checkingType === 1)
            {
                if (this.props.onlySelectLeaves)
                {
                    if (!node.child || node.child.length === 0)
                    {
                        nodeSelected.push(node);
                    }
                }
                else
                {
                    nodeSelected.push(node);
                }
            }
        }, () =>
        {
            this.setState({ data: this.state.data, nodeSelected });
            this.props.onChecked && this.props.onChecked(nodeSelected);
        });
    };

    handleExpand = (node, expand) =>
    {
        node.expand = expand;

        // can make remote expand here by call out expand event and wait to receive child than use it to set data state again
        this.setState({ data: this.state.data });
    };

    onSearch = (keySearch) =>
    {
        const { data } = this.state;

        if (keySearch !== '')
        {
            this.treeIterate(data, (node) =>
            {
                node.visible = node.label.toLowerCase().includes(keySearch.toLowerCase());

                if (node.visible)
                {
                    this.treeUp(node, (parent) =>
                    {
                        parent.visible = true;
                    });
                }
            });

            this.expandAll(data, true, true);
        }
        else
        {
            this.treeIterate(data, (node) =>
            {
                node.visible = true;
            });

            this.restoreExpand(data);
        }

        this.setState({ data, keySearch: keySearch });
    };

    onRemoveTag = (node) =>
    {
        this.handleChecked(node, false);
    };

    renderTreeItem(node)
    {
        return (
            node.visible !== false &&
            <TreeItem
                key={node.id}
                node={node}
                onChecked={this.handleChecked}
                onExpand={this.handleExpand}
            >
                {
                    node.expand && node.child && node.child.map((child) =>
                        this.renderTreeItem(child)
                    )
                }
            </TreeItem>
        );
    }

    render()
    {
        const { keySearch, data, nodeSelected } = this.state;

        return (
            <div>
                <FormControlLabel
                    control={
                        <Input
                            placeholder={'Tìm kiếm'}
                            onChange={this.onSearch}
                            value={keySearch}
                        />
                    }
                />

                {
                    this.props.showTag && nodeSelected.length > 0 &&
                    <TagSelected
                        data={nodeSelected}
                        size={'small'}
                        onRemoveTag={this.onRemoveTag}
                    />
                }

                <PerfectScrollbar style={{ height: this.props.height }}>
                    {
                        data && data.length &&
                        data.map((item) => this.renderTreeItem(item))
                    }
                </PerfectScrollbar>

            </div>
        );
    }
}

TreeSelect.propTypes = {
    data: PropTypes.array,
    nodeSelected: PropTypes.array,
    onlySelectLeaves: PropTypes.bool,
    showTag: PropTypes.bool,
    height: PropTypes.string,
    expandAll: PropTypes.bool,
    onChecked: PropTypes.func
};

TreeSelect.defaultProps = {
    data: [],
    onlySelectLeaves: true,
    height: 'auto',
    showTag: true,
    expandAll: false
};
