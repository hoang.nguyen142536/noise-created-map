import React, { useState, useEffect } from 'react';
import { action } from '@storybook/addon-actions';

import { AdvanceSelectPopup } from 'components/bases/AdvanceSelect2/AdvanceSelectPopup';

export default {
    title: 'Bases/AdvanceSelect/v2/Popup',
    component: AdvanceSelectPopup,
    argTypes: {},
};

const options = [{ id: 'option_1', label: 'Option 1' }, { id: 'option_2', label: 'Option 2' }, { id: 'option_3', label: 'Option 3' }];

const Template = (args) =>
{
    const [selectedValue, setSelectedValue] = useState([]);

    const onChangeSelectEventHandler = (e, value) =>
    {
        setSelectedValue(value);
    };

    return (
        <AdvanceSelectPopup
            {...args}
            selectedValue={selectedValue}
            multi
            options={options}
            onSelectChange={onChangeSelectEventHandler}
        />
    );
};

export const Default = Template.bind({});


