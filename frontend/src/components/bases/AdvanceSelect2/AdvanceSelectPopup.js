import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';

import SearchBox from 'components/bases/Input/SearchBox';
import { T } from 'components/bases/Translate/Translate';
import { Constants } from 'constant/Constants';

export function AdvanceSelectPopup(props)
{
    const [searchKey, setSearchKey] = useState();
    const [keyPressIndex, setKeyPressIndex] = useState(-1);
    const [isSearching, setIsSearching] = useState(false);
    const [options, setOptions] = useState(props.options || []);
    const [isVisible, setIsVisible] = useState(props.isVisible);

    // Keypress feature
    useEffect(() =>
    {
        document.addEventListener('keydown', handleKeyPress);

        const element = document.querySelector('.as-dropdown-item.active');
        element && element.scrollIntoView({ block: 'center' });

        return () => document.removeEventListener('keydown', handleKeyPress);
    });

    const handleKeyPress = (e) =>
    {
        if (isVisible || options.length > 0)
        {
            let newIndex;
            switch (e.which) 
            {
                case Constants.KEYS.ENTER:
                    props.onSelectChange && onSelectItem(e, options[keyPressIndex]);
                    return;
                case Constants.KEYS.UP:
                    newIndex = keyPressIndex - 1;
                    newIndex = (newIndex < 0) ? options.length - 1 : newIndex;
                    setKeyPressIndex(newIndex);
                    break;
                case Constants.KEYS.DOWN:
                    newIndex = keyPressIndex + 1;
                    newIndex = (newIndex >= options.length) ? 0 : newIndex;
                    setKeyPressIndex(newIndex);
                    break;
                default:
                    return;
            }
        }
    }

    // Searchbox feature
    const handleChangeSearchValue = (searchKey) =>
    {
        let filtered_options = [];
        if (searchKey)
        {
            filtered_options = options.filter((o) => o.label.toLowerCase().includes(searchKey.toLowerCase()));
        }
        setOptions(filtered_options);
    }

    const handleTextBoxFocus = () =>
    {
        setIsSearching(true);
    }

    // Main functions
    const onSelectItem = (e, option) =>
    {
        setKeyPressIndex(-1);
        const notActive = !isActive(option);
        let values;
        if (props.multi && Array.isArray(props.selectedValue))
        {
            if (notActive)
            {
                values = [...props.selectedValue, option.id];
            }
            else
            {
                values = props.selectedValue.filter((val) => val !== option.id);
            }
            props.onSelectChange(e, values, option.id);
        } else
        {
            props.onSelectChange(e, option.id);
        }
    }

    const isActive = (option) =>
    {
        let isActive = false;
        if (props.multi && Array.isArray(props.selectedValue))
        {
            isActive = props.selectedValue.includes(option.id);
        } else
        {
            isActive = props.selectedValue === option.id;
        }
        return isActive;
    }
    // Rendering 

    if (!props.noneSelectValue || Object.keys(props.noneSelectValue).length === 0)
    {
        // TODO
        // options.unshift({ id: 'none-select', label: props.noneSelectValue });
    }

    const renderDropdownContent = (option, index) =>
    {
        return (
            <li
                className={`as-dropdown-item ${isActive(option) ? 'active' : ''} ${keyPressIndex === index ? 'keypress-active' : ''}`}
                key={option.id}
                onClick={(e) => onSelectItem(e, option)}
            >
                <div className="as-dropdown-item-button">
                    <T>{option.label}</T>
                </div>
            </li>
        );
    }

    return (
        <>
            <div
                className={`as-dropdown-container ${props.isVisible ? 'visible' : ''}`}
                style={props.width}
            >
                {props.titleText && (
                    <div className="as-dropdown-title">
                        <span><T>{props.titleText}</T></span>
                    </div>
                )}

                {
                    props.hasSearch &&
                    <div className={'as-dropdown-filter-cover'}>
                        <SearchBox
                            value={searchKey}
                            onChange={handleChangeSearchValue}
                            placeholder={'Nhập giá trị cần tìm'}
                            icon='search'
                            onFocus={handleTextBoxFocus}
                            size={'small'}
                        />
                    </div>
                }

                <PerfectScrollbar>
                    <ul className="as-dropdown-list">
                        {
                            options && options.map((option, index) =>
                            {
                                return renderDropdownContent(option, index);
                            })
                        }
                    </ul>
                </PerfectScrollbar>
            </div>
        </>
    )
}

AdvanceSelectPopup.propTypes = {
    titleText: PropTypes.string,
    hasSearch: PropTypes.bool,
    isVisible: PropTypes.bool,
    multi: PropTypes.bool,
    width: PropTypes.string,
    noneSelectValue: PropTypes.string,
    // Mainly used props
    options: PropTypes.array.isRequired,
    onSelectChange: PropTypes.func,
    selectedValue: PropTypes.any,
};

AdvanceSelectPopup.defaultProps = {
    isVisible: true,
};