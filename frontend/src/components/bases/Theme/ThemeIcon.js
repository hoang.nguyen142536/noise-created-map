import './ThemeIcon.scss';

import PropTypes from 'prop-types';
import React from 'react';

import { FAIcon } from 'components/bases/Icon/FAIcon';
import { translate } from 'components/bases/Translate/Translate';

export const ThemeIcon = (props) =>
{
    return (
        <FAIcon
            className={`theme-icon ${props.className}`}
            icon={'circle'} type={'solid'}
            size={'1.5rem'}
            color={props.color}
            title={translate('Màu giao diện')}
        />
    );
};

ThemeIcon.propTypes = {
    color: PropTypes.string,
    className: PropTypes.string,
    title: PropTypes.string
};

ThemeIcon.defaultProps = {
    color: 'white',
    className: ''
};
