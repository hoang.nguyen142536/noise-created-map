import React, { createContext, useEffect, useState } from 'react';

import { tenantConfig } from 'helper/tenant.helper';

export const themeList = tenantConfig['themeList'] || [
    { name: 'Xanh tối', base: 'dark', className: 'theme-blue' },
    { name: 'Thép tối', base: 'dark', className: 'theme-steel' },
    { name: 'Đỏ sáng', base: 'light', className: 'theme-red' },
    { name: 'Đỏ tối', base: 'dark', className: 'theme-red' },
    { name: 'Xanh lá', base: 'light', className: 'theme-green' },
    { name: 'Tím', base: 'light', className: 'theme-purple' },
    { name: 'Xám tối', base: 'dark', className: 'theme-gray' }
];

export const ThemeContext = createContext();

const themeName = localStorage.getItem('theme') || tenantConfig['theme'];
const initTheme = themeList.find((theme) => theme.name === themeName) || themeList[0];

const ThemeProvider = (props) =>
{
    const [theme, setTheme] = useState();

    useEffect(() =>
    {
        handleThemeChange(props.theme || initTheme);
    }, [props.theme]);

    const handleThemeChange = (theme) =>
    {
        document.body.className = `theme-base theme-${theme.base} ${theme.className}`;
        localStorage.setItem('theme', theme.name);

        setTheme(theme);
    };

    return (
        <ThemeContext.Provider value={{ theme, setTheme: handleThemeChange }}>
            {props?.children}
        </ThemeContext.Provider>
    );
};

export { ThemeProvider };
