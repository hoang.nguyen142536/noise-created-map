import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class Spacer extends Component
{
    render()
    {
        return (
            <div
                style={{
                    width: this.props.direction === 'horizontal' ? this.props.size : '0',
                    height: this.props.direction === 'vertical' ? this.props.size : '0',
                    display: 'inline-block'
                }}
                {...this.props}
            />
        );
    }
}

Spacer.propTypes = {
    direction: PropTypes.oneOf(['vertical', 'horizontal']),
    size: PropTypes.string
};

Spacer.defaultProps = {
    direction: 'horizontal',
    size: '1rem'
};
