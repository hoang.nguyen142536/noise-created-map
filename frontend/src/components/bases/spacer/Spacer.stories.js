import React, { useState, useEffect } from 'react';

import { Spacer } from 'components/bases/spacer/Spacer';

export default {
    title: 'Bases/Layout/Spacer',
    component: Spacer,
};

const Template = (args) =>
{
    return (
        <>
            <div>Dummy item 1</div>
            <Spacer {...args} />
            <div>Dummy item 1</div>
        </>
    );
};

export const Vertical = Template.bind({});
Vertical.args = {
    direction: 'vertical',
    size: '1rem',
};
