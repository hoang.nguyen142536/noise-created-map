import React, { useState } from 'react';

import { RichText } from 'components/bases/Text/RichText';
import { action } from '@storybook/addon-actions';

export default {
    title: 'Bases/Display/Text/RichText',
    component: RichText,
    args: {}
};

const Template = (args) =>
{
    const [value, setValue] = useState('');
    const onChangeEventHandler = (value) =>
    {
        setValue(value);
        (action('onChange'))(value);
    };

    return (
        <RichText
            {...args}
            value={value}
            onChange={onChangeEventHandler}
        />
    );
};

export const Default = Template.bind({});
