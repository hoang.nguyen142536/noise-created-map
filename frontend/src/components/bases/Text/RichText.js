import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class RichText extends Component
{
    handleChange = (event) =>
    {
        if (typeof this.props.onChange === 'function')
        {
            this.props.onChange(event.target.value);
        }
    };

    render()
    {
        return (
            <textarea
                className={this.props.className}
                style={{
                    minHeight: (this.props.rows * 17) + 'px',
                    maxHeight: (this.props.rows * 17) + 'px',
                    color: this.props.color,
                    padding: '5px',
                    border: this.props.border
                }}
                rows={this.props.rows}
                onChange={this.handleChange}
                disabled={this.props.disabled}
                value={this.props.value}
            />
        );
    }
}

RichText.propTypes = {
    className: PropTypes.string,
    color: PropTypes.string,
    rows: PropTypes.number,
    value: PropTypes.string,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    border: PropTypes.string
};

RichText.defaultProps = {
    className: '',
    color: 'white',
    rows: 5,
    value: '',
    disabled: false,
    border: 'block',
    onChange: () =>
    {
        // todo
    }
};
