import './Text.scss';

import React from 'react';
import { T } from 'components/bases/Translate/Translate';

const TB = (props) =>
{
    return (
        <span
            className={`${props.className} ${props.color ? `sub-${props.color}` : ''}`}
            onClick={props.onClick}
            style={props.style}
        >
            <T>{props.children}</T>
        </span>
    );
};

TB.defaultProps = {
    className: '',
    style: {},
    onClick: () =>
    {
    }
};

const TB1 = (props) =>
    <TB
        className={'tb tb1'}
        {...props}
    />;

const TB2 = (props) =>
    <TB
        className={'tb tb2'}
        {...props}
    />;

const HD1 = (props) =>
    <TB
        className={'hd hd1'}
        {...props}
    />;

const HD2 = (props) =>
    <TB
        className={'hd hd2'}
        {...props}
    />;

const HD3 = (props) =>
    <TB
        className={'hd hd3'}
        {...props}
    />;

const HD4 = (props) =>
    <TB
        className={'hd hd4'}
        {...props}
    />;

const HD5 = (props) =>
    <TB
        className={'hd hd5'}
        {...props}
    />;

const HD6 = (props) =>
    <TB
        className={'hd hd6'}
        {...props}
    />;

const Sub1 = (props) =>
    <TB
        className={'sub sub1'}
        {...props}
    />;

const Sub2 = (props) =>
    <TB
        className={'sub sub2'}
        {...props}
    />;

export { TB1, TB2, HD1, HD2, HD3, HD4, HD5, HD6, Sub1, Sub2 };
