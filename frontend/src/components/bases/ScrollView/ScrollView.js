import './ScrollView.scss';

import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';

export const ScrollView = forwardRef((props, ref) =>
{
    return (
        <PerfectScrollbar
            ref={ref}
            options={{
                suppressScrollX: !props.scrollX,
                suppressScrollY: !props.scrollY
            }}
            containerRef={ref =>
            {
                if (ref)
                {
                    // fix scroll exceed content
                    ref._getBoundingClientRect = ref.getBoundingClientRect;
                    ref.getBoundingClientRect = () =>
                    {
                        const original = ref._getBoundingClientRect();
                        return { ...original, height: Math.round(original.height), width: Math.round(original.width) };
                    };

                    // set external ref
                    props.containerRef && props.containerRef(ref);
                }
            }}
            onScrollDown={props.onScrollDown}
            onScrollUp={props.onScrollUp}
            onYReachEnd={props.onYReachEnd}
            onYReachStart={props.onYReachStart}
            onSync={props.onSync}
        >
            {props.children}
        </PerfectScrollbar>
    );
});

ScrollView.displayName = 'ScrollView';


ScrollView.propTypes = {
    className: PropTypes.string,
    // containerRef: PropTypes.func,
    // properties
    scrollX: PropTypes.bool,
    scrollY: PropTypes.bool,
    // handlers
    onScrollUp: PropTypes.func,
    onScrollDown: PropTypes.func,
    onYReachStart: PropTypes.func,
    onYReachEnd: PropTypes.func,
    onSync: PropTypes.func,
};

ScrollView.defaultProps = {
    className: '',
    scrollX: true,
    scrollY: true
};
