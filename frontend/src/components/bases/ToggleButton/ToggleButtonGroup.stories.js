import React, { useState } from 'react';
import { action } from '@storybook/addon-actions';

import { ToggleButtonGroup, ToggleButton } from 'components/bases/ToggleButton/ToggleButton';

export default {
    title: 'Bases/Inputs/ToggleButtonGroup',
    component: ToggleButtonGroup,
    subcomponents: { ToggleButton },
};

const actions = [
    {
        id: 1,
        active: true,
        icon: 'tool-label',
    },
    {
        id: 2,
        active: true,
        icon: 'tool-add-marker',
    },
    {
        id: 3,
        active: true,
        icon: 'tool-add-line',
    },
];

const Template = (args) =>
{
    const [selectedActionIndex, setSelectedActionIndex] = useState(-1);

    const handleClickAction = (i) =>
    {
        setSelectedActionIndex(i);
        (action('onClick'))(i);
    };

    return (
        <ToggleButtonGroup {...args}>
            {
                actions.map((action, i) =>
                    <ToggleButton
                        key={action.id}
                        active={action.active}
                        icon={action.icon}
                        onClick={() => handleClickAction(i)}
                    />
                )
            }
        </ToggleButtonGroup>
    );
};

export const Default = Template.bind({});
