import React, { useState } from 'react';
import { action } from '@storybook/addon-actions';

import { ToggleButton } from 'components/bases/ToggleButton/ToggleButton';

export default {
    title: 'Bases/Inputs/ToggleButtonGroup/ToggleButton',
    component: ToggleButton,
};

const Template = (args) =>
{
    return (
        <ToggleButton {...args} />
    );
};

export const Default = Template.bind({});
Default.args = {
    key: 'home',
    active: true,
    icon: 'tool-select',
    onClick: action('onClick'),
};
