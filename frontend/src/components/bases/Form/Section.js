import './Section.scss';

import PropTypes from 'prop-types';
import React from 'react';
import { T } from 'components/bases/Translate/Translate';
import { FAIcon } from '../Icon/FAIcon';

export const Section = ({ className, children, header, actions }) =>
{
    return (
        <div className={`section-panel ${className}`}>
            {
                header &&
                <SectionHeader actions={actions}><h3><T>{header}</T></h3></SectionHeader>
            }
            <div className={'section-panel-body'}>
                {children}
            </div>
        </div>
    );
};

Section.propTypes = {
    className: PropTypes.string,
    actions: PropTypes.array
};

Section.defaultProps = {
    className: '',
    actions: []
};

export const SectionHeader = ({ className, children, textAlign, actions }) =>
{
    return (
        <div
            className={`section-header ${className}`}
            style={{
                textAlign
            }}
        >
            <T>{children}</T>
            {
                Array.isArray(actions) && <div className={'section-header-actions'}>
                    {
                        actions.map((action) =>
                            <button
                                key={action.icon}
                                onClick={action.onClick}
                                className={action.className}
                            >
                                <FAIcon
                                    icon={action.icon}
                                    size={'1rem'}
                                    type={'light'}
                                />
                                <span><T>{action.title}</T></span>
                            </button>
                        )
                    }
                </div>
            }
        </div>
    );
};

SectionHeader.propTypes = {
    className: PropTypes.string,
    textAlign: PropTypes.string,
    actions: PropTypes.array
};

SectionHeader.defaultProps = {
    className: '',
    textAlign: '',
    actions: []
};
