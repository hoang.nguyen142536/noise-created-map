import './Form.scss';

import React from 'react';
import PropTypes from 'prop-types';
import { T } from 'components/bases/Translate/Translate';

export const InputGroup = ({ className, children }) =>
{
    return (
        <div className={`form-input-group ${className}`}>
            {children}
        </div>
    );
};

InputGroup.propTypes = {
    className: PropTypes.string
};

InputGroup.defaultProps = {
    className: ''
};

export const InputAppend = ({ className, children }) =>
{
    if (typeof children === 'string')
    {
        children = <span><T>{children}</T></span>;
    }

    return (
        <div className={`input-append ${className}`}>
            {children}
        </div>
    );
};

InputAppend.propTypes = {
    className: PropTypes.string,
    formFlex: PropTypes.number,
    border: PropTypes.string,
    background: PropTypes.string
};

InputAppend.defaultProps = {
    className: ''
};

export const InputPrepend = ({ className, children }) =>
{
    if (typeof children === 'string')
    {
        children = <span><T>{children}</T></span>;
    }

    return (
        <div className={`input-prepend ${className}`}>
            {children}
        </div>
    );
};

InputPrepend.propTypes = {
    className: PropTypes.string,
    formFlex: PropTypes.number,
    border: PropTypes.string,
    background: PropTypes.string
};

InputPrepend.defaultProps = {
    className: ''
};


