import './FormControl.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class FormControl extends Component
{
    render()
    {
        return (
            <div className={`form-control ${this.props.className || ''}`}>
                {this.props.children}
            </div>
        );
    }
}

FormControl.propTypes = {
    className: PropTypes.string
};

FormControl.defaultProps = {
    className: ''
};
