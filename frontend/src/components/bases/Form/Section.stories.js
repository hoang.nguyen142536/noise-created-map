import React from 'react';

import { Section } from 'components/bases/Form/Section';

export default {
    title: 'Bases/Inputs/Form/Section',
    component: Section,
};

const Template = (args) =>
{
    return (
        <Section {...args}>
            Section inner text
        </Section>
    );
};

export const Default = Template.bind({});
