import React, { useState } from 'react';
import { action } from '@storybook/addon-actions';


import { FormGroup, FormControlLabel } from 'components/bases/Form/Form';
import { Button } from 'components/bases/Button/Button';
import { Input } from 'components/bases/Input/Input';

export default {
    title: 'Bases/Inputs/Form',
    component: FormGroup,
};

const Template = (args) =>
{
    const [formFields, setFormFields] = useState({});

    function handleChangeUsername(value)
    {
        formFields.username = value;
        setFormFields({ ...formFields });
    }

    function handleChangePassword(value)
    {
        formFields.password = value;
        setFormFields({ ...formFields });
    }

    function handleSubmit()
    {
        (action('onSubmit'))();
    }

    const [value,] = useState();
    return (
        <FormGroup>
            <FormControlLabel
                label="Username"
                control={
                    <Input
                        type="text"
                        value={formFields.username}
                        onChange={handleChangeUsername}
                    />
                }
            />
            <FormControlLabel
                label="Password"
                control={
                    <Input
                        type="password"
                        value={formFields.password}
                        onChange={handleChangePassword}
                    />
                }
            />
            <Button
                onClick={handleSubmit}
                text="Submit"
            />
        </FormGroup>
    );
};

export const SampleLoginForm = Template.bind({});
