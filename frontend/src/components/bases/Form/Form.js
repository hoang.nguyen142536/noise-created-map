import './Form.scss';

import React from 'react';
import PropTypes from 'prop-types';
import { T } from 'components/bases/Translate/Translate';
import { FAIcon } from 'components/bases/Icon/FAIcon';
import { Sub2 } from 'components/bases/Text/Text';

export const FormGroup = ({ direction, children, className }) =>
{
    return (
        <div
            className={`form-group form-group-${direction} ${className}`}
        >
            {children}
        </div>
    );
};

FormGroup.propTypes = {
    className: PropTypes.string,
    direction: PropTypes.oneOf(['row', 'column']),
    padding: PropTypes.string,
    formFlex: PropTypes.number
};

FormGroup.defaultProps = {
    className: '',
    direction: 'column'
};

export const FormControlLabel = ({ control, icon, iconClassName, iconType, iconSize, label, direction, className, labelWidth, labelLocation, required, dirty, rules, errorText }) =>
{
    const isInvalid = rules?.length > 0 || errorText;
    let controlClassName = required ? 'form-control-container required' : 'form-control-container';

    if (isInvalid)
    {
        controlClassName += ' invalid';
    }

    if (dirty)
    {
        controlClassName += ' dirty';
    }

    return (
        <>
            <div className={`form-control-label form-control-${direction} ${className}`}>
                {
                    icon &&
                    <span className={'icon-text-control-container'}>
                        <FAIcon
                            icon={icon}
                            className={iconClassName}
                            type={iconType}
                            size={iconSize}
                        />
                    </span>
                }
                {
                    label &&
                    <div
                        className={`form-label form-label-align-${labelLocation}`}
                        style={{ width: direction === 'row' ? labelWidth : '' }}
                    >
                        <T>{label}</T>
                    </div>
                }
                {
                    control && React.isValidElement(control) ?
                        <div className={controlClassName}>
                            {React.cloneElement(control, { className: control.props.className + ' form-control' })}
                        </div> :
                        <div className={controlClassName}>
                            {control}
                        </div>
                }

            </div>
            {
                isInvalid &&
                <Sub2
                    color={'danger'}
                    style={{ marginLeft: labelWidth, paddingLeft: 10, lineHeight: 'unset' }}
                >
                    <T>{errorText}</T>&nbsp;
                </Sub2>
            }

        </>
    );
};

FormControlLabel.propTypes = {
    required: PropTypes.bool,
    className: PropTypes.string,
    control: PropTypes.any,
    iconName: PropTypes.string,
    iconClassName: PropTypes.string,
    iconType: PropTypes.oneOf(['solid', 'regular', 'light']),
    iconSize: PropTypes.string,
    label: PropTypes.any,
    labelWidth: PropTypes.string,
    labelLocation: PropTypes.oneOf(['left', 'right']),
    direction: PropTypes.oneOf(['row', 'column']),
    rules: PropTypes.array,
    dirty: PropTypes.bool
};

FormControlLabel.defaultProps = {
    direction: 'row',
    iconType: 'solid',
    iconSize: '1rem',
    labelWidth: '70px',
    labelLocation: 'left',
    required: false,
    className: ''
};
