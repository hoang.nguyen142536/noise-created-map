import React from 'react';

import { SectionHeader } from 'components/bases/Form/Section';

export default {
    title: 'Bases/Inputs/Form/SectionHeader',
    component: SectionHeader,
};

const Template = (args) =>
{
    return (
        <SectionHeader {...args}>
            SectionHeader inner text
        </SectionHeader>
    );
};

export const Default = Template.bind({});
