import './ChatActions.scss';

import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';

import { Row } from 'components/bases/Row/Row';
import { Column } from 'components/bases/Column/Column';
import { EmptyButton } from 'components/bases/Button/Button';

export function ChatActions(props)
{
    const inputRef = useRef();
    const [draft, setDraft] = useState({});

    const textValue = draft[props.groupId] || '';

    const setTextValue = (value) =>
    {
        setDraft({ ...draft, [props.groupId]: value });
    };

    useEffect(() =>
    {
        inputRef.current.focus();
    }, []);

    useEffect(() =>
    {
        updateInputHeight();
    }, [textValue]);

    const handleKeyPress = (e) =>
    {
        if (!e.shiftKey && e.key === 'Enter')
        {
            e.preventDefault();
            handleClickSend();
        }
    };

    useEffect(() =>
    {
        updateInputHeight();
    }, [draft[props.groupId]]);

    const handleClickSend = () =>
    {
        if (textValue)
        {
            props.onSendMessage && props.onSendMessage(textValue);
            setTextValue('');
            inputRef.current.focus();
        }
    };

    const handleOnChange = (e) =>
    {
        setTextValue(e.currentTarget.value);
    };

    const updateInputHeight = () =>
    {
        if (!inputRef.current)
        {
            return;
        }
        inputRef.current.style.height = 'auto';
        inputRef.current.style.height = inputRef.current.scrollHeight + 'px';

        // Update scroll position
        props.scrollToBottom && props.scrollToBottom();
    };

    return (
        <div
            className={`chat-actions ${(props.className) ? props.className : ''}`}
            style={{ maxHeight: props.maxTextareaHeight }}
        >
            <Row>
                <Column>
                    <PerfectScrollbar>
                        <form className="post-form">
                            <textarea
                                placeholder={'Reply...'}
                                ref={inputRef}
                                className="post-textarea"
                                id="post-textarea"
                                autoComplete="off"
                                value={textValue}
                                onChange={handleOnChange}
                                onKeyDown={handleKeyPress}
                                rows="1"
                            />
                        </form>
                    </PerfectScrollbar>
                </Column>
                <Column crossAxisSize='min' mainAxisAlignment='center' crossAxisAlignment='center'>
                    <EmptyButton
                        icon={'paper-plane'}
                        iconSize={'1rem'}
                        onlyIcon
                        onClick={handleClickSend}
                    />
                </Column>
            </Row>

        </div>
    );
}

ChatActions.propTypes = {
    className: PropTypes.string,
    onSendMessage: PropTypes.func,
    maxTextareaHeight: PropTypes.string,
    scrollToBottom: PropTypes.func,
    groupId: PropTypes.string
};

ChatActions.defaultProps = {
    maxTextareaHeight: '10rem'
};

export default ChatActions;
