import './UserListItem.scss';

import React from 'react';
import PropTypes from 'prop-types';

import UserAvatar from './UserAvatar';
import { Row } from 'components/bases/Row/Row';
import { CheckBox } from 'components/bases/CheckBox/CheckBox';
import { Column } from 'components/bases/Column/Column';
import { FAIcon } from 'components/bases/Icon/FAIcon';

export function UserListItem(props)
{
    const user = props.user;
    const { info, actions } = user;

    const handleAction = (e, action, user) =>
    {
        e.stopPropagation();
        action.onClick && action.onClick(user.userId);
    };

    return (
        <Row
            className='user-list-item'
            mainAxisAlignment="space-between"
            crossAxisAlignment="center"
        >
            <Column onClick={() => props.onClick && props.onClick(user)}>
                <Row>
                    {
                        !props.hideCheckbox &&
                        <CheckBox
                            checked={props.value}
                        />
                    }
                    <UserAvatar src={user.avatar} lastActiveAt={user.lastActiveAt} />
                    <Column>
                        <div className='user-name'>{user.displayName}</div>
                        {info && <span className="info">{info}</span>}
                    </Column>
                </Row>
            </Column>
            {
                actions &&
                <Column
                    crossAxisAlignment="end"
                    crossAxisSize="min"
                    className="actions"
                >
                    <Row>
                        {
                            actions.map((action, index) =>
                            {
                                if (!action.disabled)
                                {
                                    return (
                                        <Column
                                            className="item"
                                            mainAxisAlignment={'center'}
                                            key={index}
                                        >
                                            <FAIcon
                                                size="1rem"
                                                icon={action.icon}
                                                onClick={(e) => handleAction(e, action, user)}
                                                type="solid"
                                            />
                                        </Column>
                                    );
                                }
                            })
                        }
                    </Row>
                </Column>
            }
        </Row>
    );
}

UserListItem.propTypes = {
    hideCheckbox: PropTypes.bool,
    user: PropTypes.object,
    value: PropTypes.bool,
    info: PropTypes.any
};

export default UserListItem;
