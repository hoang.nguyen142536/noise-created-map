import React, { Component } from 'react';
import { T } from 'components/bases/Translate/Translate';
import PropTypes from 'prop-types';

export class Container extends Component
{
    divRef = React.createRef();

    dimensions = {
        width: 0,
        height: 0
    };

    componentDidMount()
    {
        this.dimensions = {
            width: this.divRef.current.offsetWidth,
            height: this.divRef.current.offsetHeight
        };
    }

    render()
    {
        const style = this.props.style || {};

        if (this.props.width || this.props.height)
        {
            style.width = this.props.width;
            style.height = this.props.height;
        }
        else if (this.props.flex)
        {
            style.flex = this.props.flex;
        }

        return (
            <div
                ref={this.divRef}
                id={this.props.id}
                className={this.props.className || ''}
                style={style}
                onClick={this.props.onClick}
            >
                {typeof this.props.children === 'string' ? <T>{this.props.children}</T> : this.props.children}
            </div>
        );
    }
}

Container.propTypes = {
    id: PropTypes.string,
    className: PropTypes.string,
    style: PropTypes.object,
    flex: PropTypes.number,
    width: PropTypes.string,
    height: PropTypes.string,
    onClick: PropTypes.func
};

Container.defaultProps = {
    onClick: () =>
    {}
};
