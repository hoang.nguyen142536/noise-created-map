import React, { useState, useEffect } from 'react';

import { Container } from 'components/bases/Container/Container';

export default {
    title: 'Bases/Layout/Container',
    component: Container,
}

const Template = (args) =>
{
    return (
        <Container {...args}>
            Inner DOM element
        </Container>
    );
}

export const Default = Template.bind({});



