import './Table.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { T } from 'components/bases/Translate/Translate';
import { Row } from 'components/bases/Row/Row';
import { Container } from 'components/bases/Container/Container';
import { ScrollView } from 'components/bases/ScrollView/ScrollView';
import { Column } from 'components/bases/Column/Column';
import { FAIcon } from 'components/bases/Icon/FAIcon';

export class Table extends Component
{
    render()
    {
        const { headers, isFixedHeader, className, isLoading } = this.props;

        return (
            <ScrollView className={`${className}`}>
                <table className={`table-panel ${isFixedHeader ? 'fixed-header' : ''}`}>
                    <thead>
                        <tr>
                            {
                                Array.isArray(headers) && headers.map((h, index) =>
                                    h.isUseChild ?
                                        <th
                                            key={index}
                                            style={{ width: h.width }}
                                            colSpan={h.col}
                                        >
                                            {h.child}
                                        </th> :
                                        <th
                                            key={index}
                                            style={{ width: h.width }}
                                            colSpan={h.col || h.subCols?.length}
                                        >
                                            <Container><T>{h.label}</T></Container>
                                            {
                                                h.subCols &&
                                            (
                                                <Row
                                                    crossAxisSize={'min'}
                                                    mainAxisAlignment={'space-around'}
                                                    itemMargin={'md'}
                                                >
                                                    {h.subCols.map((sub, index) => <Container key={index}>{sub.label}</Container>)}
                                                </Row>
                                            )
                                            }
                                        </th>
                                )
                            }
                        </tr>
                    </thead>

                    {
                        !isLoading && this.props.children &&
                        <tbody>
                            {this.props.children}
                        </tbody>
                    }

                </table>

                {
                    !isLoading &&
                    (
                        !this.props.children ||
                        (Array.isArray(this.props.children) && this.props.children.length === 0) ||
                        typeof this.props.children === 'boolean'
                    ) &&
                    <div
                        style={{
                            width: '100%',
                            top: '50%',
                            textAlign: 'center',
                            paddingTop: '2rem',
                            position: 'absolute'
                        }}
                    >
                        <div style={{ opacity: '0.7' }}>Không có dữ liệu</div>
                    </div>
                }

                {
                    isLoading &&
                    <Column
                        mainAxisAlignment={'center'}
                        crossAxisAlignment={'center'}
                    >
                        <FAIcon
                            icon={'spinner-third'}
                            type={'duotone'}
                            size={'3rem'}
                            spin
                        />
                    </Column>
                }
            </ScrollView>
        );
    }
}

Table.propTypes = {
    className: PropTypes.string,
    headers: PropTypes.array,
    isFixedHeader: PropTypes.bool,
    width: PropTypes.string,
    isLoading: PropTypes.bool
};

Table.defaultProps = {
    className: '',
    headers: [],
    isFixedHeader: false
};

export class TableRow extends Component
{
    render()
    {
        return (
            <tr
                className={this.props.className}
                onClick={this.props.onClick}
                style={this.props.isSelected ? {
                    backgroundColor: "rgb(0,84,165)",
                }
              : {}}
            >
                {this.props.children}
            </tr>
        );
    }
}

TableRow.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
    isSelected: PropTypes.bool
};

TableRow.defaultProps = {
    className: '',
    onClick: () =>
    {
    },
    isSelected: false,
};

export class TableRowCell extends Component
{
    render()
    {
        return (
            <td className={this.props.className}>
                {this.props.children}
            </td>
        );
    }
}

TableRowCell.propTypes = {
    className: PropTypes.string
};

TableRowCell.defaultProps = {
    className: ''
};
