import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

import { Row } from 'components/bases/Row/Row';
import SearchBox from 'components/bases/Input/SearchBox';
import { Button } from 'components/bases/Button/Button';
import { PopOver } from 'components/bases/Modal/PopOver';
import { ColumnSelector } from 'components/bases/DataGrid/ColumnSelector';

export const DataGridToolbar = (props) =>
{
    // const { showFullScreenSelector, showSortSelector, showStyleSelector } = props.toolbarVisibility;
    const { cols, setCols } = props;

    const [columnSelectorVisibility, setColumnSelectorVisibility] = useState(false);
    const columnSelectorButtonRef = useRef();

    const [searchValue, setSearchValue] = useState(props.searching?.searchKey);
    const handleSearch = (value) =>
    {
        setSearchValue(value);
        props.searching?.onSearch && props.searching.onSearch(value);
    };

    return (
        <div className={'toolbar'}>
            <Row>
                <Row
                    itemMargin={'md'}
                >
                    {
                        props.searching &&
                        <SearchBox
                            width={'18rem'}
                            placeholder="Nhập từ khóa để tìm kiếm"
                            value={searchValue}
                            onChange={handleSearch}
                        />
                    }
                    {
                        props.toolbarVisibility?.showColumnSelector &&
                        <Button
                            innerRef={columnSelectorButtonRef}
                            tooltip={'Cột hiển thị'}
                            icon={'line-columns'}
                            onlyIcon
                            onClick={() => setColumnSelectorVisibility(!columnSelectorVisibility)}
                        />
                    }
                    {
                        columnSelectorVisibility &&
                        <PopOver
                            anchorEl={columnSelectorButtonRef}
                            onBackgroundClick={() => setColumnSelectorVisibility(!columnSelectorVisibility)}
                        >
                            <DndProvider backend={HTML5Backend}>
                                <ColumnSelector
                                    columns={cols}
                                    setColumns={setCols}
                                    onClick={props.toggleColumnVisibility}
                                    hideAllColumns={props.hideAllColumns}
                                    showAllColumns={props.showAllColumns}
                                    defaultColumns={props.defaultColumns}
                                />
                            </DndProvider>
                        </PopOver>
                    }
                    {
                        props.loading &&
                        <Button
                            icon={'redo-alt'}
                            tooltip={'Tải lại danh sách'}
                            onlyIcon
                            isLoading={props.loading.isLoading}
                            onClick={props.loading.onRefresh}
                        />
                    }
                </Row>
            </Row>
        </div>
    );
};

DataGridToolbar.propTypes = {
    items: PropTypes.arrayOf(PropTypes.object),
    searching: PropTypes.object,
    loading: PropTypes.object,

    cols: PropTypes.array,
    setCols: PropTypes.func,

    // not implement yet - nice to have
    toolbarVisibility: PropTypes.shape({
        showColumnSelector: PropTypes.bool,
        showStyleSelector: PropTypes.bool,
        showSortSelector: PropTypes.bool,
        showFullScreenSelector: PropTypes.bool
    }),
    toggleColumnVisibility: PropTypes.func,
    
    // Button click handler
    defaultColumns: PropTypes.func,
    hideAllColumns: PropTypes.func,
    showAllColumns: PropTypes.func,
};

export default DataGridToolbar;
