import React from 'react';
import moment from 'moment';

import { CheckBox } from 'components/bases/CheckBox/CheckBox';
import { Link } from 'components/bases/Link/Link';
import { Tag } from 'components/bases/Tag/Tag';
import { Image } from 'components/bases/Image/Image';
import { DataGridChartCell } from './DataGridChartCell';

// The Regex and test cases can be found in this link
// https://stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript
const formatCurrency = (number, locale) =>
{
    switch (locale)
    {
        case 'vi':
        {
            const parts = number.toString().split('.');
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
            return parts.join('.') + 'đ';
        }
        default:
        {
            const parts = number.toString().split('.');
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            return '$' + parts.join('.');
        }
    }
};

// format: \# or #.## or #,##
const formatNumeric = (number, format) =>
{
    switch (format)
    {
        case '#,##':
            return Number(number).toFixed(2).toString().replaceAll('.', ',');
        case '#.##':
            return Number(number).toFixed(2);
        default:
            return number;
    }
};

const DataGridCell = ({ definition, content, style }) =>
{
    const { schema, locale = 'vi', format, options } = definition;

    switch (schema)
    {
        case 'boolean':
            return <CheckBox checked={!!content}/>;

        case 'currency':
            return <div className={'number'} style={style}>{formatCurrency(content, locale)}</div>;

        case 'datetime':
        case 'date':
            return (
                content ? <div className='datetime'>
                    <span className='date'>{moment(content).format(format || 'L')}</span>
                    {schema === 'datetime' && <span className='time'> {moment(content).format(format || 'LT')}</span>}
                </div> : ''
            );

        case 'numeric':
            return <div className={'number'} style={style}>{formatNumeric(content, format)}</div>;

        case 'json':
            return content ? JSON.stringify(content) : null;

        case 'link':
            return <Link href={content ?? '#'}>{content}</Link>;

        case 'select':
        {
            const option = options?.find((option) => option.id === content);
            const defaultColor = 'var(--default-color)';
            return option ?
                <Tag
                    text={option.label}
                    color={option.color || defaultColor}
                    textStyle={style}
                /> :
                <Tag
                    text={content}
                    color={defaultColor}
                    textStyle={style}
                />;
        }

        case 'multi-select':
            return options?.filter((option) => content?.includes(option.id)).map((option) =>
                <Tag
                    text={option?.label}
                    color={option?.color}
                    key={option?.id}
                    textStyle={style}
                />
            );

        case 'image':
            return <Image src={content} />;

        case 'react-node':
            return content || null;

        case 'chart':
            return <DataGridChartCell content={content} options={definition} />;

        default:
            return content ? <div style={style} className={'text'}>{String(content)}</div> : null;
    }
};

export { DataGridCell };
