import React from 'react';
import { PropTypes } from 'prop-types';

import { Row } from 'components/bases/Row/Row';
import { PaginationRow } from 'components/bases/DataGrid/PaginationRow';
import { PagingCount } from 'components/bases/DataGrid/PagingCount';
import { Spacer } from 'components/bases/spacer/Spacer';
import { FAIcon } from 'components/bases/Icon/FAIcon';
import { T } from 'components/bases/Translate/Translate';

const DataGridBottomBar = (props) =>
{
    const { total = props.items?.length, pagination, externalPaginationRow } = props;

    return (
        <div className={'toolbar'}>
            <Row
                mainAxisAlignment={'end'}
                crossAxisAlignment={'center'}
                itemMargin={'md'}
            >
                {
                    props.isLoading &&
                    <>
                        <T>Loading</T>
                        <FAIcon
                            icon={'spinner'}
                            spin
                            size={'1rem'}
                        />
                    </>
                }
                {
                    !pagination?.useInfiniteScroll &&
                    <>
                        <PagingCount
                            total={total}
                            pageSize={pagination?.pageSize}
                            currentPage={pagination?.pageIndex}
                        />
                        {
                            pagination && !externalPaginationRow &&
                            <>
                                <Spacer
                                    style={{ marginLeft: 'auto' }}
                                    size={'1.5rem'}
                                />
                                <PaginationRow
                                    total={total}
                                    {...pagination}
                                />
                            </>
                        }
                    </>
                }
            </Row>
        </div>
    );
};

DataGridBottomBar.propTypes = {
    isLoading: PropTypes.bool
};

export { DataGridBottomBar };
