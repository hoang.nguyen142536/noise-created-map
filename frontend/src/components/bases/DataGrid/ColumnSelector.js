import './ColumnSelector.scss';

import React, { useEffect, useState } from 'react';
import { useDrag, useDrop } from 'react-dnd';
import ReactSwitch from 'react-switch';
import PerfectScrollbar from 'react-perfect-scrollbar';
import update from 'immutability-helper';

import { Row } from 'components/bases/Row/Row';
import { FAIcon } from 'components/bases/Icon/FAIcon';
import { Button } from 'components/bases/Button/Button';
import { T } from 'components/bases/Translate/Translate';

const ColumnSelector = ({ columns, setColumns, onClick, className, hideAllColumns, showAllColumns, defaultColumns }) =>
{
    const [items, setItems] = useState(columns || []);

    useEffect(() =>
    {
        setItems(columns);
    }, [columns]);

    const [, drop] = useDrop({ accept: 'column' });

    const cards = items?.map((card, index) => (
        <Card
            key={card.id || index}
            id={`${card.id}`}
            moveCard={moveCard}
            findCard={findCard}
        >
            <Row
                className={'row'}
                itemMargin={'sm'}
            >
                <ReactSwitch
                    checked={!card.hidden}
                    onChange={() => onClick(card.id)}
                    width={28}
                    height={14}
                    onColor="#86d3ff"
                    onHandleColor="#2693e6"
                    uncheckedIcon={false}
                    checkedIcon={false}
                />
                <span className={'text'}>
                    <T>{card.displayAsText || card.display}</T>
                </span>
                <FAIcon
                    icon={'grip-vertical'}
                    type={'solid'}
                    size={'0.75rem'}
                />
            </Row>
        </Card>
    ));

    return (
        <div
            className={'cs-container ' + className}
            ref={drop}
            onDrop={() => setColumns(items)}
        >
            <PerfectScrollbar
                options={{ suppressScrollX: true }}
                className={'cs-cards'}
            >
                {cards}
            </PerfectScrollbar>

            <div className={'cs-buttons'}>
                <Button
                    text={'Ẩn tất cả'}
                    onClick={hideAllColumns}
                />
                <Button
                    text={'Hiện tất cả'}
                    onClick={showAllColumns}
                />
                <Button
                    text={'Mặc định'}
                    onClick={defaultColumns}
                />
            </div>
        </div>
    );

    function moveCard(id, atIndex)
    {
        const { card, index } = findCard(id);
        setItems(update(items, { $splice: [[index, 1], [atIndex, 0, card]] }));
    }

    function findCard(id)
    {
        for (let index = 0; index < items.length; index++)
        {
            if (items[index].id === id)
            {
                return { card: items[index], index };
            }
        }
    }
};

const Card = ({ key, id, moveCard, findCard, children }) =>
{
    const originalIndex = findCard(id).index;

    const [{ isDragging }, drag] = useDrag(
        {
            item: { type: 'column', id, originalIndex },
            collect: (monitor) => ({ isDragging: monitor.isDragging() }),
            end: (dropResult, monitor) =>
            {
                if (!monitor.didDrop())
                {
                    const { id: droppedId, originalIndex } = monitor.getItem();
                    moveCard(droppedId, originalIndex);
                }
            }
        }
    );

    const [, drop] = useDrop(
        {
            accept: 'column',
            canDrop: () => false,
            hover({ id: draggedId })
            {
                if (draggedId !== id)
                {
                    const { index: overIndex } = findCard(id);
                    moveCard(draggedId, overIndex);
                }
            }
        }
    );

    return (
        <div
            key={key}
            ref={(node) => drag(drop(node))}
            style={{ opacity: isDragging ? 0 : 1 }}
        >
            {children}
        </div>
    );
};

export { ColumnSelector };
