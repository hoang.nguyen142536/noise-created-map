import React, { useState } from 'react';
import faker from 'faker';
import moment from 'moment';

import { DataGrid } from './DataGrid';
import { DATA_TYPE, isFilterActive } from 'helper/data.helper';

export default {
    title: 'Bases/DataGrid/Enhancements',
    component: DataGrid
};

export function Filter()
{
    const [conds, setConds] = useState([]);
    const [data, setData] = useState(genData(50) || []);

    const handleFilterChange = (newConds) =>
    {
        console.log(newConds);
        setConds(newConds);
    };

    let filteredData = [...data];
    if (conds && conds.length > 0)
    {
        conds.forEach((cond) =>
        {
            if (!isFilterActive(cond))
            {
                return;
            }

            switch (cond.dataType)
            {
                case DATA_TYPE.integer:
                    filteredData = filteredData.filter((item) => compareInt(item[cond.columnName], cond.value, cond.operator));
                    break;
                case DATA_TYPE.string:
                    filteredData = filteredData.filter((item) =>
                    {
                        if (cond.operator === '=')
                        {
                            return item[cond.columnName] === cond.value;
                        }
                        if (cond.operator === 'like')
                        {
                            return item[cond.columnName].includes(cond.value);
                        }
                        return ((cond.operator === 'is null' && item[cond.columnName] === '') || (cond.operator === 'is not null' && item[cond.columnName] !== ''));
                    });
                    break;
                case DATA_TYPE.boolean:
                    filteredData = filteredData.filter((item) =>
                    {
                        if (cond.operator === '=')
                        {
                            return !(Boolean(item[cond.columnName]) ^ Boolean(cond.value));
                        }
                        return true;
                    });
                    break;
                case DATA_TYPE.datetime:
                    filteredData = filteredData.filter((item) => compareDateTime(item[cond.columnName], cond.value, cond.operator));
                    break;
                case DATA_TYPE.array:
                    filteredData = filteredData.filter((item) =>
                    {
                        if (cond.operator === '=')
                        {
                            return item[cond.columnName] === cond.value;
                        }
                        return true;
                    });
                    break;
                default:
            }
        });
    }

    return (
        <>
            <DataGrid
                items={filteredData}
                columns={columnDefinitions}
                rowKey={'numeric'}

                filter={{
                    conditions: conds,
                    onChange: handleFilterChange
                }}
            />
        </>
    );
}

export function InfiniteScroll()
{
    const [data, setData] = useState(genData(400) || []);
    const [start, setStart] = useState(0);
    const [end, setEnd] = useState(100);

    const [tableData, setTableData] = useState([]);

    const pageSize = 100;

    const handleOnChangePageSize = (pageIndex, keepOldItems, onDone) =>
    {
        return new Promise(resolve =>
        {
            setTimeout(() =>
            {
                setEnd(end + pageSize);
                onDone && onDone();
                resolve();
            }, 1000);
        });

    };

    return (
        <>
            <DataGrid
                items={data.slice(start, end)}
                columns={columnDefinitions}
                rowKey={'numeric'}
                pagination={{
                    useInfiniteScroll: true,
                    pageIndex: 0,
                    pageSize: pageSize,
                    pageSizeOptions: [100],
                    onChangePage: handleOnChangePageSize
                }}
            />
        </>
    );
}

const columnDefinitions = [
    {
        id: 'numeric',
        width: 200,
        displayAsText: 'Numeric',
        schema: 'numeric'
    },

    {
        id: 'currency',
        width: 200,
        displayAsText: 'Currency',
        schema: 'currency',
        locale: 'vi'
    },
    {
        id: 'datetime',
        displayAsText: 'Datetime',
        schema: 'datetime',
        format: 'YYYY/MM/DD mm:HH',
        width: 200
    },
    {
        id: 'date',
        displayAsText: 'Date',
        schema: 'date',
        width: 200
    },
    {
        id: 'boolean',
        width: 200,
        displayAsText: 'Boolean',
        schema: 'boolean'
    },
    {
        id: 'select',
        displayAsText: 'Select',
        display: <span style={{ color: 'red' }}>Code</span>,
        schema: 'select',
        isSortable: true,
        width: 200,
        defaultSortDirection: 'desc',
        options: [
            { label: 'code1', id: 'Code 1', color: 'green' },
            { label: 'code2', id: 'Code 2', color: 'orange' },
            { label: 'code3', id: 'Code 3', color: 'brown' }
        ]
    },
    {
        id: 'multiSelect',
        displayAsText: 'Multi-select',
        display: <span style={{ color: 'red' }}>Code</span>,
        schema: 'select',
        isSortable: true,
        width: 200,
        defaultSortDirection: 'desc',
        options: [
            { label: 'code1', id: 'Code 1', color: 'green' },
            { label: 'code2', id: 'Code 2', color: 'orange' },
            { label: 'code3', id: 'Code 3', color: 'brown' }
        ]
    },
    {
        id: 'chart',
        width: 200,
        height: 120,
        displayAsText: 'Chart',
        schema: 'chart',
        chartType: 'line',
        isMiniStyle: true
    },
    {
        id: 'string',
        width: 200,
        displayAsText: 'String'
    },
    {
        id: 'link',
        schema: 'link',
        displayAsText: 'Link',
        width: 200
    },
    {
        id: 'json',
        schema: 'json',
        displayAsText: 'JSON',
        width: 200
    }
];

function generateUser(index)
{
    return {
        numeric: index,
        currency: faker.random.number() * 1000,
        datetime: faker.date.future(),
        date: faker.date.past(),
        boolean: faker.random.boolean(),
        select: String(faker.random.arrayElement(columnDefinitions.find(item => item.id === 'select')?.options?.map(item => item.id))),
        multiSelect: faker.random.arrayElements(columnDefinitions.find(item => item.id === 'multiSelect')?.options?.map(item => item.id)),
        chart: {
            labels: ['0s', '10s', '20s', '40s', '50s', '60s'],
            datasets: [{
                data: [0, 59, 75, 20, 20, 55, 40]
            }]
        },
        string: faker.lorem.sentence(),
        link: 'www.google.com',
        json: { 'field1': 'value1', 'field2': 'value2' }
    };
}

function genData(count)
{
    const result = [];
    for (var i = 0; i < count; i++)
    {
        result.push(generateUser(i));
    }
    return result;
}

function compareInt(x, y, operator)
{
    if (operator === 'between')
    {
        const x_int = parseInt(x);
        const values = y.split('AND');
        if (values.length < 3)
        {
            return;
        }

        const y1_int = parseInt(values[0].trim());
        const y2_int = parseInt(values[1].trim());

        if (isNaN(y1_int) || isNaN(y2_int) || isNaN(x_int))
        {
            return true;
        }
        return ((y1_int <= x_int && x_int <= y2_int) || (y2_int <= x_int && x_int <= y1_int));
    }

    const x_int = parseInt(x);
    const y_int = parseInt(y);
    if (isNaN(x_int) || isNaN(y_int))
    {
        return true;
    }
    switch (operator)
    {
        case '=':
            return x_int === y_int;
        case '>':
            return x_int > y_int;
        case '<':
            return x_int < y_int;
        case '>=':
            return x_int >= y_int;
        case '<=':
            return x_int <= y_int;
    }
    return true;
}

function compareDateTime(x, y, operator)
{
    if (operator === 'between')
    {
        const x_m = moment(x);
        const values = y.split('AND');
        if (values.length < 3)
        {
            return;
        }

        const y1_m = moment(values[0].trim());
        const y2_m = moment(values[1].trim());

        return ((y1_m.isSameOrBefore(x_m) && x_m.isSameOrBefore(y2_m)) || (y2_m.isSameOrBefore(x_m) && x_m.isSameOrBefore(y1_m)));
    }

    const x_int = moment(x);
    const y_int = moment(y);
    switch (operator)
    {
        case '=':
            return x_int.isSame(y_int, 'day');
        case '>':
            return x_int.isAfter(y_int);
        case '<':
            return x_int.isBefore(y_int);
        case '>=':
            return x_int.isSameOrAfter(y_int);
        case '<=':
            return x_int.isSameOrBefore(y_int);
    }
    return true;
}
