import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { DataGridCell } from 'components/bases/DataGrid/DataGridCell';

export const DataGridSummaryCell = (props) =>
{
    const { col, style, items } = props;
    const { label, formula } = col.summary || {};
    const [result, setResult] = useState('');

    const colItems = items.map(row => row[col.id]);

    const sum = () => colItems.reduce((pre, cur) => pre += cur);

    const calcSummary = async () =>
    {
        if (typeof (formula) === 'function')
        {
            return setResult(await formula(colItems));
        }

        if (typeof (formula) === 'string')
        {
            switch (formula)
            {
                case 'average':
                    return setResult(sum() / (colItems.length));
                default:
                    return setResult(sum());
            }
        }
    };

    useEffect(() =>
    {
        calcSummary();
    }, [formula]);

    return (
        <div
            className={'dg-header-item'}
            style={style}
        >
            {
                formula &&
                <div className={'dg-cell'}>
                    {label ? label + ': ' : ''}
                    <DataGridCell
                        definition={col}
                        content={`${result}`}
                    />
                </div>
            }
        </div>
    );
};

DataGridSummaryCell.propTypes = {
    col: PropTypes.object,
    style: PropTypes.object,
    items: PropTypes.array
};
