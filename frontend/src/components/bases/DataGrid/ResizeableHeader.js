import React, { useRef, useState } from 'react';
import { Resizable } from 'react-resizable';
import PropTypes from 'prop-types';

import { FAIcon } from 'components/bases/Icon/FAIcon';
import { T } from 'components/bases/Translate/Translate';
import { FilterInput } from 'components/bases/DataToolBar/DataToolBarActive/FilterInput';
import { PopOver } from 'components/bases/Modal/PopOver';
import { EmptyButton } from 'components/bases/Button/Button';

import { SCHEMAS, getDataType, isFilterActive } from 'helper/data.helper';

const ResizeableHeader = (props) =>
{
    const {
        id,
        width,
        displayAsText,
        schema,
        style,
        isResizable,
        onResize,
        isDraggable,
        display,
        defaultSortDirection,
        isSortable,
        sortDirection,
        onClick,
        // Filter props
        filter,
        onFilterChange,
        
        freeze,
        freezeEnd,
        ...restProps
    } = props;

    const filterButtonRef = useRef();
    const [isOpeningFilterPopover, setOpeningFilterPopover] = useState(false);
    const [resizing, setResizing] = useState(false);

    const cond = filter && filter.conditions.find((f) => f.columnName === id);
    const options = cond?.options;

    const mappedFilter = {
        ...cond,
        columnName: id || '',
        dataType: getDataType(schema)
    };

    const mappedOptions = (schema && getDataType(schema) === 'array') ? options?.map((op) => ({ Display: op.label, Value: op.id })) : null;

    const plainHeader = (
        <div
            className={`dg-header-item ${freeze ? 'dg-freeze' : ''} ${freezeEnd ? 'dg-freeze-end' : ''} ${isSortable ? 'dg-header-sort' : ''} ${sortDirection ? 'dg-header-sorting' : ''}`}
            style={{
                ...style
                // cursor: `${isDraggable ? 'move' : 'default'}`,
                // userSelect: `${isDraggable ? 'auto' : 'none'}`
            }}
        >
            <div
                className={'dg-cell'}
                onClick={() => isSortable && onClick({ id, direction: changeSortDirection(sortDirection) })}
            >
                <div>
                    <T>{display || displayAsText}</T>
                    {
                        (isSortable || defaultSortDirection) &&
                        <FAIcon
                            icon={getSortIcon(sortDirection)}
                            type={'duotone'}
                            size={'1rem'}
                            className={'dg-header-sort-icon'}
                        />
                    }
                </div>
                {
                    filter &&
                    <div
                        ref={filterButtonRef}
                        className={`cell-filter-icon ${isFilterActive(cond) ? 'active' : 'inactive'}`}
                    >
                        <EmptyButton
                            icon={'filter'}
                            iconType={'solid'}
                            iconSize={'0.8rem'}
                            iconColor={`${isFilterActive(cond) ? 'var(--primary)' : ''}`}
                            onlyIcon
                            onClick={(e) =>
                            {
                                e.stopPropagation();
                                setOpeningFilterPopover(true);
                            }}
                        />
                    </div>
                }
                {
                    isOpeningFilterPopover &&
                    <PopOver
                        anchorEl={filterButtonRef}
                        onBackgroundClick={() => setOpeningFilterPopover(false)}
                    >
                        <FilterInput
                            isStandalone
                            showVertical
                            clearable
                            isChangedOnConfirm
                            filter={mappedFilter}
                            config={mappedOptions}
                            onFilterChange={onFilterChange}
                        />
                    </PopOver>
                }
            </div>
        </div>
    );

    function getSortIcon(direction)
    {
        switch (direction)
        {
            case 'asc':
                return 'sort-up';
            case 'desc':
                return 'sort-down';
            default:
                return 'sort';
        }
    }

    function changeSortDirection(direction)
    {
        switch (direction)
        {
            case 'asc':
                return 'desc';
            case 'desc':
                return undefined;
            default:
                return 'asc';
        }
    }

    if (!isResizable)
    {
        return plainHeader;
    }

    return (
        <Resizable
            width={width}
            height={0}
            onResizeStart={() => setResizing(true)}
            onResizeStop={() => setResizing(false)}
            onResize={onResize}
            draggable={isDraggable && !resizing}
        >
            {plainHeader}
        </Resizable>
    );
};

ResizeableHeader.propTypes = {
    id: PropTypes.string,
    style: PropTypes.object,
    displayAsText: PropTypes.string,
    isResizable: PropTypes.bool,
    isDraggable: PropTypes.bool,
    width: PropTypes.number,
    format: PropTypes.string, // use ### for numbers, ISO format for date time
    schema: PropTypes.oneOf(SCHEMAS), // A Schema to use for the column. Can be expanded by defining your own
    locale: PropTypes.oneOf(['vi']),
    display: PropTypes.node, // A ReactNode used when rendering the column header. When providing complicated content, please make sure to utilize CSS to respect truncation as space allows.
    onClick: PropTypes.func,
    onResize: PropTypes.func,

    // Sorting
    isSortable: PropTypes.bool,
    defaultSortDirection: PropTypes.oneOf(['asc', 'desc']),
    sortDirection: PropTypes.oneOf(['asc', 'desc']),

    // Filters
    filter: PropTypes.shape({
        conditions: PropTypes.arrayOf(
            PropTypes.shape({
                operator: PropTypes.string,
                value: PropTypes.any,
                options: PropTypes.array // select: [{id, label, color}]
            })
        ),
        onChange: PropTypes.func
    }),

    onFilterChange: PropTypes.func,

    // not implement yet but nice to have
    isExpandable: PropTypes.bool // Defaults to true. Defines whether or not the column's cells can be expanded with a popup onClick / keydown.
};

ResizeableHeader.defaultProps = {
    isResizable: true,
    isDraggable: true,
    width: 50
};

export { ResizeableHeader };
