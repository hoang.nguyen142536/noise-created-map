import './DataGrid.scss';

import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import { DataGridToolbar } from 'components/bases/DataGrid/DataGridToolbar';
import { DataGridBottomBar } from 'components/bases/DataGrid/DataGridBottomBar';
import { DataGridCell } from 'components/bases/DataGrid/DataGridCell';
import { ResizeableHeader } from 'components/bases/DataGrid/ResizeableHeader';
import { ScrollView } from 'components/bases/ScrollView/ScrollView';
import { T } from 'components/bases/Translate/Translate';
import { DataGridSummaryCell } from './DataGridSummaryCell';
import { EmptyData } from 'components/bases/Data/EmptyData';
import { CheckBox } from 'components/bases/CheckBox/CheckBox';

export const DataGrid = (props) =>
{
    const {
        columns,
        items,
        sorting,
        leadingControlColumns,
        trailingControlColumns,
        rowNumber,
        pagination,
        loading,
        summary,
        selectRows
    } = props;

    const [cols, setCols] = useState(columns);
    const [hideCols, setHideCols] = useState(true);
    const [dragOver, setDragOver] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const scrollRef = useRef();
    const [containerRef, setContainerRef] = useState();

    useEffect(() =>
    {
        setCols(columns);

        const checkColumn = columns?.find(col => !col.hidden);
        setHideCols(checkColumn);
    }, [columns]);

    const handleResize = (index) => (e, { size }) =>
    {
        const nextCols = [...cols]; // clone
        nextCols[index] = {
            ...nextCols[index],
            width: size.width
        };

        setCols(nextCols);
    };

    const handleDragStart = (e) =>
    {
        const { id } = e.target;
        const idx = cols.findIndex((i) => i.id === id);
        e.dataTransfer.setData('colIdx', idx);
    };

    const handleDragOver = (e) => e.preventDefault();

    const handleDragEnter = (e) =>
    {
        const { id } = e.target;
        setDragOver(id);
    };

    const handleOnDrop = (e) =>
    {
        let { id } = e.target;
        if (!id)
        {
            id = e.target.parentNode.id; // fix element was dropped on resizable icon
        }

        const tempCols = [...cols];
        const draggedColIdx = parseInt(e.dataTransfer.getData('colIdx'));
        const cutOut = tempCols.splice(draggedColIdx, 1)[0]; // cut the element at index 'draggedColIdx'

        let droppedColIdx = parseInt(tempCols.findIndex((i) => i.id === id));

        // calculate new position of dragged element
        const rect = e.target.getBoundingClientRect();
        const isFirstHalf = e.clientX < rect.left + rect.width / 2;
        if (!isFirstHalf)
        {
            droppedColIdx += 1;
        }

        tempCols.splice(droppedColIdx, 0, cutOut); // insert it at index 'droppedColIdx'

        setCols(tempCols);
        setDragOver('');
    };

    function hideAllColumns()
    {
        setHideCols(false);
        setCols(cols?.map((col) =>
        {
            return { ...col, hidden: true };
        }));
    }

    function showAllColumns()
    {
        setHideCols(true);
        setCols(cols?.map((col) =>
        {
            return { ...col, hidden: false };
        }));
    }

    function defaultColumns()
    {
        setHideCols(true);
        setCols(columns?.map((col) =>
        {
            return { ...col };
        }));
    }

    function toggleColumnVisibility(id)
    {
        setCols(cols?.map((col) =>
        {
            if (col.id === id)
            {
                return { ...col, hidden: !col.hidden };
            }
            return col;
        }));
    }

    const getPosition = (col) => 
    {
        const freezeArr = cols.filter(col => col.freeze);
        if (selectRows) 
        {
            freezeArr.unshift({
                width: 50,
                id: 'auto_number_id',
            });
        }
        if (rowNumber)
        {
            freezeArr.unshift({
                width: 50,
                id: 'auto_checkbox_id',
            });
        }
        let index = freezeArr.findIndex(freeze => freeze.id === col.id);
        let arrCopy = [];
        for (let i = 0; i < index; i++)
        {
            arrCopy.push(freezeArr[i]);
        }
        const sum = 0;
        const left = arrCopy.reduce((pre, cur) => pre + cur.width, sum);

        return left
    };

    const getCellStyle = (col) =>
    {
        if (col.width)
        {
            if(col.freeze)
            {
                return { flex: `0 0 ${col.width}px`, maxWidth: col.width, left: getPosition(col) }; 
            }
            else
            {
                return { flex: `0 0 ${col.width}px`, maxWidth: col.width };
            }
        }
        else
        {
            return { flex: 1 };
        }
    };

    const HeaderItem = (col, index) =>
    {
        return (
            <div
                key={col.id || index}
                className={`dg-header-item ${col.freeze ? 'dg-freeze' : ''} ${col.freezeEnd ? 'dg-freeze-end' : ''}`}
                style={getCellStyle(col)}
            >
                <div className={'dg-cell'}>
                    <T>{col.headerCellRender}</T>
                </div>
            </div>
        );
    };

    const CellItem = ({ row, col, index }) =>
    {
        return (
            <div
                key={col.id || index}
                className={`dg-row-item ${col.freeze ? 'dg-freeze' : ''} ${col.freezeEnd ? 'dg-freeze-end' : ''}`}
                style={getCellStyle(col)}
            >
                <div className={'dg-cell'}>
                    {col.rowCellRender(row, index)}
                </div>
            </div>
        );
    };

    const cellContentStyle = (col) =>
    {
        switch (col.textWrapping) {
            case 'nowrap':
                return { textOverflow: 'ellipsis',  maxWidth: '100%', whiteSpace: 'nowrap', overflow: 'hidden'};
            default:
                return {};
        }
    };

    const RenderNumberRows = ({ col, index, pageSize, pageIndex }) =>
    {
        const startIndex = index + pageSize * pageIndex - pageSize;

        return (
            <div
                key={col.id || index}
                className={`dg-row-item ${col.freeze ? 'dg-freeze' : ''}`}
                style={getCellStyle(col)}
            >
                <div className={'dg-cell'}>
                    {startIndex + 1}
                </div>
            </div>
        );
    };

    const handleSort = ({ id: sortingId, direction: sortingDirection }) =>
    {
        if (sorting)
        {
            if (sorting.isSingleSort)
            {
                sorting.onSort([{ id: sortingId, direction: sortingDirection }]);
                return;
            }

            let found = false;
            const columns = sorting.columns?.map(({ id, direction }) =>
            {
                if (id === sortingId)
                {
                    found = true;
                    return { id, direction: sortingDirection };
                }

                return { id, direction };
            }) || [];

            if (!found)
            {
                columns.push({ id: sortingId, direction: sortingDirection });
            }

            sorting.onSort(columns);
        }
    };

    const handleFilterChange = (filter) =>
    {
        if (props.filter)
        {
            const newFilters = (props.filter.conditions || []).filter((f) => f.columnName !== filter.columnName);
            newFilters.push(filter);

            props.filter.onChange && props.filter.onChange(newFilters);
        }
    };

    const handleScrollDown = () =>
    {
        if (!isLoading &&
            props.pagination?.useInfiniteScroll &&
            containerRef.scrollHeight - containerRef.scrollTop < 3 * containerRef.clientHeight
        )
        {
            // Load more when 2 view height left
            setIsLoading(true);

            const prm = props.pagination?.onChangePage && props.pagination.onChangePage(pagination?.pageIndex, true, () => setIsLoading(false));

            if (prm && prm.then)
            {
                prm.then(() =>
                {
                    setIsLoading(false);
                });
            }
        }
    };

    const autoNumberRows = [];
    if (rowNumber)
    {
        autoNumberRows.push({
            headerCellRender: 'STT',
            width: 50,
            id: 'auto_number_id',
            freeze: true
        });
    }

    const checkboxHeaderControl = [];
    if (selectRows)
    {
        checkboxHeaderControl.push({
            width: 50,
            id: 'auto_checkbox_id',
            freeze: true
        });
    }

    const CheckBoxHeaderItem = (col, index) =>
    {
        const selected = items.filter(row =>row.isSelected);

        return (
            <div
                key={col.id || index}
                className={`dg-header-item ${col.freeze ? 'dg-freeze' : ''}`}
                style={getCellStyle(col)}
            >
                <div className={'dg-cell check-cell'}>
                    <CheckBox
                        indeterminate={selected.length > 0 && selected.length < items.length}
                        checked={selected.length > 0 && selected.length <= items.length}
                        onChange={(event) => selectRows.onChangeAll(event, items)}
                    />
                </div>
            </div>
        );
    }

    const CheckBoxItem = ({ row, col, index }) =>
    {
        return (
            <div
                key={col.id || index}
                className={`dg-row-item ${col.freeze ? 'dg-freeze' : ''}`}
                style={getCellStyle(col)}
            >
                <div className={'dg-cell check-cell'}>
                    <CheckBox
                        checked={row.isSelected}
                        onChange={(event) => selectRows.onChange(event, row)}
                    />
                </div>
            </div>
        )
    }

    return (
        <div className={'data-grid-wrapper'}>
            <div className={'data-grid-content'}>
                <DataGridToolbar
                    {...props}
                    loading={loading}
                    cols={cols}
                    setCols={setCols}
                    hideAllColumns={hideAllColumns}
                    showAllColumns={showAllColumns}
                    defaultColumns={defaultColumns}
                    toggleColumnVisibility={toggleColumnVisibility}
                />

                {props.header && <h1>{props.header}</h1>}

                <div className={'data-grid-container'}>
                    {
                        cols?.length > 0 &&
                        <ScrollView
                            ref={scrollRef}
                            containerRef={(ref) => setContainerRef(ref)}
                            onScrollDown={() => handleScrollDown()}
                        >
                            <div className={`data-grid ${props.className || ''}`}>
                                <div className={'dg-header'}>
                                    {hideCols ? checkboxHeaderControl?.map(CheckBoxHeaderItem) : null}
                                    {hideCols ? autoNumberRows?.map(HeaderItem) : null}
                                    {hideCols ? leadingControlColumns?.map(HeaderItem) : null}

                                    {
                                        cols?.map((col, index) => !col.hidden &&
                                            <ResizeableHeader
                                                key={col.id}
                                                width={col.width}
                                                style={getCellStyle(col)}
                                                onDragStart={handleDragStart}
                                                onDragOver={handleDragOver}
                                                onResize={handleResize(index)}
                                                onDrop={handleOnDrop}
                                                onDragEnter={handleDragEnter}
                                                sortDirection={sorting?.columns?.filter(({ id, direction }) => id === col.id)[0]?.direction}
                                                onClick={handleSort}
                                                isDraggable={false}
                                                filter={props.filter}
                                                onFilterChange={handleFilterChange}
                                                {...col}
                                            />
                                            // isDraggable too buggy, disabled
                                        )
                                    }

                                    {hideCols ? trailingControlColumns?.map(HeaderItem) : null}
                                </div>

                                <div className={'dg-body'}>
                                    {
                                        items && items.length > 0 ? items.map((row, rowIndex) =>
                                            <div
                                                className='dg-row'
                                                key={row[props.rowKey]}
                                            >
                                                {
                                                    hideCols &&
                                                    checkboxHeaderControl?.map((col, index) =>
                                                        <CheckBoxItem
                                                            row={row}
                                                            col={col}
                                                            index={index}
                                                        />
                                                    )
                                                }

                                                {
                                                    hideCols &&
                                                    autoNumberRows?.map((col) =>
                                                        <RenderNumberRows
                                                            col={col}
                                                            index={rowIndex}
                                                            pageSize={pagination?.pageSize}
                                                            pageIndex={pagination?.pageIndex}
                                                        />
                                                    )
                                                }

                                                {
                                                    hideCols &&
                                                    leadingControlColumns?.map((col, index) =>
                                                        <CellItem
                                                            row={row}
                                                            col={col}
                                                            index={index}
                                                        />
                                                    )
                                                }

                                                {
                                                    cols?.map((col) =>
                                                    {
                                                        const isSorting = sorting?.columns?.find(c => c.id === col.id)?.direction;

                                                        if (!col.hidden)
                                                        {
                                                            return (
                                                                <div
                                                                    className={`dg-row-item ${col.freeze ? 'dg-freeze' : ''} ${col.freezeEnd ? 'dg-freeze-end' : ''} ${isSorting ? 'dg-row-sorting' : ''}`}
                                                                    key={`${row[props.rowKey]}_${col.id}`}
                                                                    style={getCellStyle(col)}
                                                                >
                                                                    <div className={'dg-cell'}>
                                                                        {
                                                                            row[col.id] &&
                                                                            <DataGridCell
                                                                                definition={col}
                                                                                content={row[col.id]}
                                                                                style={cellContentStyle(col)}
                                                                            />
                                                                        }
                                                                    </div>
                                                                </div>
                                                            );
                                                        }
                                                        else
                                                        {
                                                            return null;
                                                        }
                                                    })
                                                }

                                                {
                                                    hideCols && trailingControlColumns?.map((col, index) =>
                                                        <CellItem
                                                            key={index}
                                                            row={row}
                                                            col={col}
                                                            index={index}
                                                        />
                                                    )
                                                }
                                            </div>
                                        ) : <EmptyData />
                                    }
                                </div>
                                {
                                    summary &&
                                    <div className={'dg-header summary'} style={{ position: summary.stick ? 'sticky' : 'relative' }}>
                                        {
                                            cols?.map((col, index) => !col.hidden &&
                                                <DataGridSummaryCell
                                                    key={col.id}
                                                    col={col}
                                                    items={items}
                                                    style={getCellStyle(col)}
                                                />
                                            )
                                        }

                                    </div>
                                }

                            </div>
                        </ScrollView>
                    }
                </div>

                <DataGridBottomBar {...props} isLoading={isLoading} />
            </div>
        </div>
    );
};

DataGrid.propTypes = {
    // Display
    className: PropTypes.string,
    header: PropTypes.string,
    columns: PropTypes.arrayOf(PropTypes.shape({
        // Display
        width: PropTypes.number,
        // Data
        id: PropTypes.string,
        displayAsText: PropTypes.string,
        display: PropTypes.node,
        schema: PropTypes.string,
        locale: PropTypes.string,
        format: PropTypes.string,
        // Feature
        isSortable: PropTypes.bool,
        defaultSortDirection: PropTypes.oneOf(['desc', 'asc']),
        isMiniStyle: PropTypes.bool
    })),

    // Data
    items: PropTypes.arrayOf(PropTypes.object),
    pagination: PropTypes.shape({
        // Infinite Scroll
        useInfiniteScroll: PropTypes.bool,
        pageIndex: PropTypes.number,
        pageSize: PropTypes.number,
        pageSizeOptions: PropTypes.arrayOf(PropTypes.number), // [50, 100, 200] An array of page sizes the user can select from. Leave this prop undefined or use an empty array to hide "Rows per page" select button
        onChangePage: PropTypes.func, // (itemsPerPage: number) => void
        onChangeItemsPerPage: PropTypes.func // (pageIndex: number) => void
    }),
    externalPaginationRow: PropTypes.bool,

    // Sorting feature
    total: PropTypes.number,
    sorting: PropTypes.shape({
        isSingleSort: PropTypes.bool,
        columns: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.string,
                direction: PropTypes.string
            })
        ), // [{ id: 'C', direction: 'asc' }]
        onSort: PropTypes.func // (columns: { id: string; direction: "asc" | "desc"; }[]) => void
    }),

    // Filter feature
    filter: PropTypes.shape({
        conditions: PropTypes.arrayOf(
            PropTypes.shape({
                operator: PropTypes.string,
                value: PropTypes.any,
                options: PropTypes.array // select: [{id, label, color}]
            })
        ),
        onChange: PropTypes.func
    }),

    // Search feature
    searching: PropTypes.shape({
        searchKey: PropTypes.string,
        onSearch: PropTypes.func
    }),

    rowKey: PropTypes.string,

    leadingControlColumns: PropTypes.array,
    trailingControlColumns: PropTypes.array,
    rowNumber: PropTypes.bool,
    loading: PropTypes.shape({
        isLoading: PropTypes.bool,
        onRefresh: PropTypes.func
    }),

    // not implement yet - nice to have
    toolbarVisibility: PropTypes.shape({
        showColumnSelector: PropTypes.bool,
        showStyleSelector: PropTypes.bool,
        showSortSelector: PropTypes.bool,
        showFullScreenSelector: PropTypes.bool
    }),

    // Summary feature
    summary: PropTypes.shape({
        stick: PropTypes.bool
    }),

    selectRows: PropTypes.shape({
        onChange: PropTypes.func,
        onChangeAll: PropTypes.func
    }),
    
};

DataGrid.defaultProps = {
    currentPage: 1,
    pageSize: 10,
    toolbarVisibility: {},
    rowNumber: false
};

export default DataGrid;
