import React, { useEffect, useState } from 'react';
import faker from 'faker';

import DataGrid, { usePagination } from 'components/bases/DataGrid/DataGrid';
import { AdvanceSelect } from 'components/bases/AdvanceSelect/AdvanceSelect';
import { Paging } from 'components/bases/Paging/Paging';
import { CheckBox } from 'components/bases/CheckBox/CheckBox';
import { Button } from 'components/bases/Button/Button';
import { FAIcon } from 'components/bases/Icon/FAIcon';

export default {
    title: 'Bases/DataGrid/Base',
    component: DataGrid
};

export function Default()
{
    return <DataGrid
        items={users}
        columns={userColumns}
        rowKey={'guid'}
    />;
}

export function Sorting()
{
    const [columns, setColumns] = useState([{ id: 'code', direction: 'asc' }]);

    function onSort(columns)
    {
        setColumns(columns);
    }

    useEffect(() =>
    {
        console.log(columns);
    }, [columns]);
    const items = users.sort((a, b) => a.code < b.code ? -1 : 1);

    return <DataGrid
        items={items}
        columns={userColumns}
        sorting={{ columns, onSort }}
        rowKey={'guid'}
    />;
}

export function SingleSort()
{
    const [columns, setColumns] = useState([{ id: 'code', direction: 'asc' }]);

    function onSort(columns)
    {
        setColumns(columns);
    }

    useEffect(() =>
    {
        console.log(columns);
    }, [columns]);
    const items = users.sort((a, b) => a.code < b.code ? -1 : 1);

    return <DataGrid
        items={items}
        columns={userColumns}
        sorting={{ columns, onSort, isSingleSort: true }}
        rowKey={'guid'}
    />;
}

export function WithPagination()
{
    const [items, setItems] = useState([]);
    const [total, setTotal] = useState(0);

    const [pageIndex, setPageIndex] = useState(1);
    const [pageSize, setPageSize] = useState(10);

    useEffect(() =>
    {
        fetchUsersSize().then((total) => setTotal(total));
    }, []);

    useEffect(() =>
    {
        if (pageIndex && pageSize)
        {
            fetchUsers(pageIndex, pageSize).then((value) => setItems(value));
        }
    }, [pageIndex, pageSize]);

    return <DataGrid
        items={items}
        columns={userColumns}
        pagination={{
            pageIndex,
            pageSize,
            pageSizeOptions: [10, 20],
            onChangePage: setPageIndex,
            onChangeItemsPerPage: setPageSize
        }}
        total={total}
        rowKey={'guid'}
    />;
}

export function PaginationHooks()
{
    const [items, setItems] = useState([]);
    const [total, setTotal] = useState(0);

    const [pageIndex, pageSize, Pagination] = usePagination(total);

    useEffect(() =>
    {
        fetchUsersSize().then((total) => setTotal(total));
        if (pageIndex && pageSize)
        {
            fetchUsers(pageIndex, pageSize).then((value) => setItems(value));
        }
    }, [pageIndex, pageSize]);


    return (
        <>
            <DataGrid
                items={items}
                columns={userColumns}
                rowKey={'guid'}
            />
            <Pagination.Row />
        </>);
}

async function fetchUsers(pageIndex = 1, pageSize = 10)
{
    const start = (pageIndex - 1) * pageSize;
    const end = pageIndex * pageSize;
    return users.slice(start, end);
}

async function fetchUsersSize()
{
    return users.length;
}

export function PaginationOutside()
{
    const [items, setItems] = useState([]);
    const [total, setTotal] = useState(0);

    useEffect(() =>
    {
        fetchUsersSize().then((total) => setTotal(total));
    }, []);

    const pageSizeOptions = [10, 20, 50, 100, 200];
    const [pageIndex, setPageIndex] = useState(1);
    const [pageSize, setPageSize] = useState(10);

    useEffect(() =>
    {
        fetchUsers(pageIndex, pageSize).then((value) => setItems(value));
    }, [pageIndex, pageSize]);

    const selectOptions = pageSizeOptions.map((pageSize) => ({
        id: pageSize,
        label: `${pageSize} rows`
    })) || [];

    return (
        <>
            <DataGrid
                items={items}
                columns={userColumns}
                rowKey={'guid'}
            />
            <AdvanceSelect
                options={selectOptions}
                onChange={setPageSize}
                width={200}
                value={pageSize}
            />
            <Paging
                total={total}
                pageSize={pageSize}
                currentPage={pageIndex}
                onChange={setPageIndex}
            />
        </>
    );
}

function ControlledCheckBox(props)
{
    const [checked, setChecked] = useState(props.checked || false);

    useEffect(() =>
    {
        if (props.onChange)
        {
            props.onChange(checked);
        }
    }, [checked]);

    return (
        <CheckBox
            checked={checked}
            onChange={setChecked}
        />
    );
}

export function SelectionColumnControls()
{
    const id = 'guid';
    const [selectedRows, setSelectedRows] = useState([]);

    function onCheck(checked, row)
    {
        if (!checked)
        {
            const index = selectedRows.indexOf(row[id]);
            if (index > -1)
            {
                selectedRows.splice(index, 1);
                setSelectedRows(selectedRows);
            }
        }
        else
        {
            selectedRows.push(row[id]);
            setSelectedRows(selectedRows);
        }
    }

    const selectionColumn = {
        id: id,
        width: 200,
        headerCellRender: <Button
            onClick={() => console.log(selectedRows)}
            text={'selected'}
        />,
        rowCellRender: (row, index) => <ControlledCheckBox onChange={(value) => onCheck(value, row)} />
    };
    return (
        <DataGrid
            items={users}
            columns={userColumns}
            rowKey={id}
            leadingControlColumns={[selectionColumn]}
        />
    );
}

export function ActionsColumnControls()
{
    const id = 'guid';
    const actionsColumn = {
        id: id,
        headerCellRender: <span>Selection</span>,
        width: 50,
        rowCellRender: function ActionsField(row, index)
        {
            return (
                <>
                    <FAIcon
                        icon={'edit'}
                        size={'1rem'}
                        className={'action-btn'}
                        onClick={() =>
                        {
                            console.log('Edit ' + row[id]);
                        }}
                    />
                    <FAIcon
                        icon={'trash-alt'}
                        size={'1rem'}
                        className={'action-btn'}
                        onClick={() =>
                        {
                            console.log('Delete ' + row[id]);
                        }}
                    />
                </>
            );
        }
    };
    return (
        <DataGrid
            items={users}
            columns={userColumns}
            rowKey={id}
            trailingControlColumns={[actionsColumn]}
        />
    );
}

export function ColumnSelector()
{
    return <DataGrid
        items={users}
        columns={userColumns}
        rowKey={'guid'}
        toolbarVisibility={{ showColumnSelector: true }}
    />;
}

export function SearchBox()
{
    return (
        <DataGrid
            items={users}
            columns={userColumns}
            rowKey={'guid'}
            toolbarVisibility={{ showSearchBox: true }}
            searching={{
                searchKey: '',
                onSearch: onSearch
            }}
        />
    );

    function onSearch(value)
    {
        console.log(value);
    }
}

function generateUser()
{
    return {
        name: faker.name.firstName('male'),
        code: faker.random.number(),
        guid: faker.random.uuid(),
        active: faker.random.boolean(),
        createdAt: faker.date.past(),
        spent: faker.random.number() * 99,
        spentVND: faker.random.number() * 1000,

        randomNumber: faker.random.number() + 0.888,
        randomNumberVN: faker.random.number() + 0.888,
        friendsWith: { name: faker.name.firstName('male') },
        avatar: 'https://i.ytimg.com/vi/EOGT548ZToY/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLBkgqTu9_LuzYasWCF8sZdipcKuKw',
        profile: 'https://www.google.com',
        status: Math.floor(Math.random() * Math.floor(3)),
        location: 'Ho Chi Minh',
        cities: ['Ho Chi Minh', 'Ha Noi']
    };
}

const users = [];

for (let i = 0; i < 110; i++)
{
    users.push(generateUser());
}

const userColumns = [
    {
        id: 'guid',
        displayAsText: 'GUID'
    },
    {
        id: 'name',
        displayAsText: 'Name'
    },
    {
        id: 'code',
        displayAsText: 'Code',
        display: <span style={{ color: 'red' }}>Code</span>,
        isSortable: true,
        defaultSortDirection: 'desc'
    },
    {
        id: 'active',
        displayAsText: 'Active',
        schema: 'boolean',
        isSortable: true
    },
    {
        id: 'createdAt',
        displayAsText: 'HH:mm',
        schema: 'datetime',
        isSortable: true,
        format: 'HH:mm'
    },
    {
        id: 'spent',
        displayAsText: 'Spent',
        isSortable: true,
        schema: 'currency'
    },
    {
        id: 'spentVND',
        displayAsText: 'Spent in Vietnam',
        schema: 'currency',
        locale: 'vi'
    },
    {
        id: 'randomNumber',
        displayAsText: 'Format #',
        schema: 'numeric'
    },
    {
        id: 'randomNumberVN',
        displayAsText: 'Format #,##',
        schema: 'numeric',
        format: '#,##'
    },
    {
        id: 'friendsWith',
        displayAsText: 'Friends With',
        schema: 'json'
    },
    {
        id: 'avatar',
        schema: 'image',
        displayAsText: 'Avatar'
    },
    {
        id: 'profile',
        schema: 'link',
        displayAsText: 'Profile'
    },
    {
        id: 'status',
        displayAsText: 'Status',
        schema: 'select',
        options: [
            { id: 0, label: 'offline', color: 'grey' },
            { id: 1, label: 'online', color: 'green' },
            { id: 2, label: 'busy', color: 'red' }
        ]
    },
    {
        id: 'cities',
        displayAsText: 'Cities',
        schema: 'multi-select',
        options: [
            { label: 'Ho Chi Minh', id: 'Ho Chi Minh', color: 'green' },
            { label: 'Ha Noi', id: 'Ha Noi', color: 'orange' },
            { label: 'Da Nang', id: 'Da Nang', color: 'brown' }
        ]
    }

];
