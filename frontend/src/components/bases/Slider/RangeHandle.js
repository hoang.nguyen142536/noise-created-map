import './RangeHandle.scss';

import React from 'react';
import PropTypes from 'prop-types';

export const RangeHandle = (props) =>
{
    const { className, start, end, disabled, style, color } = props;

    return (
        <div
            className={`slider-range-handle ${className} ${disabled ? 'disabled' : 'active'}`}
            style={{
                ...style,
                left: start + '%',
                right: (100 - end) + '%',
                ...color && { backgroundColor: disabled ? 'gray' : color },
            }}
        />
    );
};

RangeHandle.propTypes = {
    className: PropTypes.string,
    color: PropTypes.string,
    style: PropTypes.object,
    disabled: PropTypes.bool,
    orientation: PropTypes.oneOf(['vertical', 'horizontal']),
    start: PropTypes.number,
    end: PropTypes.number
};

RangeHandle.defaultProps = {
    className: '',
    orientation: 'horizontal',
    disabled: false,
    start: 0,
    end: 100,
    color: 'var(--primary)'
};
