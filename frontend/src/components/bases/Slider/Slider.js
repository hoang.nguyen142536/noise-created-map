import './Slider.scss';

import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import { CommonHelper } from 'helper/common.helper';
import { Input } from 'components/bases/Input/Input';
import { TB1 } from 'components/bases/Text/Text';

import { Track } from './Track';
import { RangeHandle } from './RangeHandle';
import { RangeThumb } from './RangeThumb';
import { SliderMarks, SliderLabels } from './MarksLabels';


export const Slider = ({
    max,
    min,
    step,
    value,
    onChange,
    onAfterChange,
    range,
    reverse,
    disabled,
    thumbSize,
    thumbColor,
    rangeSize,
    rangeColor,
    wrapper,
    marks,
    thumbCustom,
    typeMark,
    showIndicator,
    displayMap
}) =>
{
    const [valueSlider, setValueSlider] = useState([]);
    const [thumbLeft, setThumbLeft] = useState({ hover: false, active: false });
    const [thumbRight, setThumbRight] = useState({ hover: false, active: false });
    const _setTrackDimensions = useRef();
    const inputLeftRef = useRef();

    const [start, end] = valueSlider;

    useEffect(() =>
    {
        const maxThumb = parseInt(((max - min) / step), 10) * step;
        let valueSliderUpdate;

        if (!Array.isArray(value) && !range)
        {
            if (reverse)
            {
                valueSliderUpdate = [(max - value), maxThumb];
            }
            else
            {
                valueSliderUpdate = [min, value];
            }
        }
        else if (range)
        {
            if (!Array.isArray(value))
            {
                const avg = (max - min) / 2;
                if (value === avg)
                {
                    valueSliderUpdate = [min, avg];
                }
                else
                {
                    valueSliderUpdate = value > avg ? [avg, value] : [value, avg];
                }
            }
            else if (value?.length === 1)
            {
                valueSliderUpdate = [value[0], maxThumb];
            }
            else if (value?.length === 0)
            {
                valueSliderUpdate = [min, max];
            }
            else
            {
                valueSliderUpdate = value;
            }
        }
        else
        {
            if (range)
            {
                valueSliderUpdate = [min, max];
            }
            else
            {
                valueSliderUpdate = reverse ? [(max - min) / 2, maxThumb] : [min, (max - min) / 2];
            }
        }

        setValueSlider([formatValueWithStep(valueSliderUpdate[0]), formatValueWithStep(valueSliderUpdate[1])]);
    }, []);

    const getValueByTypeRange = (value) =>
    {
        if (range)
        {
            return value;
        }
        else if (reverse)
        {
            return value[0];
        }
        else
        {
            return value[1];
        }
    };

    const handleChangeSlider = (data, index) =>
    {
        const valueUpdate = CommonHelper.clone(valueSlider);

        valueUpdate[index] = parseFloat(data);
        setValueSlider(valueUpdate);

        onChange && onChange(getValueByTypeRange(valueUpdate));
    };

    const handleSliderMouseUp = (data, setData) =>
    {
        if (data.hasOwnProperty('active'))
        {
            setData({ ...data, active: false });
        }

        onAfterChange && onAfterChange(getValueByTypeRange(valueSlider));
    };

    const handleSliderMouseDown = (data, setData) =>
    {
        if (data.hasOwnProperty('active'))
        {
            setData({ ...data, active: true });
        }
    };

    const handleSliderMouseOver = (data, setData) =>
    {
        if (data.hasOwnProperty('hover'))
        {
            setData({ ...data, hover: true });
        }
    };

    const handleSliderMouseOut = (data, setData) =>
    {
        if (data.hasOwnProperty('hover'))
        {
            setData({ ...data, hover: false });
        }
    };

    const formatValueWithStep = (value) =>
    {
        return parseInt((value / step), 10) * step;
    };

    const renderWrapper = (data, type) =>
    {
        if (typeof data === 'string' || typeof data === 'number')
        {
            return <TB1 className={`wrapper-${type}`}>{data}</TB1>;
        }

        return data;
    };

    const thumbSizeNumb = parseInt(thumbSize, 10) + 4;

    return (
        <div className={'range-slider-labels-container'}>
            <div className={'range-slider-container'}>

                {wrapper?.left && renderWrapper(wrapper.left, 'left')}

                <div className="range-slider-content">
                    {
                        (reverse || range) &&
                        (
                            <Input
                                ref={inputLeftRef}
                                type="range"
                                className={`slider-input input-left ${disabled ? 'disabled' : 'active'} thumb-real-size-${thumbSizeNumb}`}
                                min={min}
                                max={max}
                                step={step}
                                disabled={disabled}
                                value={start}
                                onChange={(value) => handleChangeSlider(value, 0)}
                                onMouseOut={() => handleSliderMouseOut(thumbLeft, setThumbLeft)}
                                onMouseOver={() => handleSliderMouseOver(thumbLeft, setThumbLeft)}
                                onMouseUp={() => handleSliderMouseUp(thumbLeft, setThumbLeft)}
                                onMouseDown={() => handleSliderMouseDown(thumbLeft, setThumbLeft)}
                            />
                        )
                    }

                    {
                        (!reverse || range) &&
                        (
                            <Input
                                type="range"
                                className={`slider-input input-right  ${disabled ? 'disabled' : 'active'}  thumb-real-size-${thumbSizeNumb}`}
                                min={min}
                                max={max}
                                step={step}
                                value={end}
                                disabled={disabled}
                                onChange={(value) => handleChangeSlider(value, 1)}
                                onMouseOut={() => handleSliderMouseOut(thumbRight, setThumbRight)}
                                onMouseOver={() => handleSliderMouseOver(thumbRight, setThumbRight)}
                                onMouseUp={() => handleSliderMouseUp(thumbRight, setThumbRight)}
                                onMouseDown={() => handleSliderMouseDown(thumbRight, setThumbRight)}
                            />
                        )
                    }

                    <div className="slider-container" style={{ height: rangeSize }}>
                        <Track
                            trackRef={_setTrackDimensions}
                            size={rangeSize}
                        />
                        <RangeHandle
                            start={((Math.min(start, end) - min) / (max - min)) * 100}
                            end={((Math.max(start, end) - min) / (max - min)) * 100}
                            disabled={disabled}
                            color={rangeColor}
                            size={rangeSize}
                        />
                        {
                            (reverse || range) &&
                            (
                                <RangeThumb
                                    position={((start - min) / (max - min)) * 100}
                                    className={`left  ${thumbLeft.hover ? 'hover' : ''}  ${thumbLeft.active ? 'active' : ''}`}
                                    size={thumbSize}
                                    color={thumbColor}
                                    rangeSize={rangeSize}
                                    disabled={disabled}
                                    custom={thumbCustom?.start ? thumbCustom.start : thumbCustom}
                                    showIndicator={showIndicator && thumbLeft.active}
                                    displayMap={displayMap}
                                />
                            )
                        }

                        {
                            (!reverse || range) &&
                            (
                                <RangeThumb
                                    position={((end - min) / (max - min)) * 100}
                                    className={`right  ${thumbRight.hover ? 'hover' : ''}  ${thumbRight.active ? 'active' : ''}`}
                                    size={thumbSize}
                                    color={thumbColor}
                                    rangeSize={rangeSize}
                                    disabled={disabled}
                                    custom={thumbCustom?.end ? thumbCustom.end : thumbCustom}
                                    showIndicator={showIndicator && thumbRight.active}
                                    displayMap={displayMap}
                                />
                            )
                        }

                        {marks && <SliderMarks positions={Object.keys(marks).map(marks => parseFloat(marks))} size={rangeSize} type={typeMark} />}

                    </div>
                </div>

                {wrapper?.right && renderWrapper(wrapper.right, 'right')}
            </div>

            {marks && <SliderLabels marks={marks} />}
        </div>
    );
};

Slider.propTypes = {
    wrapper: PropTypes.object,
    marks: PropTypes.object,

    range: PropTypes.bool,
    reverse: PropTypes.bool,
    disabled: PropTypes.bool,

    className: PropTypes.string,
    thumbSize: PropTypes.string,
    thumbColor: PropTypes.string,
    rangeSize: PropTypes.string,
    rangeColor: PropTypes.string,
    typeMark: PropTypes.oneOf(['dot', 'square', 'line']),

    step: PropTypes.number,
    max: PropTypes.number,
    min: PropTypes.number,
    value: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.number
    ]),

    thumbCustom: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.object
    ]),

    showIndicator: PropTypes.bool,
    displayMap: PropTypes.object
};

Slider.defaultProps = {
    step: 1,
    max: 30,
    min: 0,
    value: [],
    range: false,
    reverse: false,
    disabled: false,
    thumbSize: '14px',
    thumbColor: 'var(--primary)',
    rangeSize: '5px',
    rangeColor: 'var(--primary)',
    wrapper: {},
    marks: null,
    thumbCustom: null,
    showIndicator: true,
    typeMark: 'dot',
    onChange: () =>
    {
    }
};


