import React, { useState } from 'react';
import { Slider } from './Slider';
import { FAIcon } from '../Icon/FAIcon';

export default {
    title: 'Bases/Inputs/Slider',
    component: Slider
};

const Template = (args) =>
{
    const [value, setValue] = useState(10);

    const handleChangeValue = (value) =>
    {
        setValue(value);
    };

    const handleAfterChange = (value) =>
    {
        console.log(value);
    };

    return (
        <div style={{ width: '80%', margin: 'auto' }}>
            <Slider
                value={value}
                onChange={handleChangeValue}
                onAfterChange={handleAfterChange}
                {...args}
            />
        </div>
    );
};

export const Default = Template.bind({});
Default.args = {
    min: 0,
    max: 40,
    step: 6,
    value: 20
};

export const Wrapper = Template.bind({});
Wrapper.args = {
    max: 100,
    min: 0,
    step: 1,
    value: 100,
    wrapper: { left: 1234, right: 4567 }
};

export const Marks = Template.bind({});
Marks.args = {
    max: 100,
    min: 0,
    step: 1,
    value: 100,
    marks: {
        0: { label: '0°C', style: { color: 'red' } },
        26: '26°C',
        37: '37°C',
        100: '100 độ C'
    }
};

export const CustomThumb = Template.bind({});
CustomThumb.args = {
    max: 100,
    min: 0,
    step: 1,
    value: 100,
    thumbCustom: (
        <FAIcon
            icon={'times'}
            size={'1.125rem'}
            backgroundColor={'white'}
            color={'black'}
        />
    )
};

export const CustomThumbRange = Template.bind({});
CustomThumbRange.args = {
    max: 100,
    min: 0,
    step: 1,
    value: 80,
    range: true,
    thumbCustom: {
        start: (
            <FAIcon
                icon={'play'}
                size={'1.125rem'}
                backgroundColor={'white'}
                color={'black'}
            />
        ),
        end: (
            <FAIcon
                icon={'star'}
                size={'1.125rem'}
                backgroundColor={'white'}
                color={'black'}
            />
        )
    }
};


