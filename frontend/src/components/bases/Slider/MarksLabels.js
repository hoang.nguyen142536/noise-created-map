import './MarksLabels.scss';

import React from 'react';
import PropTypes from 'prop-types';

import { TB1 } from 'components/bases/Text/Text';

export const SliderMarks = function (props)
{
    const { positions, size, onClick, type } = props;

    return (
        <div className={'slider-mark-container'}>
            <div className={'slider-marks-dot'}>
                {
                    positions.map(position =>
                        <Mark
                            key={'mark-' + position}
                            position={position}
                            size={size}
                            onClick={onClick}
                            type={type}
                        />
                    )
                }
            </div>
        </div>
    );
};

SliderMarks.propTypes = {
    positions: PropTypes.array.isRequired,
    size: PropTypes.string,
    onClick: PropTypes.func,
    type: PropTypes.oneOf(['dot', 'square', 'line'])
};

SliderMarks.defaultProps = {
    position: [],
    size: '',
    type: 'dot',
    onClick: () =>
    {
    }
};

const Mark = ({ position, size, onClick, type }) =>
{
    return (
        <div
            className={'slider-mark'}
            style={{
                left: position + '%',
                width: type !== 'line' && size,
                height: type === 'line' ? `${(parseInt(size, 10) * 1.5)}px` : size,
                transform: `translateX(-${position - 2 < 0 ? Math.abs(position - 2) : position - 2}%)`,
                borderRadius: type === 'dot' && '50%',
                ...type === 'line' && { border: '1px solid #f0f0f0' }
            }}
            onClick={() => onClick(position)}

        />
    );
};

Mark.propTypes = {
    position: PropTypes.number.isRequired,
    size: PropTypes.string,
    onClick: PropTypes.func,
    type: PropTypes.oneOf(['dot', 'square', 'line'])
};

Mark.defaultProps = {
    position: null,
    size: '',
    type: 'dot',
    onClick: () =>
    {
    }
};

export const SliderLabels = function ({ marks })
{
    return (
        <div className={'slider-mark-label-container'}>
            {
                Object.keys(marks).map(key =>
                    <Label
                        key={'dots' + key}
                        position={parseFloat(key)}
                        label={marks[key]?.label ? marks[key]?.label : marks[key]}
                        style={marks[key].style ? marks[key].style : {}}
                    />
                )
            }
        </div>
    );
};

SliderLabels.propTypes = {
    marks: PropTypes.object.isRequired
};

SliderLabels.defaultProps = {
    marks: {}
};

const Label = ({ position, label, style }) =>
{
    return (
        <TB1
            className={'slider-label'}
            style={{
                position: 'absolute',
                left: position + '%',
                transform: `translateX(-${position - 2 < 0 ? Math.abs(position - 2) : position - 2}%)`,
                ...style && style
            }}
        >
            {label}
        </TB1>
    );
};

Label.propTypes = {
    position: PropTypes.number.isRequired,
    label: PropTypes.string,
    style: PropTypes.object
};

Label.defaultProps = {
    position: null,
    style: {},
    label: ''
};
