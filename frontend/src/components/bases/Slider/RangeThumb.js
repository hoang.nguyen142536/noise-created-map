import './RangeThumb.scss';

import React from 'react';
import PropTypes from 'prop-types';

export const RangeThumb = (props) =>
{
    const { className, position, size, color, style, rangeSize, disabled, custom } = props;

    const rangeSizeNumb = parseInt(rangeSize, 10);
    const sizeNumb = (parseInt(size, 10));

    const thumbOffsetX = sizeNumb > rangeSizeNumb ? ((sizeNumb + 4 - rangeSizeNumb) / 2) + 'px' : '0px';
    const customOffsetX = sizeNumb > rangeSizeNumb ? ((sizeNumb - rangeSizeNumb) / 2) + 'px' : '0px';

    const getDisplayValue = (position) =>
    {
        if (props.displayMap)
        {
            const nearestKey = Object.keys(props.displayMap).reduce((acc, val) =>
            {
                return Math.abs(val - position) < Math.abs(acc - position) ? val : acc;
            });

            return props.displayMap[nearestKey];
        }
    };

    return (
        <>
            <div
                className="slider-thumb-thumb-indicator"
                style={{
                    ...style,
                    left: position + '%',
                    transform: `translate(-${position - 2 < 0 ? Math.abs(position - 2) : position - 2}%, ${custom ? customOffsetX : thumbOffsetX})`,
                    display: (props.showIndicator && props.displayMap) ? 'block' : 'none'
                }}
            >
                <div className="indicator-text">
                    {getDisplayValue(position)}
                </div>
            </div>

            <div
                className={`slider-range-thumb ${className}`}
                style={{
                    ...style,
                    left: position + '%',
                    transform: `translate(-${position - 2 < 0 ? Math.abs(position - 2) : position - 2}%, ${custom ? customOffsetX : thumbOffsetX})`
                }}
            >
                {
                    custom ? custom :
                        (
                            // default  thumb
                            <div
                                style={{
                                    width: size,
                                    height: size,
                                    ...color && { border: `2px solid ${disabled ? 'gray' : color}` },
                                    borderRadius: '50%',
                                    background: 'white'
                                }}
                            />
                        )
                }
            </div>
        </>
    );
};

RangeThumb.propTypes = {
    style: PropTypes.object,
    disabled: PropTypes.bool,

    orientation: PropTypes.oneOf(['vertical', 'horizontal']),
    position: PropTypes.number,
    start: PropTypes.number,
    end: PropTypes.number,

    size: PropTypes.string,
    color: PropTypes.string,
    className: PropTypes.string,
    rangeSize: PropTypes.string,

    custom: PropTypes.element,
    showIndicator: PropTypes.bool,
    displayMap: PropTypes.object
};

RangeThumb.defaultProps = {
    className: '',
    orientation: 'horizontal',
    disabled: false,
    start: 0,
    end: 100,
    size: '14px',
    color: 'var(--primary)',
    style: {},
    custom: null
};
