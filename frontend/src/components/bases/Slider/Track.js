import './Track.scss';

import React from 'react';
import PropTypes from 'prop-types';

export const Track = (props) =>
{
    const { className, trackRef, style, disabled, orientation } = props;

    return (
        <div
            ref={trackRef}
            disabled={disabled}
            className={`${className} ` +
                `${orientation === 'vertical' ? 'track-vertical' : 'track'} ` +
                `${disabled ? 'disabled' : ''} `
            }
            style={{ ...style }}
        />
    );

};

Track.propTypes = {
    trackRef: PropTypes.any.isRequired,
    className: PropTypes.string,
    style: PropTypes.object,
    disabled: PropTypes.bool,
    orientation: PropTypes.oneOf(['vertical', 'horizontal']),
};

Track.defaultProps = {
    className: '',
    orientation: 'horizontal',
    disabled: false
};

export default Track;


