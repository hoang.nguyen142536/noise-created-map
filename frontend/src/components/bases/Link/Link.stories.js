import React, { useState, useEffect } from 'react';

import { Link } from 'components/bases/Link/Link';

export default {
    title: 'Bases/Navigation/Link',
    component: Link,
};

const Template = (args) =>
{
    return (
        <Link {...args}>
            Google
        </Link>
    );
};

export const Default = Template.bind({});
Default.args = {
    href: 'https://www.google.com',
    target: '_blank',
};
