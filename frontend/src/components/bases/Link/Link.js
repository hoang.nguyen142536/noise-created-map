import './Link.scss';

import React from 'react';
import PropTypes from 'prop-types';

import { T } from 'components/bases/Translate/Translate';

export const Link = ({ children, href, target, onClick, className }) =>
{
    return (
        <a
            className={`link ${className}`}
            href={href}
            target={target}
            onClick={onClick}
            rel={'noopener noreferrer'}
        >
            <T>{children}</T>
        </a>
    );
};

Link.propTypes = {
    target: PropTypes.string,
    href: PropTypes.string
};

Link.defaultProps = {
    href: null,
    target: '_self',
    className: ''
};
