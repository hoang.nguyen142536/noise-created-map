import './Tag.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { FAIcon } from 'components/bases/Icon/FAIcon';
import { T } from 'components/bases/Translate/Translate';

export class Tag extends Component
{
    render()
    {
        const { size, color, textStyle } = this.props;
        const style = color ? textStyle ? Object.assign(textStyle, { backgroundColor: color }) : { backgroundColor: color } : undefined;

        return (
            <div
                className={`tag-container ${size}`}
                style={style}
            >
                <T>{this.props.text}</T>
                {this.props.onCloseClick &&
                <button
                    className={'tag-close-button'}
                    onClick={this.props.onCloseClick}
                >
                    <FAIcon
                        icon={'times'}
                        size={size === 'small' ? '0.75rem' : '1rem'}
                    />
                </button>}
            </div>
        );
    }
}

Tag.propTypes = {
    text: PropTypes.node,
    onCloseClick: PropTypes.func,
    color: PropTypes.string,
    size: PropTypes.oneOf(['small', 'large']),
    textStyle: PropTypes.object
};

Tag.defaultProps = {
    text: '',
    size: 'small'
};
