import React from 'react';

import { Tag } from 'components/bases/Tag/Tag';

export default {
    title: 'Bases/Display/Tag',
    component: Tag,
    args: {}
};

const Template = (args) =>
{
    return (
        <Tag {...args} />
    );
};

export const SmallRed = Template.bind({});
SmallRed.args = {
    text: 'Put your text here',
    size: 'small',
    color: 'red',
};

export const LargeGreen = Template.bind({});
LargeGreen.args = {
    text: 'Pika pika chuuuuu',
    size: 'small',
    color: 'green',
};
