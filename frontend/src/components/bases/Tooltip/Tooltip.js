import './Tooltip.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { T } from 'components/bases/Translate/Translate';
import { OverLay } from 'components/bases/Modal/OverLay';

export default class Tooltip extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            show: false
        };

        this.inputRef = React.createRef();

        this.handleMouseIn = this.handleMouseIn.bind(this);
        this.handleMouseOut = this.handleMouseOut.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleMouseIn()
    {
        this.setState({ show: true });
    }

    handleMouseOut()
    {
        this.setState({ show: false });
    }

    handleClick()
    {
        this.setState({ show: !this.state.show });
    }


    render()
    {
        return (
            <span
                ref={this.inputRef}
                onMouseOver={this.props.trigger === 'hover' ? this.handleMouseIn : undefined}
                onMouseLeave={this.props.trigger === 'hover' ? this.handleMouseOut : undefined}
                onClick={this.props.trigger === 'click' ? this.handleClick : undefined}
            >
                {
                    this.state.show &&
                    <OverLay
                        anchorEl={this.inputRef}
                        backdrop={false}
                    >
                        <div className={`tooltip ${this.props.position}`} style={{ width: this.props.width, height: this.props.height }}>
                            <T>{this.props.content}</T>
                        </div>
                    </OverLay>
                }

                {this.props.children}
            </span>
        );
    }
}

Tooltip.propTypes = {
    position: PropTypes.string,
    trigger: PropTypes.string,
};

Tooltip.defaultProps = {
    position: 'top',
    trigger: 'hover'
};
