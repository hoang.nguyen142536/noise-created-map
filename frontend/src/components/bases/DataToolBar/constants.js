export const COMBINE_OPTIONS = [
    { id: 'AND', label: 'Và' },
    { id: 'OR', label: 'Hoặc' },
    { id: 'NOT', label: 'Không' }
];

export const ADVANCE_FILTER_FEATURE = [
    {
        id: 'search-feature',
        icon: 'search',
        title: 'Tìm kiếm'
    },
    {
        id: 'data-sort',
        icon: 'sort',
        title: 'Sắp xếp'
    },
    {
        id: 'data-filter',
        icon: 'filter',
        title: 'Bộ lọc'
    },
    {
        id: 'column-toggle',
        icon: 'list-ol',
        title: 'Dữ liệu hiển thị'
    }
];
