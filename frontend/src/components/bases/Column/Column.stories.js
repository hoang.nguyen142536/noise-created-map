import React, { useState, useEffect } from 'react';

import { Column } from 'components/bases/Column/Column';

export default {
    title: 'Bases/Layout/Row - Column/Column',
    component: Column,
    parameters: {},
    args: {
        style: { backgroundColor: 'aqua' },
    },
    decorators: [(Story) => <div style={{ width: '50vw', height: '50vh' }}><Story /></div>]
};

const Template = (args) =>
{

    return (
        <Column {...args}>
            <div style={{ backgroundColor: 'aquamarine' }}>
                Pikachu
            </div>
            <div style={{ backgroundColor: 'lime' }}>
                Songoku
            </div>
        </Column>
    );
};

export const Default = Template.bind({});

export const Center = Template.bind({});
Center.args = {
    mainAxisAlignment: 'center',
    crossAxisAlignment: 'center',
};


