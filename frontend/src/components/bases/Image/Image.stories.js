import React, { useState, useEffect } from 'react';
import { action } from '@storybook/addon-actions';

import { Image } from 'components/bases/Image/Image';
import altImage from 'images/map-style-boundary.png';

export default {
    title: 'Bases/Display/Image/Image',
    component: Image,
};

const Template = (args) =>
{
    const [src, setSrc] = useState(args.src);
    
    // Reset event handler
    Object.keys(args).forEach((prop) =>
    {
        if (new RegExp('^on.*').test(prop))
        {
            args[prop] = action(prop + '');
        }
    });

    const handleChange = (id, { filename, data }) =>
    {
        setSrc(data);
        (action('onChange'))();
    };

    const handleDelete = (id) =>
    {
        setSrc(args.altSrc || '');
        (action('onDelete'))();
    };

    args.alt = 'Image here';

    return (
        <Image
            {...args}
            src={src}
            onChange={handleChange}
            onDelete={handleDelete}
        />
    );
};

export const Default = Template.bind({});
Default.args = {
    width: '100px',
    height: '100px',
    background: 'cyan',
};

export const WithEnlarger = Template.bind({});
WithEnlarger.args = {
    width: '100px',
    height: '100px',
    src: 'https://i.ytimg.com/vi/MPV2METPeJU/maxresdefault.jpg',
    altSrc: altImage,
    background: 'cyan',
    canEnlarge: true,
};

export const WithAlternativeImage = Template.bind({});
WithAlternativeImage.args = {
    width: '100px',
    height: '100px',
    background: 'cyan',
    altSrc: altImage,
    canChange: true,
};

export const WithUpdateAndDelete = Template.bind({});
WithUpdateAndDelete.args = {
    width: '100px',
    height: '100px',
    background: 'cyan',
    canChange: true,
    canDelete: true,
};
