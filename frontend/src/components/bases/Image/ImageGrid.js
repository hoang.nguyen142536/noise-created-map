import './ImageGrid.scss';

import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import { Image } from 'components/bases/Image/Image';
import { ScrollView } from 'components/bases/ScrollView/ScrollView';
import { FAIcon } from 'components/bases/Icon/FAIcon';
import { Column } from 'components/bases/Column/Column';

const ImageGrid = (props) =>
{
    const gridRef = useRef();
    const blockRef = useRef();
    const cellRef = useRef();
    const [cellMargin, setCellMargin] = useState('0px');

    useEffect(() =>
    {
        adjustMargin();
        window.addEventListener('resize', adjustMargin);

        return () =>
        {
            window.removeEventListener('resize', adjustMargin);
        };
    }, []);

    const adjustMargin = () =>
    {
        if (!gridRef || !blockRef || !cellRef)
        {
            return;
        }

        // Reset padding, let browser do all the calculation again
        setCellMargin('0px');

        // Recalculate manually
        const cellWidth = cellRef.current.clientWidth + 2; // 2px of the border
        const gridWidth = gridRef.current.clientWidth;

        const imgCount = parseInt(gridWidth / cellWidth);
        const remain = gridWidth - imgCount * cellWidth;

        setCellMargin(`${remain / (imgCount + 1)}px`);
    };

    return (
        <ScrollView scrollX={false}>
            {
                !props.isLoading &&
                (
                    !props.data ||
                    !Array.isArray(props.data) ||
                    (Array.isArray(props.data) && props.data.length === 0)
                ) &&
                <div
                    style={{
                        width: '100%',
                        top: '40%',
                        textAlign: 'center',
                        position: 'absolute'
                    }}
                >
                    <div style={{ opacity: '0.7' }}>Không có dữ liệu</div>
                </div>
            }
            {
                !props.isLoading &&
                (Array.isArray(props.data) && props.data.length > 0) &&
                <div
                    className="img-grid" ref={gridRef}
                    style={{ marginBottom: cellMargin }}
                >
                    <div className="img-grid-block" ref={blockRef}>
                        {
                            props.data.map((img, i) =>
                                <div
                                    className={`img-grid-cell ${(img.isSelected ? 'selected' : '')}`}
                                    key={i}
                                    ref={(i === 0) ? cellRef : null}
                                    style={{ marginLeft: cellMargin, marginTop: cellMargin }}
                                >
                                    {
                                        img.onSelect &&
                                        <div className={'img-grid-cell-select'}>
                                            <FAIcon
                                                size={'20px'}
                                                color={img.isSelected ? 'var(--primary)' : 'var(--text-color)'}
                                                icon={img.isSelected ? 'check-circle' : 'circle'}
                                                onClick={() => img.onSelect(img)}
                                            />
                                        </div>
                                    }

                                    <Image
                                        width={'10rem'}
                                        height={'10rem'}
                                        src={img.src}
                                        onClick={(e) => img.onClick && img.onClick(e, img)}
                                    />
                                </div>
                            )
                        }
                    </div>
                </div>
            }
            {
                props.isLoading &&
                <Column
                    mainAxisAlignment={'center'}
                    crossAxisAlignment={'center'}
                >
                    <FAIcon
                        icon={'spinner-third'}
                        type={'duotone'}
                        size={'3rem'}
                        spin
                    />
                </Column>
            }
        </ScrollView>
    );
};

ImageGrid.propTypes = {
    isLoading: PropTypes.bool,
    data: PropTypes.arrayOf(PropTypes.shape({
        isSelected: PropTypes.bool,
        onClick: PropTypes.func,
        src: PropTypes.any,
    })),
    onSelect: PropTypes.func,
};

export { ImageGrid };
