import './Panel.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollView } from 'components/bases/ScrollView/ScrollView';

export class BorderPanel extends Component
{
    render()
    {
        return (
            <FlexPanel
                {...this.props}
                className={`border-panel ${this.props.className}`}
            >
                {this.props.children}
            </FlexPanel>
        );
    }
}

BorderPanel.propTypes = {
    className: PropTypes.string,
    width: PropTypes.string,
    flex: PropTypes.number
};

BorderPanel.defaultProps = {
    className: '',
    width: '',
    flex: 0,
};

export class Panel extends Component
{
    render()
    {
        return (
            <div
                className={`panel ${this.props.className}`}
                {...this.props}
            >
                {this.props.children}
            </div>
        );
    }
}

Panel.propTypes = {
    className: PropTypes.string,
};

Panel.defaultProps = {
    className: '',
};

export class FlexPanel extends Component
{
    render()
    {
        let width = this.props.width;
        if (this.props.flex)
        {
            width = '';
        }

        return (
            <div
                className={`flex-panel ${this.props.className}`}
                style={{
                    flex: `${this.props.flex} ${this.props.flex} ${width}`,
                    width: width
                }}
            >
                {this.props.children}
            </div>
        );
    }
}

FlexPanel.propTypes = {
    className: PropTypes.string,
    width: PropTypes.string,
    flex: PropTypes.number
};

FlexPanel.defaultProps = {
    className: '',
    width: '',
    flex: 0
};

export class PanelBody extends Component
{
    render()
    {
        return (
            <div
                className={`panel-body ${this.props.className}`}
                style={this.props.style}
            >
                {
                    this.props.scroll ?
                        <ScrollView scrollX={false}>
                            {this.props.children}
                        </ScrollView> :
                        this.props.children
                }
            </div>
        );
    }
}

PanelBody.propTypes = {
    className: PropTypes.string,
    scroll: PropTypes.bool
};

PanelBody.defaultProps = {
    className: ''
};
