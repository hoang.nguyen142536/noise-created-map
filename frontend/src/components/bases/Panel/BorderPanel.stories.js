import React, { useState, useEffect } from 'react';

import { BorderPanel, PanelBody } from 'components/bases/Panel/Panel';
import { PanelHeader } from 'components/bases/Panel/PanelHeader';
import { PanelFooter } from 'components/bases/Panel/PanelFooter';

export default {
    title: 'Bases/Panel/BorderPanel',
    component: BorderPanel,
    subcomponents: { PanelBody, PanelHeader, PanelFooter },
};

export const Default = (args) =>
{
    return (
        <BorderPanel {...args}>
            <PanelHeader>
                Header
            </PanelHeader>
            <PanelBody>
                Body
            </PanelBody>
            <PanelFooter>
                Footer
            </PanelFooter>
        </BorderPanel>
    );
};


