import React, { useState, useEffect } from 'react';

import { Panel, PanelBody } from 'components/bases/Panel/Panel';
import { PanelHeader } from 'components/bases/Panel/PanelHeader';
import { PanelFooter } from 'components/bases/Panel/PanelFooter';

export default {
    title: 'Bases/Panel/Panel',
    component: Panel,
    subcomponents: { PanelBody, PanelHeader, PanelFooter },
};

export const Default = (args) =>
{
    return (
        <Panel {...args}>
            <PanelHeader>
                Header
            </PanelHeader>
            <PanelBody>
                Body
            </PanelBody>
            <PanelFooter>
                Footer
            </PanelFooter>
        </Panel>
    );
};


