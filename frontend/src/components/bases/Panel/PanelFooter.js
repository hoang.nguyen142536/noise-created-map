import './PanelFooter.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Button } from 'components/bases/Button/Button';
import { T } from 'components/bases/Translate/Translate';

export class PanelFooter extends Component
{
    render()
    {
        return (
            <div className={`panel-footer ${this.props.docked ? 'panel-footer-docked' : ''}`}>
                {
                    this.props.actions ?
                        <div className={'panel-footer-actions'}>
                            {
                                this.props.actions.map((action) =>
                                {
                                    return (
                                        <Button
                                            key={action.text}
                                            className={action.className}
                                            onClick={action.onClick}
                                            disabled={this.props.disabled || action.disabled}
                                            text={
                                                <>
                                                    <T>{action.text}</T>
                                                    {action.isDirty && <span style={{ color: 'red' }}>&nbsp;(*)</span>}
                                                </>
                                            }
                                        />
                                    );
                                })
                            }
                        </div> :
                        this.props.children
                }
            </div>
        );
    }
}

PanelFooter.propTypes = {
    docked: PropTypes.bool,
    disabled: PropTypes.bool
};

PanelFooter.defaultProps = {
    docked: true,
    disabled: false
};
