import './PanelHeaderWithSwitch.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Switch from 'react-switch';
import { T } from 'components/bases/Translate/Translate';

export class PanelHeaderWithSwitcher extends Component
{
    handleSwitchStateChange = () =>
    {
        this.props.onChanged(!this.props.value);
    };

    render()
    {
        const anchorBtn = this.props.settingRef ?
            <button
                onClick={this.props.onSettingClick}
                ref={this.props.settingRef}
                className="setting-btn"
            /> :
            <button onClick={this.props.onSettingClick} className="setting-btn" />;

        return (
            <div className={'panel-header-switch'}>
                <h3><T>{this.props.children}</T></h3>
                <div className={'panel-header-switch-actions'}>
                    {
                        this.props.onSettingClick && anchorBtn
                    }

                    <Switch
                        onChange={this.handleSwitchStateChange}
                        checked={this.props.value === 1}
                        uncheckedIcon={false}
                        checkedIcon={false}
                        width={38}
                        height={22}
                        activeBoxShadow='none'
                        disabled={this.props.disabled}
                        className={'switch-toggle ' + (this.props.value === 1 ? 'active' : 'disable')}
                    />
                </div>
            </div>
        );
    }
}

PanelHeaderWithSwitcher.propTypes = {
    value: PropTypes.number,
    data: PropTypes.object,
    onChanged: PropTypes.func,
    disabled: PropTypes.bool,
    settingRef: PropTypes.object
};

PanelHeaderWithSwitcher.defaultProps = {
    value: 0,
    data: {},
    disabled: false,
    onChanged: () =>
    {

    }
};
