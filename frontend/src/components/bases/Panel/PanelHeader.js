import './PanelHeader.scss';

import React, { Component } from 'react';
import { FAIcon } from '../Icon/FAIcon';
import PropTypes from 'prop-types';
import { T } from 'components/bases/Translate/Translate';

export class PanelHeader extends Component
{
    render()
    {
        return (
            <div className={'panel-header'}>
                {
                    this.props.children && <h3><T>{this.props.children}</T></h3>
                }
                <div className={'panel-header-actions'}>
                    {
                        this.props.actions.map((action) =>
                            !action.disabled &&
                            <button
                                key={action.icon}
                                onClick={action.onClick}
                                className={action.className}
                                ref={action.innerRef}
                            >
                                <FAIcon
                                    icon={action.icon}
                                    size={'1rem'}
                                    type={'light'}
                                    title={action.title || (Array.isArray(this.props.defaultTitleList) && 
                                                            this.props.defaultTitleList.find(i => i.icon === action.icon) ? 
                                                            this.props.defaultTitleList.find(i => i.icon === action.icon).title : '')}
                                />
                                <span><T>{action.title}</T></span>
                            </button>
                        )
                    }
                </div>
            </div>
        );
    }
}

PanelHeader.propTypes = {
    actions: PropTypes.array,
    defaultTitleList: PropTypes.array
};

PanelHeader.defaultProps = {
    actions: [],
    defaultTitleList: [
        {
            icon: 'times',
            title: 'Tắt'
        },
        {
            icon: 'trash-alt',
            title: 'Xóa'
        },
        {
            icon: 'plus',
            title: 'Thêm mới'
        }
    ]
};
