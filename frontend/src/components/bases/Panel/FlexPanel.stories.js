import React, { useState, useEffect } from 'react';

import { FlexPanel, PanelBody } from 'components/bases/Panel/Panel';
import { PanelHeader } from 'components/bases/Panel/PanelHeader';
import { PanelFooter } from 'components/bases/Panel/PanelFooter';

export default {
    title: 'Bases/Panel/FlexPanel',
    component: FlexPanel,
    subcomponents: { PanelBody, PanelHeader, PanelFooter },
};

export const Default = (args) =>
{
    return (
        <FlexPanel {...args}>
            <PanelHeader>
                Header
            </PanelHeader>
            <PanelBody>
                Body
            </PanelBody>
            <PanelFooter>
                Footer
            </PanelFooter>
        </FlexPanel>
    );
};


