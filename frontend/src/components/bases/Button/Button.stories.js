import React from 'react';

import { Button } from 'components/bases/Button/Button';

export default {
    title: 'Bases/Inputs/Button',
    component: Button,
    parameters: {
        controls: { expanded: true },
        backgrounds: {
            default: '',
            values: [
                { name: 'twitter', value: '#00aced' },
                { name: 'facebook', value: '#3b5998' }
            ]
        },
    },
    argTypes: {
        iconColor: { control: 'color' },
        color: { control: 'color' },
        backgroundColor: { control: 'color' }
    }
};

const Template = (args) => <Button {...args} />;

export const Default = Template.bind({});
Default.args = {};

export const Primary = Template.bind({});
Primary.args = {
    type: 'primary'
};

export const Success = Template.bind({});
Success.args = {
    type: 'success'
};

export const Info = Template.bind({});
Info.args = {
    type: 'info'
};

export const Danger = Template.bind({});
Danger.args = {
    type: 'danger'
};

export const Warning = Template.bind({});
Warning.args = {
    type: 'warning'
};

export const WithIcon = Template.bind({});
WithIcon.args = {
    icon: 'user',
    iconType: 'solid',
    iconLocation: 'right',
    border: '3px solid red',
    padding: '20px',
};

export const Disabled = Template.bind({});
Disabled.args = {
    disabled: true,
};
