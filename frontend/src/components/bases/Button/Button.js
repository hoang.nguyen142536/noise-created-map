import './Button.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { FAIcon } from 'components/bases/Icon/FAIcon';
import { T, translate } from 'components/bases/Translate/Translate';

export class Button extends Component
{
    render()
    {
        const { isLoading } = this.props;

        const icon = (
            <FAIcon
                icon={isLoading ? 'spinner' : this.props.icon}
                spin={isLoading}
                type={this.props.iconType}
                size={this.props.iconSize}
                color={this.props.iconColor}
                title={this.props.title}
            />
        );

        return (
            <button
                className={this.props.isDefault ? this.props.className : `btn btn-${this.props.type} ${this.props.className}`}
                onClick={this.props.onClick}
                disabled={this.props.disabled}
                title={translate(this.props.tooltip)}
                style={{
                    padding: this.props.padding ? this.props.padding : null,
                    color: this.props.color,
                    border: this.props.noBorder === true ? 'none' : this.props.border,
                    backgroundColor: this.props.backgroundColor,
                    cursor: this.props.disabled === true ? 'auto' : ''
                }}
                ref={this.props.innerRef}
            >
                {this.props.iconLocation === 'left' && icon}

                {this.props.iconLocation === 'top' && (
                    <div style={{ textAlign: 'center' }}>
                        {icon}
                    </div>
                )}

                {this.props.onlyIcon !== true && <span style={{ margin: '0px 5px' }}><T>{this.props.text}</T></span>}

                {this.props.iconLocation === 'right' && icon}

                {this.props.iconLocation === 'bottom' && (
                    <div style={{ textAlign: 'center' }}>
                        {icon}
                    </div>
                )}
            </button>
        );
    }
}

Button.propTypes = {
    className: PropTypes.string,
    text: PropTypes.any,
    color: PropTypes.string,
    type: PropTypes.oneOf(['default', 'primary', 'success', 'info', 'danger', 'warning']),
    backgroundColor: PropTypes.string,
    noBorder: PropTypes.bool,
    onlyIcon: PropTypes.bool,
    disabled: PropTypes.bool,
    tooltip: PropTypes.string,
    /**
     Fontawesome icon, Ex: user for fa-user
     */
    icon: PropTypes.string,
    iconType: PropTypes.oneOf(['solid', 'regular', 'light']),
    iconLocation: PropTypes.string,
    iconSize: PropTypes.string,
    iconColor: PropTypes.string,
    onClick: PropTypes.func,
    padding: PropTypes.string,
    border: PropTypes.string,
    isDefault: PropTypes.bool,
    innerRef: PropTypes.object,
    isLoading: PropTypes.bool
};

Button.defaultProps = {
    disabled: false,
    className: '',
    text: 'Button',
    color: '',
    type: 'default',
    icon: '',
    iconSize: '',
    iconType: 'light',
    iconColor: '',
    iconLocation: 'left',
    border: '',
    noBorder: false,
    onlyIcon: false,
    onClick: () =>
    {
    },
    isDefault: false,
    isLoading: false
};

export class EmptyButton extends Component
{
    render()
    {
        const { className, ...rest } = this.props;

        return (
            <Button
                className={`btn-empty ${className}`}
                {...rest}
            />
        );
    }
}

EmptyButton.propTypes = Button.propTypes;
EmptyButton.defaultProps = {
    text: ''
};

