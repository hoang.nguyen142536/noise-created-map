import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class Iframe extends Component
{
    iframeError = false;

    render()
    {
        const { width, height, background, src, frameBorder } = this.props;

        return (
            <iframe
                frameBorder={frameBorder}
                style={{ width, height, background }}
                src={src}
            />
        );
    }
}

Iframe.propTypes = {
    className: PropTypes.string,
    frameBorder: PropTypes.number,
    width: PropTypes.string,
    height: PropTypes.string,
    src: PropTypes.string.isRequired,
    background: PropTypes.string
};

Iframe.defaultProps = {
    className: '',
    frameBorder: 0,
    width: '',
    height: '',
    src: '',
    background: ''
};
