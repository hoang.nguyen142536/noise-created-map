import React, { useState, useEffect } from 'react';

import { Iframe } from 'components/bases/Iframe/Iframe';

export default {
    title: 'Bases/Display/Iframe',
    component: Iframe,
};

const Template = (args) =>
{
    return (
        <Iframe {...args} />
    );
};

export const Default = Template.bind({});
Default.args = {
    background: 'cyan',
    width: '50%',
    height: '50px',
    frameBorder: '1'
};
