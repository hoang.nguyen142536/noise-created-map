import React, { Component } from 'react';

import { CheckBox } from 'components/bases/CheckBox/CheckBox';

export class Radio extends Component
{
    render()
    {
        return (
            <CheckBox {...this.props} displayAs={'radio'} />
        );
    }
}

Radio.propTypes = CheckBox.propTypes;
Radio.defaultProps = CheckBox.defaultProps;

