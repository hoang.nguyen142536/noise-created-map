import React, { useState, useEffect } from 'react';

import { ListItem } from 'components/bases/List/List';
import CloseBlueIcon from 'images/icon/close_blue.png';
import { action } from '@storybook/addon-actions';

export default {
    title: 'Bases/List/ListItem',
    component: ListItem,
};

const Template = (args) =>
{
    const handleMenuClick = () =>
    {
        (action('onMenuClick'))();
    };

    return (
        <ListItem
            {...args}
            onMenuClick={handleMenuClick}
        />
    );
};

export const Default = Template.bind({});
Default.args = {
    iconUrl: CloseBlueIcon,
    icon: <div>Icon</div>
};
