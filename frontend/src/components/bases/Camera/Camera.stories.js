import React, { useState } from 'react';
import { action } from '@storybook/addon-actions';

import { Camera } from 'components/bases/Camera/Camera';

export default {
    title: 'Bases/Camera',
    component: Camera,
    parameters: {
    },
    argTypes: {}
}

const Template = (args) =>
{
    const [data, setData] = useState({
        streamUrl: 'tcp://localhost:1234',
        MjpegUrl: 'MjpegUrl',
    });

    const onClickEventHandler = () =>
    {
        (action('onClick'))();
    }

    args.data = data;
    args.onClick = onClickEventHandler;
    return <Camera {...args} />;
}

export const Default = Template.bind({});
Default.args = {
    width: '200px',
    height: '200px',
}



