import './StreamCamera.scss';
import playButton from 'images/play.png';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import videojs from 'video.js';

import WebRTCAdaptor from './webrtc_adaptor';

import { CommonHelper } from 'helper/common.helper';
import { AppConstant } from 'constant/app-constant';
import { T } from 'components/bases/Translate/Translate';

export class StreamCamera extends Component
{
    state = {
        type: 'none'
    };

    componentDidMount()
    {
        this._ismounted = false;

        this.playOrder = ['webrtc', 'hls'];
        this.name = this.props.streamName;
        this.token = AppConstant.streamInfo.token;
        this.videoGenerateId = CommonHelper.uuid();

        this.webRTCAdaptor = undefined;
        this.streamsFolder = 'streams';

        if (this.name !== 'null')
        {
            if (this.name.startsWith(this.streamsFolder))
            {
                // If name starts with streams, it's hls or mp4 file to be played
                const lastIndexOfDot = this.name.lastIndexOf('.');
                const streamPath = this.name.substring(this.streamsFolder.length + 1, lastIndexOfDot);
                const extension = this.name.substring(lastIndexOfDot + 1);

                this.startHLSPlayer(streamPath, extension, this.token);
            }
            else
            {
                // Check that which one is in the first order
                if (this.playOrder[0] === 'webrtc')
                {
                    this.initializeWebRTCPlayer(this.name, this.token, this.webrtcNoStreamCallback);
                }
                else if (this.playOrder[0] === 'hls')
                {
                    // this.tryToHLSPlay(this.name, this.token, this.hlsNoStreamCallback);
                }
                else
                {
                    alert('Unsupported play order requested. Supported formats are webrtc and hls. Use something like playOrder=webrtc,hls');
                }
            }
        }
        else
        {
            alert('No stream specified. Please add ?id={STREAM_ID}  to the url');
        }
    }

    componentWillUnmount()
    {
        this._ismounted = true;

        if (this.player)
        {
            this.player.dispose();
        }

        if (this.webRTCAdaptor)
        {
            this.webRTCAdaptor.closeWebSocket();
        }
    }

    webrtcNoStreamCallback = () =>
    {
        // If HLS is in the play order then try to play HLS, if not wait for WebRTC stream
        // In some cases user may want to remove HLS from the order and force to play WebRTC only
        // in these cases player only waits for WebRTC streams
        setTimeout(() =>
        {
            if (this.playOrder.includes('hls'))
            {
                this.tryToHLSPlay(this.name, this.token, this.hlsNoStreamCallback);
            }
            else
            {
                this.webRTCAdaptor.getStreamInfo(this.name);
            }
        }, 3000);
    };

    hlsNoStreamCallback = () =>
    {
        setTimeout(() =>
        {
            if (this.playOrder.includes('webrtc'))
            {
                // It means there is no HLS stream, so try to play WebRTC stream
                if (!this.webRTCAdaptor)
                {
                    this.initializeWebRTCPlayer(this.name, this.token, this.webrtcNoStreamCallback);
                }
                else
                {
                    this.webRTCAdaptor.getStreamInfo(this.name);
                }
            }
            else
            {
                this.tryToHLSPlay(this.name, this.token, this.hlsNoStreamCallback);
            }
        }, 3000);
    };

    initializeWebRTCPlayer = (name, token, noStreamCallback) =>
    {
        const pcConfig = null;

        const sdpConstraints = {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: true
        };

        const mediaConstraints = {
            video: false,
            audio: false
        };

        const websocketURL = AppConstant.streamInfo.rtcUrl;

        this.webRTCAdaptor = new WebRTCAdaptor({
            websocket_url: websocketURL,
            mediaConstraints: mediaConstraints,
            peerconnection_config: pcConfig,
            sdp_constraints: sdpConstraints,
            remoteVideoId: `remoteVideo-${this.videoGenerateId}`,
            isPlayMode: true,
            debug: this.props.debug,
            callback: (info, description) =>
            {
                if (info === 'initialized')
                {
                    this.webRTCAdaptor.getStreamInfo(this.name);
                }
                else if (info === 'streamInformation')
                {
                    this.webRTCAdaptor.play(this.name, this.token);
                }
                else if (info === 'play_started')
                {
                    this.playWebRTCVideo();
                }
                else if (info === 'play_finished')
                {
                    setTimeout(() =>
                    {
                        this.webRTCAdaptor.getStreamInfo(this.name);
                    }, 3000);
                }
                else if (info === 'closed')
                {
                    if (typeof description != 'undefined')
                    {
                        this.log('Connecton closed: ' + JSON.stringify(description));
                    }
                }
            },
            callbackError: (error) =>
            {
                this.log('error callback: ' + JSON.stringify(error));

                if (error === 'no_stream_exist')
                {
                    if (typeof noStreamCallback != 'undefined')
                    {
                        noStreamCallback();
                    }
                }
                if (error === 'notSetRemoteDescription')
                {
                    // If getting codec incompatible or remote description error, it will redirect HLS player.
                    this.tryToHLSPlay(name, token, this.hlsNoStreamCallback);
                }
            }
        });
    };

    playWebRTCVideo = () =>
    {
        this.setState({ type: 'rtc-dont-play' });

        if (this.props.autoPlay)
        {
            document.getElementById(`remoteVideo-${this.videoGenerateId}`).play().then(() =>
            {
                this.setState({ type: 'rtc-play' });
            }).catch(() =>
            {
                this.setState({ type: 'rtc-dont-play' });
            });
        }
    };

    startHLSPlayer = (name, extension, token) =>
    {
        if (this._ismounted)
        {
            return;
        }

        this.setState({ type: 'hls' });

        let type;
        if (extension === 'mp4')
        {
            type = 'video/mp4';
        }
        else if (extension === 'm3u8')
        {
            type = 'application/x-mpegURL';
        }
        else
        {
            this.log('Unknown extension: ' + extension);
            return;
        }

        let preview = name;
        if (name.endsWith('_adaptive'))
        {
            preview = name.substring(0, name.indexOf('_adaptive'));
        }

        this.player = videojs(`video-player-${this.videoGenerateId}`, {
            poster: 'previews/' + preview + '.png'
        });

        this.player.src({
            src: AppConstant.streamInfo.hlsUrl + name + '.' + extension + '?token=' + token,
            type: type
        });

        this.player.poster('previews/' + preview + '.png');

        if (this.props.autoPlay)
        {
            this.player.play();
        }
    };

    tryToHLSPlay = (name, token, noStreamCallback) =>
    {
        fetch(AppConstant.streamInfo.hlsUrl + name + '_adaptive.m3u8', { method: 'HEAD' }).then((response) =>
        {
            if (response.status === 200)
            {
                // adaptive m3u8 exists, play it
                this.startHLSPlayer(name + '_adaptive', 'm3u8', token);
            }
            else
            {
                // adaptive m3u8 not exists, try m3u8 exists.
                fetch(AppConstant.streamInfo.hlsUrl + name + '.m3u8', { method: 'HEAD' }).then((response) =>
                {
                    if (response.status === 200)
                    {
                        // m3u8 exists, play it
                        this.startHLSPlayer(name, 'm3u8', token);
                    }
                    else
                    {
                        // no m3u8 exists, try vod file
                        fetch(AppConstant.streamInfo.hlsUrl + name + '.mp4', { method: 'HEAD' }).then((response) =>
                        {
                            if (response.status === 200)
                            {
                                // mp4 exists, play it
                                this.startHLSPlayer(name, 'mp4', token);
                            }
                            else
                            {
                                this.log('No stream found');

                                if (typeof noStreamCallback != 'undefined')
                                {
                                    noStreamCallback();
                                }
                            }
                        }).catch((err) =>
                        {
                            this.log('Error: ' + err);
                        });
                    }
                }).catch((err) =>
                {
                    this.log('Error: ' + err);
                });
            }
        }).catch((err) =>
        {
            this.log('Error: ' + err);
        });
    };

    log = (message) =>
    {
        if (this.props.debug)
        {
        }
    };

    render()
    {
        return (
            <div className={'video-stream'}>
                {this.props.name && <h2>{this.props.name}</h2>}
                <div
                    className={'video_container'}
                >
                    <div
                        className={'video_info'}
                        style={{ display: this.state.type === 'none' ? 'block' : 'none' }}
                    >
                        <T>Camera đang tải</T><br/><T>Sẽ tự động hiển thị khi có dữ liệu</T>
                    </div>

                    <video
                        id={`video-player-${this.videoGenerateId}`}
                        className="video-js vjs-default-skin vjs-fill vjs-big-play-centered"
                        style={{ display: this.state.type === 'hls' ? 'block' : 'none' }}
                        controls
                        preload="auto"
                    >
                        <p className="vjs-no-js">
                            <T>Để xem được video, vui lòng bật Javascript và xem xét cập nhật
                            phiên bản web bowser mới nhất hỗ trợ HTML5</T>
                        </p>
                    </video>

                    <video
                        className={'remoteVideo'}
                        id={`remoteVideo-${this.videoGenerateId}`}
                        autoPlay
                        controls
                        playsInline
                        style={{ display: this.state.type === 'rtc-play' || this.state.type === 'rtc-dont-play' ? 'block' : 'none' }}
                    />
                    <img
                        alt={'play'}
                        className={'play_button'}
                        id={`play_button-${this.videoGenerateId}`}
                        style={{ display: this.state.type === 'rtc-dont-play' ? 'block' : 'none' }}
                        src={playButton}
                    />
                </div>
            </div>
        );
    }
}

StreamCamera.propTypes = {
    className: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
    name: PropTypes.string,
    streamName: PropTypes.string,
    autoPlay: PropTypes.bool,
    onClick: PropTypes.func,
    debug: PropTypes.bool
};

StreamCamera.defaultProps = {
    className: '',
    width: '100%',
    height: '100%',
    name: '',
    streamName: '',
    autoPlay: true,
    debug: false,
    onClick: () =>
    {
    }
};
