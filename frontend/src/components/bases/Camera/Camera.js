import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { CameraService } from 'services/camera.service';
import { StreamCamera } from 'components/bases/Camera/StreamCamera';

export class Camera extends Component
{
    cameraSvc = new CameraService();

    eventId = undefined;
    state = {
        src: undefined
    };

    componentDidMount()
    {
        if (this.props.type === 'Snapshot')
        {
            this.getSnapshotImage();
            this.eventId = setInterval(() =>
            {
                this.getSnapshotImage();
            }, 5000);
        }
        else if (this.props.type === 'Mjpeg')
        {
            this.setState({ src: this.props.data.MjpegUrl });
        }
        else if (this.props.type === 'Stream')
        {
            this.setState({ src: this.props.data.streamUrl });
        }
    }

    getSnapshotImage = () =>
    {
        this.cameraSvc.getSnapShotImage(this.props.data.CamId).then((rs) =>
        {
            rs.blob().then((blob) =>
            {
                const reader = new FileReader();
                reader.onloadend = (function()
                {
                    const base64data = reader.result;
                    this.setState({ src: base64data });
                }).bind(this);

                reader.readAsDataURL(blob);
            });
        });
    };

    componentWillUnmount()
    {
        if (this.eventId !== undefined)
        {
            clearInterval(this.eventId);
        }
    }


    render()
    {
        const { className, width, height, onClick, type } = this.props;

        const src = this.state.src;

        if (type === 'Stream')
        {
            const streamName = src ? src.substr(src.indexOf('name=') + 5) : '';
            return streamName && <StreamCamera streamName={streamName}/>;
        }
        else
        {
            return (
                <img
                    className={className}
                    style={{ width: width, height: height }}
                    src={src}
                    onClick={onClick}
                    alt={''}
                />
            );
        }
    }
}

Camera.propTypes = {
    className: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
    type: PropTypes.oneOf(['Mjpeg', 'Snapshot', 'Stream']),
    data: PropTypes.object,
    onClick: PropTypes.func
};

Camera.defaultProps = {
    className: '',
    width: '100%',
    height: '100%',
    type: 'Mjpeg',
    data: {},
    onClick: () =>
    {
    }
};
