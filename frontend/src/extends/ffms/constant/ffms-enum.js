module.exports = {
    EMPLOYEE_STATUS: {
        new: '1',
        inactive: '2',
        active: '3',
        disabled: '4'
    },

    JOB_STATUS: {
        new: 1,
        assigned: 2,
        processing: 3,
        done: 4,
        cancel: 5
    },

    IMPORT_STATUS: {
        notImported: '0',
        success: 1,
        update: 2,
        dataFailed: -256,
        importFailed: -1,
        geoCodeFailed: -2
    }
};
