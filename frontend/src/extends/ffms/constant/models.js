module.exports = {
    LAYERS: {
        ACTIVITY_LOG: 'activity-logs',
        CASE: 'cases',
        CONTAINER: 'containers',
        CUSTOMER_TYPE: 'customer-types',
        CUSTOMER: 'customers',
        DEVICE_STATUS: 'device-statuses',
        EMPLOYEE_TYPE: 'employee-types',
        EMPLOYEE: 'employees',
        HUB: 'hubs',
        JOB_SHEET: 'job-sheets',
        JOB_STATUS: 'job-statuses',
        JOB_TYPE: 'job-types',
        JOB: 'jobs',
        JOBREPORT: 'job-reports',
        JOBTYPE_EMPLOYEETYPE: 'job-types-employee-types',
        ORGANIZATION_TYPE: 'organization-types',
        ORGANIZATION: 'organizations',
        SCHEDULE: 'schedules',
        SHIFT: 'shifts',
        TEAM: 'teams',
        LOCATION: 'locations',
        PHOTO_TYPE: 'photo-types'
    }
};
