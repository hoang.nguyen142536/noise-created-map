import { EmployeeStore } from 'extends/ffms/views/EmployeePanel/EmployeeStore';
import { JobStore } from 'extends/ffms/views/JobPanel/JobStore';
import { TrackingDutyStore } from 'extends/ffms/views/TrackingDuty/TrackingDutyStore';
import { JobFilterStore } from 'extends/ffms/views/JobFilterView/JobFilterStore';
import { HistoryStore } from 'extends/ffms/views/TrackingHistory/HistoryStore';
import { OverlayPopupStore } from 'extends/ffms/components/OverlayPopup/OverlayPopupStore';
import { DashboardStore } from 'extends/ffms/pages/Dashboard/DashboardStore';
import { ReportStore } from 'extends/ffms/pages/Report/ReportStore';
import { CustomerStore } from './views/JobPanel/Customer/CustomerStore';
import CommonService from 'extends/ffms/services/CommonService';
import { JobUploaderStore } from './views/JobPanel/JobImport/JobUploadStore';
import { CustomerUploaderStore } from './views/CustomerPanel/CustomerImport/CustomerUploadStore';
import { WorkerStore } from './views/TrackingWorker/WorkerStore';
import { ManagerLayerStore } from './pages/Layerdata/ManagerLayerStore';
import { SpatialSearchStore } from 'components/app/SpatialSearch/SpatialSearchStore';

class FieldForceStore
{
    comSvc = new CommonService();

    constructor()
    {
        this.empStore = new EmployeeStore();
        this.jobStore = new JobStore();
        this.jobUploadStore = new JobUploaderStore();
        this.customerUploadStore = new CustomerUploaderStore();
        this.historyStore = new HistoryStore();
        this.jobFilterStore = new JobFilterStore(this);
        this.customerStore = new CustomerStore();
        this.trackingDutyStore = new TrackingDutyStore();
        this.overlayPopupStore = new OverlayPopupStore();
        this.dashboardStore = new DashboardStore();
        this.reportStore = new ReportStore();
        this.workerStore = new WorkerStore(this);
        this.managerLayerStore = new ManagerLayerStore();
        this.spatialSearchStore = new SpatialSearchStore();
    }

    // TODO read from vdms
    getJobStatusColor = (statusId) =>
    {
        switch (statusId)
        {
            case 1:
            case 'New':
                return '#3dcad4';
            case 2:
            case 'Assigned':
                return '#bb99f7';
            case 3:
            case 'In Progress':
                return '#37a6ff';
            case 4:
            case 'Done':
                return '#8ae355';
            case 5:
            case 'Cancel':
                return '#d4e3f6';
            default:
                return '#ccc';
        }
    };

    // TODO: replace this with comSvc.getDataReferences(layers);
    loadDataReferences = (layers) =>
    {
        return this.comSvc.getDataReferences(layers);
    };

    // TODO: move this to helper/utils
    getDataReferenceOptions = (layer, idField, labelField) =>
    {
        return this.comSvc.dataRefs[layer] && Array.isArray(this.comSvc.dataRefs[layer]) ? this.comSvc.dataRefs[layer].map((data) =>
        {
            return {
                id: data[idField],
                label: data[labelField],
                color: 'var(--primary)',
                ...data
            };
        }) : [];
    }
}

export default new FieldForceStore();
