/**
 * All available routes for the app
 */

export const FFMS = '/ffms';

export const MONITORING = '/ffms/monitoring';

export const JOBS = '/ffms/jobs';

export const LOCATION = '/ffms/location';

export const ROUTE_PLANNING = '/ffms/direction';

export const DASHBOARD = '/ffms/dashboard';

export const JOBS_MANAGER = '/ffms/jobs-manager';

export const EMPLOYEES_MANAGER = '/ffms/employees-manager';

export const HISTORY = '/ffms/history';

export const SEARCH = '/ffms/search';

export const CUSTOMER_MANAGER = '/ffms/customer-manager';

export const REPORT = '/ffms/report';

export const WORKER = '/ffms/worker';

export const MANAGER_LAYER_DATA = '/ffms/data-manager';

export const REGISTER = '/ffms/invite';

export const USER_LOGIN = '/ffms/welcome';


