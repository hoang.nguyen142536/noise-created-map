import 'index.scss';

import React from 'react';

import { AppBody } from 'components/bases/AppBody/AppBody';
import { themeList, ThemeProvider } from 'components/bases/Theme/ThemeContext';

export const globalTypes = {
    theme: {
        name: 'Theme',
        description: 'Global theme for components',
        defaultValue: themeList[0].name,
        toolbar: {
            icon: 'circle',
            items: themeList.map((item) => item.name)
        }
    }
};

export const parameters = {
    actions: { argTypesRegex: '^on[A-Z].*' },
    layout: 'fullscreen'
};

function getThemeByName(name)
{
    let result;

    themeList.forEach(function (item)
    {
        if (item.name && item.name === name)
        {
            result = item;
        }
    });

    return result;
}

const withThemeProvider = (Story, context) =>
{
    const theme = getThemeByName(context.globals.theme);

    return (
        <ThemeProvider theme={theme}>
            <AppBody>
                <Story />
            </AppBody>
        </ThemeProvider>
    );
};

export const decorators = [
    withThemeProvider
];
